;;; zenbu-theme.el --- A parametric theme for Emacs.  -*- lexical-binding: t; -*-

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
(deftheme zenbu "The zenbu color theme")

;; zenbu Color Pallette
(defun sndr/fix-color-name (str)
  (concat "#"(substring str -6))
  )
(setq sndr/zenbu-fg+1     (color-lighten-name "#c8e9ed" 5) )  
(setq sndr/zenbu-fg-1     (color-desaturate-name (color-darken-name "#c8e9ed" 10)  50) )
(setq sndr/zenbu-fg-2     (color-desaturate-name (color-darken-name "#c8e9ed" 20)  50) )
(setq sndr/zenbu-bg+1     (color-desaturate-name (color-lighten-name "#1b1b1b" 30) 10) )
(setq sndr/zenbu-bg+2     (color-desaturate-name (color-lighten-name "#1b1b1b" 40) 10) )
(setq sndr/zenbu-bg+3     (color-desaturate-name (color-lighten-name "#1b1b1b" 50) 20) )
(setq sndr/zenbu-bg-1     (color-darken-name "#1b1b1b" 5) ) 
(setq sndr/zenbu-charcoal (color-desaturate-name (color-darken-name "#c8e9ed" 5)10))

;; (require 'color)
;; (defun hexcolour-luminance (color)
;;   "Calculate the luminance of a color string (e.g. \"#ffaa00\", \"blue\").
;;   This is 0.3 red + 0.59 green + 0.11 blue and always between 0 and 255."
;;   (let* ((values (x-color-values color)) (r (car values)) (g (cadr values))
;; 	 (b (caddr values))) (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))

;; (defun set-colors-based-on-theme ()
;;   (interactive)
;;   "Sets the hl-line face to have no foregorund and a background
;;         that is 10% darker than the default face's background."
;;   (setq bg
;;         (if (or (eq (face-background 'default) nil)
;; 		            (eq (face-background 'default) "unspecified-bg"))
;;             (color-darken-name "#000000" 0 )
;;           (face-background 'default )
;;           ))
;;   (if (< (hexcolour-luminance bg ) 20)
;;       (setq sgn -1) (setq sgn 1) )
;;   (set-face-attribute 'hl-line nil
;;                       :foreground nil
;;                       :background (color-desaturate-name
;; 			                             (color-lighten-name bg (* sgn 40))
;; 			                             10))
;;   (set-face-background 'show-paren-match
;; 		                   (color-desaturate-name
;; 			                  (color-lighten-name bg (* sgn 40))
;; 			100))
;;   (set-face-background 'region
;; 		                   (color-desaturate-name
;; 			                  (color-lighten-name bg (* sgn 20))
;; 			                  10))
;;   )
;; (set-colors-based-on-theme)

(defvar zenbu-colors-alist
  '(("zenbu-fg"            . "#c8e9ed"  )
    ("zenbu-bg"            . "#1b1b1b"  )
    ("zenbu-fg+1"          . sndr/zenbu-fg+1)
    ("zenbu-fg-1"          . sndr/zenbu-fg-1)
    ("zenbu-fg-2"          . "#accdd1")
    ("zenbu-bg+1"          . sndr/zenbu-bg+1)
    ("zenbu-bg+2"          . sndr/zenbu-bg+2)
    ("zenbu-bg+3"          . sndr/zenbu-bg+3)

    ("zenbu-comment"       . "#52aeba" )
    ("zenbu-bg-1"          . sndr/zenbu-bg-1)

    ;; Primary Hues
    ("zenbu-blue"          . "#a3d6d4" )
    ("zenbu-charcoal"      . sndr/zenbu-charcoal)
    ("zenbu-salmon"        . "#eac3b5" )
    ("zenbu-violet"        . "#b5cfec" )
    ("zenbu-orange"        . "#dcc4e4" )
    ("zenbu-green"         . "#a3d6d4" )
    ("zenbu-yellow"        . "#dec8ac" )
    ("zenbu-sand"          . "#b0b9b4" )
    ;; Secondary Hues
    ("zenbu-lime"          . "#bbd3b3" )
    ("zenbu-teal"          . "#75a6d4" )
    ("zenbu-pink"          . "#b994c7" )
    ("zenbu-brown"         . "#bb9b6c" )
    ("zenbu-red"           . "#cd927b" )
    ("zenbu-dull-red"      . "#53afad" )
    ("zenbu-dark-violet"   . "#75a6d4" )
    ("zenbu-darker-violet" . "#53afad" )
    ("zenbu-olive"         . "#83ab79" )
    ;; Misc.
    ("zenbu-link"          . "#a3d6d4" )
    ("zenbu-warn"          . "#b5cfec" )
    ("zenbu-succ"          . "#dcc4e4" )
    ("zenbu-hl"            . sndr/zenbu-bg+3 )))

(defmacro zenbu/with-color-variables (&rest body)
  "`let' bind all colors defined in `zenbu-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (cons)
                     (list (intern (car cons)) (cdr cons)))
                   zenbu-colors-alist))
     ,@body))

(zenbu/with-color-variables 
  (custom-theme-set-faces
   'zenbu
   ;; >>>>> Built-in
   '(button ((t (:underline t))))
   `(link ((t (:bold t :foreground ,zenbu-blue :underline nil :weight bold))))
   `(variable-pitch ((t (
		                     :family, "Roboto"
			                   :foreground ,zenbu-fg
			                   :background ,zenbu-bg
			                   :distance-foreground, zenbu-fg-1
			                   :inverse-video nil
			                   :box nil
			                   :strike-through nil
			                   :overline nil
			                   :underline nil
			                   :slant normal
			                   :weight normal
			                   :width wide
                         :height 120
                         ))))

   '(fixed-pitch ((t ( :family "RobotoMono Nerd Font"
                       :weight normal
                       :height 120
                       :width wide
			                 :inverse-video nil
			                 :box nil
			                 :strike-through nil
			                 :overline nil
			                 :underline nil
			                 :slant normal))))

   `(default ((t (
		              :family, "RobotoMono Nerd Font"
                  :height 120
			            :foreground ,zenbu-fg
			            :background ,zenbu-bg
			            :distance-foreground, zenbu-fg-1
			            :inverse-video nil
			            :box nil
			            :strike-through nil
			            :overline nil
			            :underline nil
			            :slant normal
			            :weight normal
			            :width normal
                  ))))
   ;;The :background attribute of this face specifies the color of the text cursor
   ;; `(cursor ((t (:background ,zenbu-bg+1 )))) 
   `(cursor ((t (:background ,zenbu-fg )))) 
   `(border ((t (:foreground ,zenbu-bg+1 )))) 
   `(vertical-border ((t (:foreground ,zenbu-bg+1 )))) 
   `(mouse ((t (:background ,zenbu-fg)))) 
   `(window-divider             ((t (:foreground ,zenbu-bg+1)))) 
   `(window-divider-first-pixel ((t (:foreground ,zenbu-bg+1)))) 
   `(window-divider-last-pixel  ((t (:foreground ,zenbu-bg+1)))) 

   ;; The face for displaying control characters and escape sequences
   `(escape-glyph ((t (:foreground ,zenbu-salmon :bold t))))

   ;; The face for the narrow fringes to the left and right of windows on graphic displays.
   `(fringe ((t (:foreground ,zenbu-bg+1 :background ,zenbu-bg))))

   ;; fixed line displayed at the top of the emacs window, not in XEmacs
   `(header-line ((t (:foreground ,zenbu-salmon
                                  ))))

   `(which-func ((t (:foreground ,zenbu-salmon
                                 ))))

   ;;text highlighting in various contexts, when the mouse cursor is moved over a hyperlink. 
   `(highlight ((t (:background ,zenbu-hl))))
   `(ivy-current-match ((t(:background, zenbu-bg-1 :weight bold))))
   `(ivy-minibuffer-match-face-1 ((t(:background, zenbu-bg-1 :foreground ,zenbu-lime ))))
   `(ivy-minibuffer-match-face-2 ((t(:background, zenbu-bg-1 :foreground ,zenbu-lime ))))
   `(ivy-minibuffer-match-face-3 ((t(:background, zenbu-bg-1 :foreground ,zenbu-lime ))))
   `(ivy-minibuffer-match-face-4 ((t(:background, zenbu-bg-1 :foreground ,zenbu-lime ))))
   `(show-paren-match ((t (:background ,zenbu-bg+2))))

   ;; “lazy matches” for Isearch and Query Replace (matches other than the current one). 
   `(lazy-highlight ((t (:background ,zenbu-yellow :foreground ,zenbu-bg :weight extra-bold))))

   ;; This face is used to highlight the current Isearch match 
   `(isearch ((t (:background ,zenbu-succ :foreground ,zenbu-bg :weight extra-bold))))
   
   `(success ((t (:foreground ,zenbu-link :weight bold))))
   `(warning ((t (:foreground ,zenbu-pink :weight bold)))) 

   ;; This face is used for displaying an active region 
   `(region ((t (:background ,(substring (color-desaturate-name zenbu-bg+1 100)0 7) ) ) ))

   ;; >>>>> mode-line
   `(mode-line    ((,class (:foreground ,zenbu-salmon
                                        :weight light
                                        :box (:line-width 12 :color ,zenbu-bg) 
                                        ))
                   (t :inverse-video nil)))

   `(mode-line-inactive ((t (:foreground ,zenbu-bg+2
                                         :weight light
                                         :box (:line-width 12 :color ,zenbu-bg) 
                                         ))))
   `(mode-line-highlight ((t (:background ,zenbu-teal :foreground ,zenbu-bg :box nil))))
   `(mode-line-buffer-id ((t (:foreground ,zenbu-salmon))))
   `(mode-line-highlight ((t (:foreground ,zenbu-lime))))
   `(minibuffer-prompt ((t (:foreground ,zenbu-salmon ))))

   ;; linum
   `(linum ((t (:foreground, zenbu-bg+2 :background, zenbu-bg ))))

   ;; mu4e
   `( mu4e-header-highlight-face      ((t (:foreground ,zenbu-fg ))))
   `( mu4e-header-key-face            ((t (:foreground ,zenbu-lime  ))))
   `( mu4e-header-contact-face        ((t (:foreground ,zenbu-lime :family, "Gill Sans" ))))
   `( mu4e-header-value-face          ((t (:foreground ,zenbu-salmon ))))
   `( mu4e-special-header-value-face  ((t (:foreground ,zenbu-fg-1))))
   `( mu4e-view-body-face             ((t (:foreground ,zenbu-fg ))))

   ;;   ;; >>>>> font-lock
   `(font-lock-warning-face ((t (:foreground ,zenbu-yellow :weight bold))))
   `(font-lock-function-name-face ((t (:foreground ,zenbu-salmon ))))
   `(font-lock-variable-name-face ((t (:foreground , "#b5cfec"))))
   `(font-lock-keyword-face ((t (:foreground ,zenbu-blue))))
   `(font-lock-comment-face ((t (:foreground ,zenbu-comment
                                             :weight light
                                             :slant italic
                                             ))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,zenbu-comment :weight light :slant italic))))
   `(font-lock-type-face ((t (:foreground ,zenbu-lime))))
   `(font-lock-constant-face ((t (:foreground ,zenbu-dark-violet))))
   `(font-lock-builtin-face ((t (:foreground ,zenbu-violet))))
   `(font-lock-preprocessor-face ((t (:foreground ,zenbu-green))))
   `(font-lock-string-face ((t (:foreground ,zenbu-green))))
   `(font-lock-doc-face ((t (:foreground ,zenbu-green))))

   ;;`(helm-selection              ((t (:background ,zenbu-bg :underline nil))))
   `(helm-selection              ((t (:inherit hl-line))))
   `(helm-match                  ((t (:foreground ,zenbu-salmon :underline nil)) ))
   `(helm-source-header          ((t (:background ,zenbu-blue :foreground ,zenbu-fg-1)) ))
   `(helm-swoop-target-line-face ((t (:foreground ,zenbu-fg-1 :inverse-video t)) ))
   `(helm-ff-file                ((t (:foreground ,zenbu-fg :underline nil)) ))
   `(helm-ff-prefix              ((t (:foreground ,zenbu-green :underline nil)) ))
   `(helm-ff-dotted-directory    ((t (:foreground ,zenbu-bg+2 :underline nil)) ))
   `(helm-ff-directory           ((t (:foreground ,zenbu-salmon :underline nil)) ))
   `(helm-ff-executable          ((t (:foreground ,zenbu-fg :underline nil :slant italic)) ))

  `(vertico-current             ((t (:inherit hl-line)) ))
  `(vertico-multiline           ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg)) ))
  `(vertico-group-title         ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg)) ))
  `(vertico-group-separator     ((t (:background ,zenbu-blue :foreground ,zenbu-fg)) ))
   
   ;; >>>>> IDO
   `(ido-incomplete-regexp ((t (:foreground ,zenbu-lime))))
   `(ido-indicator ((t (:foreground ,zenbu-violet :background ,zenbu-orange))))
   `(ido-first-match ((t (:foreground ,zenbu-lime))))
   `(ido-only-match ((t (:foreground ,zenbu-lime))))
   `(ido-subdir ((t (:foreground ,zenbu-lime))))
   `(ido-virtual ((t (:foreground ,zenbu-lime))))

   ;; >>>>> eshell 
   `(eshell-prompt ((t (:foreground ,zenbu-lime))))
   `(eshell-ls-archive ((t (:foreground ,zenbu-orange :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,zenbu-violet :weight normal))))
   `(eshell-ls-executable ((t (:foreground ,zenbu-yellow :weight normal))))
   `(eshell-ls-unreadable ((t (:foreground ,zenbu-fg))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,zenbu-blue :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,zenbu-link :weight bold))))

   ;; >>>>> Org mode
   '(org-block ((t (:inherit fixed-pitch))))
   '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)
                                 ))))
   '(org-drawer ((t (:inherit org-meta-line))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold
                           ))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
   '(org-ellipsis ((t (:inherit font-lock-comment-face :underline nil))))
   '(org-table ((t (:inherit fixed-pitch ))))
   `(org-todo ((t (:foreground ,zenbu-lime))))
   '(org-document-info-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)
                                             ))))
   `(org-document-title ((t (:inherit variable-pitch
                                      ))))
   '(org-document-info ((t (:inherit variable-pitch
                                     ))))
   `(org-archived ((t (:foreground ,zenbu-fg :weight bold))))
   `(org-checkbox ((t (:foreground ,zenbu-fg+1 :foreground ,zenbu-olive
                                   :box (:line-width 1 :style released-button)))))
   `(org-done ((t (:foreground ,zenbu-lime :strike-through t))))
   `(org-todo ((t (:foreground ,zenbu-lime))))
   `(org-formula ((t (:foreground ,zenbu-lime))))
   `(org-headline-done ((t (:foreground ,zenbu-charcoal))))
   `(org-hide ((t (:foreground ,zenbu-bg))))
   `(org-level-1 ((t (:inherit fixed-pitch :foreground, zenbu-fg))))
   `(org-level-2 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-3 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-4 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-5 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-6 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-7 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-level-8 ((t (:inherit fixed-pitch :foreground, zenbu-fg ))))
   `(org-link ((t (:foreground ,zenbu-link :underline nil))))
   
   `(org-agenda-date ((t (:foreground ,zenbu-blue))))
   `(org-deadline-announce ((t (:foreground ,zenbu-dull-red))))
   `(org-date ((t (:foreground ,zenbu-link :underline nil))))
   `(org-agenda-date-today  ((t (:foreground ,zenbu-salmon :weight light :slant italic))))
   `(org-agenda-structure  ((t (:inherit font-lock-comment-face))))
   ;; `(org-scheduled ((t (:foreground ,zenburn-green+4))))x
   ;; `(org-scheduled-previously ((t (:foreground ,zenburn-red-4))))
   ;; `(org-scheduled-today ((t (:foreground ,zenburn-blue+1))))
   ;; `(org-sexp-date ((t (:foreground ,zenburn-blue+1 :underline t))))
   ;; `(org-time-grid ((t (:foreground ,zenburn-orange))))
   ;; `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))

   `(org-special-keyword ((t (:foreground ,zenbu-olive :weight normal))))
   `(org-table ((t (:foreground ,zenbu-olive))))
   `(org-tag ((t (:bold t :foreground ,zenbu-orange :strike-through nil))))
   `(org-warning ((t (:bold t :foreground ,zenbu-pink :weight bold))))
   `(org-column ((t (:background ,zenbu-bg))))
   `(org-column-title ((t (:background ,zenbu-bg :foreground ,zenbu-lime :underline nil))))
   `(org-mode-line-clock ((t (:foreground ,zenbu-yellow))))
   `(org-footnote ((t (:foreground ,zenbu-link :underline nil))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   `(org-verbatim ((t (:inherit org-code))))
   
   ;; >>>>> elpy and ipython
   `(highlight-indentation-face ((t (:background ,zenbu-bg))))
   `(comint-highlight-prompt ((t (:inherit eshell-prompt))))
   
   ;; >>>>> auto-complete and popup
   `(ac-candidate-face ((t (:background ,zenbu-sand :foreground ,zenbu-bg))))
   `(ac-selection-face ((t (:background ,zenbu-violet :foreground ,zenbu-bg))))
   `(popup-tip-face ((t (:background ,zenbu-sand :foreground ,zenbu-bg))))
   `(popup-scroll-bar-foreground-face ((t (:background ,zenbu-dark-violet))))
   `(popup-scroll-bar-background-face ((t (:background ,zenbu-olive))))
   `(popup-isearch-match ((t (:background ,zenbu-yellow :foreground ,zenbu-bg))))

   `(ebrowse-root-class ((t (:foreground ,zenbu-salmon))))
   `(ebrowse-progress ((t (:background ,zenbu-salmon))))
   `(ebrowse-member-class ((t (:foreground ,zenbu-olive))))
   `(ebrowse-member-attribute ((t (:foreground ,zenbu-blue))))
   `(ebrowse-tree-mark ((t (:foreground ,zenbu-violet))))

   `(cscope-file-face ((t (:foreground ,zenbu-salmon))))
   `(cscope-function-face ((t (:foreground ,zenbu-violet))))
   `(cscope-line-number-face ((t (:foreground ,zenbu-salmon))))
   `(cscope-separator-face ((t (:foreground ,zenbu-salmon))))
   `(cscope-mouse-face ((t (:background ,zenbu-bg+1))))
   
   `(company-scrollbar-bg ((t (:background ,zenbu-bg+1))))
   `(company-scrollbar-fg ((t (:foreground ,zenbu-fg))))

   `(company-tooltip ((t (:background ,zenbu-bg+1 :foreground ,zenbu-olive))))
   `(company-tooltip-selection ((t (:background ,zenbu-bg :foreground ,zenbu-olive))))

   `(company-tooltip-mouse ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg))))
   `(company-tooltip-common ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg))))
   `(company-tooltip-common-selection ((t (:background ,zenbu-bg :foreground ,zenbu-olive))))
   `(company-preview ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg))))
   `(company-preview-common ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg))))
   
   `(company-tooltip-annotation ((t (:background ,zenbu-bg+1 :foreground ,zenbu-bg+2 ))))
   `(company-tooltip-annotation-selection ((t (:background ,zenbu-bg :foreground ,zenbu-bg+2 ))))

   `(company-template-field ((t (:background ,zenbu-bg+1 :foreground ,zenbu-fg))))


   ;; >>>>> smart-mode-line
   ;;`(sml/global ((t (:background ,zenbu-bg :inverse-video nil))))
   `(sml/folder ((t (:foreground ,zenbu-charcoal))))
   `(sml/filename ((t (:foreground ,zenbu-salmon :weight normal))))
   `(sml/prefix   ((t (:foreground ,zenbu-salmon :weight normal))))
   `(sml/line-number ((t (:foreground ,zenbu-blue :weight normal))))  
   `(sml/col-number ((t (:foreground ,zenbu-green :weight normal))))
   `(sml/read-only ((t (:foreground ,zenbu-charcoal))))
   `(sml/outside-modified ((t (:foreground ,zenbu-red))))
   `(sml/modified ((t (:foreground ,zenbu-red))))
   `(sml/remote ((t (:foreground ,zenbu-charcoal))))
   `(sml/numbers-separator ((t (:foreground ,zenbu-charcoal))))
   ;;`(sml/client ((t (:foreground ,zenbu-succ))))
   ;;`(sml/not-modified ((t (:foreground ,zenbu-yellow))))
   `(sml/git  ((t (:foreground ,zenbu-blue))))
   `(sml/vc-edited  ((t (:foreground ,zenbu-blue))))
   `(sml/modes ((t (:foreground ,zenbu-pink))))
   `(sml/position-percentage ((t (:foreground ,zenbu-charcoal))))

   `(flyspell-incorrect ((t (:underline (:color ,zenbu-red :style wave)))))
   `(flyspell-duplicate ((t (:underline (:color ,zenbu-yellow :style wave)))))

   ;; > Evil
   `(evil-ex-info ((,class (:foreground ,zenbu-fg))))
   `(evil-ex-substitute-replacement ((,class (:foreground ,zenbu-yellow))))
   `(evil-ex-substitute-matches ((,class (:inherit isearch))))

;; > dashboard
   `(dashboard-items-face ((,class (:weight light ))))

   ;; > Doom modeline
   `(doom-modeline-bar ((t (:inherit mode-line ))))
   `(doom-modeline-bar-inactive ((t (:inherit mode-line-inactive ))))
   `(doom-modeline-buffer-file ((,class ( :weight light ))))
   `(doom-modeline-buffer-path ((,class (:weight light ))))
   `(doom-modeline-buffer-project-root ((,class (:foreground ,zenbu-red ))))
   ;; > Powerline
   ;; `(powerline-active1 ((,class (:foreground ,zenbu-red :background ,zenbu-bg+1))))
   `(powerline-active0 ((,class ( :foreground ,zenbu-fg :background ,zenbu-bg+2 ))))
   `(powerline-active1 ((,class ( :foreground ,zenbu-fg :background ,zenbu-bg+1 ))))
   `(powerline-active2 ((,class (:foreground ,zenbu-bg :background ,zenbu-salmon))))
   ;; `(powerline-inactive1 ((,class (:background ,zenbu-charcoal))))
   ;; `(powerline-inactive2 ((,class (:background ,zenbu-charcoal))))
   `(powerline-inactive0 ((,class (:foreground ,zenbu-charcoal  :background ,zenbu-bg ))))
   `(powerline-inactive1 ((,class (:foreground ,zenbu-charcoal  :background ,zenbu-bg+2 ))))
   `(powerline-inactive2 ((,class (:foreground ,zenbu-charcoal  :background ,zenbu-bg+1))))
   ;; > Powerline Evil
   `(powerline-evil-base-face ((,class (:foreground ,zenbu-bg))))
   `(powerline-evil-normal-face ((,class (:background , zenbu-bg ))))
   `(powerline-evil-insert-face ((,class (:foreground ,zenbu-fg :background ,zenbu-teal))))
   `(powerline-evil-visual-face ((,class (:foreground ,zenbu-fg :background , zenbu-red))))
   `(powerline-evil-replace-face ((,class (:foreground ,zenbu-fg :background ,zenbu-yellow))))

   ;; > Elscreen
   `(elscreen-tab-background-face ((,class ( :foreground, zenbu-fg-1 :background,
							                               (substring( color-desaturate-name zenbu-bg-1 100)0 7)))))
   `(elscreen-tab-background-face ((,class ( :foreground, zenbu-fg :background, zenbu-bg))))
   `(elscreen-tab-control-face ((,class ( :foreground, zenbu-fg :background, zenbu-bg :box, nil))))
   `(elscreen-tab-current-screen-face ((,class ( :foreground, zenbu-fg+1 :background,
							                                   (substring(color-desaturate-name zenbu-bg+1 100)0 7) ))))
   `(elscreen-tab-other-screen-face ((,class ( :foreground, zenbu-charcoal :background, zenbu-bg))))

   `(neo-root-dir-face      ((t ( :foreground, zenbu-salmon ))))
   `(neo-button-face        ((t ( :foreground, zenbu-salmon ))))
   `(neo-expand-button-face ((t ( :foreground, zenbu-salmon ))))
   `(neo-header-face        ((t ( :foreground, zenbu-red    ))))
   `(neo-dir-link-face      ((t ( :foreground, zenbu-teal   ))))
   `(neo-file-link-face     ((t ( :foreground, zenbu-blue   ))))
   
                                        ; neo-banner-face            neo-buffer--insert-with-face    neo-button-face
                                        ; neo-dir-link-face          neo-expand-btn-face     neo-file-link-face
                                        ; neo-header-face            neo-root-dir-face       neo-vc-added-face
                                        ; neo-vc-conflict-face       neo-vc-default-face     neo-vc-edited-face
                                        ; neo-vc-ignored-face        neo-vc-missing-face     neo-vc-needs-merge-face
                                        ; neo-vc-needs-update-face   neo-vc-removed-face     neo-vc-unlocked-changes-face
                                        ; neo-vc-unregistered-face   neo-vc-up-to-date-face  neo-vc-user-face
   ))


(zenbu/with-color-variables
  (custom-theme-set-variables
   'zenbu
   `(ansi-color-names-vector [,"black" ,"#E2434C" ,"#86B187" ,"#E0D063" ,"#84C452" ,"#E18CBB" ,"#8AC6F2" ,"white"])))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'zenbu)


;;; zenbu-theme.el ends here
