;;; utils.el --- Utilities developed for emacs-groundup
;;;
;;; emacs-groundup is a GNU emacs configuration for elisp novices.
;;; emacs-groundup Copyright (C) 2022 Tushar Chauhan.
;;;
;;; Author      : Tushar Chauhan <research@tusharchauhan.com>
;;; License     : GNU GPLv3
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; beg: Filesystem ;;;

;; 1. +/FS/create-path (&rest path-fragments-in-order)
;;    "Use 'expand-file-name' repeatedly on the arguments
;;    PATH-FRAGMENTS-IN-ORDER."
;; 
;; 2. +/FS/rename-buffer-file (new-path)
;;    "Rename file visited by current buffer to NEW-PATH. Only works
;;    interactively."
;; 
;; 3. +/FS/copy-buffer-file-silent (new-path)
;;    "Make a copy of the file being visited by the current buffer to
;;    NEW-PATH."
;; 
;; 4. +/FS/touch-file-recursive (new-path)
;;    "Touch a new file at NEW-PATH."
;; 
;; 5. +/FS/move-to-dir (target-dir &optional message-text)
;;    "Move file open in buffer to a target directory TARGET-DIR."

;; 1. f() : create-path
(defun +/FS/create-path (&rest path-fragments-in-order)
  "Use 'expand-file-name' repeatedly on the arguments PATH-FRAGMENTS-IN-ORDER.

The path is assumed to go from left to right.  The validity of the path is NOT verified."
  (interactive)

  (let ((full-path (car path-fragments-in-order))
	(fragments (cdr path-fragments-in-order)))

    (if (> (length fragments) 0)
	(dolist (fragment fragments full-path)
	  (setq full-path (expand-file-name fragment full-path)))
      (setq full-path (expand-file-name full-path)))

    full-path))


;; 2. f() : rename file open in buffer
(defun +/FS/rename-buffer-file (new-path)
  "Rename file visited by current buffer to NEW-PATH.  Only works interactively."
  (interactive "FMove to : ")

  (let ((old-path buffer-file-name))

        (rename-file old-path new-path)
        (set-visited-file-name new-path)
        (save-buffer)

        (message "Renamed: %s -> %s."
                 (abbreviate-file-name old-path)
                 (abbreviate-file-name new-path))))


;; 3. f() : copy visited file in the background
(defun +/FS/copy-buffer-file-silent (new-path)
  "Make a copy of the file being visited by the current buffer to NEW-PATH.  Only works interactively."
  (interactive "FSave as : ")

  (let ((old-path buffer-file-name))

    ;; save old
    (save-buffer)
    ;; save new
    (set-visited-file-name new-path)
    (save-buffer)
    ;; open old again
    (set-visited-file-name old-path)
    (save-buffer)

    (message "Saved: %s." (abbreviate-file-name new-path))))


;; 4. f() : touch a file, creating directories in the path recursively
(defun +/FS/touch-file-recursive (new-path)
  "Touch a new file at NEW-PATH.

The directories in the path are created recursively.  Only works interactively."
  (interactive "FTouch : ")

  (if (file-exists-p new-path)
      (message "%s already exists." new-path)
    (progn
      (make-directory (file-name-directory new-path) t)
      (make-empty-file new-path)
      (message "Touched: %s." (abbreviate-file-name new-path)))))


;; 5. f() : move file open in buffer to a specific directory
(defun +/FS/move-to-dir (target-dir &optional message-text)
  "Move file open in buffer to a target directory TARGET-DIR.

The function also offers to save changes if the buffer is modified, and to kill the buffer after the file being visited has been moved.  An optional message MESSAGE-TEXT can also be displayed with the kill-prompt."
  (interactive)

  ;; Ask user if they want to save the buffer
  (when (buffer-modified-p)
    (if (y-or-n-p "Buffer modified.  Save before continuing? ")
        (save-buffer)))

  (let* ((target-path (+/FS/create-path
		       target-dir
		       (file-name-nondirectory buffer-file-name)))
	 (kill-prompt "Kill the loop buffer? ")
	 (kill-prompt (if message-text
			  (concat message-text " " kill-prompt))))

    ;; Create directory for the current year if it does not exist
    (unless (file-exists-p target-dir)
      (make-directory target-dir t))

    ;; Move the file and make sure it's saved
    (+/FS/rename-buffer-file target-path)

    ;; Offer to kill the buffer for the user
    (if (y-or-n-p kill-prompt)
        (kill-buffer (current-buffer)))))

(defun +/config/load-relative-directory (subdir)
  "Load all .el files in the SUBDIR relative to the directory of the current file.
Only loads files matching the pattern [number]-[name].el, and ensures they are
loaded in ascending order of their numeric prefixes."
  (let* ((current-dir (file-name-directory (or load-file-name buffer-file-name)))
         (target-dir (expand-file-name subdir current-dir))
         (files (directory-files-recursively target-dir "^[0-9]+-[a-zA-Z0-9_-]+\\.el$")))
    (dolist (file (sort files
                        (lambda (a b)
                          (< (string-to-number (car (split-string (file-name-nondirectory a) "-")))
                             (string-to-number (car (split-string (file-name-nondirectory b) "-")))))))
      (load file))))

;;; end: Filesystem ;;;

(provide 'utils)
