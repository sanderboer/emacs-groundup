(add-to-list 'custom-theme-load-path "~/.config/emacs/assets/themes/")
(setq theme-default 'omega)
;(setq theme-default 'zenbu)
;(setq theme-default 'sigma-dark)

(use-package emojify
  :straight t
  :init (setq emojify-download-emojis-p t)
  :hook (after-init . global-emojify-mode))

(use-package all-the-icons
  :straight t
  :if (display-graphic-p)
  :config
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t))
  )

(use-package imenu-list
  :after org
  :straight t
  :config
  (setq
   org-imenu-depth 5
   imenu-max-item-length 30
   imenu-list-auto-resize t
   imenu-list-mode-line-format nil
   imenu-list-position 'left)

  ;; Hook
  (add-hook 'imenu-list-major-mode-hook
            #'+/UIX/disable-visual-line-wrapping-hook))

(defadvice text-scale-increase (around all-buffers (arg) activate)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      ad-do-it)))

(use-package hide-mode-line
  :straight t)

(use-package rainbow-mode
  :straight t
  :config
  (rainbow-mode 1))

