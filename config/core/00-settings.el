(setq
 +/info/name  "Sander Boer"
 +/info/email "sander@starling.studio")

(setq user-full-name    +/info/name
      user-mail-address +/info/email)
(setq
 ;; Font
 ;; +/fonts/font "JuliaMono-14:regular"
 ;; Modeleader key
 +/UIX/modeleader "SPC"
 ;; Split width threshold that works for 1920x1080 screen
 +/UIX/vertical-split-threshold 125)

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)
(setq-default indent-tabs-mode nil)  ;; SPACES over TABS !
(setq pixel-scroll-mode 1)
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;;(setq linum-format "%4d \u2502 " )
(setq linum-format "%4d ")
(set-face-attribute 'show-paren-mismatch nil :background "red")
;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)
;; Performance tweaks, see
;; https://github.com/emacs-lsp/lsp-mode#performance
(setq read-process-output-max (* 1024 1024)) ;; 1mb
(setq lsp-idle-delay 0.500)

(fset 'yes-or-no-p 'y-or-n-p)

;; Speed up startup
;; Adapted from https://github.com/seagle0128/.config/emacs/.
(defvar default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(unless debug-on-error
  (setq inhibit-redisplay t))
(add-hook 'emacs-startup-hook
          (lambda ()
            "Restore defalut values after init."
            (setq file-name-handler-alist default-file-name-handler-alist)
            (setq inhibit-redisplay nil)
            (redisplay t)
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'focus-out-hook 'garbage-collect))))
(unless debug-on-error
  (setq inhibit-redisplay t))


;; UTF-8 please and thank you
(set-language-environment "English")    ; Set up multilingual environment
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top
(define-coding-system-alias 'UTF-8 'utf-8)
(define-coding-system-alias 'utf8 'utf-8)


;; Character encodings default to utf-8.
(set-language-environment "UTF-8")

(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)

;; MS Windows clipboard is UTF-16LE
(if (eq system-type 'gnu/linux)
    (set-clipboard-coding-system 'utf-8)
  (set-clipboard-coding-system 'utf-16le-dos)
  )



(setq auto-save-list-file-prefix ; Prefix for generating auto-save-list-file-name
      (expand-file-name ".auto-save-list/.saves-" user-emacs-directory)
      auto-save-default t        ; Auto-save every buffer that visits a file
      auto-save-timeout 20       ; Number of seconds between auto-save
      auto-save-interval 200)    ; Number of keystrokes between auto-saves


;; Use no-littering to automatically set common paths to the new user-emacs-directory


(setq user-emacs-directory (expand-file-name "~/.cache/emacs"))
(use-package no-littering
  :straight t)
(setq
 auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
 custom-file (expand-file-name "custom.el" user-emacs-directory)
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

(setq create-lockfiles nil)

;; Keep customization settings in a temporary file (thanks Ambrevar!)
;; (setq custom-file
;;       (if (boundp 'server-socket-dir)
;;           (expand-file-name "custom.el" server-socket-dir)
;;         (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))


(setq custom-file (+/FS/create-path user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))


(setq backup-directory-alist       ; File name patterns and backup directory names.
      `(("." . ,(expand-file-name "backups" user-emacs-directory)))
      make-backup-files t          ; Backup of a file the first time it is saved.
      vc-make-backup-files t       ; No backup of files under version contr
      backup-by-copying t          ; Don't clobber symlinks
      version-control t            ; Version numbers for backup files
      delete-old-versions t        ; Delete excess backup files silently
      kept-old-versions 6          ; Number of old versions to keep
      kept-new-versions 9          ; Number of new versions to keep
      delete-by-moving-to-trash t) ; Delete files to trash


(require 'savehist)

(setq kill-ring-max 50
      history-length 50)

(setq savehist-additional-variables
      '(kill-ring
        command-history
        set-variable-value-history
        custom-variable-history   
        query-replace-history     
        read-expression-history   
        minibuffer-history        
        read-char-history         
        face-name-history         
        bookmark-history
        file-name-history))

(put 'minibuffer-history         'history-length 50)
(put 'file-name-history          'history-length 50)
(put 'set-variable-value-history 'history-length 25)
(put 'custom-variable-history    'history-length 25)
(put 'query-replace-history      'history-length 25)
(put 'read-expression-history    'history-length 25)
(put 'read-char-history          'history-length 25)
(put 'face-name-history          'history-length 25)
(put 'bookmark-history           'history-length 25)

(setq history-delete-duplicates t)
;;(let (message-log-max)
;;   (savehist-mode))
(setq save-place-file (expand-file-name "saveplace" user-emacs-directory)
      save-place-forget-unreadable-files t)

(save-place-mode 1)


(setq visible-bell 1)
(setq inhibit-startup-message t
      initial-scratch-message nil)
;(scroll-bar-mode -1)
(tool-bar-mode -1)
;(set-fringe-mode 10)
(setq scroll-conservatively 101)
(menu-bar-mode -1)
(setq ediff-split-window-function #'split-window-horizontally)
(global-visual-line-mode t)
(set-mouse-color "white")

(setq calendar-week-start-day 1)

(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'font-lock-warning-face))

(setq calendar-intermonth-header
      (propertize "Wk"                  ; or e.g. "KW" in Germany
                  'font-lock-face 'font-lock-keyword-face))

(setq
 ;; Set the locale
 +/lang/LANG "nl_NL"

 ;; Paths for the dictionaries
 +/lang/DICPATH "/usr/share/hunspell/:/home/sander/.config/emacs/config/dic"

 ;; Configure multi-language dictionary
 +/lang/MULTIDIC "en_GB,en_US")

(global-prettify-symbols-mode nil)
(setq prettify-symbols-unprettify-at-point t)

(setq make-backup-files t
      backup-by-copying t
      vc-make-backup-files t
      backup-directory-alist `(("." . ,+/UIX/dir-backup))
      version-control t
      kept-new-versions 3
      kept-old-versions 3
      delete-old-versions t)

(setq auto-save-default t
      auto-save-timeout 20
      auto-save-interval 150)

(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(setq delete-by-moving-to-trash t)

(show-paren-mode t)
(setq show-paren-style 'expression)
(electric-pair-mode t)
(setq frame-resize-pixelwise t)
