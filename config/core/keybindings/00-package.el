(use-package which-key
  :straight t
  :init (which-key-mode)
  :config
  (setq
   which-key-idle-delay 0.3
   ;; which-key-sort-order 'which-key-key-order))
   ;; which-key-sort-order 'which-key-prefix-then-key-order))
   ;; which-key-sort-order 'which-key-description-order))
   which-key-sort-order 'which-key-key-order-alpha))

(use-package helpful
  :straight t)

(use-package hydra
  :straight t)

(use-package yasnippet
  :straight t
  :config
  (setq yas-snippet-dirs `(,(+/FS/create-path +/config/dir-config
                                              +/yas/reldir-snippets)))
  (yas-global-mode 1))

(use-package general
  :straight t
  :after (evil pdf-tools)
  :config
  (general-evil-setup t)
  ;; Risky but we'll take it...
  (general-auto-unbind-keys nil))

(with-eval-after-load 'evil
  (general-add-hook 'after-init-hook
                    (lambda (&rest _)
                      (when-let ((messages-buffer (get-buffer "*Messages*")))
                        (with-current-buffer messages-buffer
                          (evil-normalize-keymaps))))
                    nil nil t))

(general-create-definer +/UIX/map-to-leader
			:keymaps 'override
			:prefix "SPC"
			"" '(:ignore t :wk "leader-key"))

(general-create-definer +/UIX/map-to-modeleader
			:keymaps 'override
			:prefix (concat "SPC " +/UIX/modeleader)
			"" '(:ignore t :wk "modebindings"))

(defun +/FS/bookmarks/keybind-dirs (binding-prefix bookmarks)
  "Keybind directory bookmarks.
First argument is the binding prefix, e.g., 'SPC o d', and the second argument is a list of directory bookmarks where each entry is an alist. The format of this alist is ('description' . ('x' '/path/')), where 'x' is the character bound to the directory at '/path/', and 'description' is a short key which describes the directory."
  (let ((bm))
    (dolist (bm bookmarks)
      (general-define-key
       :states '(normal visual motion)
       :keymaps 'override
       (concat binding-prefix " " (nth 0 (cdr bm)))
       `((lambda () (interactive) (dired ,(nth 1 (cdr bm)))) :wk ,(car bm))))))


(defun +/FS/bookmarks/keybind-files (binding-prefix bookmarks)
  "Keybind file bookmarks.
First argument is the binding prefix, e.g., 'SPC o f', and the second argument is a list of file bookmarks where each entry is an alist. The format of this alist is ('description' . ('x' '/path-to-file')), where 'x' is the character bound to the file at '/path-to-file', and 'description' is a short key which describes the file."
  (let ((bm))
    (dolist (bm bookmarks)
      (general-define-key
       :states '(normal visual motion)
       :keymaps 'override
       (concat binding-prefix " " (nth 0 (cdr bm)))
       `((lambda () (interactive) (find-file ,(nth 1 (cdr bm)))) :wk ,(car bm))))))
