
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "TAB" '(:wk "perspective")
  ;; Move b/w persps
  "TAB TAB" '(persp-switch :wk "switch/new")
  "TAB SPC" '(persp-switch-last :wk "toggle")
  "TAB j" '(persp-prev :wk "previous")
  "TAB k" '(persp-next :wk "next")
  ;; Operations on persps
  "TAB r" '(persp-rename :wk "rename")
  "TAB d" '(persp-kill :wk "delete")
  "TAB s" '(persp-state-save :wk "save state")
  "TAB l" '(persp-state-load :wk "load state")
  ;; Buffers in a persp
  "TAB b"  '(:wk "buffer")
  "TAB b a" '(persp-add-buffer :wk "add buffer")
  "TAB b A" '(persp-set-buffer :wk "add buffer exclusively")
  "TAB b d" '(persp-remove-buffer :wk "delete buffer")
  "TAB b B" '(persp-ibuffer :wk "list persp buffers"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "#" '(org-capture :wk "capture"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "." '(:wk "registers")
  ". a" '(point-to-register :wk "add")
  ". ." '(register-to-point :wk "goto")
  ". /" '(consult-register :wk "consult registers"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/" '(:wk "utils")
  ;; Outline
  "/ /" '(consult-outline :wk "outline")
  ;; insert emoji
  "/ e" '(emojify-insert-emoji :wk "insert emoji")
  ;; imenu sidebar toggle
  "/ m" '(imenu-list-smart-toggle :wk "imenu sidebar")
  ;; Exchange point and mark
  "/ ." '(exchange-point-and-mark :wk "exchange point & mark")
  ;; Pass (may be best to remove keybinding to pass)
  "/ ?"  '(pass :wk "pass")
  ;; Diff
  "/ D" '(:wk "ediff")
  "/ D f"'(ediff :wk "files")
  "/ D b"'(ediff-buffers :wk "buffers")
  ;; Save unsaved buffers
  "/ S" '(save-some-buffers :wk "save unsaved")
  ;; Remove whitespace
  "/ W" '(whitespace-cleanup :wk "remove whitespace"))

;; Map the hydra
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ F" '(+/UIX/hydra-font-scaling/body :wk "font scaling"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :mode 'override
  "/ c"    '(:wk "comment")
  "/ c c"  '(evilnc-comment-or-uncomment-lines :wk "comment/uncomment")
  "/ c p"  '(evilnc-copy-and-comment-lines :wk "comment and copy")
  "/ c y"  '(evilnc-comment-and-kill-ring-save :wk "comment and yank"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ d" '(:wk "thesaurus/dictionary")

  "/ d a" '(powerthesaurus-lookup-antonyms-dwim :wk "antonyms")
  "/ d s" '(powerthesaurus-lookup-synonyms-dwim :wk "synonyms")
  "/ d r" '(powerthesaurus-lookup-related-dwim :wk "related")
  "/ d d" '(powerthesaurus-lookup-definitions-dwim :wk "show definitions")
  "/ d e" '(powerthesaurus-lookup-sentences-dwim :wk "show examples")
  "/ d M" '(powerthesaurus-lookup-dwim :wk "menu") )

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ n" '(:wk "narrow/widen")
  "/ n f" '(narrow-to-defun :wk "function")
  "/ n r" '(narrow-to-region :wk "region")
  "/ n p" '(narrow-to-page :wk "page")
  "/ n SPC" '(widen :wk "widen"))

;; Mapping
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ o" '(:wk "olivetti-mode")
  ;; Toggle olivetti mode
  "/ o o" '(olivetti-mode :wk "toggle")
  ;; Resize
  "/ o R" '(+/UIX/hydra-resize-olivetti/body :wk "resize"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ s" '(:wk "spelling")
  ;; Spelling corrections (no spell-checking)
  "/ s" '(:wk "spelling")
  "/ s s" '(flyspell-correct-at-point :wk "correct at point")
  ;; Buffer-wide operations
  "/ s b" '(flyspell-buffer :wk "spellcheck buffer")
  "/ s B" '(+/writing/spellcheck-and-correct-buffer :wk "check & correct buffer")
  ;; Region/Selection restricted operations
  ;; "/ e r" '(+/writing/spellcheck-region :wk "spellcheck region")
  ;; "/ e r" '(+/writing/spellcheck-and-correct-region :wk "check & correct region")
  ;; Continuous checking: Only works if the buffer has already been spellchecked
  "/ s j" '(flyspell-correct-next :wk "correct next")
  "/ s k" '(flyspell-correct-previous :wk "correct previous"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "b" '(:wk "buffer")
  "b SPC" '(evil-switch-to-windows-last-buffer :wk "switch")
  "b b" '(switch-to-buffer :wk "switch")
  "b B" '(ibuffer-other-window :wk "list")
  "b c" '(+/UIX/buffer-to-clipboard :wk "clipboard")
  "b s" '(save-buffer :wk "save")
  "b k" '(+/UIX/kill-this-buffer :wk "kill")
  "b e" '(eval-buffer :wk "evaluate")
  "b U" '(sudo-edit :wk "sudo visited"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "d" '(:wk "dired")

  "d d" '(dired-jump :wk "pwd")
  "d o" '(dired-jump-other-window :wk "pwd in other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "e" '(:ignore t :wk "evaluate")
  ;; M-x
  "e SPC" '(execute-extended-command :wk "M-x")
  ;; Evaluate sexp/region/buffer
  "e e" '(eval-last-sexp :wk "sexp")
  "e r" '(eval-region :wk "region")
  "e b" '(eval-buffer :wk "buffer")
  ;; Compose and evaluate expression
  "e E" '(eval-expression :wk "expression"))


(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "f" '(:ignore t :wk "file")
  "f f" '(find-file :wk "find/visit")
  "f t" '(+/FS/touch-file-recursive :wk "touch")
  "f r" '(consult-recent-file :wk "recently visited")
  "f u" '(sudo-edit-find-file :wk "sudo find/visit")
  "f s" '(save-buffer :wk "save")
  "f c" '(write-file :wk "copy & visit")
  "f C" '(+/FS/copy-buffer-file-silent :wk "copy silently")
  "f R" '(+/FS/rename-buffer-file :wk "rename")
  "f !" '(recover-this-file :wk "recover")
  "f d" '(dired :wk "find directory"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "h" '(:wk "help")
  ;; Colours and faces help
  "h C" '(list-colors-display :wk "colours")
  "h F" '(describe-face    :wk "face at point")
  "h c" '(helpful-command  :wk "commands")
  "h f" '(helpful-callable :wk "callables") ;; Callables include functions.
  "h k" '(helpful-key      :wk "keys")
  "h m" '(describe-mode    :wk "local modes")
  "h p" '(describe-package :wk "package")
  "h v" '(helpful-variable :wk "variables")
  "h x" '(helpful-at-point :wk "at point"))
  (+/UIX/map-to-leader
    :states '(normal visual motion)
    :keymaps 'override
    "k" '( :wk "kill")
    "k b" '(kill-buffer :wk "kill buffer")
    "k k" '(kill-this-buffer :wk "kill this buffer")
)

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "m" '(:wk "bookmarks")
  "m a" '(bookmark-set :wk "new")
  "m m" '(consult-bookmark :wk "consult bookmarks")
  "m d" '(bookmark-delete :wk "delete"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "n" '(treemacs :wk "treemacs")
  ;; "n" '(neotree-toggle :wk "neotree")
  ;; M-x
  ;; "n SPC" '(execute-extended-command :wk "M-x")
)

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o" '(:wk "open"))

(+/UIX/map-to-leader
 :states '(normal visual motion)
 :keymaps 'override
 "o a" '(:wk "agenda")
 "o a a" '((lambda () (interactive) (org-agenda "a" "A")) :wk "daily")
 "o a p" '((lambda () (interactive) (org-agenda "a" "P")) :wk "prune & reassess")
 "o a r" '((lambda () (interactive) (org-agenda "a" "R")) :wk "re-plan")
 "o a t" '((lambda () (interactive) (org-agenda "a" "T")) :wk "triage"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o d" '(:wk "directory"))

;; User-configured keybindings
;; (+/FS/bookmarks/keybind-dirs "SPC o d" +/FS/bookmarks/dirs)

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o f" '(:wk "file"))

;; User-configured keybindings
;; (+/FS/bookmarks/keybind-files "SPC o f" +/FS/bookmarks/files)

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "o j" '(org-journal-new-entry :wk "journal"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o l" '(view-echo-area-messages :wk "*Messages*"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "o m" '(magit :wk "magit"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o t" '("vterm")
  "o t t" '(vterm :wk "this window")
  "o t o" '(vterm-other-window :wk "other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "p" '(:wk "projects")
  "p a" '(projectile-add-known-project :wk "add new")
  "p c" '(projectile-compile-project :wk "compile project; run make.")
  ;; "p p" '(consult-projectile-switch-project :wk "open/switch")
  "p p" '(projectile-persp-switch-project :wk "open/switch")
  ;; "p p" '(projectile-switch-project :wk "open/switch")
  "p D" '(projectile-remove-known-project :wk "delete")
  ;; "p f" '(projectile-find-file :wk "find in project")
  "p b" '(consult-projectile-switch-to-buffer :wk "switch buffers")
  "p f" '(+/consult-projectile/enhanced-find-file :wk "find in project")
  "p r" '(consult-projectile-recentf :wk "recent in project")
  "p B" '(projectile-ibuffer :wk "ibuffer project")
  "p d" '(:wk "project root")
  "p d d" '(projectile-dired :wk "this window")
  "p d o" '(projectile-dired-other-window :wk "other window")
  ;; "p b" '(projectile-switch-to-buffer :wk "switch buffer")
  "p s" '(projectile-save-project-buffers :wk "save project buffers")
  "p k" '(projectile-kill-buffers :wk "kill project buffers")
  "p m" '(:wk "git")
  "p m m" '(+/projectile/vc-here :wk "this window")
  "p m o" '(projectile-vc :wk "other window")
  "p t" '(:wk "terminal")
  "p t t" '(projectile-run-vterm :wk "this window")
  "p t o" '(+/projectile/run-vterm-other-window :wk "other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "q" '(:ignore t :wk "quit")
  "q q" '(delete-frame :wk "quit frame")
  ;; "q q" '(+/UIX/quit-frame :wk "quit frame")
  "q r" '(restart-emacs :wk "restart")
  "q Q" '(kill-emacs :wk "kill server"))

;; Remap for standalone mode
(unless (daemonp)
  (+/UIX/map-to-leader
    :states '(normal visual motion)
    :keymaps 'override
    "q q" '(kill-emacs :wk "quit frame")))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "r" '(:ignore t :wk "roam")
  "r r" '(org-roam-node-find :wk "find node")
  ;; "r c" '(+/org/roam-freeze-loop :wk "cold-store loop")
  ;; "r x" '(+/org/roam-close-loop :wk "close loop")
  ;; "r d" '(+/org/roam-bin-loop :wk "bin loop")
  "r f" '(org-roam-node-find :wk "find node")
  "r i" '(org-roam-node-insert :wk "insert reference")
  "r l" '(org-roam-buffer-toggle :wk "toggle backlinks buffer")
  "r L" '(org-roam-buffer-display-dedicated :wk "backlinks buffer")
  "r s" '(org-roam-db-sync :wk "sync database")
  "r g" '(org-roam-graph :wk "graph"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "s" '(:wk "search")
  "s s" '(consult-line    :wk "buffer")
  "s p" '(consult-ripgrep :wk "project/directory"))

;; Mapping
(+/UIX/map-to-leader
  :states '(normal visual motion emacs)
  :keymaps 'override
  "w" '(:ignore t :wk "window")
  "w w" '(ace-window :wk "other window")
  "w d" '(delete-window :wk "delete")
  "w f" '(other-frame :wk "other frame")
  "w m" '(delete-other-windows :wk "maximise")
  "w s" '(split-window-below :wk "horizontal split")
  "w v" '(split-window-right :wk "vertical split")
  ;; Move focus
  "w F" '(+/UIX/hydra-window-switch-focus/body :wk ":focus")
  ;; Move windows
  "w M" '(+/UIX/hydra-window-move-window/body :wk ":move")
  ;; Resize windows
  "w R" '(+/UIX/hydra-resize-window/body :wk ":resize"))

