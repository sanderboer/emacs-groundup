(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'org-mode-map
  ;; Priorities
  "p" '(+/UIX/hydra-org-priority/body :wk "priority")
  ;; TODOs
  "t" '(+/UIX/hydra-org-todo/body :wk "todo")
  ;; Checklist toggling
  "x" '(org-toggle-checkbox :wk "toggle checkbox")
  ;; Cookie update
  "k" '(:wk "update cookie(s)")
  "k k" '(org-update-statistics-cookies :wk "update at point")
  "k K" '((lambda () (interactive) (org-update-statistics-cookies t))
          :wk "update buffer")
  ;; Tags
  "T" '(org-set-tags-command :wk "choose tags")
  ;; Dates/Schedules/Deadlines
  "d" '(:wk "date")
  "d s" '(org-schedule :wk "schedule")
  "d d" '(org-deadline :wk "deadline")
  "d t" '((lambda () (interactive) (org-schedule nil "")) :wk "today")
  "d T" '((lambda () (interactive) (org-deadline nil "")) :wk "deadline today")
  ;; Links
  "l" '(:wk "link")
  "l o" '(org-open-at-point :wk "open")
  "l l" '(org-insert-link :wk "insert")
  "l y" '(org-store-link :wk "copy")
  "l t" '(org-toggle-link-display :wk "toggle")
  ;; Source blocks
  "s" '(:wk "src-block")
  "s s" '(org-babel-execute-src-block :wk "execute")
  "s e" '(org-edit-src-code :wk "edit")
  "s i" '(+/org/indent-org-src-block :wk "indent")
  ;; Toggle markup
  "M" '(+/org/toggle-markup :wk "toggle markup")
  "I" '(org-toggle-inline-images :wk "toggle images")
  ;; Toggle latex previews
  "L" '(org-latex-preview :wk "toggle LaTex")
  ;; Narrow/Widen subtree
  "n" '(org-toggle-narrow-to-subtree :wk "narrow/widen")
  ;; Add to agenda
  "A" '(org-agenda-file-to-front :wk "add to agenda")
  ;; Archiving logs
  "Z" '(+/org/org-archive-logbook :wk "archive logbook"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map
  "f" '(:wk "fold")
  "f f" '(hs-toggle-hiding :wk "toggle")
  "f x" '(hs-hide-block :wk "fold block")
  "f X" '(hs-hide-all :wk "fold all")
  "f o" '(hs-show-block :wk "show block")
  "f O" '(hs-show-all :wk "show all")
  "f R" '(hs-hide-level :wk "fold sub-blocks"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map
  "d" '(:wk "debug")
  "d d" '(consult-flycheck        :wk "buffer diagnostic")
  "d D" '(consult-lsp-diagnostics :wk "project diagnostic"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map
  ;; Code-related operation
  "c" '(:wk "code")
  "c /" '(consult-lsp-file-symbols :wk "consult local symbols")
  "c ?" '(consult-lsp-symbols :wk "consult project symbols")
  "c r" '(lsp-rename :wk "rename symbol")
  "c i" '(lsp-organize-imports :wk "organise imports")
  ;; Jump to/from code
  "." '(:wk "jump to")
  ". d"  '(lsp-find-definition :wk "definition")
  ". r"  '(lsp-find-references :wk "reference")
  ". j"  '(evil-jump-backward :wk "backward")
  ". k"  '(evil-jump-forward :wk "forward again"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'web-mode-map
  "f t" '(web-mode-fold-or-unfold :wk "toggle-tag"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'prog-mode
  :keymaps 'python-ts-mode-map
  "c f" '(:wk "format")
  "c f I" '(py-isort-buffer :wk "isort buffer")
  "c f i" '(py-isort-region :wk "isort region")
  "c f F" '(python-black-buffer :wk "black buffer")
  "c f f" '(python-black-partial-dwim :wk "black region")
  "R" '(run-python :wk "REPL")
  "r" '(:wk "eval in REPL")
  "r r" '(python-shell-send-region :wk "region")
  "r b" '(python-shell-send-buffer :wk "buffer"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'dired-mode
  :keymaps 'dired-mode-map
  "d" '(dired-du-mode :wk "dired-du")
  "o" '(dired-omit-mode :wk "dired-omit")
  "h" '(dired-hide-details-mode :wk "hide details"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'markdown-mode-map
  ;; Links
  "l" '(:wk "link")
  "l o" '(markdown-follow-link-at-point :wk "open")
  "l l" '(markdown-insert-link :wk "insert")
  "l t" '(markdown-toggle-url-hiding :wk "toggle")
  ;; Show/Hide markup
  "M" '(markdown-toggle-markup-hiding :wk "toggle markup"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'pdf-view-mode
  :keymaps 'pdf-view-mode-map
  ;; Activate noter
  "N" '(org-noter :wk "org-noter"))

(+/UIX/map-to-modeleader
  :states '(normal visual)
  :keymaps 'org-src-mode-map
  "s" '(:ignore t :wk "src-block")
  "s q" '(org-edit-src-exit  :wk "quit/finish edit")
  "s k" '(org-edit-src-abort :wk "kill/abort edit"))

(general-define-key
 :states '(visual normal motion)
 :keymaps 'dired-mode-map
 "SPC" nil ;; Makes modeleader bindings possible in dired
 "TAB" #'+/FS/visit-file-from-dired-using-ace-window
 "s"   #'hydra-dired-quick-sort/body
 "q"   #'+/FS/prompt-and-kill-current-dired-buffer)

(general-define-key
 :states '(normal visual motion insert)
 :keymaps 'org-mode-map
 "<M-return>" #'+/org/org-sensible-m-ret
 "<C-return>" #'+/org/org-sensible-c-ret
 ;; orgshifts
 "<" 'org-shiftleft
 ">" 'org-shiftright
 ;; Sensible M-j/k
 "M-j" #'+/org/org-sensible-m-j
 "M-k" #'+/org/org-sensible-m-k)

(general-define-key
 :states '(visual normal motion)
 :keymaps 'org-agenda-mode-map
 ">" '(org-agenda-later :wk "next")
 "<" '(org-agenda-earlier :wk "prev"))

(general-define-key
 :keymaps 'org-super-agenda-header-map
 "<tab>" '(origami-toggle-node :wk "visit")
 "r" '(org-agenda-redo :wk "refresh")
 "q" '(org-agenda-quit :wk "quit"))

(general-define-key
 :keymaps 'pdf-outline-buffer-mode-map
 :states 'normal
  "<return>" '(pdf-outline-follow-link :wk "follow link"))

(general-define-key
 :keymaps 'override
 "C-M-k" 'scroll-other-window
 "C-M-j" 'scroll-other-window-down)

(general-define-key
 :mode evil-insert-state-minor-mode
 :states 'insert
 "C-k" nil)

;; Bindings
(general-define-key
 :states '(insert motion)
 :keymaps 'vertico-map
 "C-j" 'vertico-next
 "C-k" 'vertico-previous)

(general-define-key
 :keymaps 'isearch-mode-map
 "C-j" 'isearch-ring-advance
 "C-k" 'isearch-ring-retreat)

(general-define-key
 :keymaps 'minibuffer-local-isearch-map
 "C-j" 'next-history-element
 "C-k" 'previous-history-element)

(general-define-key
 :keymaps 'imenu-list-major-mode-map
 :states 'normal
  "M-<return>" '(imenu-list-display-entry :wk "show")
  "C-S-i" '(+/DEV/editorconfig-reformat :wk "format buffer")
  )

(general-define-key
 :keymaps 'prog-mode-map
 :states '(normal visual motion insert)
  "C-<f12>" '(xref-go-back :wk "jump back definition")
  "<f12>" '(lsp-find-definition :wk "find definition")
  "C-S-i" '(+/DEV/editorconfig-reformat :wk "format buffer")
  "C-<return>" '(hs-toggle-hiding :wk "toggle folding")
  "M-<return>" '(hs-hide-all :wk "fold all")
  "M-C-<return>" '(hs-show-all :wk "show all")
  "M-<up>" '(+/UIX/move-line-up :wk "move line up")
  "M-<down>" '(+/UIX/move-line-down :wk "move line down")
  "C-/" '(evilnc-comment-or-uncomment-lines  :wk "toggle comment")
  )

(general-define-key
 :keymaps 'mu4e-headers-mode-map
 :states '(normal visual motion insert)
 "C-c c" '(mu4e-org-store-and-capture :wk "org store and capture")
 "C-c c" '(mu4e-org-store-and-capture :wk "org store and capture")
 )
