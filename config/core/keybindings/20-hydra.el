
(defhydra +/UIX/hydra-org-priority ()
  "Set priority: "
  ("a" (lambda () (interactive) (org-priority ?a)) "A" :color teal)
  ("b" (lambda () (interactive) (org-priority ?b)) "B" :color teal)
  ("c" (lambda () (interactive) (org-priority ?c)) "C" :color teal)
  ("j" org-priority-down "-" :color amaranth)
  ("k" org-priority-up   "+" :color amaranth)
  ("x" (lambda () (interactive) (org-priority 'remove)) "Remove" :color teal)
  ("SPC" nil "quit"))

;; hydra: org todo actions
(defhydra +/UIX/hydra-org-todo ()
  "Set TODO state: "
  ("t" (lambda () (interactive) (org-todo 1)) "todo" :color teal)
  ("w" (lambda () (interactive) (org-todo 2)) "waiting" :color teal)
  ("c" (lambda () (interactive) (org-todo 3)) "cancelled" :color teal)
  ("d" (lambda () (interactive) (org-todo 4)) "done" :color teal)
  ("j" (org-call-with-arg 'org-todo 'left) "previous" :color amaranth)
  ("k" (org-call-with-arg 'org-todo 'right) "next" :color amaranth)
  ("x" (lambda () (interactive) (org-todo 5)) "remove" :color teal)
  ("SPC" nil "quit"))

(defhydra +/UIX/hydra-font-scaling ()
  "font scaling"
  ("=" text-scale-increase "bigger")
  ("-" text-scale-decrease "smaller")
  ("r" +/fonts/text-scale-reset "reset")
  ("SPC" nil "quit"))

(defhydra +/UIX/hydra-resize-olivetti ()
  "Resize olivetti region size."
  ("-" olivetti-shrink "shrink")
  ("=" olivetti-expand "expand")
  ("R" olivetti-set-width "set width")
  ("SPC" nil "quit"))

(defhydra +/UIX/hydra-window-switch-focus ()
  "switch focus"
  ("l" windmove-right "right" :color amaranth)
  ("h" windmove-left "left" :color amaranth)
  ("j" windmove-down "down" :color amaranth)
  ("k" windmove-up "up" :color amaranth)
  ("SPC" nil "quit"))

;; hydra: Move windows
(defhydra +/UIX/hydra-window-move-window ()
  "move window"
  ("l" buf-move-right "move right" :color teal)
  ("h" buf-move-left "move left" :color teal)
  ("j" buf-move-down "move down" :color teal)
  ("k" buf-move-up "move up" :color teal)
  ("a" rotate-frame-anticlockwise "rotate +90" :color amaranth)
  ("c" rotate-frame-clockwise "rotate -90" :color amaranth)
  ("SPC" nil "quit"))

;; hydra: Resize window
(defhydra +/UIX/hydra-resize-window ()
  "resize window"
  ("=" balance-windows "balance windows" :color teal)
  ("l" enlarge-window-horizontally "+ width" :color amaranth)
  ("h" shrink-window-horizontally "- width" :color amaranth)
  ("j" shrink-window "- height" :color amaranth)
  ("k" enlarge-window "+ height" :color amaranth)
  ("SPC" nil "quit"))
