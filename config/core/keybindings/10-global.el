
;; (add-hook 'org-metaup-hook 'move-line-up)
;; (add-hook 'org-metadown-hook 'move-line-down)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-3") 'split-window-horizontally) ; was digit-argument
(global-set-key (kbd "M-2") 'split-window-vertically) ; was digit-argument
(global-set-key (kbd "M-1") 'delete-other-windows) ; was digit-argument
(global-set-key (kbd "M-0") 'delete-window) ; was digit-argument
(global-set-key (kbd "M-o") 'other-window) ; was facemenu-keymap
(global-set-key (kbd "M-b") 'ido-switch-buffer) ; was facemenu-keymap
(global-set-key (kbd "M-t") 'treemacs)
(global-set-key (kbd "M-n") 'neotree-toggle)
(global-set-key (kbd "C-s") 'save-buffer)

(global-set-key (kbd "C-S") 'isearch-forward-symbol-at-point)
(global-set-key [f5] 'revert-buffer)
(global-set-key "\C-h\C-e" 'hippie-expand)
(global-set-key (kbd "M-f") 'find-file)
(global-set-key (kbd "M-q") 'delete-frame)
(global-set-key (kbd "M-p") 'fill-paragraph)
(global-set-key (kbd "M-z") 'keyboard-quit)

(global-unset-key (kbd "C-x 3")) ; was split-window-horizontally
(global-unset-key (kbd "C-x 2")) ; was split-window-vertically
(global-unset-key (kbd "C-x 1")) ; was delete-other-windows
(windmove-default-keybindings)
;; (windmove-default-keybindings 'meta)

(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

(global-set-key (kbd "M-C-/") 'bjm-comment-box)
(global-set-key (kbd "C-S-<prior>") 'text-scale-increase)
(global-set-key (kbd "C-S-<next>") 'text-scale-decrease)

