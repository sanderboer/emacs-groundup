;; (use-package nano-theme 
;;   :straight (:type git
;;                    :host github
;;                    :repo "rougier/nano-theme")
;;   )

;; (use-package sigma-theme 
;;   :straight (:type git
;;                    :host github
;;                    :repo "sanderboer/sigma-theme")
;;   )


(use-package lambda-themes
  :straight (:type git :host github :repo "lambda-emacs/lambda-themes") 
  :custom
  (lambda-themes-set-italic-comments t)
  (lambda-themes-set-italic-keywords t)
  (lambda-themes-set-variable-pitch t) 
  :config
  ;; load preferred theme 
  ;;(load-theme 'lambda-light)
  )

;; Dim inactive windows
(use-package dimmer
  :straight (:host github :repo "gonewest818/dimmer.el")
  :hook (after-init . dimmer-mode)
  :config
  (setq dimmer-fraction 0.5)
  (setq dimmer-adjustment-mode :foreground)
  (setq dimmer-use-colorspace :rgb)
  (setq dimmer-watch-frame-focus-events nil)
  (dimmer-configure-which-key)
  (dimmer-configure-helm)
  (dimmer-configure-hydra)
  (dimmer-configure-org)
  (dimmer-configure-magit)
  (dimmer-configure-posframe))

;; Make a clean & minimalist frame
;; (use-package frame
;;   :straight (:type built-in)
;;   :config
;;   (setq-default default-frame-alist
;;                 (append (list
;;                          ;; '(font . "Roboto Sans Mono:style=medium:size=15") ;; NOTE: substitute whatever font you prefer here
;;                          '(internal-border-width . 20)
;;                          '(left-fringe    . 0)
;;                          '(right-fringe   . 0)
;;                          '(tool-bar-lines . 0)
;;                          '(menu-bar-lines . 0)
;;                          '(vertical-scroll-bars . nil))))
;;   (setq-default window-resize-pixelwise t)
;;   (setq-default frame-resize-pixelwise t))
