(use-package evil
  :straight t
  :demand t
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-undo-system 'undo-fu
        evil-respect-visual-line-mode t)

  :config
  (setq
   evil-auto-indent t
   evil-want-fine-undo t
   ;; evil-default-cursor t
   evil-want-minibuffer t
   evil-overriding-maps t)

  (evil-normalize-keymaps)
  (evil-mode 1))

(use-package undo-fu
  :straight t)

(use-package evil-collection
  :straight t
  :after evil
  :custom
  (evil-collection-setup-minibuffer t)
  :init
  (evil-collection-init))

(use-package evil-surround
  :straight t
  :after evil
  :config
  (global-evil-surround-mode 1))

;(use-package evil-org
;  :straight t
;  :after (org evil)
;  :hook (org-mode . (lambda () evil-org-mode))
;  :config
;  (require 'evil-org-agenda)
;  (evil-org-agenda-set-keys))
;
(use-package evil-snipe
  :straight t
  :demand t
  :after evil
  :hook (magit-mode . turn-off-evil-snipe-override-mode)
  :custom
  (evil-snipe-scope 'whole-visible)
  (evil-snipe-repeat-scope 'whole-buffer)
  :config
  (evil-snipe-mode t)
  (evil-snipe-override-mode t))

(use-package key-chord
  :straight t
  :after evil
  :config
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kk" 'evil-normal-state)

  (key-chord-mode 1))

(use-package evil-nerd-commenter
  :straight t
  :after evil
  :bind ("M-/" . evilnc-comment-or-uncomment-lines)
  )
