(straight-use-package
 '(nano :type git :host github :repo "rougier/nano-emacs"))

;; Display org properties in the agenda buffer (modified version)
;; (straight-use-package
;;  '(org-agenda-property :type git :host github :repo "Malabarba/org-agenda-property"
;;                        :fork (:host github :repo "rougier/org-agenda-property")))

;; ;; NANO splash
;; (straight-use-package
;;  '(nano-splash :type git :host github :repo "rougier/nano-splash"))

;; ;; NANO theme
;; (straight-use-package
;;  '(nano-theme :type git :host github :repo "rougier/nano-theme"))

;; ;; NANO modeline
;; (straight-use-package
;;  '(nano-modeline :type git :host github :repo "rougier/nano-modeline"))

;; ;; NANO agenda
;; (straight-use-package
;;  '(nano-agenda :type git :host github :repo "rougier/nano-agenda"))

;; ;; NANO agenda
;; (straight-use-package
;;  '(minibuffer-header :type git :host github :repo "rougier/minibuffer-header"))

;; ;; SVG tags, progress bars & icons
;; (straight-use-package
;;  '(svg-lib :type git :host github :repo "rougier/svg-lib"))

;; ;; Replace keywords with SVG tags
;; (straight-use-package
;;  '(svg-tag-mode :type git :host github :repo "rougier/svg-tag-mode"))

;; ;; Dashboard for mu4e
;; (straight-use-package
;;  '(mu4e-dashboard :type git :host github :repo "rougier/mu4e-dashboard"))

;; ;; Folding mode for mu4e
;; (straight-use-package
;;  '(mu4e-folding :type git :host github :repo "rougier/mu4e-folding"))

;; ;; Relative date formatting
;; (straight-use-package
;;  '(relative-date :type git :host github :repo "rougier/relative-date"))

;; ;; org imenu
;; (straight-use-package
;;  '(org-imenu :type git :host github :repo "rougier/org-imenu"))

;; ;; pdf-drop-mode
;; (straight-use-package
;;  '(pdf-drop-mode :type git :host github :repo "rougier/pdf-drop-mode"))


;; ;; Bilbliography manager in org mode
;; (straight-use-package
;;  '(org-bib :type git :host github :branch "org-imenu" :repo "rougier/org-bib-mode"))
