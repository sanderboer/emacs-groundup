(require 'color)
(use-package hsluv
  :straight t)

(defun +/fonts/prettify-symbols-push-to-alist (symbol-list)
  "Set symbols for prettification. Used in conjuction with modal hooks."
  (mapc (lambda (pair) (push pair prettify-symbols-alist))
        symbol-list)
  (prettify-symbols-mode 1))

(defun +/UIX/disable-visual-line-wrapping-hook ()
  "Disable visual line wrapping in the current buffer."
  (visual-line-mode nil)
  (setq truncate-lines 1))

(defun hexcolour-luminance (color)
  "Calculate the luminance of a color string (e.g. \"#ffaa00\", \"blue\").
  This is 0.3 red + 0.59 green + 0.11 blue and always between 0 and 255."
  (let* ((values (x-color-values color)) (r (car values)) (g (cadr values))
	 (b (caddr values))) (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))

(defun +/UIX/set-colors-based-on-theme ()
  (interactive)
  "Sets the hl-line face to have no foreground and a background
that is adjusted based on the current theme's default background."
  (let* ((default-bg (face-background 'default nil 'default))
         (bg (if (or (not default-bg) (string= default-bg "unspecified-bg"))
                 "#000000" ;; Fallback to black if no background is specified
               default-bg))
         (luminance (if bg (hexcolour-luminance bg) 0))
         (sgn (if (< luminance 20) -1 1)))

    ;; Safeguard against invalid color operations
    (unless (stringp bg)
      (setq bg "#000000"))

    ;; Adjust the hl-line face
    (set-face-attribute 'hl-line nil
                        :foreground nil
                        ;; :underline (color-desaturate-name
                        ;;             (color-lighten-name bg (* sgn 120))
                        ;;             30)
                        :background nil
                        :inherit nil
                        :background (color-desaturate-name
                                    (color-lighten-name bg (* sgn 120))
                                    30)
                        )
    ;; Adjust show-paren-match-expression face
    (set-face-background 'show-paren-match-expression
                         (color-desaturate-name
                          (color-lighten-name bg (* sgn 130))
                          100))

    ;; Adjust show-paren-match face
    (set-face-background 'show-paren-match
                         (color-desaturate-name
                          (color-lighten-name bg (* sgn 130))
                          100))

    ;; Adjust region face
    (set-face-background 'region
                         (color-desaturate-name
                          (color-lighten-name bg (* sgn 80))
                          50))))

;(defun +/UIX/set-colors-based-on-theme ()
;  (interactive)
;  "Sets the hl-line face to have no foregorund and a background
;        that is 10% darker than the default face's background."
;  (setq bg
;        (if (or (eq (face-background 'default) nil)
;		            (eq (face-background 'default) "unspecified-bg"))
;            (color-darken-name "#000000" 0 )
;          (face-background 'default )
;          ))
;  (if (< (hexcolour-luminance bg ) 20)
;      (setq sgn -1) (setq sgn 1) )
;  (set-face-attribute 'hl-line nil
;                      :foreground nil
;                      :background (color-desaturate-name
;			                             (color-lighten-name bg (* sgn 120))
;			                             30))
;(set-face-background 'show-paren-match-expression
;		                   (color-desaturate-name
;			                  (color-lighten-name bg (* sgn 130))
;			100))
;  (set-face-background 'show-paren-match
;		                   (color-desaturate-name
;			                  (color-lighten-name bg (* sgn 130))
;			100))
;  (set-face-background 'region
;		                   (color-desaturate-name
;			                  (color-lighten-name bg (* sgn 80))
;			                   50))
;  )
;

(defun +UIX/term-keys()
  (interactive)
  (progn
    (define-key input-decode-map "\e[1;2D" [S-left])
    (define-key input-decode-map "\e[1;2C" [S-right])
    (define-key input-decode-map "\e[1;2B" [S-down])
    (define-key input-decode-map "\e[1;2A" [S-up])
    (define-key input-decode-map "\e[1;2F" [S-end])
    (define-key input-decode-map "\e[1;2H" [S-home])
    (define-key input-decode-map "\e[1;5D" [C-left])
    (define-key input-decode-map "\e[1;5C" [C-right])
    (define-key input-decode-map "\e[1;5B" [C-down])
    (define-key input-decode-map "\e[1;5A" [C-up])
    (define-key input-decode-map "\e[1;3A" [M-up])
    (define-key input-decode-map "\e[1;3B" [M-down])
    (define-key input-decode-map "\e[1;3C" [M-right])
    (define-key input-decode-map "\e[1;3D" [M-left])
    )
  )

(defun +/UIX/buffer-to-clipboard ()
    "Copy entire buffer to clipboard"
    (interactive)
    (clipboard-kill-ring-save (point-min) (point-max)))

(defun +/UIX/indent-buffer ()
    "Copy entire buffer to clipboard"
    (interactive)
    (indent-region (point-min) (point-max)))

(defmacro +/UIX/save-column (&rest body)
  `(let ((column (current-column)))
     (unwind-protect
         (progn ,@body)
       (move-to-column column))))
(put 'save-column 'lisp-indent-function 0)

(defun +/UIX/move-line-up ()
  (interactive)
  (+/UIX/save-column
   (transpose-lines 1)
   (forward-line -2)))

(defun +/UIX/move-line-down ()
  (interactive)
      (+/UIX/save-column
       (forward-line 1)
       (transpose-lines 1)
       (forward-line -1)))



(defun +/UIX/base-look-term()
  "How a term should look."
  (interactive)
  (global-hl-line-mode t)
  (load-theme theme-default t)
  (+/UIX/set-colors-based-on-theme)
  ;; (sndr-set-nano-theme) 
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (set-face-background 'default "unspecified-bg" (selected-frame))
  (+/UIX/term-keys)
  ;;(set-colors-based-on-theme )
  )


(defun +/UIX/base-look()
  "How a window should look."
  (interactive)
  (progn
    (if (string= (system-name) "sun-ra")
	      (progn
	        ;; (set-face-attribute 'default nil :height 120)
	        )
      (progn
	      ;; (set-face-attribute 'default nil :height 120)
	      )
      )
    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (tooltip-mode -1)
    (global-hl-line-mode t)
    (load-theme theme-default t)
    ;; (sndr-set-nano-theme) 
    (+/UIX/set-colors-based-on-theme)
    (set-frame-parameter (selected-frame) 'internal-border-width 30)
    ;; (set-frame-parameter (selected-frame) 'alpha '(95 90))
    ;; (add-to-list 'default-frame-alist '(alpha 95 90))
    (setq header-line-format " ")
    (set-frame-parameter (selected-frame) 'bottom-divider-width 1)
    (dolist
        (buf (list " *Minibuf-0*" " *Minibuf-1*" " *Echo Area 0*" " *Echo Area 1*" "*Quail Completions*"))
      (when (get-buffer buf)
        (with-current-buffer buf
          (setq-local face-remapping-alist '((default (:height 1.0)))))))
    )
  )

(defun +/fonts/text-scale-reset ()
  (interactive)
  (text-scale-set 0))

(defun +/UIX/kill-this-buffer ()
  "Kill the current buffer."
  (interactive)
  (kill-buffer (current-buffer)))

(defun +/UIX/kill-current-persp-if-in-project ()
  "If inside a project, kill perspective and close project."
  (interactive)

  (let* ((project-name (projectile-project-name)))
    (if project-name
        (progn (projectile-kill-buffers)
               (persp-kill project-name))
      (message "Not in a project. You need to kill the persp using \"SPC TAB d\"."))))

(defun +/UIX/comment-box (b e)
  "Draw a box comment around the region but arrange for the region to extend to at least the fill column. Place the point after the comment box."

  (interactive "r")

  (let ((e (copy-marker e t)))
    (goto-char b)
    (end-of-line)
    (insert-char ?  (- fill-column (current-column)))
    (comment-box b e 1)
    (goto-char e)
    (set-marker e nil)))

