
(setq use-package-always-defer t )
(setq gc-cons-threshold (* 128 1024 1024))
(setq gc-cons-percentage 0.7)
;; Set up the utilities; FS functions

(setq +/yas/reldir-snippets (expand-file-name "assets/snippets" user-emacs-directory) )

(defvar +/UIX/dir-backup (+/FS/create-path user-emacs-directory "backups/"))
(unless (file-exists-p +/UIX/dir-backup)
  (make-directory +/UIX/dir-backup t))

;; url for straight.el installer
(defvar +/straight/url
  "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el")

;; bootstrapping variables
(defvar +/straight/path-bootstrap "straight/repos/straight.el/bootstrap.el")

;; bootstrapping
(let ((bootstrap-file (+/FS/create-path user-emacs-directory
					+/straight/path-bootstrap)))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously +/straight/url 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq
 ;; Folder for non-MELPA packages (relative to X/C/)
 +/packages/reldir-non-melpa "src/non-melpa-packages/"
 ;; Ligature support (ligature.el)
 ;; This should be emacs proper in the future.
 +/packages/file-ligature "ligature")


;; Suppress cl-related warning
;; Ref: https://github.com/kiwanami/emacs-epc/issues/35
(setq byte-compile-warnings '(cl-functions))

;; Libraries: emacs-groundup 'base'
(defvar +/packages/early-install '(org use-package))
(dolist (pkg +/packages/early-install)
  (unless (require 'pkg nil 'noerror)
    (straight-use-package pkg)))
(make-variable-buffer-local 'compile-command)

(use-package exec-path-from-shell
  :straight t
  :config
  (when (daemonp)
    (exec-path-from-shell-initialize)))
