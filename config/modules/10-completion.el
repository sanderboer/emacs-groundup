(message "Loading modules/10-compeltion.el")

(use-package vertico
  :demand t
  :straight t
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package vertico-posframe
  :straight t
  :demand t
  :after
  (vertico-mode)
  :init
  (vertico-posframe-mode 1))


(use-package orderless
  :straight t
  :demand t
  :custom
  (completion-styles '(orderless)))

(use-package marginalia
  :straight t
  :demand t
  :after vertico
  :init (marginalia-mode 1))

(use-package consult
  :straight t
  :demand t
  :config
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args))))

(use-package company
  :straight t
  ;; :disabled nil
  :demand t
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay (lambda () (if (company-in-string-or-comment) nil 0.1))
        company-selection-wrap-around t
        company-require-match nil
        company-tooltip-align-annotations t
        company-tooltip-limit 5
        company-icon-size '(auto-scale . 20)
        company-icon-margin 2
        company-tooltip-minimum 10
        company-search-regexp-function 'company-search-words-in-any-order-regexp)

  (global-company-mode))

(use-package all-the-icons-completion
  :demand t
  :straight t
  :after (all-the-icons marginalia)
  :config
  (add-hook 'marginalia-mode-hook
            #'all-the-icons-completion-marginalia-setup)
  (all-the-icons-completion-mode))



(use-package corfu
  ;; Optional customizations
  :straight t
  :disabled t
  :demand nil
  :custom
  (corfu-cycle t)                 ; Allows cycling through candidates
  (corfu-auto nil)                  ; Enable auto completion
  (corfu-auto-prefix 2)
  (corfu-auto-delay 0.0)
  (corfu-popupinfo-delay '(0.5 . 0.2))
  (corfu-preview-current 'insert) ; Do not preview current candidate
  (corfu-preselect 'prompt)
  (corfu-on-exact-match nil)      ; Don't auto expand tempel snippets

  ;; Optionally use TAB for cycling, default is `corfu-complete'.
  :bind (:map corfu-map
              ("M-SPC"      . corfu-insert-separator)
              ("TAB"        . corfu-complete)
              ([tab]        . corfu-next)
              ("S-TAB"      . corfu-previous)
              ([backtab]    . corfu-previous)
              ("S-<return>" . corfu-insert)
              ("RET"        . nil))

  :init
  (global-corfu-mode)
  ;; (corfu-history-mode)
  ;; (corfu-popupinfo-mode) ; Popup completion info
  :config
  (add-hook 'eshell-mode-hook
            (lambda () (setq-local corfu-quit-at-boundary t
                                   corfu-quit-no-match t
                                   corfu-auto nil)
              (corfu-mode))))

(use-package cape
  :straight t
  :disabled t
  :demand t
  :bind ("C-c f" . cape-file)
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (defalias 'dabbrev-after-2 (cape-capf-prefix-length #'cape-dabbrev 2))
  (add-to-list 'completion-at-point-functions 'dabbrev-after-2 t)
  (cl-pushnew #'cape-file completion-at-point-functions)
  :config
  ;; Silence then pcomplete capf, no errors or messages!
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

  ;; Ensure that pcomplete does not write to the buffer
  ;; and behaves as a pure `completion-at-point-function'.
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify))

