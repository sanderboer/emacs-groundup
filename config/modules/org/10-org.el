(setq
 ;; org
 +/org/dir-org "~/.org/"
 ;; org-journal
 +/org/dir-journal "~/.org/journal/"
 ;; org-roam
 +/org/dir-roam "~/.org/roam/"
 ;; org-roam: Loop and notes directories (relative to the roam directory)
 +/org/reldir-roam-loops-open   "loops/open/"
 +/org/reldir-roam-loops-cold   "loops/cold/"
 +/org/reldir-roam-loops-closed "loops/closed/"
 +/org/reldir-roam-loops-binned "loops/binned/"
 +/org/reldir-roam-notes        "notes/"
 ;; org-roam: Template directory (relative to X/C/)
 +/org/reldir-roam-templates "templates/roam/"
 ;; org-capture (relative to the roam directory)
 +/org/reldir-capture +/org/reldir-roam-loops-open
 +/org/file-capture "_-captures.org"
 ;; org-capture: Templates
 +/org/config-capture-templates
 '(;; Quick tasks
   ("P" . ("process-soon" "Tasks"
  "* TODO %:fromname: %a %?\nDEADLINE: %(org-insert-time-stamp (org-read-date nil t \"+2d\"))"))
   ("t" . ("quick task" "Tasks"
           "* TODO [#%^{Priority|A|B|C}] %^{Task}\nSCHEDULED: %^t\n%?"))
   ;; email follow-ups
   ("e" . ("email follow-up" "email follow-ups"
           "* TODO [#%^{Priority|A|B|C}] :email: %^{Recipients}\nSCHEDULED: %^t\n%?"))
   ;; Notes
   ("n" . ("quick note" "Notes" "* %^{title} :triage:\n%?")))
 ;; org-agenda: Directories for generating agenda
 +/org/dirs-agenda (list
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-notes)
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-loops-cold)
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-loops-open)))

(setq
 ;; Directories
 +/FS/bookmarks/dirs
 `(("root"      . ("/" "/"))
   ("home"      . ("~" "~/"))
   ("loops"     . ("l" ,(+/FS/create-path +/org/dir-roam
                                          +/org/reldir-roam-loops-open)))
   ("notes"     . ("n" ,(+/FS/create-path +/org/dir-roam
                                          +/org/reldir-roam-notes)))
   ("config"    . ("c" ,+/config/dir-config))
   )
 ;; Files
 +/FS/bookmarks/files
 `(;; captures (SPC #)
   ("captures"         . ("#" ,(+/FS/create-path +/org/dir-roam
                                                 +/org/reldir-capture
                                                 +/org/file-capture)))))

(use-package org
  :straight t
  :demand t
  :init (setq org-directory +/org/dir-org)
  :config
  ;; Create org-directory if it does not exist
  (make-directory +/org/dir-org t)
  ;; Config
  (setq
   org-archive-location (concat org-directory "/misc/archive.org::")
   org-default-notes-file (concat org-directory "/verzameling.org")
   ;; UI/UX
   ;; org-startup-folded 'content
   org-startup-folded t
   org-startup-indented t
   org-startup-with-inline-images t
   org-hide-leading-stars t
   ;; org-blank-before-new-entry t ;; messes up M-RER
   org-blank-before-new-entry '((heading . nil) (plain-list-item . nil))
   org-M-RET-may-split-line '((item . nil))
   org-cycle-separator-lines 1
   org-edit-src-content-indentation 0
   org-cycle-separator-lines 2
   org-image-actual-width 200
   org-ellipsis "  " ;; folding symbol 
   org-outline-path-complete-in-steps nil
   org-pretty-entities t
   org-hide-emphasis-markers t ;; show actually italicized text instead of /italicized text/
   org-fontify-done-headline t
   org-fontify-whole-heading-line t
   org-refile-targets (quote ((nil :maxlevel . 1)
                              (org-agenda-files :maxlevel . 1)))
   org-refile-use-outline-path nil
   org-refile-allow-creating-parent-nodes (quote confirm)
   org-tag-alist '(
                   ("aquisitie" . ?a)
                   ("project" . ?p)
                   ("home" . ?h)
                   )
   ;; Agenda
   org-agenda-block-separator ""
   ;; TODOs
   org-todo-keywords '((sequence "DOEN" "VOLGENDE" "|" "VOLTOOID" "OOIT" "WACHTEN" ))
   ;; org-todo-keywords '((sequence "TODO(t)" "WAITING(w!/!)" "|" "CANCELLED(c!/!)" "DONE(d!/!)"))
   org-todo-keyword-faces
   '(("DOEN" . "#cd927b")
     ("VOLGENDE" . "#bb9b6c")
     ("OPVOLGEN" . "#75a6d4")
     ("AFSPRAAK" . "#75a6d4")
     ("DOORGESTUURD" . "#75a6d4")
     ("WACHTEN" . "#53afad")
     ("VOLTOOID" . "#83ab79")
     ("OOIT" . "#53afad")
     ("OPGEHEVEN" . "grey25")
     )
   ;; Priorities
   org-priority-highest '?A
   org-priority-lowest  '?C
   org-priority-default '?B
   org-priority-faces '((?A :foreground "magenta2"
                            :background "white smoke"
                            :box (:line-width 1))
                        (?B :foreground "darkorange1"
                            :background "white smoke"
                            :box (:line-width 1))
                        (?C :foreground "turquoise3"
                            :background "white smoke"
                            :box (:line-width 1)))
   org-priority-start-cycle-with-default t
   ;; quotes and verses
   org-fontify-quote-and-verse-blocks t
   ;; src-blocks
   org-src-tab-acts-natively t
   org-src-fontify-natively t
   org-edit-src-content-indentation 0
   ;; latex previews
   org-preview-latex-image-directory (+/FS/create-path
                                      user-emacs-directory
                                      "ltximg/")
   ;; logging
   org-log-into-drawer t)
  ;; hide markup
  (+/org/toggle-markup nil t)
  )

(use-package org-capture
  :straight nil
  :after org
  :config

  (setq org-capture-templates
	'(
	  ("t" "Taak" entry (file+headline org-default-notes-file "Taken")
	   "* DOEN %?\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n%i\n\n")

	  ("i" "Idee" entry (file+headline org-default-notes-file "Taken")
	   "* :OOIT: %?\n:PROPERTIES:\n:Context: %a\nCaptured: %U\n:END:\n%i\n\n")

	  ("d" "Dagboek" entry
	   (file+olp+datetree "misc/dagboek.org")
	   "* %?\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n %i\n\n")

	  ("s" "Screenshot" entry
	   (file+olp+datetree "misc/screenshots.org")
	   "* %T\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n%i\n\n")

	  ("l" "Link" entry (file+headline org-default-notes-file "Weblinks")
	   "* %a\n %?\n %i \n")
	  )
	))

(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

(setq inhibit-compacting-font-caches t)
(use-package org-superstar
  :after org
  :straight t
  :config
  (org-superstar-configure-like-org-bullets)
  (setq org-superstar-headline-bullets-list
        ;; '(128312 	128313) ;; orange and blue diamonds
        '(8858 8226 8728 8278 8759)
	)
  :hook (org-mode . org-superstar-mode))

(use-package org-fancy-priorities
  :after org
  :straight t
  :after org
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  ;; (setq org-fancy-priorities-list '("🅐" "🅑" "🅒")))
  (setq org-fancy-priorities-list '("[A]" "[B]" "[C]")))

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src sh"))
(add-to-list 'org-structure-template-alist '("pu" . "src plantuml :file images/xxx.png"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("ts" . "src typescript"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("go" . "src go"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

(use-package org-auto-tangle
  :after org
  :demand t
  :straight t
  ;; :load-path "site-lisp/org-auto-tangle/"    ;; this line is necessary only if you cloned the repo in your site-lisp directory 
  :hook (org-mode . org-auto-tangle-mode))
