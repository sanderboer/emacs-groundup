(add-hook 'org-babel-after-execute-hook
	  'org-display-inline-images 'append)

;; Editing src-blocks (evil-keys don't work otherwise)
;; org-src minor mode buffer
(add-hook 'org-src-mode-hook 'evil-normalize-keymaps)

;; Don't ask before evaluating each block
(setq org-confirm-babel-evaluate nil)

;; Support the following languages
(with-eval-after-load 'org
  (org-babel-do-load-languages 'org-babel-load-languages
			       '((emacs-lisp . t)
				 (python     . t)
				 (plantuml . t)
				 ))
  )
