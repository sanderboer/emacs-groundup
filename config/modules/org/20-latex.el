(require 'ox-latex)
(setq-default
 org-latex-logfiles-extensions (quote ("lof" "lot" "tex" "tex~" "aux" "idx" "log" "out" "toc" "nav" "snm" "vrb" "dvi" "fdb_latexmk" "blg" "brf" "fls" "entoc" "ps" "spl" "bbl")) ;; these extensions get deleted on export.
 org-latex-compiler "xelatex"
 org-export-in-background 'nil) ;; TODO: Seems buggy when set to t
(setq-default org-latex-listings 'minted)  ;; Use python minted to fontify
(setq org-latex-minted-options '(("bgcolor" "gray!5") ))
(setq org-export-publishing-directory "/tmp/org-export/")
;; PDF output process with comments
;; 1. `--shell-escape' required for minted. See `org-latex-listings'
;; 2. "bibtex %b" ensures bibliography (of org-ref) is processed during pdf generation
;; 3. Remove output logs, out, auto at the end of generation
(setq org-latex-pdf-process
      '("%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "bibtex %b"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        ))

(setq org-latex-default-packages-alist
      (quote (("AUTO" "inputenc" t)
              ("" "fixltx2e" nil)
              ("" "graphicx" t)
              ("" "longtable" nil)
              ("" "float" nil)
              ("" "wrapfig" nil)
              ("" "soul" t)
              ("" "svg" t)
              ("" "marvosym" t)
              ("" "wasysym" t)
              ("" "latexsym" t)
              ("" "amssymb" t)/
              ("" "hyperref" nil)
              "\\tolerance=1000")))

(setq org-latex-title-command "\\maketitle\\cleardoublepage")

(defun org-latex-format-toc-default (depth)
  (when depth
    (format "\\setcounter{tocdepth}{%s}\n\\tableofcontents\n\\vspace*{1cm}\n\\cleardoublepage"
            depth)))
(add-to-list 'org-latex-classes
             '("basic"
               "\\input{~/.config/emacs/assets/templates/tex/class-basic.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc.tex}
  \\input{~/.config/emacs/assets/templates/tex/niji.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("starling"
               "\\input{~/.config/emacs/assets/templates/tex/class-starling.tex}
  \\input{~/.config/emacs/assets/templates/tex/niji.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("starling-landscape"
               "\\input{~/.config/emacs/assets/templates/tex/class-starling-landscape.tex}
    \\input{~/.config/emacs/assets/templates/tex/niji.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc-landscape"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-landscape.tex}
                                      [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-landscape-3cols"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-landscape-3cols.tex}
                                      [NO-DEFAULT-PACKAGES]
                   \\begin{multicols}{3}
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc-notes"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-notes.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("vabi"
               "\\input{~/.config/emacs/assets/templates/tex/class-vabi.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-article"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-article.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-scape"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-scape.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(require 'ox-koma-letter)
(add-to-list 'org-latex-classes
             '("mauc-letter"
               "\\input{~/.config/emacs/assets/templates/tex/class-mauc-letter.tex}
                                     [NO-DEFAULT-PACKAGES]
                                     "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("my-letter"
               "\\documentclass\{scrlttr2\}
                                      \\usepackage[english]{babel}
                                      \\setkomavar{frombank}{(1234)\\,567\\,890}
                                      \[DEFAULT-PACKAGES]
                                      \[PACKAGES]
                                      \[EXTRA]"))

(setq org-koma-letter-default-class "mauc-letter")
