;; Load svg-lib
(use-package svg-lib
  :straight (:type git :host github :repo "rougier/svg-lib"))

;; Load svg-tag-mode
(use-package svg-tag-mode
  :straight (:type git :host github :repo "rougier/svg-tag-mode"))

;; Define custom faces
(defface sndr-org-todo
  '((t :foreground "black"
       :background "#cd927b"
       :weight semi-bold))
  "Face for TODO tags."
  :group 'sndr-faces)

(defface sndr-org-next
  '((t :foreground "black"
       :background "#bb9b6c"
       :weight semi-bold))
  "Face for NEXT tags."
  :group 'sndr-faces)

(defface sndr-org-done
  '((t :foreground "black"
       :background "#83ab79"
       :weight semi-bold))
  "Face for DONE tags."
  :group 'sndr-faces)

(defface sndr-org-fade
  '((t :foreground "black"
       :background "#444"
       :weight semi-bold))
  "Face for faded tags."
  :group 'sndr-faces)

;; Define svg tags
(setq svg-tag-tags
      '(("DOEN" . ((lambda (tag) (svg-tag-make "DOEN"
                                               :face 'sndr-org-todo
                                               :radius 3
                                               :margin 0))))
        ("VOLGENDE" . ((lambda (tag) (svg-tag-make "VOLGENDE"
                                                   :face 'sndr-org-next
                                                   :radius 3
                                                   :margin 0))))
        ("VOLTOOID" . ((lambda (tag) (svg-tag-make "VOLTOOID"
                                                   :face 'sndr-org-done
                                                   :radius 3
                                                   :margin 0))))
        ("OOIT" . ((lambda (tag) (svg-tag-make "OOIT"
                                               :face 'sndr-org-fade
                                               :radius 3
                                               :margin 0))))
        ("WACHTEN" . ((lambda (tag) (svg-tag-make "WACHTEN"
                                                  :face 'sndr-org-fade
                                                  :radius 3
                                                  :margin 0))))
        ("#\\+BEGIN_SRC \\([a-zA-Z-]+\\)" . ((lambda (tag)
                                               (svg-tag-make (upcase tag)
                                                             :face 'org-meta-line
                                                             :inverse t
                                                             :crop-left t))))
        ("#\\+END_SRC" . ((lambda (tag) (svg-tag-make "END"
                                                      :face 'org-meta-line))))
        ("\\(#\\+BEGIN_SRC\\)" . ((lambda (tag) (svg-tag-make "BEGIN"
                                                              :face 'org-meta-line
                                                              :crop-right t))))))

;; Enable svg-tag-mode in org-mode
(add-hook 'org-mode-hook
          (lambda ()
            (require 'svg-lib)  ; Ensure svg-lib is loaded
            (plist-put svg-lib-style-default :font-family "RobotoMono Nerd Font")
            (plist-put svg-lib-style-default :font-size 12)
            (svg-tag-mode t)))




;; (use-package svg-lib
;;   :straight( :type git :host github :repo "rougier/svg-lib")
;;   )

;; ;; Replace keywords with SVG tags
;; (use-package svg-tag-mode
;;   :straight 
;;   (:type git :host github :repo "rougier/svg-tag-mode")
;;   )

;; (defface sndr-org-todo
;;   '((t :foreground "black"
;;        :background "#cd927b"
;;        :font-weight 500
;;        ))
;;   "Face for function parameters."
;;   :group 'sndr-faces )

;; (defface sndr-org-next
;;   '((t :foreground "black"
;;        :background "#bb9b6c"
;;        :font-weight 500
;;        ))
;;   "Face for global variables."
;;   :group 'sndr-faces )
;; (defface sndr-org-done
;;   '((t :foreground "black"
;;        :background "#83ab79"
;;        :font-weight 500
;;        ))
;;   "Face for global variables."
;;   :group 'sndr-faces )
;; (defface sndr-org-fade
;;   '((t :foreground "black"
;;        :background "#444"
;;        :font-weight 500
;;        ))
;;   "Face for global variables."
;;   :group 'sndr-faces )

;; (setq svg-tag-tags
;;       '(
;;         ("DOEN" . ((lambda (tag) (svg-tag-make "DOEN"
;;                                                :face 'sndr-org-todo
;;                                                :radius 3
;;                                                :margin 0
;;                                                ))))
;;         ("VOLGENDE" . ((lambda (tag) (svg-tag-make "VOLGENDE"
;;                                                    :face 'sndr-org-next
;;                                                    :radius 3
;;                                                    :margin 0
;;                                                    ))))
;;         ("VOLTOOID" . ((lambda (tag) (svg-tag-make "VOLTOOID"
;;                                                    :face 'sndr-org-done
;;                                                    :radius 3
;;                                                    :margin 0 
;;                                                    ))))
;;         ("OOIT" . ((lambda (tag) (svg-tag-make "OOIT"
;;                                                :face 'sndr-org-fade
;;                                                :radius 3
;;                                                :margin 0 
;;                                                ))))
;;         ("WACHTEN" . ((lambda (tag) (svg-tag-make "WACHTEN"
;;                                                   :face 'sndr-org-fade
;;                                                   :radius 3
;;                                                   :margin 0 
;;                                                   ))))
;;         ("#\\+[bB][eE][gG][iI][nN]_[sS][rR][cC]\\( [a-zA-Z\-]+\\)" .  ((lambda (tag)
;; 									 (svg-tag-make (upcase tag)
;; 										       :face 'org-meta-line
;; 										       :inverse t
;; 										       :crop-left t))))
;;         ("#\\+[eE][nN][dD]_[sS][rR][cC]" .    ((lambda (tag) (svg-tag-make "END"
;; 									   :face 'org-meta-line))))
;;         ("\\(#\\+[bB][eE][gG][iI][nN]_[sS][rR][cC]\\)" .    ((lambda (tag) (svg-tag-make "BEGIN"
;; 											 :face 'org-meta-line
;; 											 :crop-right t
;; 											 ))))
;;         )
;;       )


;; ;; (add-hook 'org-mode-hook (lambda () 
;; ;;                            (plist-put svg-lib-style-default :font-family "RobotoMono Nerd Font")
;; ;;                            (plist-put svg-lib-style-default :font-size 12)
;; ;;                            (svg-tag-mode t)
;; ;;                            ))

;; (add-hook 'org-mode-hook (lambda () 
;;                            (require 'svg-lib)  ; Ensure svg-lib is loaded
;;                            (setq svg-lib-style-default '(:font-family "RobotoMono Nerd Font"
;;                                                                    :font-size 12))  ; Initialize as a plist
;;                            (svg-tag-mode t)
;;                            ))
