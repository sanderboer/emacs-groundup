(defun +/org/evil-insert-state-in-edit-buffer (fun &rest args)
  "Bind `evil-default-state' to `insert' before calling FUN with ARGS."

  (let ((evil-default-state 'insert)
        ;; Force insert state
        evil-emacs-state-modes
        evil-normal-state-modes
        evil-motion-state-modes
        evil-visual-state-modes
        evil-operator-state-modes
        evil-replace-state-modes)
    (apply fun args)
    (evil-refresh-cursor)))

(advice-add 'org-babel-do-key-sequence-in-edit-buffer
            :around #'+/org/evil-insert-state-in-edit-buffer)

(defun +/org/toggle-markup (&optional force-show force-hide)
  "Toggle emphasis markers in org.
The optional arguments, evaluated left to right, force showing and hiding respectively."
  (interactive)
  (when force-show
    (setq org-hide-emphasis-markers nil)
    (when org-link-descriptive
      (org-toggle-link-display)))
  (when force-hide
    (setq org-hide-emphasis-markers t)
    (unless org-link-descriptive
      (org-toggle-link-display)))
  (when
      (not (or force-show force-hide))
    (setq org-hide-emphasis-markers
          (not org-hide-emphasis-markers))
    (org-toggle-link-display)))

(defun +/org/org-sensible-m-ret ()
  "Sensible M-ret."
  (interactive)
  (org-insert-heading-after-current)
  (evil-append 1))

(defun +/org/org-sensible-c-ret ()
  "Sensible C-ret."
  (interactive)
  (org-insert-heading-respect-content)
  (evil-append 1))

(defun +/org/indent-org-src-block ()
  "Indent org source block."
  (interactive)
  (when (org-in-src-block-p)
    (org-edit-special)
    (indent-region (point-min) (point-max))
    (org-edit-src-exit)))

(defun +/org/org-sensible-m-j ()
  "Move items and headings down."
  (interactive)
  (cond ((org-at-heading-p) (org-move-subtree-down))
        ((org-at-item-p) (org-move-item-down))))

(defun +/org/org-sensible-m-k ()
  "Move items and headings up."
  (interactive)
  (cond ((org-at-heading-p) (org-move-subtree-up))
        ((org-at-item-p) (org-move-item-up))))
