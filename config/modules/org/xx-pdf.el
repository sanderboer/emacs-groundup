(use-package pdf-tools
  :straight (:type git :host github :repo "vedang/pdf-tools")
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :demand t
  :init
  ;; Stop cursor blinking hack
  (add-hook 'pdf-view-mode-hook (lambda () (blink-cursor-mode -1)))
  ;; Remove modeline from the outline-buffer
  (add-hook 'pdf-outline-buffer-mode-hook #'hide-mode-line-mode)

  :config
  (setq pdf-view-use-scaling t
        ;; pdf-outline-display-labels t
        pdf-annot-activate-created-annotations t
        pdf-annot-list-format '((page . 3)
                                (type . 10)
                                (date . 24)))

  ;; outline buffer appearance (SPC / m)
  ;; FIXME: How to do something similar for annots buffer?
  (customize-set-variable
   'display-buffer-alist
   '(("^\\*outline"
      display-buffer-in-side-window
      (side . left)
      (window-width . 0.35)
      (inhibit-switch-frame . t))))

  (pdf-loader-install))

(use-package org-pdftools
  :after org
  :straight t
  :after (org pdf-tools)
  :hook (org-mode . org-pdftools-setup-link))

(use-package org-noter
  :after org
  :straight t
  :after (org pdf-tools)
  :config
  (setq org-noter-doc-split-fraction '(0.6 . 0.5)))

