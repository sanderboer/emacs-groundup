
(add-hook 'org-agenda-mode-hook
          (lambda ()
            (define-key org-agenda-mode-map " " 'org-agenda-cycle-show)))

(add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1)))  ;; Always hilight the current agenda line
(setq org-agenda-todo-ignore-deadlines nil)
(setq org-agenda-date-weekend t)  ;; weekends in a different colour
(setq org-agenda-todo-ignore-with-date t)
(setq org-agenda-todo-list-sublevels nil)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-scheduled-if-deadline-is-shown nil)
(setq org-agenda-skip-additional-timestamps-same-entry nil)
(setq org-agenda-start-on-weekday 1)

(setq org-agenda-custom-commands
      '(("a" "Primary agenda view"
         ((agenda "day" ((org-agenda-ndays 1) (org-agenda-overriding-header "Vandaag")))

          (todo ":VOLGENDE:"
                ((org-agenda-todo-ignore-scheduled nil)
                 (org-agenda-todo-ignore-deadlines nil)
                 (org-agenda-todo-ignore-with-date nil)
                 (org-agenda-overriding-header "Unstuck project tasks")))
          (todo ":WACHTEN:" ((org-agenda-todo-ignore-scheduled nil)
                             (org-agenda-todo-ignore-deadlines nil)
                             (org-agenda-todo-ignore-with-date nil)
                             (org-agenda-overriding-header "Things waiting on the perennially disorganised masses")))
          (todo "DOEN"
                ((org-agenda-todo-ignore-scheduled nil)
                 (org-agenda-todo-ignore-deadlines nil)
                 (org-agenda-todo-ignore-with-date nil)
                 (org-agenda-overriding-header "Ontheemde taken")))
          )
         ((org-habit-show-habits t)
          ;;(org-agenda-log-mode-items '(closed clock state))
          (org-agenda-start-with-log-mode t)
          ;;(org-agenda-start-with-log-mode '(4))
          (org-agenda-start-with-follow-mode nil)
          (org-agenda-dim-blocked-tasks 'invisible))
         ("~/.org/agenda.html" )
         )

        ("w" "Weekly agenda"
         ((agenda "week" ((org-agenda-ndays 7)))))


        ("fm" "Other" tags-todo "-ToRead-ToWrite-ToCheckOut-ProjectIdea-TechFix/OOIT"
         ((org-use-tag-inheritance nil)
          (org-agenda-todo-ignore-scheduled nil)
          (org-agenda-todo-ignore-deadlines nil)
          (org-agenda-todo-ignore-with-date nil)
          (org-agenda-overriding-header "Miscellaneous SOMEDAY")))
        ))
