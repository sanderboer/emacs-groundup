(message "Loading modules/10-windows.el")

(use-package ace-window
  :straight t
  :custom
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :config
  (setq aw-scope 'frame)
  (custom-set-faces
   '(aw-leading-char-face
     ((t (:foreground "red" :background nil :weight bold :height 3.0 :box nil))))))

(require 'ace-window)
(defun +/FS/visit-file-from-dired-using-ace-window ()
  "Use ace window to select a window for opening a file from dired."
  (interactive)

  (let ((file (dired-get-file-for-visit)))
    (if (> (length (aw-window-list)) 1)
	      (aw-select "" (lambda (window)
			                  (aw-switch-to-window window)
			                  (find-file file)))
      (find-file-other-window file)))
  )
