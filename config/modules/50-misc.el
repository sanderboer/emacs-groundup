(message "Loading modules/50-misc.el")

(use-package gptel
  :straight t
  :config
  (setq gptel-api-key
        (string-trim (shell-command-to-string "pass openai/apikey")))
  )

(use-package openai
  :straight (openai :type git :host github :repo "emacs-openai/openai")
  :config
  (setq openai-key
        (string-trim (shell-command-to-string "pass openai/apikey")))
  )


(use-package dall-e
  :straight (dall-e :type git :host github :repo "emacs-openai/dall-e")
  :config
  (setq

   dall-e-n 3 ;; - The number of images to generate. Must be between 1 and 10. (Default: 5)
   dall-e-size "512x512" ;;- The size of the generated images. (Default: "256x256")
   dall-e-spinner-type 'moon;; - The type of the spinner. (Default: 'moon)
   dall-e-cache-dir '(expand-file-name "~/.org/org-ai-images") ;; - Absolute path to download image files.
   dall-e-display-width  200
   )
  )

;; (use-package chatgpt
;;   :straight (chatgpt :type git :host github :repo "emacs-openai/chatgpt")
;;  :config
;;  )

(use-package org-ai
  :after org
  :straight (org-ai :type git :host github :repo "rksm/org-ai"
                    :local-repo "org-ai"
                    :files ("*.el" "README.md" "snippets"))
  :config
  (setq org-ai-openai-api-token
        (string-trim (shell-command-to-string "pass openai/apikey")))
  )
