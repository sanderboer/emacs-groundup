(message "Loading modules/10-spelling.el")

(use-package flycheck
  :straight t
  :init
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (global-flycheck-mode))

(use-package flyspell
  :straight nil
  :after ispell

  :init
  ;; Set the locale for the dictionary
  (setenv "LANG" +/lang/LANG)

  ;; Dictionary path
  (setenv "DICPATH" +/lang/DICPATH)

  :config
  ;; Use hunspell
  (setq ispell-program-name "hunspell")

  ;; ispell-set-spellchecker-params has to be called before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)

  ;; Declare a multi-language dictionary and switch to it
  (ispell-hunspell-add-multi-dic +/lang/MULTIDIC)
  (ispell-change-dictionary +/lang/MULTIDIC t))

(defun +/writing/spellcheck-and-correct-region (beg end)
  "Spellcheck and correct the active region."
  (interactive "r")

  (when (use-region-p)
    (flyspell-region beg end)
    (flyspell-correct-wrapper)))

(defun +/writing/spellcheck-region (beg end)
  "Spellcheck and correct the active region."
  (interactive "r")

  (let ((saved-point (point)))
    (when (use-region-p)
      (flyspell-region beg end)
      (goto-char saved-point))))

(defun +/writing/spellcheck-and-correct-buffer ()
  "Spellcheck and then correct the entire buffer."
  (interactive)

  (flyspell-buffer)
  (flyspell-correct-wrapper))

(use-package powerthesaurus
  :straight t)

(use-package flyspell-correct
  :straight t
  :after flyspell)

