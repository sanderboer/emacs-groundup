(message "Loading modules/10-project.el")

(use-package projectile
  :demand t
  :straight t
  :after (dired magit)
  :init
  (projectile-mode +1)
  :config
  (setq projectile-require-project-root t
        ;; projectile-switch-project-action #'projectile-dired
        projectile-switch-project-action #'projectile-find-file
        projectile-sort-order 'recentf))


(defun +/projectile/run-vterm-other-window ()
  "Open vterm in other window."
  (interactive)

  (save-window-excursion (projectile-run-vterm))
  (switch-to-buffer-other-window (concat "*vterm "
                                         (projectile-project-name)
                                         "*")))
(defun +/projectile/vc-here ()
  "Open magit in this window."
  (interactive)

  (save-window-excursion (projectile-vc))
  (switch-to-buffer (concat "magit: " (projectile-project-name))))

(use-package perspective
  :demand t
  :straight t
  :after (consult projectile)
  :config
  (setq persp-modestring-short t
        persp-sort 'access
        persp-suppress-no-prefix-key-warning t
        persp-state-default-file (+/FS/create-path user-emacs-directory
                                                   "persp-state"))

  (persp-mode))

(use-package persp-projectile
  :demand t
  :straight t
  :after (perspective projectile))

(use-package consult-projectile
  :demand t
  :straight t
  :after (consult projectile))

(defun +/consult-projectile/enhanced-find-file ()
  "Show a list of recently visited files in a project using `consult'."
  (interactive)

  (let ((consult-projectile-sources
         '(consult-projectile--source-projectile-recentf
           consult-projectile--source-projectile-file)))
    (call-interactively #'consult-projectile)))

(use-package magit
  :demand t
  :straight t)

(use-package git-modes
  :straight t)
