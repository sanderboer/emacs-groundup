(use-package dired-x
  :straight nil
  :hook
  ((dired-mode . (lambda () (interactive)
                   (all-the-icons-dired-mode)
                   (dired-omit-mode))))
  :config
  (setq dired-dwim-target t
        find-file-visit-truename nil
        dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")

  ;; macos hack
  (when (string= system-type "darwin")
    (setq dired-use-ls-dired t
          insert-directory-program "/opt/homebrew/bin/gls"
          dired-listing-switches "-aBhl --group-directories-first"))

  (add-hook 'dired-mode-hook #'+/UIX/disable-visual-line-wrapping-hook))

(use-package all-the-icons-dired
  :straight t
  :config
  (setq all-the-icons-dired-monochrome nil))

(use-package dired-du
  :straight t
  :after dired
  :config
  (setq dired-du-size-format t))

(use-package dired-quick-sort
  :straight t
  :after dired
  :init
  (add-hook 'dired-mode-hook 'dired-quick-sort))

(defun +/FS/prompt-and-kill-current-dired-buffer ()
  "Prompt and kill dired buffer."
  (interactive)

  (if (y-or-n-p "kill this dired buffer?")
      (kill-current-buffer)))
