(use-package neotree
  :straight t
  :config
  (setq neo-theme (if (display-graphic-p) 'nerd 'ascii))
  (setq neo-window-width 35)
  (setq neo-window-fixed-size nil)
  ;; (setq neo-autorefresh 1)
  (setq neo-autorefresh nil)
  (setq neo-auto-indent-point 1)

  )

;; Disable line-numbers minor mode for neotree
(add-hook 'neo-after-create-hook (lambda (&optional dummy)
                                   (display-line-numbers-mode -1)
                                   (visual-line-mode nil)
                                   (setq truncate-lines 1)
                                   ))
