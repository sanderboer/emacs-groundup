(message "Loading modules/dev/10-python.el")


;; (use-package lsp-pyright
;;   :straight t
;;   :after lsp
;;   ;; :hook
;;   ;; (python-mode . (lambda () (require 'lsp-pyright)
;;   ;;                  (lsp-deferred)
;;   ;;                  ))
;;   :config
;;   (setq lsp-pyright-auto-import-completions t
;;         lsp-pyright-use-library-code-for-types t
;;         lsp-pyright-auto-search-paths t
;;         lsp-pyright-typechecking-mode "basic" ;; {"off","basic","strict"}
;; 	      ;; lsp-pyright-venv-path "~/.local/share/virtualenvs/"
;;         )
;;   )

(use-package lsp-pyright
  :straight t
  :after lsp
  :config
  (setq lsp-pyright-auto-import-completions t
        lsp-pyright-use-library-code-for-types t
        lsp-pyright-auto-search-paths t
        lsp-pyright-typechecking-mode "basic")
  (add-hook 'python-ts-mode-hook
            (lambda ()
              (let ((venv-path (or (locate-dominating-file default-directory ".venv")
                                   (poetry-venv-path)))) ;; Fallback to Poetry's default venv path
                (when venv-path
                  (setq-local lsp-pyright-venv-path (expand-file-name ".venv" venv-path)))))))


;;(add-hook 'python-mode-hook (lambda () (message "Python mode hook activated")))
(add-hook 'python-ts-mode-hook (lambda () (message "Python treesitter mode hook activated")))

(add-to-list 'auto-mode-alist '("\\.py\\'" . python-ts-mode))

(use-package py-isort
  :straight t
  :after python)

(use-package poetry
  :straight t
  ;; :hook (python-ts-mode . poetry-tracking-mode)
  )

(use-package python-black
  :straight t
  :demand t
  :after python
  :hook (python-ts-mode . python-black-on-save-mode))
