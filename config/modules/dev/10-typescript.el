(message "Loading modules/dev/10-typescript.el")


(use-package prettier-js :straight t :demand t)

(defun +/DEV/typescript-mode-hook()
  (interactive)
  (lsp-deferred)
  (prettier-js-mode)
  )

(add-hook 'typescript-ts-mode-hook ' +/DEV/typescript-mode-hook)
(add-hook 'typescript-mode- ' +/DEV/typescript-mode-hook)

