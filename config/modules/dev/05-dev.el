(message "Loading modules/dev/05-dev.el")
(add-to-list 'auto-mode-alist '("\\.py\\'"      . python-ts-mode))
(add-to-list 'auto-mode-alist '("\\.texinfo\\'" . texinfo-mode))
(add-to-list 'auto-mode-alist '("\\.texi\\'"    . texinfo-mode))
(add-to-list 'auto-mode-alist '("\\.el\\'"      . emacs-lisp-mode))
(add-to-list 'auto-mode-alist '("\\.c\\'"       . c-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'"       . c-mode))
(add-to-list 'auto-mode-alist '("\\.sh\\'"      . shell-script-mode))
(add-to-list 'auto-mode-alist '("\\.jpe?g\\'"   . image-mode))
(add-to-list 'auto-mode-alist '("\\.png\\'"     . image-mode))
(add-to-list 'auto-mode-alist '("\\.gif\\'"     . image-mode))
(add-to-list 'auto-mode-alist '("\\.svg\\'"     . image-mode))
(add-to-list 'auto-mode-alist '("\\.pdf\\'"     . pdf-view-mode))
;; (add-to-list 'auto-mode-alist '("\\.html\\'"    . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'"      . js-mode))
;; (add-to-list 'auto-mode-alist '("\\.ts\\'"      . typescript-ts-mode))
;; (add-to-list 'auto-mode-alist '("\\.tsx\\'"     . typescript-ts-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'"     . vue-mode))
;; (add-to-list 'auto-mode-alist '("\\.json\\'"    . json-mode))
(add-to-list 'auto-mode-alist '("\\.yaml\\'"    . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.yml\\'"     . yaml-mode))
(add-to-list 'auto-mode-alist '("\\.toml\\'"    . toml-mode))
(add-to-list 'auto-mode-alist '("\\.md\\'"      . markdown-mode))
(add-to-list 'auto-mode-alist '("\\.org\\'"     . org-mode))
(add-to-list 'auto-mode-alist '("\\.cpp\\'"     . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hpp\\'"     . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hxx\\'"     . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cxx\\'"     . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cc\\'"      . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hh\\'"      . c++-mode))

(add-hook 'prog-mode-hook (lambda ()
                            (display-line-numbers-mode t)
                            ;; (apheleia-mode +1)
                            ))

(add-hook 'prog-mode-hook #'hs-minor-mode)

(use-package lsp-mode
  :straight t
  :hook
  (
   ;; (lsp-mode    . lsp-enable-which-key-integration) ;; redundant ?
   (python-ts-mode . lsp-deferred)
   ;; (python-mode . lsp-deferred)
   (typescript-mode-hook . lsp-deferred)
   (typescript-ts-mode-hook . lsp-deferred)
   ;; (vue-mode-hook . lsp)
   ;; (vue-mode-hook . lsp-vue-mmm-enable)
   ;; (web-mode    . lsp-deferred)
   )

  :config
  (setq
   ;; UI
   lsp-ui-doc-show-with-mouse nil
   lsp-ui-sideline-show-diagnostics nil
   lsp-headerline-breadcrumb-enable nil
   lsp-modeline-code-actions-enable nil

   ;; Enforce python3
   ;; Ref: https://github.com/emacs-lsp/lsp-pyright/issues/42#issuecomment-812910334
   lsp-pyright-python-executable-cmd "python3")

  :commands
  (lsp lsp-deferred))

(use-package lsp-ui
  :straight t
  )

(use-package lsp-treemacs
  :straight t
  :after lsp
  )

(use-package consult-lsp
  :straight t
  :after (consult lsp-mode))

(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
	      (cmake "https://github.com/uyha/tree-sitter-cmake")
	      (css "https://github.com/tree-sitter/tree-sitter-css")
	      (elisp "https://github.com/Wilfred/tree-sitter-elisp")
	      (go "https://github.com/tree-sitter/tree-sitter-go")
	      (html "https://github.com/tree-sitter/tree-sitter-html")
	      (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
	      (json "https://github.com/tree-sitter/tree-sitter-json")
	      (make "https://github.com/alemuller/tree-sitter-make")
	      (markdown "https://github.com/ikatyang/tree-sitter-markdown")
	      (python "https://github.com/tree-sitter/tree-sitter-python")
	      (toml "https://github.com/tree-sitter/tree-sitter-toml")
	      (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
	      (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
	      (yaml "https://github.com/ikatyang/tree-sitter-yaml")))


(use-package treesit-auto
  :straight t
  :demand t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package apheleia
  :straight (apheleia :host github :repo "radian-software/apheleia")
  :config
  (apheleia-global-mode t)
  )

(use-package editorconfig
  :straight t
  :demand t
  :config
  (editorconfig-mode 1)
  ;; (define-key global-map (kbd "<M-s M-f>") #'sndr-editorconfig-reformat)
  )
