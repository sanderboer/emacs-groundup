(message "Loading modules/dev/10-vue.el")

(use-package vue-mode
  :straight t
  :after tree-sitter
  :mode "\\.vue\\'"
  :hook
  (vue-mode . lsp-deferred)
  (vue-mode . web-mode)
  (vue-mode . prettier-js-mode)
  :config
  (setq mmm-submode-decoration-level 0)
  (setq-default vue-html-extra-indent 0)
  (setq-default vue-indent-level 0)
  (setq css-indent-offset 2)
  (setq prettier-js-args '("--parser vue")
        )

  (defconst vue--front-tag-lang-regex
    (concat "<%s"                               ; The tag name
            "\\(?:"                             ; Zero of more of...
            "\\(?:\\s-+\\w+=[\"'].*?[\"']\\)"   ; Any optional key-value pairs like type="foo/bar"
            "\\|\\(?:\\s-+scoped\\)"            ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"            ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"             ; The optional "setup" attribute
            "\\)*"
            "\\(?:\\s-+lang=[\"']%s[\"']\\)"    ; The language specifier (required)
            "\\(?:"                             ; Zero of more of...
            "\\(?:\\s-+\\w+=[\"'].*?[\"']\\)"   ; Any optional key-value pairs like type="foo/bar"
            "\\|\\(?:\\s-+scoped\\)"            ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"            ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"             ; The optional "setup" attribute
            "\\)*"
            " *>\n")                            ; The end of the tag
    "A regular expression for the starting tags of template areas with languages.
To be formatted with the tag name, and the language.")

  (defconst vue--front-tag-regex
    (concat "<%s"                        ; The tag name
            "\\(?:"                      ; Zero of more of...
            "\\(?:\\s-+" vue--not-lang-key "[\"'][^\"']*?[\"']\\)" ; Any optional key-value pairs like type="foo/bar".
            ;; ^ Disallow "lang" in k/v pairs to avoid matching regions with non-default languages
            "\\|\\(?:\\s-+scoped\\)"      ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"      ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"      ; The optional "module" attribute
            "\\)*"
            "\\s-*>\n")                     ; The end of the tag
    "A regular expression for the starting tags of template areas.
To be formatted with the tag name.")

  )

(use-package vue-html-mode :straight t)

