(message "Loading modules/dev/10-webmode.el")

(use-package web-mode
  :straight t
  :mode (
         ("\\.html\\'" . web-mode)
         ("\\.htm\\'" . web-mode)
         ;; ("\\.htm[l]?" . web-mode)
         ("\\.css"     . web-mode)
         ;; ("\\.njk"  . web-mode)
         ;; ("\\.nunjucks"  . web-mode)
         ;; ("\\.js[x]?"  . web-mode)
         ;; ("\\.ts[x]?"  . web-mode)
         ("\\.astro"   . web-mode)
         )
  :hook
  ;; (web-mode . (lambda () (when (match-buffer-extension "vue")
  ;;                                (setq-local lsp-auto-format t))))
  (web-mode . lsp-deferred)
  :custom
  (web-mode-script-padding 0) ; For vue.js SFC : no initial padding in the script section
  (web-mode-markup-indent-offset 2)) ; For html : use an indent of size 2 (default is 4)
