(message "Loading modules/dev/10-markdown.el")

(use-package markdown-mode
  :straight t
  :config
  (setq markdown-display-inline-images t
        markdown-enable-math t
        markdown-max-image-size '(500 . 500)))
