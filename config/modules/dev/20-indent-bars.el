;;; 20-indent-bars.el ---
(message "Loading modules/dev/10-indent-bars.el")

(use-package indent-bars
  :straight t
  :hook ((yaml-mode python-ts-mode typescript-ts-mode) . indent-bars-mode)
  :config  
  
  ;; Platform-specific settings
  (cond
   ((eq system-type 'darwin) ;; macOS
    (setq indent-bars-color '(highlight :face-bg t :blend 0.75)
          indent-bars-prefer-character t
          indent-bars-unspecified-fg-color "white"
          indent-bars-unspecified-bg-color "black"
          indent-bars-color-by-depth '(:regexp "outline-\\([0-9]+\\)" :blend 1))
   (t ;; Other platforms
    ;(setq indent-bars-color '(highlight :face-bg t :blend 0.5)
    ;      indent-bars-color-by-depth '(:regexp "outline-\\([0-9]+\\)" :blend 0.8)
    ;       )
    )
   )))
  
  ;; Add reference for terminal-specific configurations
  ;; https://github.com/jdtsmith/indent-bars/blob/main/examples.md#in-terminal
