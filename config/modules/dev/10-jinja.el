
(message "Loading modules/dev/10-jinja.el")
(use-package jinja2-mode
  :straight t
  :mode(
        ("\\.njk"  .      jinja2-mode)
        ("\\.nunjucks"  . jinja2-mode)
        ("\\.jinja\\'"  .    jinja2-mode)
        )
  :config
  (setq jinja2-basic-offset 2)
  )
