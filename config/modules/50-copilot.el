(message "Loading modules/50-copilot.el")

(use-package copilot
  :straight (
	           :host github
	           :repo "zerolfx/copilot.el"
	           :files ("dist" "*.el"))
  :config (setq copilot-max-char 200000)
  (define-key copilot-completion-map (kbd "C-<tab>") 'copilot-accept-completion)
  :ensure t)

(add-hook 'prog-mode-hook 'copilot-mode)

