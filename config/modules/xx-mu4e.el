(use-package w3m :straight t )

;; (straight-use-package
;;  '(mu4e :files (:defaults "mu4e/*.el")))

;; install from package:
;; wget https://github.com/djcb/mu/releases/download/v1.8.13/mu-1.8.13.tar.xz
;; xbi meson gmime-devel xapian-core xapian-core-devel
;;   tar xfvJ mu-1.8.13.tar.xz
;; cd mu
;; ./autogen.sh
;; make && sudo make install
;; mu init --maildir ~/Maildir
;; mu index

;; (add-to-list 'load-path "/usr/local/share/emacs/site-lisp/mu4e")
;; (require 'mu4e) ;; mu4e is a system package
;; (require 'mu4e-contrib)

(use-package mu4e
  :straight ( :host github 
              :repo "djcb/mu"  
              :branch "master"
              :files ("build/mu4e/*")   
              :pre-build (("sh" "./autogen.sh") ("make"))) 
  :custom   (mu4e-mu-binary (expand-file-name "build/mu/mu" (straight--repos-dir "mu"))) 

  :config

  (setq mu4e-maildir (expand-file-name "~/Mail"))
  ;; necessary for mbsync
  (setq mu4e-change-filenames-when-moving t)
  ;;set up queue for offline email
  ;; mu mkdir ~/Maildir/queue
  ;; touch ~/Maildir/queue/.noindex

  ;; Integrate into emacs
  (setq mail-user-agent 'mu4e-user-agent)
  (set-variable 'read-mail-command 'mu4e)

  

  (set-face-attribute 'mu4e-header-highlight-face nil :underline nil)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;; hooks
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
  (add-hook 'mu4e-view-mode-hook '+/MU4E/setup-mu4e)
  (add-hook 'mu4e-compose-mode-hook 'my-do-compose-stuff)
  (add-to-list 'mu4e-view-actions
               '("View in browser" . mu4e-msgv-action-view-in-browser) t)
  (define-key mu4e-view-mode-map (kbd "M-e") 'sander-mu4e-save-all-attachments)
  (define-key message-mode-map (kbd "M-s") '+/MU4E/snip)
  (add-hook 'mu4e-headers-mode-hook '+/MU4E/mu4e-header-format)

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;; format
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  (setq mu4e-maildir-shortcuts
                   '(("/Mauc365/INBOX"            . ?i)
                     ("/Mauc365/Sent Items"             . ?s)
                     ("/Mauc365/Archive.2024"     . ?a)
                     ("/Mauc365/Mauc"             . ?m)
                     ("/Mauc365/Spam"             . ?S)
                     ("/Starling/INBOX"        . ?I)
                     ("/Starling/Sent Items"   . ?S)
                     ("/Starling/Archive.2024" . ?A)
                     ("/Starling/Starling"     . ?T)
                     ("/Starling/Spam"         . ?P)
                     ))

  (setq mu4e-contexts
        (list
         ;; Mauc account
         (make-mu4e-context
          :name "MAUC"
          :enter-func (lambda () (progn(mu4e-message "Entering MAUC context")
                                       (setq mu4e-sent-messages-behavior 'sent)
                                       ))
          :leave-func (lambda () (mu4e-message "Leaving MAUC context"))
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Mauc365" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address   . "sanderboer@mauc.nl")
                  (user-full-name      . "Sander Boer")
                  (mu4e-drafts-folder  . "/Mauc365/Drafts")
                  (mu4e-sent-folder    . "/Mauc365/Sent Items")
                  (mu4e-refile-folder  . "/Mauc365/Archive.2024")
                  (mu4e-trash-folder   . "/Mauc365/Trash")
                  (smtpmail-queue-mail . nil)  ;; start in normal mode
                  (smtpmail-queue-dir  .  "/Mail/Mauc365/queue/cur")
                  (mu4e-compose-signature .
                   "Sander Boer
    MAUC | www.mauc.nl
    T: +31(0) 6 4808 2965")
                  ))
         
         ;; Starling account
         (make-mu4e-context
          :name "Starling"
          :enter-func (lambda ()(progn (mu4e-message "Entering Starling context")
                                       (setq mu4e-sent-messages-behavior 'delete) ))
          :leave-func (lambda () (mu4e-message "Leaving Starling context"))
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Starling" (mu4e-message-field msg :maildir))))
          :vars '((user-mail-address   . "sander@starling.studio")
                  (user-full-name      . "Sander Boer")
                  (mu4e-drafts-folder  . "/Starling/Drafts")
                  (mu4e-sent-folder    . "/Starling/Sent Items")
                  (mu4e-refile-folder  . "/Starling/Archive.2024")
                  (smtpmail-queue-mail . nil)  ;; start in normal mode
                  (smtpmail-queue-dir  .  "/Mail/Starling/queue/cur")
                  (mu4e-trash-folder   . "/Starling/Trash")
                  (mu4e-compose-signature .
                   "+STARLING
  SANDER BOER
  CTO & Architect
 
  www.starling.associates
  sander@starling.studio
  +31 (0)6 - 4808 2965
 
  Schiekade 830
  3032 AL, Rotterdam
  The Netherlands")
                  )
          )
         )
        )

  
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;; smtp


  (setq message-send-mail-function 'message-send-mail-with-sendmail)
  (setq sendmail-program  "/usr/bin/msmtp")                 
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun timu/set-msmtp-account ()
  (if (message-mail-p)
      (save-excursion
        (let*
            ((from (save-restriction
                     (message-narrow-to-headers)
                     (message-fetch-field "from")))
             (account
              (cond
               ((string-match "sander@starling.studio" from) "Starling")
               ((string-match "sanderboer@mauc.nl" from) "Mauc365"))))
          (setq message-sendmail-extra-arguments (list '"-a" account))))))

(add-hook 'message-send-mail-hook 'timu/set-msmtp-account)

  
  ;
;; set the msmtp path based on system
  ;; (when (eq system-type 'cygwin)
  ;;   (setq message-send-mail-function 'message-send-mail-with-sendmail)
  ;;   (setq sendmail-program "/usr/sbin/msmtp.exe")
  ;;   )
  ;; (when (eq system-type 'gnu/linux)
  ;;   (setq message-send-mail-function 'message-send-mail-with-sendmail)
  ;;   (setq sendmail-program "/usr/bin/msmtp")
  ;;   )

  (setq mu4e-attachment-dir "~/Downloads")
  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)

    ;; Refresh mail using isync every 10 minutes
  (setq mu4e-update-interval (* 2 60))
  (setq mu4e-get-mail-command "mbsync -a")
  
  ;; don't keep message buffers around
  (setq message-kill-buffer-on-exit t)

  
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;; format
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; show images
  (setq mu4e-show-images t)
  ;; use imagemagick, if available
  (when (fboundp 'imagemagick-register-types)
    (imagemagick-register-types))
  ;; render html in buffer with w3m
  (setq mu4e-html2text-command "w3m -cols 80 -T text/html -dump -F -O UTF-8")
  (setq mu4e-headers-date-format "%d.%m.%y %H:%M")
  (setq mu4e-headers-fields
        '( (:date . 15)
           (:flags . 6)
           (:from . 22)
           (:thread-subject . nil)))
  )


;; I want my format=flowed thank you very much
;; mu4e sets up visual-line-mode and also fill (M-q) to do the right thing
;; each paragraph is a single long line; at sending, emacs will add the
;; special line continuation characters.
(setq mu4e-compose-format-flowed t)

(use-package org-mime
  :after org
  :straight t
  :config
;; for gnus – this is set by default
(setq org-mime-library 'mml)
;; OR for Wanderlust (WL)
;; (setq org-mime-library 'semi)
)

   (defun +/MU4E/setup-mu4e ()
     (interactive)
     (setq mode-line-format nil)
     (evil-mode 1)
     (local-set-key (kbd "<tab>") 'shr-next-link)
     (local-set-key (kbd "<backtab>") 'shr-previous-link)
     (setq shr-color-visible-luminance-min 80)
     (setq mu4e-html2text-command "html2text -utf8 -width 72")
     (setq mu4e-headers-mode-hook nil)
     )

   (defun my-render-html-message ()
     (let ((dom (libxml-parse-html-region (point-min) (point-max))))
       (erase-buffer)
       (shr-insert-document dom)
       (goto-char (point-min))))

   (defun +/MU4E/mu4e-header-format()
     (interactive)
     (visual-line-mode -1)
     (setq truncate-lines t)
     (setq mode-line-format nil)
     )

   ;; cut and snip in message mode
   (defun +/MU4E/snip (b e summ)
     "remove selected lines, and replace it with [snip:summary (n lines)]"
     (interactive "r\nsSummary: ")
     (let ((n (count-lines b e)))
       (delete-region b e)
       (insert (format "\n[snip %s (%d line%s)]\n"
                       (if (= 0 (length summ)) "" (concat ": " summ))
                       n
                       (if (= 1 n) "" "s")))))

   ;; compose with spell check
   (defun my-do-compose-stuff ()
     "My settings for message composition."
     (yas-global-mode 1)
     (set-fill-column 72)
     (setq mode-line-format nil)
     (flyspell-mode))


   ;;message view action
   (defun mu4e-msgv-action-view-in-browser (msg)
     "View the body of the message in a web browser."
     (interactive)
     (let ((html (mu4e-msg-field (mu4e-message-at-point t) :body-html))
           (tmpfile (format "%s/%d.html" temporary-file-directory (random)))
           (from (car (car (mu4e-message-field msg :from))))
           (subject (mu4e-message-field msg :subject))
           )
       (unless html (error "No html part for this message"))
       (with-temp-file tmpfile
         (insert
          "<html>"
          "<head><meta http-equiv=\"content-type\""
          "content=\"text/html;charset=UTF-8\">"
          "<div style='position: float; top: 20px; top: 20px; margin: 20px; padding:20px; font-family: Arial,Verdana; color: #fff; background: #555 '> <p>from: "
          from
          "</p><p>Subject: "
          subject
          "</p></div>"
          html))
       (browse-url (concat "file://" tmpfile))))


   (defun sander-mu4e-cleanse-subject (sub)
     (replace-regexp-in-string
      "[^A-Z0-9]+"
      "-"
      (downcase sub)))

   (defun sander-mu4e-cleanse-sender (sub)
     (replace-regexp-in-string
      "[@]+"
      "-at-"
      (downcase sub)))

   (defun sander-mu4e-save-all-attachments-old-view(&optional msg)
     (interactive)
     (let* ((msg (or msg (mu4e-message-at-point)))
            (sander-date-format "%y%m%d")
            (id (sander-mu4e-cleanse-subject (mu4e-message-field msg :subject)))
            (from (sander-mu4e-cleanse-sender(cdr(car(mu4e-message-field msg :from)))))
            ;; (date (decode-time(mu4e-message-field msg :date)))
            (date ( format-time-string sander-date-format (mu4e-message-field msg :date)))
            (count (hash-table-count mu4e~view-attach-map)))
       (setq subdir (format
                     "%s-%s_%s_%s" date from id count
                     ))
       (setq attdir(expand-file-name (concat "~/Incoming/" subdir)) )
       (message attdir)
       (if (> count 0)
           (progn
             (mkdir attdir t)
             (dolist (num (number-sequence 1 count))
               (let* ((att (mu4e~view-get-attach msg num))
                      (fname (plist-get att :name))
                      (index (plist-get att :index))
                      fpath)
                 (setq fpath (concat attdir "/" fname))
                 (mu4e~proc-extract
                  'save (mu4e-message-field msg :docid)
                  index mu4e-decryption-policy fpath)))
             (dired attdir)
             (revert-buffer))
         (message "Nothing to extract.")
         ))
     )

   (defun sander-mu4e-save-all-attachments (&optional arg)
     "Save all MIME parts from current mu4e gnus view buffer."
     ;; Copied from mu4e-view-save-attachments
     (interactive "P")
     (cl-assert (and (eq major-mode 'mu4e-view-mode)
                     (derived-mode-p 'gnus-article-mode)))
     (let* ((msg (mu4e-message-at-point))
            (sander-date-format "%y%m%d")
            (id (sander-mu4e-cleanse-subject (mu4e-message-field msg :subject)))
            (from (sander-mu4e-cleanse-sender(plist-get (car(mu4e-message-field msg :from)) :email)))
            (date ( format-time-string sander-date-format (mu4e-message-field msg :date)))
            ;;(attachdir (concat bulk-saved-attachments-dir "/" id))
            (parts (mu4e--view-gather-mime-parts))
            (handles '())
            (files '())
            dir)
       (setq subdir (format
                     "%s-%s_%s_%s" date from id (length '(parts))
                     ))
       (setq attachdir(expand-file-name (concat "~/Incoming/" subdir)) )
       (mkdir attachdir t)
       (dolist (part parts)
         (let ((fname (or
                       (cdr (assoc 'filename (assoc "attachment" (cdr part))))
                       (seq-find #'stringp
                                 (mapcar (lambda (item) (cdr (assoc 'name item)))
                                         (seq-filter 'listp (cdr part)))))))
           (when fname
             (push `(,fname . ,(cdr part)) handles)
             (push fname files))))
       (if files
           (progn
             (setq dir
                   (if arg (read-directory-name "Save to directory: ")
                     attachdir))
             (cl-loop for (f . h) in handles
                      when (member f files)
                      do (mm-save-part-to-file h (expand-file-name f dir)))
             (dired attachdir)
             )
         (mu4e-message "No attached files found"))))



   (defun +/MU4E/start-mail()
     (interactive)
     (mu4e)
     (split-window-horizontally)
     (other-window 1)
     (find-file "~/.org/verzameling.org")
     ;; and load the most recent file
     (other-window 1)
     )


