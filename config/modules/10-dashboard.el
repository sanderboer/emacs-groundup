(message "Loading modules/10-dashboard.el")

(use-package dashboard
  :straight t
  :demand t
  :after projectile
  :config
  (setq
   ;; Sections
   dashboard-items '((projects . 7)
                     (recents  . 7))
   ;; Items/Widgets
   dashboard-item-names '(("Projects:"     . "[p] Recent projects:")
                          ("Recent Files:" . "[r] Recent files:"))
   ;; Banner
   dashboard-startup-banner (+/FS/create-path
                             +/config/dir-assets
                             "mauc-cube-m-color.png")
   dashboard-image-banner-max-width 120
   dashboard-banner-logo-title "Welcome to Emacs, groundup!"
   ;; Hide/Show shortcut key
   dashboard-show-shortcuts nil
   ;; Centre content
   dashboard-center-content t
   ;; Icons
   dashboard-set-heading-icons t
   dashboard-set-file-icons t
   ;; No footer
   dashboard-set-footer nil
   ;; Project switching with projectile/persp
   dashboard-projects-switch-function 'projectile-persp-switch-project
   ;; Shorten path
   dashboard-path-style 'truncate-middle
   dashboard-path-max-length 60
   ;; dashboard-shorten-by-window-width t
   ;; Load dashboard for each new frame
   ;; initial-buffer-choice (lambda () (get-buffer "*dashboard*"))

   )
  ;; No visual wrapping
  (add-hook 'dashboard-mode-hook
            #'+/UIX/disable-visual-line-wrapping-hook)
  ;; Set everything up
  (dashboard-setup-startup-hook))


