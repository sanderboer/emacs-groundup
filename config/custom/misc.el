
(use-package vc-backup
  :straight t)

(use-package sudo-edit
  :straight t)

(use-package direnv
  :straight t
  :config
  (direnv-mode))

(use-package vterm
  :straight t
  :after evil-collection
  :init (setq vterm-always-compile-module t))

(use-package undo-fu
  :straight t)

(use-package tramp
  :straight t
  :disabled t
  ;; :straight (:build t :pre-build (("make" "autoloads")))
  :config
  (setq tramp-default-method "ssh"))

(use-package pass
  :straight t)

(use-package go
  :straight t
  :config
  (require 'go)
  )

(use-package saveplace-pdf-view
  :straight t
  :after pdf-view)

(use-package asdf
  :straight (:type git :host github :repo "tabfugnic/asdf.el")
  :config
  (require 'asdf))
