(require 'color)
(require 'hsluv)
(require 'cl-lib)

(defun lch-to-hex-oklab (l c h)
  "Convert LCH (Lightness, Chroma, Hue) to a 6-character hex string (#rrggbb)."
  (let* ( (hue-radians (* h (/ pi 180))) ;; Convert hue from degrees to radians
          (lab (color-lch-to-lab l c hue-radians)) ;; Convert LCH to Lab
          (l (/ (nth 0 lab) 100))
          (a (* (nth 1 lab) .0032))
          (b (* (nth 2 lab) .0032))
    (rgb (apply #'color-oklab-to-srgb (list l a b) )))
    
         
    ;; Clamp RGB values between 0 and 1, then scale to 0-255 and format as hex
    (format "#%02x%02x%02x"
            (round (* 255 (max 0.0 (min 1.0 (nth 0 rgb))))) ;; Red
            (round (* 255 (max 0.0 (min 1.0 (nth 1 rgb))))) ;; Green
            (round (* 255 (max 0.0 (min 1.0 (nth 2 rgb)))))))) ;; Blue


;; (defun lch-to-hex (l c h)
;;   "Convert LCH (Lightness, Chroma, Hue) to a 6-character hex string (#rrggbb)."
;;   (let* ( (hue-radians (* h (/ pi 180))) ;; Convert hue from degrees to radians
;;          (lab (color-lch-to-lab l c hue-radians)) ;; Convert LCH to Lab
;;          (rgb (apply #'color-lab-to-srgb lab))) ;; Convert Lab to sRGB
;;     ;; Clamp RGB values between 0 and 1, then scale to 0-255 and format as hex
;;     (format "#%02x%02x%02x"
;;             (round (* 255 (max 0.0 (min 1.0 (nth 0 rgb))))) ;; Red
;;             (round (* 255 (max 0.0 (min 1.0 (nth 1 rgb))))) ;; Green
;;             (round (* 255 (max 0.0 (min 1.0 (nth 2 rgb)))))))) ;; Blue

(defun linear-interpolation (x y num_steps)
  "Return a list of values linearly descending from X to Y in NUM_STEPS."
  (let ((step (/ (- x y) (float (1- num_steps)))) ;; Calculate step size
        (result '()))
    (dotimes (i num_steps)
      (push (- x (* i step)) result)) ;; Compute value for each step
    (nreverse result))) ;; Reverse the list to maintain descending order

(defun quadratic-interpolation (x y num_steps)
  "Return a list of values interpolating between X and Y in a quadratic manner over NUM_STEPS."
  (let ((result '()))
    (dotimes (i num_steps)
      (let* ((normalized-step (/ (float i) (1- num_steps))) ;; Normalize i to [0, 1]
             (quadratic-factor (* normalized-step normalized-step)) ;; Quadratic factor: t^2
             (value (+ (* (- 1 quadratic-factor) x) (* quadratic-factor y)))) ;; Weighted sum
        (push value result)))
    (nreverse result))) ;; Reverse the list to maintain ascending order


(defun exp-interpolation (x y num_steps exp)
  "Return a list of values interpolating between X and Y in a quadratic manner over NUM_STEPS."
  (let ((result '()))
    (dotimes (i num_steps)
      (let* ((normalized-step (/ (float i) (1- num_steps))) ;; Normalize i to [0, 1]
             (factor (expt normalized-step exp)) ;; Cubic factor: t^3
             (value (+ (* (- 1 factor) x) (* factor y)))) ;; Weighted sum
        (push value result)))
    (nreverse result))) ;; Reverse the list to maintain ascending order

(defun parabolic-interpolation (x y num_steps)
  "Return a list of values interpolating between X and Y in a parabolic manner over NUM_STEPS."
  (let ((result '()))
    (dotimes (i num_steps)
      (let* ((dt (/ (float i) (1- num_steps))) ;; Normalize i to [0, 1]
             (parabolic-factor (* dt dt))       ;; Parabolic factor: t^2
             (value (+ (* (- 1 parabolic-factor) x) (* parabolic-factor y)))) ;; Weighted sum
        (push value result))) 
    (nreverse result))) ;; Reverse the list to maintain ascending order

(defun bell-curve (min_val max_val num_steps)
  "Return a list representing a bell curve that ascends from MIN_VAL to MAX_VAL,
and then descends back to MIN_VAL in NUM_STEPS.
Handles odd num_steps properly to ensure symmetry."
  (unless (and (numberp min_val) (numberp max_val) (numberp num_steps))
    (error "All arguments must be numbers"))
  (unless (>= num_steps 3)
    (error "num_steps must be at least 3"))
  (let* ((half-steps (floor (/ num_steps 2.0))) ;; Steps for ascent
         (ascent-steps half-steps)
         (descent-steps (- num_steps ascent-steps)) ;; Steps for descent
         (curve-factor (/ 1.0 (float (1- ascent-steps)))) ;; Normalized factor for quadratic progression
         (result '()))
    ;; Ascend from min_val to max_val (quadratic progression)
    (dotimes (i ascent-steps)
      (let ((progress (expt (* i curve-factor) 2))) ;; Quadratic progression
        (push (+ min_val (* progress (- max_val min_val))) result)))
    ;; Add the peak (max_val) explicitly for symmetry
    (push max_val result)
    ;; Descend from max_val to min_val (quadratic progression)
    (dotimes (i descent-steps)
      (let ((progress (expt (* i curve-factor) 2))) ;; Quadratic progression
        (push (- max_val (* progress (- max_val min_val))) result)))
    (nreverse result))) ;; Reverse to maintain order

(defun linear-ascent-descent (min_val max_val num_steps)
  "Return a list that ascends from MIN_VAL to MAX_VAL in floor(NUM_STEPS/2),
and then descends from MAX_VAL to MIN_VAL in ceil(NUM_STEPS/2)."
  (unless (and (numberp min_val) (numberp max_val) (numberp num_steps))
    (error "All arguments must be numbers"))
  (unless (>= num_steps 2)
    (error "num_steps must be at least 2"))
  (let* ((ascent-steps (floor (/ num_steps 2.0))) ;; Steps for ascent
         (descent-steps (- num_steps ascent-steps)) ;; Remaining steps for descent
         (step-up (/ (- max_val min_val) (float (1- ascent-steps)))) ;; Step size for ascent
         (step-down (/ (- max_val min_val) (float (1- descent-steps)))) ;; Step size for descent
         (result '()))
    ;; Ascend from min_val to max_val
    (dotimes (i ascent-steps)
      (push (+ min_val (* i step-up)) result))
    ;; Descend from max_val to min_val
    (dotimes (i descent-steps)
      (push (- max_val (* i step-down)) result))
    (nreverse result))) ;; Reverse to maintain order

(defun linear-ascent-descent-value (min-val max-val x)
  "Return the value at position X (normalized to [0, 1]) on a linear ascent-descent curve
that starts at MIN-VAL, peaks at MAX-VAL, and descends back to MIN-VAL."
  (unless (and (numberp min-val) (numberp max-val) (numberp x))
    (error "All arguments must be numbers"))
  (unless (and (>= x 0.0) (<= x 1.0))
    (error "X must be in the range [0, 1]"))
  (if (<= x 0.5)
      ;; Ascent phase
      (let ((normalized-x (/ x 0.5))) ;; Normalize x to [0, 1] for ascent
        (+ min-val (* normalized-x (- max-val min-val))))
    ;; Descent phase
    (let ((normalized-x (/ (- x 0.5) 0.5))) ;; Normalize x to [0, 1] for descent
      (- max-val (* normalized-x (- max-val min-val))))))

(defun fill-list-with-number (x n)
  "Return a list of length N, filled with the number X."
  (unless (and (numberp x) (integerp n) (>= n 0))
    (error "X must be a number and N must be a non-negative integer"))
  (make-list n x))

(defun calculate-luminance (hex-color)
  "Calculate the approximate perceptual luminance of a HEX-COLOR.
The luminance is calculated using the relative luminance formula."
  (let* ((rgb (color-name-to-rgb hex-color)) ;; Convert hex to RGB values
         (r (nth 0 rgb))
         (g (nth 1 rgb))
         (b (nth 2 rgb))
         ;; Adjust values for luminance calculation
         (r-lum (if (<= r 0.03928) (/ r 12.92) (expt (/ (+ r 0.055) 1.055) 2.4)))
         (g-lum (if (<= g 0.03928) (/ g 12.92) (expt (/ (+ g 0.055) 1.055) 2.4)))
         (b-lum (if (<= b 0.03928) (/ b 12.92) (expt (/ (+ b 0.055) 1.055) 2.4))))
    ;; Combine RGB values into luminance
    (+ (* 0.2126 r-lum) (* 0.7152 g-lum) (* 0.0722 b-lum))))

(defun generate-color-grid (buffer-name max-chroma steps)
  "Generate a grid of colors based on lightness (L), chroma (C), and hue (H).
Fill an Emacs buffer with the output."
  (let* (
        (L (linear-interpolation 90 26 steps)) ;; Lightness values
        ;; (C '(12 18 24 30 36 30 24 18 12)) ;; Chroma values
        (C (linear-ascent-descent 12 max-chroma steps)) ;; 
Chroma values
        ;; (C (bell-curve 12 75 steps)) ;; Chroma values
        (buffer (get-buffer-create buffer-name))) ;; Create or get buffer
    (with-current-buffer buffer
      (erase-buffer) ;; Clear the buffer
      (insert (format "Generated Color Grid\n"))
      (cl-loop for i from 0 to 360 by 15 do
               (insert (format "Hue %03d: " i))
               (cl-loop for j from 0 below (length L) do
                        (let* ((code (format "N%03d%d" i (1+ j) )) ;; Create code
                               (chroma (if (= i 0) 0 (nth j C)))
                               (hex (lch-to-hex (nth j L) chroma i))
                               (lum (calculate-luminance hex))) ;; Get hex
                          (insert (format "%s %.2f|"hex lum))))
               (insert "\n")) ;; Add a newline between rows
      (display-buffer buffer)))) ;; Show buffer

(defun generate-color-grid-oklab (buffer-name max-chroma steps)
  "Generate a grid of colors based on lightness (L), chroma (C), and hue (H).
Fill an Emacs buffer with the output."
  (let* (
         (L (parabolic-interpolation 90 26 steps))
        ;; (L (linear-interpolation 90 26 steps)) ;; Lightness values
        ;; (C '(12 18 24 30 36 30 24 18 12)) ;; Chroma values
        (C (linear-ascent-descent 12 max-chroma steps)) ;; 
Chroma values
        ;; (C (bell-curve 12 75 steps)) ;; Chroma values
        (buffer (get-buffer-create buffer-name))) ;; Create or get buffer
    (with-current-buffer buffer
      ;; (erase-buffer) ;; Clear the buffer
      (insert (format "\nGenerated Color Grid with OKLab\n"))
      (cl-loop for i from 0 to 360 by 15 do
               (insert (format "Hue %03d: " i))
               (cl-loop for j from 0 below (length L) do
                        (let* ((code (format "N%03d%d" i (1+ j) )) ;; Create code

                               (chroma (if (= i 0) 0 (nth j C)))
                               (hex (lch-to-hex-oklab (nth j L) chroma i))

                               (lum (calculate-luminance hex))) ;; Get hex
                          (insert (format "%s %.2f|"hex lum))))
               (insert "\n")) ;; Add a newline between rows
      (display-buffer buffer)))) ;; Show buffer


(defun generate-color-grid-hsluv (buffer-name max-sat steps)
  "Generate a grid of colors based on lightness (L), chroma (C), and hue (H) using HSLuv.
Fill an Emacs buffer with the output."
  (let* (
         (L (exp-interpolation 100 0 steps 1)) ;; Lightness values
        (S (linear-ascent-descent 0 max-sat steps)) ;; 
         (buffer (get-buffer-create buffer-name))) ;; Create or get buffer
    (with-current-buffer buffer
      (erase-buffer) ;; Clear the buffer
      (insert (format "\nGenerated Color Grid Using HSLuv\n"))
      (cl-loop for i from 0 to 360 by 15 do
               (insert (format "Hue %03d: " i))
               (cl-loop for j from 0 below (length L) do
                        (let* ((hue i)
                               (lightness (nth j L))
                               (saturation (if (= hue 0) 0 (nth j S)))
                               (rgb (hsluv-hsluv-to-rgb (list hue saturation lightness)))
                               (hex (format "#%02x%02x%02x"
                                            (round (* 255 (nth 0 rgb)))
                                            (round (* 255 (nth 1 rgb)))
                                            (round (* 255 (nth 2 rgb)))))
                               (lum (calculate-luminance hex)))
                          (insert (format "%s %.2f|" hex lum))))
               (insert "\n")) ;; Add a newline between rows
      (display-buffer buffer)))) ;; Show buffer

(defun generate-color-grid-hpluv (buffer-name max-sat steps)
  "Generate a grid of colors based on lightness (L), chroma (C), and hue (H) using HSLuv.
Fill an Emacs buffer with the output."
  (let* (
         (L (quadratic-interpolation 100 0 steps)) ;; Lightness values
        ;; (S (linear-ascent-descent 0 max-sat steps)) ;; 
        (S (linear-ascent-descent 0 max-sat steps)) ;; 
       
         (buffer (get-buffer-create buffer-name))) ;; Create or get buffer
    (with-current-buffer buffer
      (insert (format "\nGenerated Color Grid Using HPLuv\n"))
      (cl-loop for i from 0 to 360 by 15 do
               (insert (format "Hue %03d: " i))
               (cl-loop for j from 0 below (length L) do
                        (let* ((hue i)
                               (lightness (nth j L))
                               (saturation (if (= hue 0) 0 (nth j S)))
                               (rgb (hsluv-hpluv-to-rgb (list hue saturation lightness)))
                               (hex (format "#%02x%02x%02x"
                                            (round (* 255 (nth 0 rgb)))
                                            (round (* 255 (nth 1 rgb)))
                                            (round (* 255 (nth 2 rgb)))))
                               (lum (calculate-luminance hex)))
                          (insert (format "%s %.2f|" hex lum))))
               (insert "\n")) ;; Add a newline between rows
      (display-buffer buffer)))) ;; Show buffer

(defun get-normalized-hsluv (x y z)
  "Return a normalized HSLuv hex color based on X (saturation), Y (hue/lightness) and Z (Saturation)."
  (let* ((hue (* y 360)) ;; Hue from 0 to 360
         (lightness (* x 100)) ;; Lightness from 0 to 100
         (saturation (if (= y 0)
                         0
                       (linear-ascent-descent-value 0 (* z 100) x))) ;; Saturation based on X
         (rgb (hsluv-hsluv-to-rgb (list hue saturation lightness))) ;; Convert to RGB
         (hex (format "#%02x%02x%02x"
                      (round (* 255 (nth 0 rgb))) ;; Red
                      (round (* 255 (nth 1 rgb))) ;; Green
                      (round (* 255 (nth 2 rgb)))))) ;; Blue
    hex)) ;; Return the hex color

(defun generate-normalized-color-grid (buffer-name)
  "Generate a grid of colors based on lightness (L), chroma (C), and hue (H) using HSLuv.
Fill an Emacs buffer with the output."
  (let* (
         (buffer (get-buffer-create buffer-name))) ;; Create or get buffer
    (with-current-buffer buffer
      (erase-buffer) ;; Clear the buffer
      (insert (format "\nGenerated Color Grid Using HSLuv\n"))
      (cl-loop for i from 0 to 1 by .025 do
               (insert (format "Hue %03d: " (* i 360)))
               (cl-loop for j from 0 to 1 by .11 do
                        (let* ((hex (get-normalized-hsluv j i .5))
                               (lum (calculate-luminance hex)))
                          (insert (format "%s %.2f|" hex lum))))
               (insert "\n")) ;; Add a newline between rows
      (display-buffer buffer)))) ;; Show buffer

(defun visualize-parabolic-hue-scheme (n saturation lightness)
  "Generate and display a parabolic hue-based color scheme in a new buffer.
N is the number of colors, SATURATION controls the intensity, and LIGHTNESS controls the brightness."
  (let* ((hues (cl-loop for i from 0 below n
                        collect (* i (/ 360.0 n)))) ;; Evenly distributed hues
         (parabolic-values (cl-loop for i from 0 below n
                                    collect (* 100 (- 1.0 (expt (- (/ i (float (1- n))) 0.5) 2.0))))) ;; Parabolic scaling
         (colors (cl-mapcar (lambda (hue parabolic-lightness)
                              (get-normalized-hsluv (/ parabolic-lightness 100.0) ;; Normalize parabolic lightness to [0, 1]
                                                    (/ hue 360.0) ;; Normalize hue to [0, 1]
                                                    saturation)) ;; Use fixed saturation
                            hues parabolic-values)) ;; Combine hues and parabolic lightnesses
         (buffer (get-buffer-create "*Parabolic Hue Scheme*"))) ;; Create or get buffer
    (with-current-buffer buffer
      (erase-buffer) ;; Clear the buffer
      (insert (format "Generated Parabolic Hue Scheme (N=%d, Saturation=%.2f, Lightness=%.2f)\n\n" n saturation lightness))
      ;; Print each color in the buffer with its visual representation
      (cl-loop for color in colors
               for i from 1
               do (insert (propertize (format "Color %d: %s\n" i color)
                                      'face `(:foreground ,color)))))
    (display-buffer buffer))) ;; Show the buffer


;; Example Usage:
(setq buffer-name "*Generated Color Grid*")
;; (generate-color-grid buffer-name 36 9)
;; (generate-color-grid-oklab buffer-name 36 9)
;; (generate-color-grid-hsluv buffer-name 100 9)
;; (generate-normalized-color-grid buffer-name )
(visualize-parabolic-hue-scheme 12 0.5 0.5)
;; (generate-color-grid-hpluv buffer-name 100 9)
