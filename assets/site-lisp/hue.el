(require 'color)
(require 'cl-lib)

(defun generate-interval-values (n)
  "Generate a list of values by dividing 1.0 into N equal intervals."
  (let ((step (/ 1.0 n)))
    (cl-loop for i from 1 to n
             collect (* i step))))

(defun generate-perceptual-color-grid (lightness hue-values chroma-values)
  "Generate a grid of colors with fixed LIGHTNESS, iterating over HUE-VALUES and CHROMA-VALUES.
Returns a list of lists, where each inner list contains hex color strings for one hue."
  (mapcar (lambda (hue)
            (mapcar (lambda (chroma)
                      (let* ((rgb (color-hsl-to-rgb hue chroma lightness))
                             (r (nth 0 rgb))
                             (g (nth 1 rgb))
                             (b (nth 2 rgb)))
                        (color-rgb-to-hex r g b 2)))
                    chroma-values))
          hue-values))

(defun generate-gray-chromas (lightness chroma-values)
  "Generate a row of greyness values based on LIGHTNESS and CHROMA-VALUES.
These are achromatic colors (gray) with the same LIGHTNESS as the chromatic colors."
  (mapcar (lambda (chroma)
            (let* ((rgb (color-hsl-to-rgb 0.0 0.0 lightness)) ;; Gray has 0 chroma
                   (r (nth 0 rgb))
                   (g (nth 1 rgb))
                   (b (nth 2 rgb)))
              (color-rgb-to-hex r g b 2)))
          chroma-values))

(defun fill-buffer-with-color-grid (buffer-name lightness n-hue n-chroma)
  "Fill BUFFER-NAME with a grid of color codes, including a row of greys and rows for HUE-VALUES and CHROMA-VALUES.
N-HUE and N-CHROMA determine the number of divisions for hue and chroma, respectively."
  (let* ((hue-values (generate-interval-values n-hue))
         (chroma-values (generate-interval-values n-chroma))
         (color-grid (generate-perceptual-color-grid lightness hue-values chroma-values))
         (gray-chromas (generate-gray-chromas lightness chroma-values))
         (buffer (get-buffer-create buffer-name)))
    (with-current-buffer buffer
      (erase-buffer) ;; Clear the buffer before adding new content
      (insert (format "Generated Color Grid with Lightness: %s\n\n" lightness))
      ;; Print header row for chroma values
      (insert "Hue\\Chroma| ")
      (dolist (chroma chroma-values)
        (insert (format "%-9s " (format "%.2f" chroma))))
      (insert "\n")
      (insert (make-string (+ 8 (* 8 (length chroma-values))) ?-) "\n")
      ;; Print the row of greys
      (insert "Greyness | ")
      (dolist (gray gray-chromas)
        (insert (format "%-8s " gray)))
      (insert "\n")
      ;; Print each row with hue and hex codes
      (dolist (row color-grid)
        (insert (format "%-8s | " (format "%.2f" (car hue-values))))
        (dolist (color row)
          (insert (format "%-8s " color)))
        (setq hue-values (cdr hue-values))
        (insert "\n"))
      (display-buffer buffer))))

;; Example Usage
(setq lightness 0.4) ;; Fixed lightness (0.0 to 1.0)
(setq n-hue 12)      ;; Number of hue intervals
(setq n-chroma 10)   ;; Number of chroma intervals

(fill-buffer-with-color-grid "*Generated Color Grid*" lightness n-hue n-chroma)
