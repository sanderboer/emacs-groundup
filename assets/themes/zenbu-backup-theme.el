;;; zenbu-theme.el --- A dark theme for Emacs 24.  -*- lexical-binding: t; -*-

;; Copyright (C) 2014  Cody Canning

;; Author: Cody Canning <cocanning11@gmail.com>
;; URL: https://github.com/ccann/zenbu-theme
;; Package-Version: 20140717.232
;; Version: 1.0.0

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Credits:

;; The structure of this theme was based on zenburn-theme.el by Bozhidar Batsov

;;; Code:
(deftheme zenbu-backup "The zenbu color theme")

;; zenbu Color Pallette
(defvar zenbu-colors-alist
  '(("zenbu-fg"            . "#f7e6ec"  )
    ("zenbu-fg+1"          . (color-lighten-name "#f7e6ec" 5)  )
    ("zenbu-fg-1"          . (color-darken-name "#f7e6ec" 5)  )
    ("zenbu-bg"            . "#06040b"  )
;;    ("zenbu-bg+1"          . "#486370" )
    ("zenbu-bg+1"          . (color-lighten-name "#06040b" 5) )
    ("zenbu-bg-1"          . (color-darken-name "#06040b" 5) )

;; Primary Hues
    ("zenbu-blue"          . "#577382" )
    ("zenbu-charcoal"      . (color-desaturate-name (color-darken-name "#f7e6ec" 5) 100) )
    ("zenbu-salmon"        . "#edd1c1" )
    ("zenbu-violet"        . "#a7dbf8" )
    ("zenbu-orange"        . "#f6e6ee" )
    ("zenbu-green"         . "#577382" )
    ("zenbu-yellow"        . "#c5e4f9" )
    ("zenbu-sand"          . "#f7e6ec" )

;; Secondary Hues
    ("zenbu-lime"          . "#364750" )
    ("zenbu-teal"          . "#86afc6" )
    ("zenbu-pink"          . "#c4b8be" )
    ("zenbu-brown"         . "#86afc6" )
    ("zenbu-red"           . "#bba599" )
    ("zenbu-dull-red"      . "#364750" )
    ("zenbu-dark-violet"   . "#86afc6" )
    ("zenbu-darker-violet" . "#364750" )
    ("zenbu-olive"         . "#9eb7c7" )

;; Misc.
    ("zenbu-link"          . "#577382" )
    ("zenbu-warn"          . "#a7dbf8" )
    ("zenbu-succ"          . "#f6e6ee" )
    ("zenbu-hl"            . "#364750" )))

(defmacro zenbu-backup/with-color-variables (&rest body)
  "`let' bind all colors defined in `zenbu-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (cons)
                     (list (intern (car cons)) (cdr cons)))
                   zenbu-colors-alist))
     ,@body))

(zenbu-backup/with-color-variables 
  (custom-theme-set-faces
   'zenbu-backup

   ;; >>>>> Built-in
   '(button ((t (:underline t))))
   `(link ((t (:bold t :foreground ,zenbu-blue :underline t :weight bold))))
   ;; `(link-visited ((t (:foreground ,zenbu-salmon-2 :underline t :weight normal))))

   ;; ordinary text. Its background color is used as the frame's background color. 
   `(default ((t (:foreground ,zenbu-fg :background ,zenbu-bg))))

   ;;The :background attribute of this face specifies the color of the text cursor
   `(cursor ((t (:background ,zenbu-salmon)))) 

   ;; The face for displaying control characters and escape sequences
   `(escape-glyph ((t (:foreground ,zenbu-salmon :bold t))))

   ;; The face for the narrow fringes to the left and right of windows on graphic displays.
   `(fringe ((t (:foreground ,zenbu-fg :background ,zenbu-bg))))

   ;; fixed line displayed at the top of the emacs window, not in XEmacs
   ;; `(header-line ((t (:foreground ,zenbu-salmon 
   ;;                                :background ,zenbu-bg
   ;;                                :box (:line-width -1 :style released-button)))))

   ;;text highlighting in various contexts, when the mouse cursor is moved over a hyperlink. 
   `(highlight ((t (:background ,zenbu-hl))))

   ;; “lazy matches” for Isearch and Query Replace (matches other than the current one). 
   `(lazy-highlight ((t (:background ,zenbu-yellow :foreground ,zenbu-bg :weight extra-bold))))

   ;; This face is used to highlight the current Isearch match 
   `(isearch ((t (:background ,zenbu-succ :foreground ,zenbu-bg :weight extra-bold))))
   
   `(success ((t (:foreground ,zenbu-link :weight bold))))
   `(warning ((t (:foreground ,zenbu-pink :weight bold)))) 

   ;; This face is used for displaying an active region 
   `(region ((t (:background ,zenbu-bg))))

   `(show-paren-match-face ((t (:background ,zenbu-lime :foreground ,zenbu-bg ))))

   ;; >>>>> mode-line
   `(mode-line    ((,class (:foreground ,zenbu-charcoal
                                        :background ,zenbu-green
                                       ;; :box (:line-width -1 :style released-button)
                                        ))
                   (t :inverse-video nil)))

   `(mode-line-highlight ((t (:background ,zenbu-salmon :foreground ,zenbu-bg :box nil))))
   `(mode-line-inactive ((t (:background ,zenbu-green :foreground ,zenbu-green :box nil))))
   `(mode-line-buffer-id ((t (:foreground ,zenbu-salmon))))
   `(minibuffer-prompt ((t (:foreground ,zenbu-violet))))

   ;;   `(mode-line-highlight ((t (:foreground ,zenbu-lime))))

   ;; linum
   `(linum ((t (:foreground ,zenbu-charcoal :background ,zenbu-bg+1))))
   

   ;; >>>>> font-lock
   `(font-lock-warning-face ((t (:foreground ,zenbu-yellow :weight bold))))
   `(font-lock-function-name-face ((t (:foreground ,zenbu-orange ))))
   `(font-lock-variable-name-face ((t (:foreground ,zenbu-salmon))))
   `(font-lock-keyword-face ((t (:foreground ,zenbu-blue))))
   `(font-lock-comment-face ((t (:foreground ,zenbu-charcoal))))
   ;;`(font-lock-comment-delimiter-face ((t (:foreground ,zenbu-charcoal :weight light :slant italic))))
   `(font-lock-type-face ((t (:foreground ,zenbu-sand))))
   `(font-lock-constant-face ((t (:foreground ,zenbu-dark-violet))))
   `(font-lock-builtin-face ((t (:foreground ,zenbu-violet))))
   `(font-lock-preprocessor-face ((t (:foreground ,zenbu-sand))))
   `(font-lock-string-face ((t (:foreground ,zenbu-green))))
 ;;  `(font-lock-doc-face ((t (:foreground ,zenbu-green))))
   

   ;; >>>>> eshell 
   `(eshell-prompt ((t (:foreground ,zenbu-lime))))
   `(eshell-ls-archive ((t (:foreground ,zenbu-orange :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,zenbu-violet :weight normal))))
   `(eshell-ls-executable ((t (:foreground ,zenbu-yellow :weight normal))))
   `(eshell-ls-unreadable ((t (:foreground ,zenbu-fg))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,zenbu-blue :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,zenbu-link :weight bold))))

   ;; >>>>> Org mode
   `(org-document-info-keyword ((t (:foreground ,zenbu-olive))))
   `(org-document-title ((t (:foreground ,zenbu-salmon :height 1.50))))
   `(org-archived ((t (:foreground ,zenbu-fg :weight bold))))
   `(org-checkbox ((t (:foreground ,zenbu-fg+1 :foreground ,zenbu-olive 
                                   :box (:line-width 1 :style released-button)))))
   `(org-done ((t (:foreground ,zenbu-lime :strike-through t))))
   `(org-todo ((t (:foreground ,zenbu-red))))
   `(org-formula ((t (:foreground ,zenbu-violet))))
   `(org-headline-done ((t (:strike-through t :foreground ,zenbu-charcoal))))
   `(org-hide ((t (:foreground ,zenbu-bg)))) 
   `(org-level-1 ((t (:foreground ,zenbu-blue))))
   `(org-level-2 ((t (:foreground ,zenbu-violet))))
   `(org-level-3 ((t (:foreground ,zenbu-orange))))
   `(org-level-4 ((t (:foreground ,zenbu-yellow))))
   `(org-level-5 ((t (:foreground ,zenbu-salmon))))
   `(org-level-6 ((t (:foreground ,zenbu-green))))
   `(org-level-7 ((t (:foreground ,zenbu-brown))))
   `(org-level-8 ((t (:foreground ,zenbu-teal))))
   `(org-link ((t (:foreground ,zenbu-link :underline t))))
   
   `(org-agenda-date ((t (:foreground ,zenbu-blue))))
   `(org-deadline-announce ((t (:foreground ,zenbu-dull-red))))
   `(org-date ((t (:foreground ,zenbu-link :underline t))))
   `(org-agenda-date-today  ((t (:foreground ,zenbu-salmon :weight light :slant italic))))
   `(org-agenda-structure  ((t (:inherit font-lock-comment-face))))
   ;; `(org-scheduled ((t (:foreground ,zenburn-green+4))))x
   ;; `(org-scheduled-previously ((t (:foreground ,zenburn-red-4))))
   ;; `(org-scheduled-today ((t (:foreground ,zenburn-blue+1))))
   ;; `(org-sexp-date ((t (:foreground ,zenburn-blue+1 :underline t))))
   ;; `(org-time-grid ((t (:foreground ,zenburn-orange))))
   ;; `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))

   `(org-special-keyword ((t (:foreground ,zenbu-olive :weight normal))))
   `(org-table ((t (:foreground ,zenbu-olive))))
   `(org-tag ((t (:bold t :foreground ,zenbu-orange :strike-through nil))))
   `(org-warning ((t (:bold t :foreground ,zenbu-pink :weight bold))))
   `(org-column ((t (:background ,zenbu-bg))))
   `(org-column-title ((t (:background ,zenbu-bg :foreground ,zenbu-lime :underline t))))
   `(org-mode-line-clock ((t (:foreground ,zenbu-yellow))))
   `(org-footnote ((t (:foreground ,zenbu-link :underline t))))
   `(org-code ((t (:foreground ,zenbu-olive))))
   `(org-verbatim ((t (:inherit org-code))))
   
   ;; >>>>> elpy and ipython
   `(highlight-indentation-face ((t (:background ,zenbu-bg))))
   `(comint-highlight-prompt ((t (:inherit eshell-prompt))))
   
   ;; >>>>> auto-complete and popup
   `(ac-candidate-face ((t (:background ,zenbu-sand :foreground ,zenbu-bg))))
   `(ac-selection-face ((t (:background ,zenbu-violet :foreground ,zenbu-bg))))
   `(popup-tip-face ((t (:background ,zenbu-sand :foreground ,zenbu-bg))))
   `(popup-scroll-bar-foreground-face ((t (:background ,zenbu-dark-violet))))
   `(popup-scroll-bar-background-face ((t (:background ,zenbu-olive))))
   `(popup-isearch-match ((t (:background ,zenbu-yellow :foreground ,zenbu-bg))))

   ;; >>>>> smart-mode-line
   ;;`(sml/global ((t (:background ,zenbu-bg :inverse-video nil))))
   `(sml/folder ((t (:foreground ,zenbu-charcoal))))
   `(sml/filename ((t (:foreground ,zenbu-salmon :weight normal))))
   `(sml/prefix   ((t (:foreground ,zenbu-salmon :weight normal))))
   `(sml/line-number ((t (:foreground ,zenbu-blue :weight normal))))  
   `(sml/col-number ((t (:foreground ,zenbu-green :weight normal))))
   `(sml/read-only ((t (:foreground ,zenbu-charcoal))))
   `(sml/outside-modified ((t (:foreground ,zenbu-red))))
   `(sml/modified ((t (:foreground ,zenbu-red))))
   `(sml/remote ((t (:foreground ,zenbu-charcoal))))
   `(sml/numbers-separator ((t (:foreground ,zenbu-charcoal))))
   ;;`(sml/client ((t (:foreground ,zenbu-succ))))
   ;;`(sml/not-modified ((t (:foreground ,zenbu-yellow))))
   `(sml/git  ((t (:foreground ,zenbu-blue))))
   `(sml/vc-edited  ((t (:foreground ,zenbu-blue))))
   `(sml/modes ((t (:foreground ,zenbu-pink))))
   `(sml/position-percentage ((t (:foreground ,zenbu-charcoal))))

   `(flyspell-incorrect ((t (:underline (:color ,zenbu-red :style wave)))))
   `(flyspell-duplicate ((t (:underline (:color ,zenbu-yellow :style wave)))))

       ;; > Evil
    `(evil-ex-info ((,class (:foreground ,zenbu-fg))))
    `(evil-ex-substitute-replacement ((,class (:foreground ,zenbu-yellow))))
    `(evil-ex-substitute-matches ((,class (:inherit isearch))))

        ;; > Powerline
     ;; `(powerline-active1 ((,class (:foreground ,zenbu-red :background ,zenbu-bg+1))))
    `(powerline-active1 ((,class (:inherit mode-line))))
    `(powerline-active2 ((,class (:foreground ,zenbu-green :background ,zenbu-salmon))))
    ;; `(powerline-inactive1 ((,class (:background ,zenbu-charcoal))))
    ;; `(powerline-inactive2 ((,class (:background ,zenbu-charcoal))))
    `(powerline-inactive1 ((,class (:inherit mode-line-inactive))))
    `(powerline-inactive2 ((,class (:foreground ,zenbu-salmon  :background ,zenbu-green))))

    ;; > Powerline Evil
    `(powerline-evil-base-face ((,class (:foreground ,zenbu-sand))))
    `(powerline-evil-normal-face ((,class (:background , zenbu-blue ))))
    `(powerline-evil-insert-face ((,class (:foreground ,zenbu-fg :background ,zenbu-sand))))
    `(powerline-evil-visual-face ((,class (:foreground ,zenbu-fg :background , zenbu-red))))
    `(powerline-evil-replace-face ((,class (:foreground ,zenbu-fg :background ,zenbu-yellow))))
    ;; > Elscreen
    `(elscreen-tab-background-face ((,class ( :foreground, zenbu-fg-1 :background,
							   (color-desaturate-name zenbu-bg-1 100)))))
    `(elscreen-tab-background-face ((,class ( :foreground, zenbu-fg :background, zenbu-bg))))
    `(elscreen-tab-control-face ((,class ( :foreground, zenbu-fg :background, zenbu-bg :box, nil))))
    `(elscreen-tab-current-screen-face ((,class ( :foreground, zenbu-fg+1 :background,
							       (color-desaturate-name zenbu-bg+1 100) ))))
    `(elscreen-tab-other-screen-face ((,class ( :foreground, zenbu-charcoal :background, zenbu-bg))))
  

   
   ))


(zenbu/with-color-variables
  (custom-theme-set-variables
   'zenbu-backup
   `(ansi-color-names-vector [,"black" ,"#E2434C" ,"#86B187" ,"#E0D063" ,"#84C452" ,"#E18CBB" ,"#8AC6F2" ,"white"])))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'zenbu-backup)


;;; zenbu-backup-theme.el ends here
    
