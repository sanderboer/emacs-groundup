;;; omega-theme.el --- A parametric theme for Emacs.  -*- lexical-binding: t; -*-

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(deftheme omega "The omega color theme")

(require 'theme-utils)

(defun omega-color (x y z)
  "Generate a normalized color using Oklab and scaling Y."
  (let (
        (scaled-y (/ (+ y 0) 24.0)))
    ;; (+/COLOR/get-normalized-oklab x scaled-y z)
    (+/COLOR/get-normalized-hpluv x scaled-y z)
    ;; (+/COLOR/get-normalized-hsluv x scaled-y z)
    ))

(setq fg 0.75)
(setq bg 0.1)
;; (setq fg .4)
;; (setq bg 0.95)


;; omega Color Palette
(defvar omega-colors-alist
  `(
    ("omega-fg"            . ,(omega-color fg         16 0.6))
    ("omega-fg+1"          . ,(omega-color (+ fg 0.1) 16 0.6))
    ("omega-fg-1"          . ,(omega-color (- fg 0.1) 16 0.6))
    ("omega-fg-2"          . ,(omega-color (- fg 0.2) 16 0.6))
    ("omega-bg"            . ,(omega-color bg 6 0.5))
    ("omega-bg-1"          . ,(omega-color (- bg 0.05) 0 0.5))
    ("omega-bg+1"          . ,(omega-color (+ bg 0.05) 0 0.5))
    ("omega-bg+2"          . ,(omega-color (+ bg 0.10) 0 0.5))
    ("omega-bg+3"          . ,(omega-color (+ bg 0.15) 0 0.5))
    ("omega-comment"       . ,(omega-color (* fg 0.7) 16 0.3))
    ;; Primary Hues
    ("omega-blue"          . ,(omega-color fg 16 1.0))
    ("omega-charcoal"      . ,(omega-color fg 16 0.1))
    ("omega-salmon"        . ,(omega-color fg 0  1.0))
    ("omega-violet"        . ,(omega-color fg 14 1.0))
    ("omega-orange"        . ,(omega-color fg 2 1.0))
    ("omega-green"         . ,(omega-color fg 4 1.0))
    ("omega-yellow"        . ,(omega-color fg 6 1.0))
    ("omega-sand"          . ,(omega-color fg 8 1.0))
    ;; Secondary Hues
    ("omega-lime"          . ,(omega-color fg  8 1.0))
    ("omega-teal"          . ,(omega-color fg  16 1.0))
    ("omega-pink"          . ,(omega-color fg  3 1.0))
    ("omega-brown"         . ,(omega-color fg  15 1.0))
    ("omega-red"           . ,(omega-color fg  0  1.0))
    ("omega-dull-red"      . ,(omega-color fg  0  0.5))
    ("omega-dark-violet"   . ,(omega-color fg  17 1.0))
    ("omega-darker-violet" . ,(omega-color (* fg 0.6)  17 1.0))
    ("omega-olive"         . ,(omega-color fg  14 1.0))
    ;; Misc.
    ("omega-link"          . ,(omega-color 0.75 8 1.0))
    ("omega-warn"          . ,(omega-color 0.75 8 1.0))
    ("omega-succ"          . ,(omega-color 0.75 8 1.0))
    ("omega-hl"            . ,(omega-color 0.75 8 0.1))
    ))

(defmacro omega/with-color-variables (&rest body)
  "`let' bind all colors defined in `omega-colors-alist' around BODY.
Also bind `class' to ((class color) (min-colors 89))."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@(mapcar (lambda (cons)
                     (list (intern (car cons)) (cdr cons)))
                   omega-colors-alist))
     ,@body))

(omega/with-color-variables 
  (custom-theme-set-faces
   'omega
   '(button ((t (:underline t))))
   `(link ((t (:bold t :foreground ,omega-blue :underline nil :weight bold))))
   `(variable-pitch ((t (
		                     :family, "Victor"
			                   :foreground ,omega-fg
			                   :background ,omega-bg
			                   :distance-foreground, omega-fg-1
			                   :inverse-video nil
			                   :box nil
			                   :strike-through nil
			                   :overline nil
			                   :underline nil
			                   :slant normal
			                   :weight light
			                   :width wide
                         :height 120
                         ))))

   '(fixed-pitch ((t ( :family "VictorMono Nerd Font"
                       :weight light
                       :height 120
                       :width wide
			                 :inverse-video nil
			                 :box nil
			                 :strike-through nil
			                 :overline nil
			                 :underline nil
			                 :slant normal))))

   `(default ((t (
		              :family, "VictorMono Nerd Font"
                  :height 120
			            :foreground ,omega-fg
			            :background ,omega-bg
			            :distance-foreground, omega-fg-1
			            :inverse-video nil
			            :box nil
			            :strike-through nil
			            :overline nil
			            :underline nil
			            :slant normal
			            :weight light
			            :width normal
                  ))))
   ;;The :background attribute of this face specifies the color of the text cursor
   ;; `(cursor ((t (:background ,omega-bg+1 )))) 
   `(cursor ((t (:background ,omega-fg )))) 
   `(border ((t (:foreground ,omega-bg+1 )))) 
   `(vertical-border ((t (:foreground ,omega-bg+1 )))) 
   `(mouse ((t (:background ,omega-fg)))) 
   `(window-divider             ((t (:foreground ,omega-salmon )))) 
   `(window-divider-first-pixel ((t (:foreground ,omega-salmon )))) 
   `(window-divider-last-pixel  ((t (:foreground ,omega-salmon )))) 

   ;; The face for displaying control characters and escape sequences
   `(escape-glyph ((t (:foreground ,omega-salmon :bold t))))

   ;; The face for the narrow fringes to the left and right of windows on graphic displays.
   `(fringe ((t (:foreground ,omega-bg+1 :background ,omega-bg))))

   ;; fixed line displayed at the top of the emacs window, not in XEmacs
   `(header-line ((t (:foreground ,omega-salmon
                                  ))))

   `(which-func ((t (:foreground ,omega-salmon
                                 ))))

   ;;text highlighting in various contexts, when the mouse cursor is moved over a hyperlink. 
   ;; `(highlight ((t (:background ,omega-hl))))
   `(highlight ((t (:inherit hl-line))))
   `(ivy-current-match ((t(:background, omega-bg-1 :weight bold))))
   `(ivy-minibuffer-match-face-1 ((t(:background, omega-bg-1 :foreground ,omega-lime ))))
   `(ivy-minibuffer-match-face-2 ((t(:background, omega-bg-1 :foreground ,omega-lime ))))
   `(ivy-minibuffer-match-face-3 ((t(:background, omega-bg-1 :foreground ,omega-lime ))))
   `(ivy-minibuffer-match-face-4 ((t(:background, omega-bg-1 :foreground ,omega-lime ))))
   `(show-paren-match ((t (:background ,omega-bg+2))))

   ;; “lazy matches” for Isearch and Query Replace (matches other than the current one). 
   `(lazy-highlight ((t (:background ,omega-yellow :foreground ,omega-bg :weight extra-bold))))

   ;; This face is used to highlight the current Isearch match 
   `(isearch ((t (:background ,omega-succ :foreground ,omega-bg :weight extra-bold))))
   
   `(success ((t (:foreground ,omega-link :weight bold))))
   `(warning ((t (:foreground ,omega-pink :weight bold)))) 

   ;; This face is used for displaying an active region 
   `(region ((t (:background ,(substring (color-desaturate-name omega-bg+1 100)0 7) ) ) ))

   ;; >>>>> mode-line
   `(mode-line    ((,class (:foreground ,omega-salmon
                                        :weight light
                                        ;:underline(:color ,omega-salmon :style line :position t)
                                        :box (:line-width 12 :color ,omega-bg) 
                                        ))
                   (t :inverse-video nil)))

   `(mode-line-inactive ((t (:foreground ,omega-bg+2
                                         :weight light
                                         :box (:line-width 12 :color ,omega-bg) 
                                         ))))
   `(mode-line-highlight ((t (:background ,omega-teal :foreground ,omega-bg :weight light :box nil))))
   `(mode-line-buffer-id ((t (:foreground ,omega-salmon :weight light ))))
   `(mode-line-highlight ((t (:foreground ,omega-lime :weight light ))))
   `(minibuffer-prompt ((t (:foreground ,omega-salmon :weight light ))))

   ;; linum
   `(linum ((t (:foreground, omega-bg+2 :background, omega-bg ))))

   ;; mu4e
   `( mu4e-header-highlight-face      ((t (:foreground ,omega-fg ))))
   `( mu4e-header-key-face            ((t (:foreground ,omega-lime  ))))
   `( mu4e-header-contact-face        ((t (:foreground ,omega-lime :family, "Gill Sans" ))))
   `( mu4e-header-value-face          ((t (:foreground ,omega-salmon ))))
   `( mu4e-special-header-value-face  ((t (:foreground ,omega-fg-1))))
   `( mu4e-view-body-face             ((t (:foreground ,omega-fg ))))

   ;;   ;; >>>>> font-lock
   `(font-lock-warning-face ((t (:foreground ,omega-yellow :weight bold))))
   `(font-lock-function-name-face ((t (:foreground ,omega-salmon ))))
   `(font-lock-variable-name-face ((t (:foreground , "#b5cfec"))))
   `(font-lock-keyword-face ((t (:foreground ,omega-blue))))
   `(font-lock-comment-face ((t (:foreground ,omega-comment
                                             :weight light
                                             :slant italic
                                             ))))
   `(font-lock-comment-delimiter-face ((t (:foreground ,omega-comment :weight light :slant italic))))
   `(font-lock-type-face ((t (:foreground ,omega-lime))))
   `(font-lock-constant-face ((t (:foreground ,omega-dark-violet))))
   `(font-lock-builtin-face ((t (:foreground ,omega-violet))))
   `(font-lock-preprocessor-face ((t (:foreground ,omega-green))))
   `(font-lock-string-face ((t (:foreground ,omega-green))))
   `(font-lock-doc-face ((t (:foreground ,omega-green))))

   ;;`(helm-selection              ((t (:background ,omega-bg :underline nil))))
   `(helm-selection              ((t (:inherit hl-line))))
   `(helm-match                  ((t (:foreground ,omega-salmon :underline nil)) ))
   `(helm-source-header          ((t (:background ,omega-blue :foreground ,omega-fg-1)) ))
   `(helm-swoop-target-line-face ((t (:foreground ,omega-fg-1 :inverse-video t)) ))
   `(helm-ff-file                ((t (:foreground ,omega-fg :underline nil)) ))
   `(helm-ff-prefix              ((t (:foreground ,omega-green :underline nil)) ))
   `(helm-ff-dotted-directory    ((t (:foreground ,omega-bg+2 :underline nil)) ))
   `(helm-ff-directory           ((t (:foreground ,omega-salmon :underline nil)) ))
   `(helm-ff-executable          ((t (:foreground ,omega-fg :underline nil :slant italic)) ))

  `(vertico-current             ((t (:inherit hl-line)) ))
  `(vertico-multiline           ((t (:background ,omega-bg+1 :foreground ,omega-fg)) ))
  `(vertico-group-title         ((t (:background ,omega-bg+1 :foreground ,omega-fg)) ))
  `(vertico-group-separator     ((t (:background ,omega-blue :foreground ,omega-fg)) ))
   
   ;; >>>>> IDO
   `(ido-incomplete-regexp ((t (:foreground ,omega-lime))))
   `(ido-indicator ((t (:foreground ,omega-violet :background ,omega-orange))))
   `(ido-first-match ((t (:foreground ,omega-lime))))
   `(ido-only-match ((t (:foreground ,omega-lime))))
   `(ido-subdir ((t (:foreground ,omega-lime))))
   `(ido-virtual ((t (:foreground ,omega-lime))))

   ;; >>>>> eshell 
   `(eshell-prompt ((t (:foreground ,omega-lime))))
   `(eshell-ls-archive ((t (:foreground ,omega-orange :weight bold))))
   `(eshell-ls-backup ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-clutter ((t (:inherit font-lock-comment-face))))
   `(eshell-ls-directory ((t (:foreground ,omega-violet :weight normal))))
   `(eshell-ls-executable ((t (:foreground ,omega-yellow :weight normal))))
   `(eshell-ls-unreadable ((t (:foreground ,omega-fg))))
   `(eshell-ls-missing ((t (:inherit font-lock-warning-face))))
   `(eshell-ls-product ((t (:inherit font-lock-doc-face))))
   `(eshell-ls-special ((t (:foreground ,omega-blue :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,omega-link :weight bold))))

   ;; >>>>> Org mode
   '(org-block ((t (:inherit fixed-pitch))))
   '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
   '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)
                                 ))))
   '(org-drawer ((t (:inherit org-meta-line))))
   '(org-property-value ((t (:inherit fixed-pitch))) t)
   '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
   '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold
                           ))))
   '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
   '(org-ellipsis ((t (:inherit font-lock-comment-face :underline nil))))
   '(org-table ((t (:inherit fixed-pitch ))))
   `(org-todo ((t (:foreground ,omega-lime))))
   '(org-document-info-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)
                                             ))))
   `(org-document-title ((t (:inherit variable-pitch
                                      ))))
   '(org-document-info ((t (:inherit variable-pitch
                                     ))))
   `(org-archived ((t (:foreground ,omega-fg :weight bold))))
   `(org-checkbox ((t (:foreground ,omega-fg+1 :foreground ,omega-olive
                                   :box (:line-width 1 :style released-button)))))
   `(org-done ((t (:foreground ,omega-lime :strike-through t))))
   `(org-todo ((t (:foreground ,omega-lime))))
   `(org-formula ((t (:foreground ,omega-lime))))
   `(org-headline-done ((t (:foreground ,omega-charcoal))))
   `(org-hide ((t (:foreground ,omega-bg))))
   `(org-level-1 ((t (:inherit fixed-pitch :foreground, omega-fg))))
   `(org-level-2 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-3 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-4 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-5 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-6 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-7 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-level-8 ((t (:inherit fixed-pitch :foreground, omega-fg ))))
   `(org-link ((t (:foreground ,omega-link :underline nil))))
   
   `(org-agenda-date ((t (:foreground ,omega-blue))))
   `(org-deadline-announce ((t (:foreground ,omega-dull-red))))
   `(org-date ((t (:foreground ,omega-link :underline nil))))
   `(org-agenda-date-today  ((t (:foreground ,omega-salmon :weight light :slant italic))))
   `(org-agenda-structure  ((t (:inherit font-lock-comment-face))))
   ;; `(org-scheduled ((t (:foreground ,omegarn-green+4))))x
   ;; `(org-scheduled-previously ((t (:foreground ,omegarn-red-4))))
   ;; `(org-scheduled-today ((t (:foreground ,omegarn-blue+1))))
   ;; `(org-sexp-date ((t (:foreground ,omegarn-blue+1 :underline t))))
   ;; `(org-time-grid ((t (:foreground ,omegarn-orange))))
   ;; `(org-upcoming-deadline ((t (:inherit font-lock-keyword-face))))

   `(org-special-keyword ((t (:foreground ,omega-olive :weight normal))))
   `(org-table ((t (:foreground ,omega-olive))))
   `(org-tag ((t (:bold t :foreground ,omega-orange :strike-through nil))))
   `(org-warning ((t (:bold t :foreground ,omega-pink :weight bold))))
   `(org-column ((t (:background ,omega-bg))))
   `(org-column-title ((t (:background ,omega-bg :foreground ,omega-lime :underline nil))))
   `(org-mode-line-clock ((t (:foreground ,omega-yellow))))
   `(org-footnote ((t (:foreground ,omega-link :underline nil))))
   '(org-code ((t (:inherit (shadow fixed-pitch)))))
   `(org-verbatim ((t (:inherit org-code))))
   
   ;; >>>>> elpy and ipython
   `(highlight-indentation-face ((t (:background ,omega-bg))))
   `(comint-highlight-prompt ((t (:inherit eshell-prompt))))
   
   ;; >>>>> auto-complete and popup
   `(ac-candidate-face ((t (:background ,omega-sand :foreground ,omega-bg))))
   `(ac-selection-face ((t (:background ,omega-violet :foreground ,omega-bg))))
   `(popup-tip-face ((t (:background ,omega-sand :foreground ,omega-bg))))
   `(popup-scroll-bar-foreground-face ((t (:background ,omega-dark-violet))))
   `(popup-scroll-bar-background-face ((t (:background ,omega-olive))))
   `(popup-isearch-match ((t (:background ,omega-yellow :foreground ,omega-bg))))

   `(ebrowse-root-class ((t (:foreground ,omega-salmon))))
   `(ebrowse-progress ((t (:background ,omega-salmon))))
   `(ebrowse-member-class ((t (:foreground ,omega-olive))))
   `(ebrowse-member-attribute ((t (:foreground ,omega-blue))))
   `(ebrowse-tree-mark ((t (:foreground ,omega-violet))))

   `(cscope-file-face ((t (:foreground ,omega-salmon))))
   `(cscope-function-face ((t (:foreground ,omega-violet))))
   `(cscope-line-number-face ((t (:foreground ,omega-salmon))))
   `(cscope-separator-face ((t (:foreground ,omega-salmon))))
   `(cscope-mouse-face ((t (:background ,omega-bg+1))))
   
   `(company-scrollbar-bg ((t (:background ,omega-bg+1))))
   `(company-scrollbar-fg ((t (:foreground ,omega-fg))))

   `(company-tooltip ((t (:background ,omega-bg+1 :foreground ,omega-olive))))
   `(company-tooltip-selection ((t (:background ,omega-bg :foreground ,omega-olive))))

   `(company-tooltip-mouse ((t (:background ,omega-bg+1 :foreground ,omega-fg))))
   `(company-tooltip-common ((t (:background ,omega-bg+1 :foreground ,omega-fg))))
   `(company-tooltip-common-selection ((t (:background ,omega-bg :foreground ,omega-olive))))
   `(company-preview ((t (:background ,omega-bg+1 :foreground ,omega-fg))))
   `(company-preview-common ((t (:background ,omega-bg+1 :foreground ,omega-fg))))
   
   `(company-tooltip-annotation ((t (:background ,omega-bg+1 :foreground ,omega-bg+2 ))))
   `(company-tooltip-annotation-selection ((t (:background ,omega-bg :foreground ,omega-bg+2 ))))

   `(company-template-field ((t (:background ,omega-bg+1 :foreground ,omega-fg))))


   ;; >>>>> smart-mode-line
   ;;`(sml/global ((t (:background ,omega-bg :inverse-video nil))))
   `(sml/folder ((t (:foreground ,omega-charcoal))))
   `(sml/filename ((t (:foreground ,omega-salmon :weight normal))))
   `(sml/prefix   ((t (:foreground ,omega-salmon :weight normal))))
   `(sml/line-number ((t (:foreground ,omega-blue :weight normal))))  
   `(sml/col-number ((t (:foreground ,omega-green :weight normal))))
   `(sml/read-only ((t (:foreground ,omega-charcoal))))
   `(sml/outside-modified ((t (:foreground ,omega-red))))
   `(sml/modified ((t (:foreground ,omega-red))))
   `(sml/remote ((t (:foreground ,omega-charcoal))))
   `(sml/numbers-separator ((t (:foreground ,omega-charcoal))))
   ;;`(sml/client ((t (:foreground ,omega-succ))))
   ;;`(sml/not-modified ((t (:foreground ,omega-yellow))))
   `(sml/git  ((t (:foreground ,omega-blue))))
   `(sml/vc-edited  ((t (:foreground ,omega-blue))))
   `(sml/modes ((t (:foreground ,omega-pink))))
   `(sml/position-percentage ((t (:foreground ,omega-charcoal))))

   `(flyspell-incorrect ((t (:underline (:color ,omega-red :style wave)))))
   `(flyspell-duplicate ((t (:underline (:color ,omega-yellow :style wave)))))

   ;; > Evil
   `(evil-ex-info ((,class (:foreground ,omega-fg))))
   `(evil-ex-substitute-replacement ((,class (:foreground ,omega-yellow))))
   `(evil-ex-substitute-matches ((,class (:inherit isearch))))

;; > dashboard
   `(dashboard-items-face ((,class (:weight light ))))

   ;; > Doom modeline
   `(doom-modeline-bar ((t (:inherit mode-line ))))
   `(doom-modeline-bar-inactive ((t (:inherit mode-line-inactive ))))
   `(doom-modeline-buffer-file ((,class ( :weight light ))))
   `(doom-modeline-buffer-path ((,class (:weight light ))))
   `(doom-modeline-buffer-project-root ((,class (:foreground ,omega-red ))))
   ;; > Powerline
   ;; `(powerline-active1 ((,class (:foreground ,omega-red :background ,omega-bg+1))))
   `(powerline-active0 ((,class ( :foreground ,omega-fg :background ,omega-bg+2 ))))
   `(powerline-active1 ((,class ( :foreground ,omega-fg :background ,omega-bg+1 ))))
   `(powerline-active2 ((,class (:foreground ,omega-bg :background ,omega-salmon))))
   ;; `(powerline-inactive1 ((,class (:background ,omega-charcoal))))
   ;; `(powerline-inactive2 ((,class (:background ,omega-charcoal))))
   `(powerline-inactive0 ((,class (:foreground ,omega-charcoal  :background ,omega-bg ))))
   `(powerline-inactive1 ((,class (:foreground ,omega-charcoal  :background ,omega-bg+2 ))))
   `(powerline-inactive2 ((,class (:foreground ,omega-charcoal  :background ,omega-bg+1))))
   ;; > Powerline Evil
   `(powerline-evil-base-face ((,class (:foreground ,omega-bg))))
   `(powerline-evil-normal-face ((,class (:background , omega-bg ))))
   `(powerline-evil-insert-face ((,class (:foreground ,omega-fg :background ,omega-teal))))
   `(powerline-evil-visual-face ((,class (:foreground ,omega-fg :background , omega-red))))
   `(powerline-evil-replace-face ((,class (:foreground ,omega-fg :background ,omega-yellow))))

   ;; > Elscreen
   `(elscreen-tab-background-face ((,class ( :foreground, omega-fg-1 :background,
							                               (substring( color-desaturate-name omega-bg-1 100)0 7)))))
   `(elscreen-tab-background-face ((,class ( :foreground, omega-fg :background, omega-bg))))
   `(elscreen-tab-control-face ((,class ( :foreground, omega-fg :background, omega-bg :box, nil))))
   `(elscreen-tab-current-screen-face ((,class ( :foreground, omega-fg+1 :background,
							                                   (substring(color-desaturate-name omega-bg+1 100)0 7) ))))
   `(elscreen-tab-other-screen-face ((,class ( :foreground, omega-charcoal :background, omega-bg))))

   `(neo-root-dir-face      ((t ( :foreground, omega-salmon ))))
   `(neo-button-face        ((t ( :foreground, omega-salmon ))))
   `(neo-expand-button-face ((t ( :foreground, omega-salmon ))))
   `(neo-header-face        ((t ( :foreground, omega-red    ))))
   `(neo-dir-link-face      ((t ( :foreground, omega-teal   ))))
   `(neo-file-link-face     ((t ( :foreground, omega-blue   ))))
      ;; >>>>> Flycheck
   `(flycheck-error ((t (:underline (:style wave :color ,omega-red) :background ,omega-bg-1))))
   `(flycheck-warning ((t (:underline (:style wave :color ,omega-yellow) :background ,omega-bg-1))))
   `(flycheck-info ((t (:underline (:style wave :color ,omega-blue) :background ,omega-bg-1))))
   `(flycheck-fringe-error ((t (:foreground ,omega-red :weight bold))))
   `(flycheck-fringe-warning ((t (:foreground ,omega-yellow :weight bold))))
   `(flycheck-fringe-info ((t (:foreground ,omega-blue :weight bold))))

   ;; >>>>> Helm
   `(helm-header ((t (:foreground ,omega-fg :background ,omega-bg :underline nil :box nil))))
   `(helm-source-header ((t (:foreground ,omega-yellow :background ,omega-bg+1 :weight bold :height 1.3 :family "Sans Serif"))))
   `(helm-selection ((t (:background ,omega-bg+2 :foreground ,omega-fg))))
   `(helm-selection-line ((t (:background ,omega-bg+2))))
   `(helm-visible-mark ((t (:foreground ,omega-bg :background ,omega-green))))
   `(helm-candidate-number ((t (:foreground ,omega-yellow :background ,omega-bg+1))))
   `(helm-separator ((t (:foreground ,omega-red :background ,omega-bg))))
   `(helm-action ((t (:foreground ,omega-fg :background ,omega-bg))))
   `(helm-match ((t (:foreground ,omega-orange :background ,omega-bg-1 :weight bold))))
   `(helm-grep-match ((t (:foreground ,omega-orange :background ,omega-bg+2 :weight bold))))
   `(helm-grep-file ((t (:foreground ,omega-fg :background ,omega-bg))))
   `(helm-grep-lineno ((t (:foreground ,omega-fg-1 :background ,omega-bg))))
   `(helm-grep-finish ((t (:foreground ,omega-green :background ,omega-bg))))
   `(helm-buffer-not-saved ((t (:foreground ,omega-red :background ,omega-bg))))
   `(helm-buffer-directory ((t (:foreground ,omega-blue :background ,omega-bg))))
   `(helm-buffer-process ((t (:foreground ,omega-yellow :background ,omega-bg))))
   `(helm-buffer-file ((t (:foreground ,omega-fg :background ,omega-bg))))
   `(helm-buffer-size ((t (:foreground ,omega-fg-1 :background ,omega-bg))))

                                        ; neo-banner-face            neo-buffer--insert-with-face    neo-button-face
                                        ; neo-dir-link-face          neo-expand-btn-face     neo-file-link-face
                                        ; neo-header-face            neo-root-dir-face       neo-vc-added-face
                                        ; neo-vc-conflict-face       neo-vc-default-face     neo-vc-edited-face
                                        ; neo-vc-ignored-face        neo-vc-missing-face     neo-vc-needs-merge-face
                                        ; neo-vc-needs-update-face   neo-vc-removed-face     neo-vc-unlocked-changes-face
                                        ; neo-vc-unregistered-face   neo-vc-up-to-date-face  neo-vc-user-face
   ))


(omega/with-color-variables
  (custom-theme-set-variables
   'omega
   `(ansi-color-names-vector [,"black" ,"#E2434C" ,"#86B187" ,"#E0D063" ,"#84C452" ,"#E18CBB" ,"#8AC6F2" ,"white"])))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'omega)


;;; omega-theme.el ends here
