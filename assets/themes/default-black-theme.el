(deftheme default-black
  "Automatically created 2013-05-20.")

(custom-theme-set-faces
 'default-black
 '(default ((t ( :family "Droid Sans Mono" :inherit nil :stipple nil :background "Black" :foreground "White" :distance-foreground i"grey10" :inverse-video nil :box nil :strike-t*hrough nil :overline nil :underline nil :slant normal :weight normal :width normal :height 98))))
 '(highlight ((((class color) (min-colors 88) (background dark)) (:background "#111111"))))
 '(region ((nil (:background "#464740"))))
 '(fringe ((nil (:background "black"))))
 '(hl-line ((nil (:background "#222222"))))
 '(yas-field-highlight-face ((nil (:background "#333399"))))
 '(js2-function-param-face ((t (:foreground "LightGoldenrod"))))
 '(font-lock-warning-face ((nil (:foreground "#ff6666"))))
 '(show-paren-match ((nil (:background "grey10"))))
 '(show-paren-mismatch ((((class color)) (:background "red"))))
 `(elscreen-tab-background-face  ((nil (:foreground "#e1e1e0" :background "grey5"))))
 `(elscreen-tab-control-face ((nil (:foreground "#e1e1e0" :background "grey5" :box nil))))
 `(elscreen-tab-current-screen-face ((nil (:foreground "#e1e1e0" :background "grey5" :weight bold))))
 `(elscreen-tab-other-screen-face ((nil (:foreground "#e1e1e0" :background "black"))))
 '(window-divider ((nil (:foreground "red"))))
 '(window-divider-first-pixel ((t (:foreground "red"))))
 '(window-divider-last-pixel ((t (:foreground "red"))))
 '(mode-line ((nil (:background "grey5" :foreground "white" :box nil))))
 '(mode-line-inactive ((nil (:background "black" :foreground "grey7" :box nil))))
 '(highlight-indentation-face ((t (:background "grey10"))))
 '(highlight-indentation-current-column-face ((t (:background "grey20"))))
 '(hideshowvis-hidable-face ((t (:foreground "#333" :box nil))))
 '(linum ((t (:background "black" :foreground "grey18" :box nil))))

 '(minibuffer-prompt ((t (:background "black" :foreground "cyan" :box nil))))
  
 '(font-lock-comment-face ((t (:foreground "grey30" :box nil))))

 '(helm-ff-directory ((t (:foreground "blue" :weight bold))))
 '(helm-ff-dotted-directory ((t (:inherit helm-ff-directory))))
 '(helm-ff-dotted-symlink-directory ((t (:foreground "DarkOrange"))))
 '(helm-selection ((t (:background "gray18" :distant-foreground "black"))))
 '(helm-source-header ((t (:background "dim gray" :foreground "white" :family "Sans Serif"))))
 
 )

(provide-theme 'default-black)
