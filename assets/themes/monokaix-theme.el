;;; monokaix-theme.el --- A fruity color theme for Emacs.

;; Copyright (C) 2011-2016

;; Author: Kelvin Smith <oneKelvinSmith@gmail.com>
;; URL: http://github.com/oneKelvinSmith/monokaix-emacs
;; Version: 3.5.3

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:
;;
;; A port of the popular Textmate theme Monokaix for Emacs 24, built on top
;; of the new built-in theme support in Emacs 24.
;;
;;; Credits:
;;
;; Wimer Hazenberg created the original theme.
;; - http://www.monokaix.nl/blog/2006/07/15/textmate-color-theme/
;;
;; Bozhidar Batsov created zenburn-theme.el and solarized-theme.el
;;  on which this file is based.
;; - https://github.com/bbatsov/zenburn-emacs
;;
;; Color Scheme Designer 3 for complementary colours.
;; - http://colorschemedesigner.com/
;;
;; Xterm 256 Color Chart
;; - https://upload.wikimedia.org/wikipedia/en/1/15/Xterm_256color_chart.svg
;;
;; K. Adam Christensen for his personal monokaix theme that addresses 256 colours.
;; - https://github.com/pope/personal/blob/master/etc/emacs.d/monokaix-theme.el
;;
;; Thomas Frössman for his work on solarized-emacs.
;; - http://github.com/bbatsov/solarized-emacs
;;
;;; Code:

(unless (>= emacs-major-version 24)
  (error "The monokaix theme requires Emacs 24 or later!"))

(deftheme monokaix "The Monokai colour theme")

(defgroup monokaix nil
  "Monokaix theme options.
The theme has to be reloaded after changing anything in this group."
  :group 'faces)

(defcustom monokaix-distinct-fringe-background nil
  "Make the fringe background different from the normal background color.
Also affects 'linum-mode' background."
  :type 'boolean
  :group 'monokaix)

(defcustom monokaix-use-variable-pitch nil
  "Use variable pitch face for some headings and titles."
  :type 'boolean
  :group 'monokaix)

(defcustom monokaix-doc-face-as-comment nil
  "Consider `font-lock-doc-face' as comment instead of a string."
  :type 'boolean
  :group 'monokaix
  :package-version "3.5.1")

(defcustom monokaix-height-minus-1 0.8
  "Font size -1."
  :type 'number
  :group 'monokaix)

(defcustom monokaix-height-plus-1 1.1
  "Font size +1."
  :type 'number
  :group 'monokaix)

(defcustom monokaix-height-plus-2 1.15
  "Font size +2."
  :type 'number
  :group 'monokaix)

(defcustom monokaix-height-plus-3 1.2
  "Font size +3."
  :type 'number
  :group 'monokaix)

(defcustom monokaix-height-plus-4 1.3
  "Font size +4."
  :type 'number
  :group 'monokaix)

;; Primary colors
(defcustom monokaix-yellow "#ebe52a"
  "Primary colors - yellow"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-orange "#ff9b65"
  "Primary colors - orange" 
  :type 'string
  :group 'monokaix)

(defcustom monokaix-red  "#f4804c"
  "Primary colors - red"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-magenta "#b70bbd"
  "Primary colors - magenta"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-blue  "#2d6b71"
  "Primary colors - blue"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-green  "#7cd71c"
  "Primary colors - green"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-cyan "#1c99ec"
  "Primary colors - cyan"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-violet "#4c7282"
  "Primary colors - violet"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-gray "#171717"
  "Primary colors - gray"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-foreground "#c6ffff"
  "Adaptive colors - foreground"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-background  "#120909"
  "Adaptive colors - background"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-comments "#f4f4f4"
  "Adaptive colors - comments"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-emphasis "#ffe147"
  "Adaptive colors - emphasis"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-line-number "#171717"
  "Adaptive colors - line number"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-highlight "#1c99ec"
  "Adaptive colors - highlight"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-highlight-alt "#4eb4ff"
  "Adaptive colors - highlight"
  :type 'string
  :group 'monokaix)

(defcustom monokaix-highlight-line "#f4804c"
  "Adaptive colors - line highlight"
  :type 'string
  :group 'monokaix)

(let* (;; Variable pitch
       (monokaix-pitch (if monokaix-use-variable-pitch
                          'variable-pitch
                        'default))

       ;; Definitions for guis that support 256 colors
       (monokaix-class '((class color) (min-colors 257)))

       ;; Functionality specific colors
       (monokaix-diff-blue-base      "#597f8f")
       (monokaix-diff-blue-emphasis  "#7399a9")
       (monokaix-diff-green-base     "#9ff13e")
       (monokaix-diff-green-emphasis "#bdff5c")
       (monokaix-diff-red-base       "#f4b323")
       (monokaix-diff-red-emphasis   "#ffce43")

       ;; Darker and lighter accented colors
       (monokaix-yellow-d       "#dcd711")
       (monokaix-yellow-l       "#faf33c")
       (monokaix-orange-d      "#d56634")
       (monokaix-orange-l      "#ff9b65")
       (monokaix-red-d         "#e47340")
       (monokaix-red-l         "#ff8d58")
       (monokaix-magenta-d     "#a800af")
       (monokaix-magenta-l     "#c627cb")
       (monokaix-violet-d      "#9a00a2")
       (monokaix-violet-l      "#d53ad9")
       (monokaix-blue-d        "#1e5f65")
       (monokaix-blue-l        "#3b787e")
       (monokaix-cyan-d        "#40a1d2")
       (monokaix-cyan-l        "#61bcee")
       (monokaix-green-d       "#6dc900")
       (monokaix-green-l       "#8be530")
       (monokaix-gray-d        "#282828")
       (monokaix-gray-l        "#171717")
       ;; Adaptive higher/lower contrast accented colors
       (monokaix-foreground-hc  "#c6ffff")
       (monokaix-foreground-lc  "#aae5e8")
       ;; High contrast colors
       (monokaix-yellow-hc     "#ffef56")
       (monokaix-yellow-lc     "#f1d338")
       (monokaix-orange-hc     "#ffce43")
       (monokaix-orange-lc     "#f4b323")
       (monokaix-red-hc        "#ff9b65")
       (monokaix-red-lc        "#d56634")
       (monokaix-magenta-hc    "#d53ad9")
       (monokaix-magenta-lc    "#b70bbd")
       (monokaix-violet-hc     "#ff47ff")
       (monokaix-violet-lc     "#e715f7")
       (monokaix-blue-hc       "#7399a9")
       (monokaix-blue-lc       "#597f8f")
       (monokaix-cyan-hc       "#4eb4ff")
       (monokaix-cyan-lc       "#1c99ec")
       (monokaix-green-hc      "#bdff5c")
       (monokaix-green-lc      "#9ff13e")

       ;; Distinct fringe
       (monokaix-fringe-bg (if monokaix-distinct-fringe-background
                              monokaix-gray
                            monokaix-background))

       ;; Definitions for terminals that do not support 256 colors
       (monokaix-256-class '((class color) (min-colors 89)))

       ;; Functionality specific colors
       (monokaix-256-diff-blue-base      "#597f8f")
       (monokaix-256-diff-blue-emphasis  "#668c9c")
       (monokaix-256-diff-green-base     "#9ff13e")
       (monokaix-256-diff-green-emphasis "#aeff4d")
       (monokaix-256-diff-red-base       "#f4b323")
       (monokaix-256-diff-red-emphasis   "#ffc134")

       ;; Primary colors
       (monokaix-256-yellow         "#ebe52a")
       (monokaix-256-orange         "#ff8d58")
       (monokaix-256-red            "#f4804c")
       (monokaix-256-magenta        "#b70bbd")
       (monokaix-256-violet         "#a800af")
       (monokaix-256-blue           "#2d6b71")
       (monokaix-256-cyan           "#51aee0")
       (monokaix-256-green          "#7cd71c")
       (monokaix-256-gray           "#282828")
       (monokaix-256-yellow-d       "#dcd711")
       (monokaix-256-yellow-l       "#faf33c")
       (monokaix-256-orange-d       "#d56634")
       (monokaix-256-orange-l       "#ff9b65")
       (monokaix-256-red-d          "#e47340")
       (monokaix-256-red-l          "#ff9b65")
       (monokaix-256-magenta-d      "#a800af")
       (monokaix-256-magenta-l      "#c627cb")
       (monokaix-256-violet-d       "#9a00a2")
       (monokaix-256-violet-l       "#d53ad9")
       (monokaix-256-blue-d         "#1e5f65")
       (monokaix-256-blue-l         "#3b787e")
       (monokaix-256-cyan-d         "#40a1d2")
       (monokaix-256-cyan-l         "#61bcee")
       (monokaix-256-green-d        "#6dc900")
       (monokaix-256-green-l        "#8be530")
       (monokaix-256-gray-d         "#282828")
       (monokaix-256-gray-l         "#f4f4f4")
       (monokaix-256-foreground     "#b8f3f6")
       (monokaix-256-background     "#282828")
       (monokaix-256-comments       "#f4f4f4")
       (monokaix-256-emphasis       "#f4b323")
       (monokaix-256-line-number    "#282828")
       (monokaix-256-highlight      "#ebebeb")
       (monokaix-256-highlight-alt  "#f1f1f1")
       (monokaix-256-highlight-line "#717171")
       (monokaix-256-foreground-hc  "#c6ffff")
       (monokaix-256-foreground-lc  "#aae5e8")
       ;; High contrast colors
       (monokaix-256-yellow-hc      monokaix-256-yellow-d)
       (monokaix-256-yellow-lc      monokaix-256-yellow-l)
       (monokaix-256-orange-hc      monokaix-256-orange-d)
       (monokaix-256-orange-lc      monokaix-256-orange-l)
       (monokaix-256-red-hc         monokaix-256-red-d)
       (monokaix-256-red-lc         monokaix-256-red-l)
       (monokaix-256-magenta-hc     monokaix-256-magenta-d)
       (monokaix-256-magenta-lc     monokaix-256-magenta-l)
       (monokaix-256-violet-hc      monokaix-256-violet-d)
       (monokaix-256-violet-lc      monokaix-256-violet-l)
       (monokaix-256-blue-hc        monokaix-256-blue-d)
       (monokaix-256-blue-lc        monokaix-256-blue-l)
       (monokaix-256-cyan-hc        monokaix-256-cyan-d)
       (monokaix-256-cyan-lc        monokaix-256-cyan-l)
       (monokaix-256-green-hc       monokaix-256-green-d)
       (monokaix-256-green-lc       monokaix-256-green-l)

       ;; Distinct fringe
       (monokaix-256-fringe-bg (if monokaix-distinct-fringe-background
                                  monokaix-256-gray
                                monokaix-256-background)))

  ;; Define faces
  (custom-theme-set-faces
   'monokaix

   ;; font lock for syntax highlighting
   `(font-lock-builtin-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight normal))))

   `(font-lock-comment-delimiter-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(font-lock-comment-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(font-lock-constant-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(font-lock-doc-face
     ((,monokaix-class (:foreground ,(if monokaix-doc-face-as-comment
                                        monokaix-comments
                                      monokaix-yellow)))
      (,monokaix-256-class (:foreground ,(if monokaix-doc-face-as-comment
                                            monokaix-256-comments
                                          monokaix-256-yellow)))))

   `(font-lock-function-name-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(font-lock-keyword-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight normal))))

   `(font-lock-negation-char-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(font-lock-preprocessor-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(font-lock-regexp-grouping-construct
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal))))

   `(font-lock-regexp-grouping-backslash
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :weight normal))))

   `(font-lock-string-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(font-lock-type-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :italic nil))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :italic nil))))

   `(font-lock-variable-name-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(font-lock-warning-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold
                                   :italic t
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold
                                        :italic t
                                        :underline t))))

   `(c-annotation-face
     ((,monokaix-class (:inherit font-lock-constant-face))
      (,monokaix-256-class (:inherit font-lock-constant-face))))

   ;; general colouring
   '(button ((t (:underline t))))

   `(default
      ((,monokaix-class (:foreground ,monokaix-foreground
                                    :background ,monokaix-background))
       (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                         :background ,monokaix-256-background))))

   `(highlight
     ((,monokaix-class (:background ,monokaix-highlight))
      (,monokaix-256-class (:background ,monokaix-256-highlight))))

   `(lazy-highlight
     ((,monokaix-class (:inherit highlight
                                :background ,monokaix-highlight-alt))
      (,monokaix-256-class (:inherit highlight
                                     :background ,monokaix-256-highlight-alt))))

   `(region
     ((,monokaix-class (:inherit highlight
                                :background ,monokaix-highlight))
      (,monokaix-256-class (:inherit highlight
                                     :background ,monokaix-256-highlight))))

   `(secondary-selection
     ((,monokaix-class (:inherit region
                                :background ,monokaix-highlight-alt))
      (,monokaix-256-class (:inherit region
                                     :background ,monokaix-256-highlight-alt))))

   `(shadow
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(match
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background
                                        :weight bold))))

   `(cursor
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-foreground
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-foreground
                                        :inverse-video t))))

   `(mouse
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-foreground
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-foreground
                                        :inverse-video t))))

   `(escape-glyph
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(escape-glyph-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(fringe
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :background ,monokaix-fringe-bg))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :background ,monokaix-256-fringe-bg))))

   `(link
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline t
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline t
                                        :weight bold))))

   `(link-visited
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :underline t
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :underline t
                                        :weight normal))))

   `(success
     ((,monokaix-class (:foreground ,monokaix-green ))
      (,monokaix-256-class (:foreground ,monokaix-256-green ))))

   `(warning
     ((,monokaix-class (:foreground ,monokaix-yellow ))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow ))))

   `(error
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(eval-sexp-fu-flash
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-green))))

   `(eval-sexp-fu-flash-error
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-red))))

   `(trailing-whitespace
     ((,monokaix-class (:background ,monokaix-red))
      (,monokaix-256-class (:background ,monokaix-256-red))))

   `(vertical-border
     ((,monokaix-class (:foreground ,monokaix-gray))
      (,monokaix-256-class (:foreground ,monokaix-256-gray))))

   `(menu
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :background ,monokaix-256-background))))

   `(minibuffer-prompt
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   ;; mode-line and powerline
   `(mode-line-buffer-id
     ((,monokaix-class (:foreground , "#e0e7e6" 
                                   :weight bold))
      (,monokaix-256-class (:foreground , "#e0e7e6" 
                                        :weight bold))))

   `(mode-line
     ((,monokaix-class (:inverse-video unspecified
                                      :underline unspecified
                                      :foreground , "#120909"
                                      :background , "#f4804c"
                                     ;; :box (:line-width 1
                                     ;;                   :color ,monokaix-gray
                                     ;;                   :style unspecified)
                       ))
      (,monokaix-256-class (:inverse-video unspecified
                                           :underline unspecified
                                           :foreground ,"#120909"
                                           :background ,"#f4804c"
;;                                           :box (:line-width 1
;;                                                             :color ,monokaix-256-gray
;;                                                             :style unspecified)
                                           )
                           )))

   `(powerline-active1
     ((,monokaix-class (:background , "#f4b323"))
      (,monokaix-256-class (:background ,"#f4b323"))))

   `(powerline-active2
     ((,monokaix-class (:background , "#ffce43"))
      (,monokaix-256-class (:background , "#ffce43"))))


   `(mode-line-inactive
     ((,monokaix-class (:inverse-video unspecified
                                      :underline unspecified
                                      :foreground , "#171717"
                                      :background , "#717171"
                                      ;;:box (:line-width 1
                                      ;;                  :color ,monokaix-gray
                                      ;;                  :style unspecified)
                                      ))
      (,monokaix-256-class (:inverse-video unspecified
                                           :underline unspecified
                                           :foreground , "#171717"
                                           :background , "#717171"
                                           ;;:box (:line-width 1
                                           ;;                  :color ,monokaix-256-gray
                                           ;;                  :style unspecified)
                                           ))))

   `(powerline-inactive1
     ((,monokaix-class (:background , "#717171"))
      (,monokaix-256-class (:background ,"#f4f4f4"))))

   `(powerline-inactive2
     ((,monokaix-class (:background , "#f4f4f4"))
      (,monokaix-256-class (:background , "#f4f4f4"))))

   ;; header-line
   `(header-line
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-highlight
                                   :box (:color ,monokaix-gray
                                                :line-width 1
                                                :style unspecified)))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-highlight
                                        :box (:color ,monokaix-256-gray
                                                     :line-width 1
                                                     :style unspecified)))))

   ;; cua
   `(cua-global-mark
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-background))))

   `(cua-rectangle
     ((,monokaix-class (:inherit region))
      (,monokaix-256-class (:inherit region))))

   `(cua-rectangle-noselect
     ((,monokaix-class (:inherit secondary-selection))
      (,monokaix-256-class (:inherit secondary-selection))))

   ;; diary
   `(diary
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   ;; dired
   `(dired-directory
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(dired-flagged
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(dired-header
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background
                                        :inherit bold))))

   `(dired-ignored
     ((,monokaix-class (:inherit shadow))
      (,monokaix-256-class (:inherit shadow))))

   `(dired-mark
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   `(dired-marked
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :inherit bold))))

   `(dired-perm-write
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :underline t))))

   `(dired-symlink
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :slant italic))))

   `(dired-warning
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :underline t))))
;; > Elscreen
   `(elscreen-tab-background-face
     ((,monokaix-class ( :foreground, monokaix-foreground :background, monokaix-background :box , nil :underline, monokaix-gray))
(,monokaix-256-class ( :foreground, monokaix-256-foreground :background, monokaix-256-background :box , nil :underline, monokaix-256-gray))
      ))

      `(elscreen-tab-control-face
     ((,monokaix-class ( :foreground, monokaix-foreground :background, monokaix-background :box , nil :underline, monokaix-gray))
(,monokaix-256-class ( :foreground, monokaix-256-foreground :background, monokaix-256-background :box , nil :underline, monokaix-256-gray))
))
      
      `(elscreen-tab-other-screen-face
     ((,monokaix-class ( :foreground, monokaix-foreground :background, monokaix-background :box , nil :underline, monokaix-gray))
(,monokaix-256-class ( :foreground, monokaix-256-foreground :background, monokaix-256-background :box , nil :underline, monokaix-256-gray))
      ))
      `(elscreen-tab-current-screen-face
     ((,monokaix-class ( :foreground, monokaix-foreground :background, monokaix-gray :box , nil :underline, monokaix-gray))
(,monokaix-256-class ( :foreground, monokaix-256-foreground :background, monokaix-256-gray :box , nil :underline, monokaix-256-gray))
      ))

   ;; dropdown
   `(dropdown-list-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-blue))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-blue))))

   `(dropdown-list-selection-face
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background))))

   ;; ecb
   `(ecb-default-highlight-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(ecb-history-bucket-node-dir-soure-path-face
     ((,monokaix-class (:inherit ecb-history-bucket-node-face
                                :foreground ,monokaix-yellow))
      (,monokaix-256-class (:inherit ecb-history-bucket-node-face
                                     :foreground ,monokaix-256-yellow))))

   `(ecb-source-in-directories-buffer-face
     ((,monokaix-class (:inherit ecb-directories-general-face
                                :foreground ,monokaix-foreground))
      (,monokaix-256-class (:inherit ecb-directories-general-face
                                     :foreground ,monokaix-256-foreground))))

   `(ecb-history-dead-buffer-face
     ((,monokaix-class (:inherit ecb-history-general-face
                                :foreground ,monokaix-comments))
      (,monokaix-256-class (:inherit ecb-history-general-face
                                     :foreground ,monokaix-256-comments))))

   `(ecb-directory-not-accessible-face
     ((,monokaix-class (:inherit ecb-directories-general-face
                                :foreground ,monokaix-comments))
      (,monokaix-256-class (:inherit ecb-directories-general-face
                                     :foreground ,monokaix-256-comments))))

   `(ecb-bucket-node-face
     ((,monokaix-class (:inherit ecb-default-general-face
                                :weight normal
                                :foreground ,monokaix-blue))
      (,monokaix-256-class (:inherit ecb-default-general-face
                                     :weight normal
                                     :foreground ,monokaix-256-blue))))

   `(ecb-tag-header-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(ecb-analyse-bucket-element-face
     ((,monokaix-class (:inherit ecb-analyse-general-face
                                :foreground ,monokaix-green))
      (,monokaix-256-class (:inherit ecb-analyse-general-face
                                     :foreground ,monokaix-256-green))))

   `(ecb-directories-general-face
     ((,monokaix-class (:inherit ecb-default-general-face
                                :height 1.0))
      (,monokaix-256-class (:inherit ecb-default-general-face
                                     :height 1.0))))

   `(ecb-method-non-semantic-face
     ((,monokaix-class (:inherit ecb-methods-general-face
                                :foreground ,monokaix-cyan))
      (,monokaix-256-class (:inherit ecb-methods-general-face
                                     :foreground ,monokaix-256-cyan))))

   `(ecb-mode-line-prefix-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(ecb-tree-guide-line-face
     ((,monokaix-class (:inherit ecb-default-general-face
                                :foreground ,monokaix-gray
                                :height 1.0))
      (,monokaix-256-class (:inherit ecb-default-general-face
                                     :foreground ,monokaix-256-gray
                                     :height 1.0))))

   ;; ee
   `(ee-bookmarked
     ((,monokaix-class (:foreground ,monokaix-emphasis))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis))))

   `(ee-category
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(ee-link
     ((,monokaix-class (:inherit link))
      (,monokaix-256-class (:inherit link))))

   `(ee-link-visited
     ((,monokaix-class (:inherit link-visited))
      (,monokaix-256-class (:inherit link-visited))))

   `(ee-marked
     ((,monokaix-class (:foreground ,monokaix-magenta
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :weight bold))))

   `(ee-omitted
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(ee-shadow
     ((,monokaix-class (:inherit shadow))
      (,monokaix-256-class (:inherit shadow))))

   ;; grep
   `(grep-context-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(grep-error-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold
                                        :underline t))))

   `(grep-hit-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(grep-match-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   ;; isearch
   `(isearch
     ((,monokaix-class (:inherit region
                                :foreground ,monokaix-background
                                :background ,monokaix-yellow))
      (,monokaix-256-class (:inherit region
                                     :foreground ,monokaix-256-background
                                     :background ,monokaix-256-yellow))))

   `(isearch-fail
     ((,monokaix-class (:inherit isearch
                                :foreground ,monokaix-red
                                :background ,monokaix-background
                                :bold t))
      (,monokaix-256-class (:inherit isearch
                                     :foreground ,monokaix-256-red
                                     :background ,monokaix-256-background
                                     :bold t))))


   ;; ace-jump-mode
   `(ace-jump-face-background
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-background
                                   :inverse-video nil))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-background
                                        :inverse-video nil))))

   `(ace-jump-face-foreground
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background ,monokaix-background
                                   :inverse-video nil
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background ,monokaix-256-background
                                        :inverse-video nil
                                        :weight bold))))

   ;; auctex
   `(font-latex-bold-face
     ((,monokaix-class (:inherit bold
                                :foreground ,monokaix-emphasis))
      (,monokaix-256-class (:inherit bold
                                     :foreground ,monokaix-256-emphasis))))

   `(font-latex-doctex-documentation-face
     ((,monokaix-class (:background unspecified))
      (,monokaix-256-class (:background unspecified))))

   `(font-latex-doctex-preprocessor-face
     ((,monokaix-class
       (:inherit (font-latex-doctex-documentation-face
                  font-lock-builtin-face
                  font-lock-preprocessor-face)))
      (,monokaix-class
       (:inherit (font-latex-doctex-documentation-face
                  font-lock-builtin-face
                  font-lock-preprocessor-face)))))

   `(font-latex-italic-face
     ((,monokaix-class (:inherit italic :foreground ,monokaix-emphasis))
      (,monokaix-256-class (:inherit italic :foreground ,monokaix-256-emphasis))))

   `(font-latex-math-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(font-latex-sectioning-0-face
     ((,monokaix-class (:inherit font-latex-sectioning-1-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit font-latex-sectioning-1-face
                                     :height ,monokaix-height-plus-1))))

   `(font-latex-sectioning-1-face
     ((,monokaix-class (:inherit font-latex-sectioning-2-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit font-latex-sectioning-2-face
                                     :height ,monokaix-height-plus-1))))

   `(font-latex-sectioning-2-face
     ((,monokaix-class (:inherit font-latex-sectioning-3-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit font-latex-sectioning-3-face
                                     :height ,monokaix-height-plus-1))))

   `(font-latex-sectioning-3-face
     ((,monokaix-class (:inherit font-latex-sectioning-4-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit font-latex-sectioning-4-face
                                     :height ,monokaix-height-plus-1))))

   `(font-latex-sectioning-4-face
     ((,monokaix-class (:inherit font-latex-sectioning-5-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit font-latex-sectioning-5-face
                                     :height ,monokaix-height-plus-1))))

   `(font-latex-sectioning-5-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-yellow
                                :weight bold))
      (,monokaix-256-class (:inherit ,monokaix-pitch :
                                     foreground ,monokaix-256-yellow
                                     :weight bold))))

   `(font-latex-sedate-face
     ((,monokaix-class (:foreground ,monokaix-emphasis))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis))))

   `(font-latex-slide-title-face
     ((,monokaix-class (:inherit (,monokaix-pitch font-lock-type-face)
                                :weight bold
                                :height ,monokaix-height-plus-3))
      (,monokaix-256-class (:inherit (,monokaix-pitch font-lock-type-face)
                                     :weight bold
                                     :height ,monokaix-height-plus-3))))

   `(font-latex-string-face
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(font-latex-subscript-face
     ((,monokaix-class (:height ,monokaix-height-minus-1))
      (,monokaix-256-class (:height ,monokaix-height-minus-1))))

   `(font-latex-superscript-face
     ((,monokaix-class (:height ,monokaix-height-minus-1))
      (,monokaix-256-class (:height ,monokaix-height-minus-1))))

   `(font-latex-verbatim-face
     ((,monokaix-class (:inherit fixed-pitch
                                :foreground ,monokaix-foreground
                                :slant italic))
      (,monokaix-256-class (:inherit fixed-pitch
                                     :foreground ,monokaix-256-foreground
                                     :slant italic))))

   `(font-latex-warning-face
     ((,monokaix-class (:inherit bold
                                :foreground ,monokaix-orange))
      (,monokaix-256-class (:inherit bold
                                     :foreground ,monokaix-256-orange))))

   ;; auto-complete
   `(ac-candidate-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-blue))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-blue))))

   `(ac-selection-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(ac-candidate-mouse-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(ac-completion-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :underline t))))

   `(ac-gtags-candidate-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-blue))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-blue))))

   `(ac-gtags-selection-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(ac-yasnippet-candidate-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-yellow))))

   `(ac-yasnippet-selection-face
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-background))))

   ;; auto highlight symbol
   `(ahs-definition-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-blue))))

   `(ahs-edit-mode-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-highlight))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-highlight))))

   `(ahs-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :background unspecified))))

   `(ahs-plugin-bod-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-violet ))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-cyan ))))

   `(ahs-plugin-defalt-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-orange))))

   `(ahs-plugin-whole-buffer-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-green))))

   `(ahs-warning-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold))))

   ;; android mode
   `(android-mode-debug-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(android-mode-error-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold))))

   `(android-mode-info-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(android-mode-verbose-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(android-mode-warning-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   ;; anzu-mode
   `(anzu-mode-line
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :weight bold))))

   ;; bm
   `(bm-face
     ((,monokaix-class (:background ,monokaix-yellow-lc
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc
                                        :foreground ,monokaix-256-background))))

   `(bm-fringe-face
     ((,monokaix-class (:background ,monokaix-yellow-lc
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc
                                        :foreground ,monokaix-256-background))))

   `(bm-fringe-persistent-face
     ((,monokaix-class (:background ,monokaix-green-lc
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-green-lc
                                        :foreground ,monokaix-256-background))))

   `(bm-persistent-face
     ((,monokaix-class (:background ,monokaix-green-lc
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-green-lc
                                        :foreground ,monokaix-256-background))))

   ;; calfw
   `(cfw:face-day-title
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(cfw:face-annotation
     ((,monokaix-class (:inherit cfw:face-day-title
                                :foreground ,monokaix-yellow))
      (,monokaix-256-class (:inherit cfw:face-day-title
                                     :foreground ,monokaix-256-yellow))))

   `(cfw:face-default-content
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(cfw:face-default-day
     ((,monokaix-class (:inherit cfw:face-day-title
                                :weight bold))
      (,monokaix-256-class (:inherit cfw:face-day-title
                                     :weight bold))))

   `(cfw:face-disable
     ((,monokaix-class (:inherit cfw:face-day-title
                                :foreground ,monokaix-comments))
      (,monokaix-256-class (:inherit cfw:face-day-title
                                     :foreground ,monokaix-256-comments))))

   `(cfw:face-grid
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(cfw:face-header
     ((,monokaix-class (:foreground ,monokaix-blue-hc
                                   :background ,monokaix-blue-lc
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-hc
                                        :background ,monokaix-256-blue-lc
                                        :weight bold))))

   `(cfw:face-holiday
     ((,monokaix-class (:background nil
                                   :foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:background nil
                                        :foreground ,monokaix-256-red
                                        :weight bold))))

   `(cfw:face-periods
     ((,monokaix-class (:foreground ,monokaix-magenta))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta))))

   `(cfw:face-select
     ((,monokaix-class (:background ,monokaix-magenta-lc
                                   :foreground ,monokaix-magenta-hc))
      (,monokaix-256-class (:background ,monokaix-256-magenta-lc
                                        :foreground ,monokaix-256-magenta-hc))))

   `(cfw:face-saturday
     ((,monokaix-class (:foreground ,monokaix-cyan-hc
                                   :background ,monokaix-cyan-lc))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan-hc
                                        :background ,monokaix-256-cyan-lc))))

   `(cfw:face-sunday
     ((,monokaix-class (:foreground ,monokaix-red-hc
                                   :background ,monokaix-red-lc
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red-hc
                                        :background ,monokaix-256-red-lc
                                        :weight bold))))

   `(cfw:face-title
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-yellow
                                :weight bold
                                :height ,monokaix-height-plus-4))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-yellow
                                     :weight bold
                                     :height ,monokaix-height-plus-4))))

   `(cfw:face-today
     ((,monokaix-class (:weight bold
                               :background ,monokaix-highlight-line
                               :foreground nil))
      (,monokaix-256-class (:weight bold
                                    :background ,monokaix-256-highlight-line
                                    :foreground nil))))

   `(cfw:face-today-title
     ((,monokaix-class (:background ,monokaix-yellow-lc
                                   :foreground ,monokaix-yellow-hc
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc
                                        :foreground ,monokaix-256-yellow-hc
                                        :weight bold))))

   `(cfw:face-toolbar
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   `(cfw:face-toolbar-button-off
     ((,monokaix-class (:background ,monokaix-yellow-lc
                                   :foreground ,monokaix-yellow-hc
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc
                                        :foreground ,monokaix-256-yellow-hc
                                        :weight bold))))

   `(cfw:face-toolbar-button-on
     ((,monokaix-class (:background ,monokaix-yellow-hc
                                   :foreground ,monokaix-yellow-lc
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-yellow-hc
                                        :foreground ,monokaix-256-yellow-lc
                                        :weight bold))))

   ;; cider
   `(cider-enlightened
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background nil
                                   :box (:color ,monokaix-yellow :line-width -1 :style nil)))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background nil
                                        :box (:color ,monokaix-256-yellow :line-width -1 :style nil))) ))

   `(cider-enlightened-local
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(cider-instrumented-face
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :background nil
                                   :box (:color ,monokaix-violet :line-width -1 :style nil)))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :background nil
                                        :box (:color ,monokaix-256-violet :line-width -1 :style nil)))))

   `(cider-result-overlay-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background nil
                                   :box (:color ,monokaix-blue :line-width -1 :style nil)))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background nil
                                        :box (:color ,monokaix-256-blue :line-width -1 :style nil)))))

   `(cider-test-error-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-orange))))

   `(cider-test-failure-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-red))))

   `(cider-test-success-face
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-green))))

   `(cider-traced-face
     ((,monokaix-class :box (:color ,monokaix-blue :line-width -1 :style nil))
      (,monokaix-256-class  :box (:color ,monokaix-256-blue :line-width -1 :style nil))))

   ;; clojure-test
   `(clojure-test-failure-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold
                                        :underline t))))

   `(clojure-test-error-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold
                                        :underline t))))

   `(clojure-test-success-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold
                                        :underline t))))

   ;; company-mode
   `(company-tooltip
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis))))

   `(company-tooltip-selection
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(company-tooltip-mouse
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(company-tooltip-common
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline t))))

   `(company-tooltip-common-selection
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-blue
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-blue
                                        :underline t))))

   `(company-preview
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis))))

   `(company-preview-common
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline t))))

   `(company-scrollbar-bg
     ((,monokaix-class (:background ,monokaix-gray))
      (,monokaix-256-class (:background ,monokaix-256-gray))))

   `(company-scrollbar-fg
     ((,monokaix-class (:background ,monokaix-comments))
      (,monokaix-256-class (:background ,monokaix-256-comments))))

   `(company-tooltip-annotation
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-green))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-green))))

   `(company-template-field
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-blue))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-blue))))

   ;; compilation
   `(compilation-column-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :underline nil))))

   `(compilation-column-number
     ((,monokaix-class (:inherit font-lock-doc-face
                                :foreground ,monokaix-cyan
                                :underline nil))
      (,monokaix-256-class (:inherit font-lock-doc-face
                                     :foreground ,monokaix-256-cyan
                                     :underline nil))))

   `(compilation-enter-directory-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :underline nil))))

   `(compilation-error
     ((,monokaix-class (:inherit error
                                :underline nil))
      (,monokaix-256-class (:inherit error
                                     :underline nil))))

   `(compilation-error-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :underline nil))))

   `(compilation-face
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :underline nil))))

   `(compilation-info
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :underline nil
                                   :bold nil))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :underline nil
                                        :bold nil))))

   `(compilation-info-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline nil))))

   `(compilation-leave-directory-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :underline nil))))

   `(compilation-line-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :underline nil))))

   `(compilation-line-number
     ((,monokaix-class (:foreground ,monokaix-green
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :underline nil))))

   `(compilation-warning
     ((,monokaix-class (:inherit warning
                                :underline nil))
      (,monokaix-256-class (:inherit warning
                                     :underline nil))))

   `(compilation-warning-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal
                                        :underline nil))))

   `(compilation-mode-line-exit
     ((,monokaix-class (:inherit compilation-info
                                :foreground ,monokaix-green
                                :weight bold))
      (,monokaix-256-class (:inherit compilation-info
                                     :foreground ,monokaix-256-green
                                     :weight bold))))

   `(compilation-mode-line-fail
     ((,monokaix-class (:inherit compilation-error
                                :foreground ,monokaix-red
                                :weight bold))
      (,monokaix-256-class (:inherit compilation-error
                                     :foreground ,monokaix-256-red
                                     :weight bold))))

   `(compilation-mode-line-run
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold))))

   ;; CSCOPE
   `(cscope-file-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   `(cscope-function-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(cscope-line-number-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(cscope-line-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(cscope-mouse-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-foreground))))

   ;; ctable
   `(ctbl:face-cell-select
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :underline ,monokaix-emphasis
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :underline ,monokaix-256-emphasis
                                        :weight bold))))

   `(ctbl:face-continue-bar
     ((,monokaix-class (:background ,monokaix-gray
                                   :foreground ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-gray
                                        :foreground ,monokaix-256-yellow))))

   `(ctbl:face-row-select
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground
                                   :underline t))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground
                                        :underline t))))

   ;; coffee
   `(coffee-mode-class-name
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(coffee-mode-function-param
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :slant italic))))

   ;; custom
   `(custom-face-tag
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :height ,monokaix-height-plus-3
                                :foreground ,monokaix-violet
                                :weight bold))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :height ,monokaix-height-plus-3
                                     :foreground ,monokaix-256-violet
                                     :weight bold))))

   `(custom-variable-tag
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-cyan
                                :height ,monokaix-height-plus-3))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-cyan
                                     :height ,monokaix-height-plus-3))))

   `(custom-comment-tag
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(custom-group-tag
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-blue
                                :height ,monokaix-height-plus-3))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-blue
                                     :height ,monokaix-height-plus-3))))

   `(custom-group-tag-1
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-red
                                :height ,monokaix-height-plus-3))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-red
                                     :height ,monokaix-height-plus-3))))

   `(custom-state
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   ;; diff
   `(diff-added
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background))))

   `(diff-changed
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background))))

   `(diff-removed
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background))))

   `(diff-header
     ((,monokaix-class (:background ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-background))))

   `(diff-file-header
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground
                                        :weight bold))))

   `(diff-refine-added
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-green))))

   `(diff-refine-change
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-blue))))

   `(diff-refine-removed
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-red))))

   ;; diff-hl
   `(diff-hl-change
     ((,monokaix-class (:background ,monokaix-blue-lc
                                   :foreground ,monokaix-blue-hc))
      (,monokaix-256-class (:background ,monokaix-256-blue-lc
                                        :foreground ,monokaix-256-blue-hc))))

   `(diff-hl-delete
     ((,monokaix-class (:background ,monokaix-red-lc
                                   :foreground ,monokaix-red-hc))
      (,monokaix-256-class (:background ,monokaix-256-red-lc
                                        :foreground ,monokaix-256-red-hc))))

   `(diff-hl-insert
     ((,monokaix-class (:background ,monokaix-green-lc
                                   :foreground ,monokaix-green-hc))
      (,monokaix-256-class (:background ,monokaix-256-green-lc
                                        :foreground ,monokaix-256-green-hc))))

   `(diff-hl-unknown
     ((,monokaix-class (:background ,monokaix-violet-lc
                                   :foreground ,monokaix-violet-hc))
      (,monokaix-256-class (:background ,monokaix-256-violet-lc
                                        :foreground ,monokaix-256-violet-hc))))

   ;; ediff
   `(ediff-fine-diff-A
     ((,monokaix-class (:background ,monokaix-diff-red-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-diff-red-emphasis))))

   `(ediff-fine-diff-B
     ((,monokaix-class (:background ,monokaix-diff-green-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-diff-green-emphasis))))

   `(ediff-fine-diff-C
     ((,monokaix-class (:background ,monokaix-diff-blue-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-diff-blue-emphasis))))

   `(ediff-current-diff-A
     ((,monokaix-class (:background ,monokaix-diff-red-base))
      (,monokaix-256-class (:background ,monokaix-256-diff-red-base))))

   `(ediff-current-diff-B
     ((,monokaix-class (:background ,monokaix-diff-green-base))
      (,monokaix-256-class (:background ,monokaix-256-diff-green-base))))

   `(ediff-current-diff-C
     ((,monokaix-class (:background ,monokaix-diff-blue-base))
      (,monokaix-256-class (:background ,monokaix-256-diff-blue-base))))

   `(ediff-even-diff-A
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-foreground-lc ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-foreground-lc ))))

   `(ediff-odd-diff-A
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-foreground-hc ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-foreground-hc ))))

   `(ediff-even-diff-B
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-foreground-hc ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-foreground-hc ))))

   `(ediff-odd-diff-B
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-foreground-lc ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-foreground-lc ))))

   `(ediff-even-diff-C
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-foreground ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-foreground ))))

   `(ediff-odd-diff-C
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-background ))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-background ))))

   ;; edts
   `(edts-face-error-line
     ((,(append '((supports :underline (:style line))) monokaix-class)
       (:underline (:style line :color ,monokaix-red)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-red-hc
                                   :background ,monokaix-red-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style line))) monokaix-256-class )
       (:underline (:style line :color ,monokaix-256-red)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-red-hc
                                        :background ,monokaix-256-red-lc
                                        :weight bold
                                        :underline t))))

   `(edts-face-warning-line
     ((,(append '((supports :underline (:style line))) monokaix-class)
       (:underline (:style line :color ,monokaix-yellow)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-yellow-hc
                                   :background ,monokaix-yellow-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style line))) monokaix-256-class )
       (:underline (:style line :color ,monokaix-256-yellow)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow-hc
                                        :background ,monokaix-256-yellow-lc
                                        :weight bold
                                        :underline t))))

   `(edts-face-error-fringe-bitmap
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background unspecified
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background unspecified
                                        :weight bold))))

   `(edts-face-warning-fringe-bitmap
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background unspecified
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background unspecified
                                        :weight bold))))

   `(edts-face-error-mode-line
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground unspecified))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground unspecified))))

   `(edts-face-warning-mode-line
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground unspecified))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground unspecified))))


   ;; elfeed
   `(elfeed-search-date-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(elfeed-search-feed-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(elfeed-search-tag-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(elfeed-search-title-face
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   ;; elixir
   `(elixir-attribute-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(elixir-atom-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   ;; ein
   `(ein:cell-input-area
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))
   `(ein:cell-input-prompt
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))
   `(ein:cell-output-prompt
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))
   `(ein:notification-tab-normal
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))
   `(ein:notification-tab-selected
     ((,monokaix-class (:foreground ,monokaix-orange :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange :inherit bold))))

   ;; enhanced ruby mode
   `(enh-ruby-string-delimiter-face
     ((,monokaix-class (:inherit font-lock-string-face))
      (,monokaix-256-class (:inherit font-lock-string-face))))

   `(enh-ruby-heredoc-delimiter-face
     ((,monokaix-class (:inherit font-lock-string-face))
      (,monokaix-256-class (:inherit font-lock-string-face))))

   `(enh-ruby-regexp-delimiter-face
     ((,monokaix-class (:inherit font-lock-string-face))
      (,monokaix-256-class (:inherit font-lock-string-face))))

   `(enh-ruby-op-face
     ((,monokaix-class (:inherit font-lock-keyword-face))
      (,monokaix-256-class (:inherit font-lock-keyword-face))))

   ;; erm-syn
   `(erm-syn-errline
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-red)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-red-hc
                                   :background ,monokaix-red-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-red)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-red-hc
                                        :background ,monokaix-256-red-lc
                                        :weight bold
                                        :underline t))))

   `(erm-syn-warnline
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-orange)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-orange-hc
                                   :background ,monokaix-orange-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-orange)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-orange-hc
                                        :background ,monokaix-256-orange-lc
                                        :weight bold
                                        :underline t))))

   ;; epc
   `(epc:face-title
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-background
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :underline nil))))

   ;; erc
   `(erc-action-face
     ((,monokaix-class (:inherit erc-default-face))
      (,monokaix-256-class (:inherit erc-default-face))))

   `(erc-bold-face
     ((,monokaix-class (:weight bold))
      (,monokaix-256-class (:weight bold))))

   `(erc-current-nick-face
     ((,monokaix-class (:foreground ,monokaix-blue :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight bold))))

   `(erc-dangerous-host-face
     ((,monokaix-class (:inherit font-lock-warning-face))
      (,monokaix-256-class (:inherit font-lock-warning-face))))

   `(erc-default-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(erc-highlight-face
     ((,monokaix-class (:inherit erc-default-face
                                :background ,monokaix-highlight))
      (,monokaix-256-class (:inherit erc-default-face
                                     :background ,monokaix-256-highlight))))

   `(erc-direct-msg-face
     ((,monokaix-class (:inherit erc-default-face))
      (,monokaix-256-class (:inherit erc-default-face))))

   `(erc-error-face
     ((,monokaix-class (:inherit font-lock-warning-face))
      (,monokaix-256-class (:inherit font-lock-warning-face))))

   `(erc-fool-face
     ((,monokaix-class (:inherit erc-default-face))
      (,monokaix-256-class (:inherit erc-default-face))))

   `(erc-input-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(erc-keyword-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight bold))))

   `(erc-nick-default-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(erc-my-nick-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold))))

   `(erc-nick-msg-face
     ((,monokaix-class (:inherit erc-default-face))
      (,monokaix-256-class (:inherit erc-default-face))))

   `(erc-notice-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(erc-pal-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold))))

   `(erc-prompt-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :background ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :background ,monokaix-256-background
                                        :weight bold))))

   `(erc-timestamp-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(erc-underline-face
     ((t (:underline t))))

   ;; eshell
   `(eshell-prompt
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :inherit bold))))

   `(eshell-ls-archive
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :inherit bold))))

   `(eshell-ls-backup
     ((,monokaix-class (:inherit font-lock-comment-face))
      (,monokaix-256-class (:inherit font-lock-comment-face))))

   `(eshell-ls-clutter
     ((,monokaix-class (:inherit font-lock-comment-face))
      (,monokaix-256-class (:inherit font-lock-comment-face))))

   `(eshell-ls-directory
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :inherit bold))))

   `(eshell-ls-executable
     ((,monokaix-class (:foreground ,monokaix-green
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :inherit bold))))

   `(eshell-ls-unreadable
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(eshell-ls-missing
     ((,monokaix-class (:inherit font-lock-warning-face))
      (,monokaix-256-class (:inherit font-lock-warning-face))))

   `(eshell-ls-product
     ((,monokaix-class (:inherit font-lock-doc-face))
      (,monokaix-256-class (:inherit font-lock-doc-face))))

   `(eshell-ls-special
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :inherit bold))))

   `(eshell-ls-symlink
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :inherit bold))))

   ;; evil-ex-substitute
   `(evil-ex-substitute-matches
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-red-l
                                   :inherit italic))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-red-l
                                        :inherit italic))))
   `(evil-ex-substitute-replacement
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-green-l
                                   :inherit italic))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line :foreground ,monokaix-256-green-l :inherit italic))))

   ;; evil-search-highlight-persist
   `(evil-search-highlight-persist-highlight-face
     ((,monokaix-class (:inherit region))
      (,monokaix-256-class (:inherit region))))

   ;; fic
   `(fic-author-face
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-orange
                                   :underline t
                                   :slant italic))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-orange
                                        :underline t
                                        :slant italic))))

   `(fic-face
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-orange
                                   :weight normal
                                   :slant italic))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-orange
                                        :weight normal
                                        :slant italic))))

   `(font-lock-fic-face
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-orange
                                   :weight normal
                                   :slant italic))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-orange
                                        :weight normal
                                        :slant italic))))

   ;; flx
   `(flx-highlight-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight normal
                                        :underline nil))))

   ;; flymake
   `(flymake-errline
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-red)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-class (:foreground ,monokaix-red-hc
                                   :background ,monokaix-red-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-red)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-red-hc
                                        :background ,monokaix-256-red-lc
                                        :weight bold
                                        :underline t))))

   `(flymake-infoline
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-green)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-class (:foreground ,monokaix-green-hc
                                   :background ,monokaix-green-lc))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-green)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-green-hc
                                        :background ,monokaix-256-green-lc))))

   `(flymake-warnline
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-yellow)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-class (:foreground ,monokaix-yellow-hc
                                   :background ,monokaix-yellow-lc
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-yellow)
                   :inherit unspecified
                   :foreground unspecified
                   :background unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow-hc
                                        :background ,monokaix-256-yellow-lc
                                        :weight bold
                                        :underline t))))

   ;; flycheck
   `(flycheck-error
     ((,(append '((supports :underline (:style line))) monokaix-class)
       (:underline (:style line :color ,monokaix-red)))
      (,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style line))) monokaix-256-class )
       (:underline (:style line :color ,monokaix-256-red)))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :weight bold
                                        :underline t))))

   `(flycheck-warning
     ((,(append '((supports :underline (:style line))) monokaix-class)
       (:underline (:style line :color ,monokaix-orange)))
      (,monokaix-class (:foreground ,monokaix-orange
                                   :background ,monokaix-background
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style line))) monokaix-256-class )
       (:underline (:style line :color ,monokaix-256-orange)))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :background ,monokaix-256-background
                                        :weight bold
                                        :underline t))))

   `(flycheck-info
     ((,(append '((supports :underline (:style line))) monokaix-class)
       (:underline (:style line :color ,monokaix-blue)))
      (,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-background
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style line))) monokaix-256-class )
       (:underline (:style line :color ,monokaix-256-blue)))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background
                                        :weight bold
                                        :underline t))))

   `(flycheck-fringe-error
     ((,monokaix-class (:foreground ,monokaix-red-l
                                   :background unspecified
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red-l
                                        :background unspecified
                                        :weight bold))))

   `(flycheck-fringe-warning
     ((,monokaix-class (:foreground ,monokaix-orange-l
                                   :background unspecified
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange-l
                                        :background unspecified
                                        :weight bold))))

   `(flycheck-fringe-info
     ((,monokaix-class (:foreground ,monokaix-blue-l
                                   :background unspecified
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-l
                                        :background unspecified
                                        :weight bold))))

   ;; flyspell
   `(flyspell-duplicate
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-yellow)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-yellow)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold
                                        :underline t))))

   `(flyspell-incorrect
     ((,(append '((supports :underline (:style wave))) monokaix-class)
       (:underline (:style wave :color ,monokaix-red)
                   :inherit unspecified))
      (,monokaix-class (:foreground ,monokaix-red
                                   :weight bold
                                   :underline t))
      (,(append '((supports :underline (:style wave))) monokaix-256-class )
       (:underline (:style wave :color ,monokaix-256-red)
                   :inherit unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold
                                        :underline t))))


   ;; git-gutter
   `(git-gutter:added
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter:deleted
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter:modified
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter:unchanged
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   ;; git-gutter-fr
   `(git-gutter-fr:added
     ((,monokaix-class (:foreground ,monokaix-green
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :inherit bold))))

   `(git-gutter-fr:deleted
     ((,monokaix-class (:foreground ,monokaix-red
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :inherit bold))))

   `(git-gutter-fr:modified
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :inherit bold))))

   ;; git-gutter+ and git-gutter+-fr
   `(git-gutter+-added
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter+-deleted
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter+-modified
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter+-unchanged
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-background
                                   :inherit bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-background
                                        :inherit bold))))

   `(git-gutter-fr+-added
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   `(git-gutter-fr+-deleted
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold))))

   `(git-gutter-fr+-modified
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight bold))))

   ;; git-timemachine
   `(git-timemachine-minibuffer-detail-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-highlight-line
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-blue
                                        :background ,monokaix-256-highlight-line
                                        :inherit bold))))

   ;; guide-key
   `(guide-key/highlight-command-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(guide-key/key-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(guide-key/prefix-command-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   ;; gnus
   `(gnus-group-mail-1
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-mail-1-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-mail-1-empty))))

   `(gnus-group-mail-1-empty
     ((,monokaix-class (:inherit gnus-group-news-1-empty))
      (,monokaix-256-class (:inherit gnus-group-news-1-empty))))

   `(gnus-group-mail-2
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-mail-2-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-mail-2-empty))))

   `(gnus-group-mail-2-empty
     ((,monokaix-class (:inherit gnus-group-news-2-empty))
      (,monokaix-256-class (:inherit gnus-group-news-2-empty))))

   `(gnus-group-mail-3
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-mail-3-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-mail-3-empty))))

   `(gnus-group-mail-3-empty
     ((,monokaix-class (:inherit gnus-group-news-3-empty))
      (,monokaix-256-class (:inherit gnus-group-news-3-empty))))

   `(gnus-group-mail-low
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-mail-low-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-mail-low-empty))))

   `(gnus-group-mail-low-empty
     ((,monokaix-class (:inherit gnus-group-news-low-empty))
      (,monokaix-256-class (:inherit gnus-group-news-low-empty))))

   `(gnus-group-news-1
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-1-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-1-empty))))

   `(gnus-group-news-2
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-2-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-2-empty))))

   `(gnus-group-news-3
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-3-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-3-empty))))

   `(gnus-group-news-4
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-4-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-4-empty))))

   `(gnus-group-news-5
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-5-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-5-empty))))

   `(gnus-group-news-6
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-6-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-6-empty))))

   `(gnus-group-news-low
     ((,monokaix-class (:weight bold
                               :inherit gnus-group-news-low-empty))
      (,monokaix-256-class (:weight bold
                                    :inherit gnus-group-news-low-empty))))

   `(gnus-header-content
     ((,monokaix-class (:inherit message-header-other))
      (,monokaix-256-class (:inherit message-header-other))))

   `(gnus-header-from
     ((,monokaix-class (:inherit message-header-other))
      (,monokaix-256-class (:inherit message-header-other))))

   `(gnus-header-name
     ((,monokaix-class (:inherit message-header-name))
      (,monokaix-256-class (:inherit message-header-name))))

   `(gnus-header-newsgroups
     ((,monokaix-class (:inherit message-header-other))
      (,monokaix-256-class (:inherit message-header-other))))

   `(gnus-header-subject
     ((,monokaix-class (:inherit message-header-subject))
      (,monokaix-256-class (:inherit message-header-subject))))

   `(gnus-summary-cancelled
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(gnus-summary-high-ancient
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight bold))))

   `(gnus-summary-high-read
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   `(gnus-summary-high-ticked
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold))))

   `(gnus-summary-high-unread
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight bold))))

   `(gnus-summary-low-ancient
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-summary-low-read
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-summary-low-ticked
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(gnus-summary-low-unread
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(gnus-summary-normal-ancient
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-summary-normal-read
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-summary-normal-ticked
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(gnus-summary-normal-unread
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(gnus-summary-selected
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(gnus-cite-1
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-cite-2
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-cite-3
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-cite-4
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-cite-5
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-cite-6
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-cite-7
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(gnus-cite-8
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(gnus-cite-9
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(gnus-cite-10
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(gnus-cite-11
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(gnus-group-news-1-empty
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(gnus-group-news-2-empty
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-group-news-3-empty
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(gnus-group-news-4-empty
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-group-news-5-empty
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(gnus-group-news-6-empty
     ((,monokaix-class (:foreground ,monokaix-blue-lc))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-lc))))

   `(gnus-group-news-low-empty
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(gnus-signature
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(gnus-x-face
     ((,monokaix-class (:background ,monokaix-foreground
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-foreground
                                        :foreground ,monokaix-256-background))))


   ;; helm
   `(helm-apt-deinstalled
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(helm-apt-installed
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-bookmark-directory
     ((,monokaix-class (:inherit helm-ff-directory))
      (,monokaix-256-class (:inherit helm-ff-directory))))

   `(helm-bookmark-file
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(helm-bookmark-gnus
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(helm-bookmark-info
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-bookmark-man
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(helm-bookmark-w3m
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(helm-bookmarks-su
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(helm-buffer-file
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(helm-buffer-directory
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(helm-buffer-process
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(helm-buffer-saved-out
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :inverse-video t))))

   `(helm-buffer-size
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(helm-candidate-number
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :bold t))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :bold t))))

   `(helm-ff-directory
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(helm-ff-executable
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-ff-file
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground))))

   `(helm-ff-invalid-symlink
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-orange
                                   :slant italic))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-orange
                                        :slant italic))))

   `(helm-ff-prefix
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background))))

   `(helm-ff-symlink
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(helm-grep-file
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :underline t))))

   `(helm-grep-finish
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-grep-lineno
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(helm-grep-match
     ((,monokaix-class (:inherit helm-match)))
     ((,monokaix-256-class (:inherit helm-match))))

   `(helm-grep-running
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(helm-header
     ((,monokaix-class (:inherit header-line))
      (,monokaix-256-class (:inherit terminal-header-line))))

   `(helm-lisp-completion-info
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(helm-lisp-show-completion
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background ,monokaix-highlight-line
                                   :bold t))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background ,monokaix-256-highlight-line
                                        :bold t))))

   `(helm-M-x-key
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :underline t))))

   `(helm-moccur-buffer
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :underline t))))

   `(helm-match
     ((,monokaix-class (:foreground ,monokaix-green :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green :inherit bold))))

   `(helm-match-item
     ((,monokaix-class (:inherit helm-match))
      (,monokaix-256-class (:inherit helm-match))))

   `(helm-selection
     ((,monokaix-class (:background ,monokaix-highlight
                                   :inherit bold
                                   :underline nil))
      (,monokaix-256-class (:background ,monokaix-256-highlight
                                        :inherit bold
                                        :underline nil))))

   `(helm-selection-line
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :underline nil))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :underline nil))))

   `(helm-separator
     ((,monokaix-class (:foreground ,monokaix-gray))
      (,monokaix-256-class (:foreground ,monokaix-256-gray))))

   `(helm-source-header
     ((,monokaix-class (:background ,monokaix-violet-l
                                   :foreground ,monokaix-background
                                   :underline nil))
      (,monokaix-256-class (:background ,monokaix-256-violet-l
                                        :foreground ,monokaix-256-background
                                        :underline nil))))

   `(helm-swoop-target-line-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(helm-swoop-target-line-block-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(helm-swoop-target-word-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-time-zone-current
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(helm-time-zone-home
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(helm-visible-mark
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-magenta :bold t))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-magenta :bold t))))

   ;; helm-ls-git
   `(helm-ls-git-modified-not-staged-face
     ((,monokaix-class :foreground ,monokaix-blue)
      (,monokaix-256-class  :foreground ,monokaix-256-blue)))

   `(helm-ls-git-modified-and-staged-face
     ((,monokaix-class :foreground ,monokaix-blue-l)
      (,monokaix-256-class  :foreground ,monokaix-256-blue-l)))

   `(helm-ls-git-renamed-modified-face
     ((,monokaix-class :foreground ,monokaix-blue-l)
      (,monokaix-256-class  :foreground ,monokaix-256-blue-l)))

   `(helm-ls-git-untracked-face
     ((,monokaix-class :foreground ,monokaix-orange)
      (,monokaix-256-class  :foreground ,monokaix-256-orange)))

   `(helm-ls-git-added-copied-face
     ((,monokaix-class :foreground ,monokaix-green)
      (,monokaix-256-class  :foreground ,monokaix-256-green)))

   `(helm-ls-git-added-modified-face
     ((,monokaix-class :foreground ,monokaix-green-l)
      (,monokaix-256-class  :foreground ,monokaix-256-green-l)))

   `(helm-ls-git-deleted-not-staged-face
     ((,monokaix-class :foreground ,monokaix-red)
      (,monokaix-256-class  :foreground ,monokaix-256-red)))

   `(helm-ls-git-deleted-and-staged-face
     ((,monokaix-class :foreground ,monokaix-red-l)
      (,monokaix-256-class  :foreground ,monokaix-256-red-l)))

   `(helm-ls-git-conflict-face
     ((,monokaix-class :foreground ,monokaix-yellow)
      (,monokaix-256-class  :foreground ,monokaix-256-yellow)))

   ;; hi-lock-mode
   `(hi-yellow
     ((,monokaix-class (:foreground ,monokaix-yellow-lc
                                   :background ,monokaix-yellow-hc))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow-lc
                                        :background ,monokaix-256-yellow-hc))))

   `(hi-pink
     ((,monokaix-class (:foreground ,monokaix-magenta-lc
                                   :background ,monokaix-magenta-hc))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta-lc
                                        :background ,monokaix-256-magenta-hc))))

   `(hi-green
     ((,monokaix-class (:foreground ,monokaix-green-lc
                                   :background ,monokaix-green-hc))
      (,monokaix-256-class (:foreground ,monokaix-256-green-lc
                                        :background ,monokaix-256-green-hc))))

   `(hi-blue
     ((,monokaix-class (:foreground ,monokaix-blue-lc
                                   :background ,monokaix-blue-hc))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-lc
                                        :background ,monokaix-256-blue-hc))))

   `(hi-black-b
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background
                                        :weight bold))))

   `(hi-blue-b
     ((,monokaix-class (:foreground ,monokaix-blue-lc
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-lc
                                        :weight bold))))

   `(hi-green-b
     ((,monokaix-class (:foreground ,monokaix-green-lc
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green-lc
                                        :weight bold))))

   `(hi-red-b
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))))

   `(hi-black-hb
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background
                                        :weight bold))))

   ;; highlight-changes
   `(highlight-changes
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(highlight-changes-delete
     ((,monokaix-class (:foreground ,monokaix-red
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :underline t))))

   ;; highlight-indentation
   `(highlight-indentation-face
     ((,monokaix-class (:background ,monokaix-gray))
      (,monokaix-256-class (:background ,monokaix-256-gray))))

   `(highlight-indentation-current-column-face
     ((,monokaix-class (:background ,monokaix-gray))
      (,monokaix-256-class (:background ,monokaix-256-gray))))

   ;; highlight-symbol
   `(highlight-symbol-face
     ((,monokaix-class (:background ,monokaix-highlight))
      (,monokaix-256-class (:background ,monokaix-256-highlight))))

   ;; hl-line-mode
   `(hl-line
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(hl-line-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   ;; ido-mode
   `(ido-first-match
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal))))

   `(ido-only-match
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-yellow
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-yellow
                                        :weight normal))))

   `(ido-subdir
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(ido-incomplete-regexp
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold ))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold ))))

   `(ido-indicator
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-background
                                   :width condensed))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-background
                                        :width condensed))))

   `(ido-virtual
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   ;; info
   `(info-header-xref
     ((,monokaix-class (:foreground ,monokaix-green
                                   :inherit bold
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :inherit bold
                                        :underline t))))

   `(info-menu
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(info-node
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :inherit bold))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :inherit bold))))

   `(info-quoted-name
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(info-reference-item
     ((,monokaix-class (:background nil
                                   :underline t
                                   :inherit bold))
      (,monokaix-256-class (:background nil
                                        :underline t
                                        :inherit bold))))

   `(info-string
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(info-title-1
     ((,monokaix-class (:height ,monokaix-height-plus-4))
      (,monokaix-256-class (:height ,monokaix-height-plus-4))))

   `(info-title-2
     ((,monokaix-class (:height ,monokaix-height-plus-3))
      (,monokaix-256-class (:height ,monokaix-height-plus-3))))

   `(info-title-3
     ((,monokaix-class (:height ,monokaix-height-plus-2))
      (,monokaix-256-class (:height ,monokaix-height-plus-2))))

   `(info-title-4
     ((,monokaix-class (:height ,monokaix-height-plus-1))
      (,monokaix-256-class (:height ,monokaix-height-plus-1))))

   ;; ivy
   `(ivy-current-match
     ((,monokaix-class (:background ,monokaix-gray :inherit bold))
      (,monokaix-256-class (:background ,monokaix-gray-l :inherit bold))))

   `(ivy-minibuffer-match-face-1
     ((,monokaix-class (:inherit bold))
      (,monokaix-256-class (:inherit bold))))

   `(ivy-minibuffer-match-face-2
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :underline t))))

   `(ivy-minibuffer-match-face-3
     ((,monokaix-class (:foreground ,monokaix-green
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :underline t))))

   `(ivy-minibuffer-match-face-4
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :underline t))))

   `(ivy-remote
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(swiper-line-face
     ((,monokaix-class (:background ,monokaix-highlight-line))))

   `(swiper-match-face-1
     ((,monokaix-class (:background ,monokaix-gray-d))))

   `(swiper-match-face-2
     ((,monokaix-class (:background ,monokaix-green))))

   `(swiper-match-face-3
     ((,monokaix-class (:background ,monokaix-orange))))

   `(swiper-match-face-4
     ((,monokaix-class (:background ,monokaix-magenta))))

   ;; jabber
   `(jabber-activity-face
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-red))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-red))))

   `(jabber-activity-personal-face
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-blue))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-blue))))

   `(jabber-chat-error
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-red))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-red))))

   `(jabber-chat-prompt-foreign
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-red))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-red))))

   `(jabber-chat-prompt-local
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-blue))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-blue))))

   `(jabber-chat-prompt-system
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-green))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-green))))

   `(jabber-chat-text-foreign
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(jabber-chat-text-local
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(jabber-chat-rare-time-face
     ((,monokaix-class (:underline t
                                  :foreground ,monokaix-green))
      (,monokaix-256-class (:underline t
                                       :foreground ,monokaix-256-green))))

   `(jabber-roster-user-away
     ((,monokaix-class (:slant italic
                              :foreground ,monokaix-green))
      (,monokaix-256-class (:slant italic
                                   :foreground ,monokaix-256-green))))

   `(jabber-roster-user-chatty
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-orange))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-orange))))

   `(jabber-roster-user-dnd
     ((,monokaix-class (:slant italic
                              :foreground ,monokaix-red))
      (,monokaix-256-class (:slant italic
                                   :foreground ,monokaix-256-red))))

   `(jabber-roster-user-error
     ((,monokaix-class (:weight light
                               :slant italic
                               :foreground ,monokaix-red))
      (,monokaix-256-class (:weight light
                                    :slant italic
                                    :foreground ,monokaix-256-red))))

   `(jabber-roster-user-offline
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(jabber-roster-user-online
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-blue))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-blue))))

   `(jabber-roster-user-xa
     ((,monokaix-class (:slant italic
                              :foreground ,monokaix-magenta))
      (,monokaix-256-class (:slant italic
                                   :foreground ,monokaix-256-magenta))))

   ;; js2-mode colors
   `(js2-error
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(js2-external-variable
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(js2-function-call
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(js2-function-param
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(js2-instance-member
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(js2-jsdoc-html-tag-delimiter
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(js2-jsdoc-html-tag-name
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(js2-jsdoc-tag
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(js2-jsdoc-type
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(js2-jsdoc-value
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(js2-magic-paren
     ((,monokaix-class (:underline t))
      (,monokaix-256-class (:underline t))))

   `(js2-object-property
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(js2-private-function-call
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(js2-private-member
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(js2-warning
     ((,monokaix-class (:underline ,monokaix-orange))
      (,monokaix-256-class (:underline ,monokaix-256-orange))))

   ;; jedi
   `(jedi:highlight-function-argument
     ((,monokaix-class (:inherit bold))
      (,monokaix-256-class (:inherit bold))))

   ;; linum-mode
   `(linum
     ((,monokaix-class (:foreground ,monokaix-line-number
                                   :background ,monokaix-fringe-bg
                                   :inherit default
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-line-number
                                        :background ,monokaix-256-fringe-bg
                                        :inherit default
                                        :underline nil))))

   ;; line-number (>= Emacs26)
   `(line-number
     ((,monokaix-class (:foreground ,monokaix-line-number
                                   :background ,monokaix-fringe-bg
                                   :inherit default
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-line-number
                                        :background ,monokaix-256-fringe-bg
                                        :inherit default
                                        :underline nil))))
   `(line-number-current-line
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :background ,monokaix-fringe-bg
                                   :inherit default
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :background ,monokaix-256-fringe-bg
                                        :inherit default
                                        :underline nil))))

   ;; linum-relative-current-face
   `(linum-relative-current-face
     ((,monokaix-class (:foreground ,monokaix-line-number
                                   :background ,monokaix-highlight-line
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-line-number
                                        :background ,monokaix-256-highlight-line
                                        :underline nil))))

   ;; lusty-explorer
   `(lusty-directory-face
     ((,monokaix-class (:inherit dimonokaix-red-directory))
      (,monokaix-256-class (:inherit dimonokaix-red-directory))))

   `(lusty-file-face
     ((,monokaix-class nil)
      (,monokaix-256-class  nil)))

   `(lusty-match-face
     ((,monokaix-class (:inherit ido-first-match))
      (,monokaix-256-class (:inherit ido-first-match))))

   `(lusty-slash-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :weight bold))))

   ;; magit
   ;;
   ;; TODO: Add supports for all magit faces
   ;; https://github.com/magit/magit/search?utf8=%E2%9C%93&q=face
   ;;
   `(magit-diff-added
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background))))

   `(magit-diff-added-highlight
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-highlight-line))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-highlight-line))))

   `(magit-diff-removed
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background))))

   `(magit-diff-removed-highlight
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-highlight-line))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-highlight-line))))

   `(magit-section-title
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(magit-branch
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight bold))))

   `(magit-item-highlight
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :weight unspecified))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :weight unspecified))))

   `(magit-log-author
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(magit-log-graph
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(magit-log-head-label-bisect-bad
     ((,monokaix-class (:background ,monokaix-red-hc
                                   :foreground ,monokaix-red-lc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-red-hc
                                        :foreground ,monokaix-256-red-lc
                                        :box 1))))

   `(magit-log-head-label-bisect-good
     ((,monokaix-class (:background ,monokaix-green-hc
                                   :foreground ,monokaix-green-lc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-green-hc
                                        :foreground ,monokaix-256-green-lc
                                        :box 1))))

   `(magit-log-head-label-default
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :box 1))))

   `(magit-log-head-label-local
     ((,monokaix-class (:background ,monokaix-blue-lc
                                   :foreground ,monokaix-blue-hc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-blue-lc
                                        :foreground ,monokaix-256-blue-hc
                                        :box 1))))

   `(magit-log-head-label-patches
     ((,monokaix-class (:background ,monokaix-red-lc
                                   :foreground ,monokaix-red-hc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-red-lc
                                        :foreground ,monokaix-256-red-hc
                                        :box 1))))

   `(magit-log-head-label-remote
     ((,monokaix-class (:background ,monokaix-green-lc
                                   :foreground ,monokaix-green-hc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-green-lc
                                        :foreground ,monokaix-256-green-hc
                                        :box 1))))

   `(magit-log-head-label-tags
     ((,monokaix-class (:background ,monokaix-yellow-lc
                                   :foreground ,monokaix-yellow-hc
                                   :box 1))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc
                                        :foreground ,monokaix-256-yellow-hc
                                        :box 1))))

   `(magit-log-sha1
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   ;; man
   `(Man-overstrike
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight bold))))

   `(Man-reverse
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(Man-underline
     ((,monokaix-class (:foreground ,monokaix-green :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-green :underline t))))

   ;; monky
   `(monky-section-title
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(monky-diff-add
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(monky-diff-del
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; markdown-mode
   `(markdown-header-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(markdown-header-face-1
     ((,monokaix-class (:inherit markdown-header-face
                                :height ,monokaix-height-plus-4))
      (,monokaix-256-class (:inherit markdown-header-face
                                     :height ,monokaix-height-plus-4))))

   `(markdown-header-face-2
     ((,monokaix-class (:inherit markdown-header-face
                                :height ,monokaix-height-plus-3))
      (,monokaix-256-class (:inherit markdown-header-face
                                     :height ,monokaix-height-plus-3))))

   `(markdown-header-face-3
     ((,monokaix-class (:inherit markdown-header-face
                                :height ,monokaix-height-plus-2))
      (,monokaix-256-class (:inherit markdown-header-face
                                     :height ,monokaix-height-plus-2))))

   `(markdown-header-face-4
     ((,monokaix-class (:inherit markdown-header-face
                                :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:inherit markdown-header-face
                                     :height ,monokaix-height-plus-1))))

   `(markdown-header-face-5
     ((,monokaix-class (:inherit markdown-header-face))
      (,monokaix-256-class (:inherit markdown-header-face))))

   `(markdown-header-face-6
     ((,monokaix-class (:inherit markdown-header-face))
      (,monokaix-256-class (:inherit markdown-header-face))))

   ;; message-mode
   `(message-cited-text
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(message-header-name
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(message-header-other
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight normal))))

   `(message-header-to
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight normal))))

   `(message-header-cc
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight normal))))

   `(message-header-newsgroups
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(message-header-subject
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :weight normal))))

   `(message-header-xheader
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(message-mml
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(message-separator
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :slant italic))))

   ;; mew
   `(mew-face-header-subject
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(mew-face-header-from
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(mew-face-header-date
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-header-to
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-header-key
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-header-private
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-header-important
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(mew-face-header-marginal
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight bold))))

   `(mew-face-header-warning
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-header-xmew
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-header-xmew-bad
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-body-url
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(mew-face-body-comment
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :slant italic))))

   `(mew-face-body-cite1
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-body-cite2
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(mew-face-body-cite3
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(mew-face-body-cite4
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(mew-face-body-cite5
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-mark-review
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(mew-face-mark-escape
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-mark-delete
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-mark-unlink
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(mew-face-mark-refile
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-mark-unread
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(mew-face-eof-message
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(mew-face-eof-part
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   ;; mingus
   `(mingus-directory-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(mingus-pausing-face
     ((,monokaix-class (:foreground ,monokaix-magenta))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta))))

   `(mingus-playing-face
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(mingus-playlist-face
     ((,monokaix-class (:foreground ,monokaix-cyan ))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan ))))

   `(mingus-song-file-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(mingus-stopped-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; mmm
   `(mmm-init-submode-face
     ((,monokaix-class (:background ,monokaix-violet-d))
      (,monokaix-256-class (:background ,monokaix-256-violet-d))))

   `(mmm-cleanup-submode-face
     ((,monokaix-class (:background ,monokaix-orange-d))
      (,monokaix-256-class (:background ,monokaix-256-orange-d))))

   `(mmm-declaration-submode-face
     ((,monokaix-class (:background ,monokaix-cyan-d))
      (,monokaix-256-class (:background ,monokaix-256-cyan-d))))

   `(mmm-comment-submode-face
     ((,monokaix-class (:background ,monokaix-blue-d))
      (,monokaix-256-class (:background ,monokaix-256-blue-d))))

   `(mmm-output-submode-face
     ((,monokaix-class (:background ,monokaix-red-d))
      (,monokaix-256-class (:background ,monokaix-256-red-d))))

   `(mmm-special-submode-face
     ((,monokaix-class (:background ,monokaix-green-d))
      (,monokaix-256-class (:background ,monokaix-256-green-d))))

   `(mmm-code-submode-face
     ((,monokaix-class (:background ,monokaix-gray))
      (,monokaix-256-class (:background ,monokaix-256-gray))))

   `(mmm-default-submode-face
     ((,monokaix-class (:background ,monokaix-gray-d))
      (,monokaix-256-class (:background ,monokaix-256-gray-d))))

   ;; moccur
   `(moccur-current-line-face
     ((,monokaix-class (:underline t))
      (,monokaix-256-class (:underline t))))

   `(moccur-edit-done-face
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-background
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-background
                                        :slant italic))))

   `(moccur-edit-face
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-background))))

   `(moccur-edit-file-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(moccur-edit-reject-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(moccur-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :weight bold))))

   `(search-buffers-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :weight bold))))

   `(search-buffers-header-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-yellow
                                        :weight bold))))

   ;; mu4e
   `(mu4e-cited-1-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-2-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-3-face
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-4-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-5-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-6-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :slant italic
                                        :weight normal))))

   `(mu4e-cited-7-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :slant italic
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :slant italic
                                        :weight normal))))

   `(mu4e-flagged-face
     ((,monokaix-class (:foreground ,monokaix-magenta
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :weight bold))))

   `(mu4e-view-url-number-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal))))

   `(mu4e-warning-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :slant normal
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :slant normal
                                        :weight bold))))

   `(mu4e-header-highlight-face
     ((,monokaix-class (:inherit unspecified
                                :foreground unspecified
                                :background ,monokaix-highlight-line
                                :underline ,monokaix-emphasis
                                :weight normal))
      (,monokaix-256-class (:inherit unspecified
                                     :foreground unspecified
                                     :background ,monokaix-256-highlight-line
                                     :underline ,monokaix-256-emphasis
                                     :weight normal))))


   `(mu4e-draft-face
     ((,monokaix-class (:inherit font-lock-string-face))
      (,monokaix-256-class (:inherit font-lock-string-face))))

   `(mu4e-footer-face
     ((,monokaix-class (:inherit font-lock-comment-face))
      (,monokaix-256-class (:inherit font-lock-comment-face))))

   `(mu4e-forwarded-face
     ((,monokaix-class (:inherit font-lock-builtin-face
                                :weight normal))
      (,monokaix-256-class (:inherit font-lock-builtin-face
                                     :weight normal))))

   `(mu4e-header-face
     ((,monokaix-class (:inherit default))
      (,monokaix-256-class (:inherit default))))

   `(mu4e-header-marks-face
     ((,monokaix-class (:inherit font-lock-preprocessor-face))
      (,monokaix-256-class (:inherit font-lock-preprocessor-face))))

   `(mu4e-header-title-face
     ((,monokaix-class (:inherit font-lock-type-face))
      (,monokaix-256-class (:inherit font-lock-type-face))))

   `(mu4e-highlight-face
     ((,monokaix-class (:inherit font-lock-pseudo-keyword-face
                                :weight bold))
      (,monokaix-256-class (:inherit font-lock-pseudo-keyword-face
                                     :weight bold))))

   `(mu4e-moved-face
     ((,monokaix-class (:inherit font-lock-comment-face
                                :slant italic))
      (,monokaix-256-class (:inherit font-lock-comment-face
                                     :slant italic))))

   `(mu4e-ok-face
     ((,monokaix-class (:inherit font-lock-comment-face
                                :slant normal
                                :weight bold))
      (,monokaix-256-class (:inherit font-lock-comment-face
                                     :slant normal
                                     :weight bold))))

   `(mu4e-replied-face
     ((,monokaix-class (:inherit font-lock-builtin-face
                                :weight normal))
      (,monokaix-256-class (:inherit font-lock-builtin-face
                                     :weight normal))))

   `(mu4e-system-face
     ((,monokaix-class (:inherit font-lock-comment-face
                                :slant italic))
      (,monokaix-256-class (:inherit font-lock-comment-face
                                     :slant italic))))

   `(mu4e-title-face
     ((,monokaix-class (:inherit font-lock-type-face
                                :weight bold))
      (,monokaix-256-class (:inherit font-lock-type-face
                                     :weight bold))))

   `(mu4e-trashed-face
     ((,monokaix-class (:inherit font-lock-comment-face
                                :strike-through t))
      (,monokaix-256-class (:inherit font-lock-comment-face
                                     :strike-through t))))

   `(mu4e-unread-face
     ((,monokaix-class (:inherit font-lock-keyword-face
                                :weight bold))
      (,monokaix-256-class (:inherit font-lock-keyword-face
                                     :weight bold))))

   `(mu4e-view-attach-number-face
     ((,monokaix-class (:inherit font-lock-variable-name-face
                                :weight bold))
      (,monokaix-256-class (:inherit font-lock-variable-name-face
                                     :weight bold))))

   `(mu4e-view-contact-face
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :weight normal))))

   `(mu4e-view-header-key-face
     ((,monokaix-class (:inherit message-header-name
                                :weight normal))
      (,monokaix-256-class (:inherit message-header-name
                                     :weight normal))))

   `(mu4e-view-header-value-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :weight normal
                                   :slant normal))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :weight normal
                                        :slant normal))))

   `(mu4e-view-link-face
     ((,monokaix-class (:inherit link))
      (,monokaix-256-class (:inherit link))))

   `(mu4e-view-special-header-value-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight normal
                                        :underline nil))))

   ;; mumamo
   `(mumamo-background-chunk-submode1
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   ;; nav
   `(nav-face-heading
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(nav-face-button-num
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(nav-face-dir
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(nav-face-hdir
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(nav-face-file
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(nav-face-hfile
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; nav-flash
   `(nav-flash-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   ;; neo-tree
   `(neo-banner-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background
                                        :weight bold))))


   `(neo-header-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background))))

   `(neo-root-dir-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background))))

   `(neo-dir-link-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-background))))

   `(neo-file-link-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(neo-button-face
     ((,monokaix-class (:underline nil))
      (,monokaix-256-class (:underline nil))))

   `(neo-expand-btn-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(neo-vc-default-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(neo-vc-user-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :slant italic))))

   `(neo-vc-up-to-date-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(neo-vc-edited-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(neo-vc-needs-update-face
     ((,monokaix-class (:underline t))
      (,monokaix-256-class (:underline t))))

   `(neo-vc-needs-merge-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(neo-vc-unlocked-changes-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-comments))))

   `(neo-vc-added-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(neo-vc-removed-face
     ((,monokaix-class (:strike-through t))
      (,monokaix-256-class (:strike-through t))))

   `(neo-vc-conflict-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(neo-vc-missing-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(neo-vc-ignored-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   ;; adoc-mode / markup
   `(markup-meta-face
     ((,monokaix-class (:foreground ,monokaix-gray-l))
      (,monokaix-256-class (:foreground ,monokaix-256-gray-l))))

   `(markup-table-face
     ((,monokaix-class (:foreground ,monokaix-blue-hc
                                   :background ,monokaix-blue-lc))
      (,monokaix-256-class (:foreground ,monokaix-256-blue-hc
                                        :background ,monokaix-256-blue-lc))))

   `(markup-verbatim-face
     ((,monokaix-class (:background ,monokaix-orange-lc))
      (,monokaix-256-class (:background ,monokaix-256-orange-lc))))

   `(markup-list-face
     ((,monokaix-class (:foreground ,monokaix-violet-hc
                                   :background ,monokaix-violet-lc))
      (,monokaix-256-class (:foreground ,monokaix-256-violet-hc
                                        :background ,monokaix-256-violet-lc))))

   `(markup-replacement-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(markup-complex-replacement-face
     ((,monokaix-class (:foreground ,monokaix-violet-hc
                                   :background ,monokaix-violet-lc))
      (,monokaix-256-class (:foreground ,monokaix-256-violet-hc
                                        :background ,monokaix-256-violet-lc))))

   `(markup-gen-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(markup-secondary-text-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; org-mode
   `(org-agenda-structure
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-highlight-line
                                   :weight bold
                                   :slant normal
                                   :inverse-video nil
                                   :height ,monokaix-height-plus-1
                                   :underline nil
                                   :box (:line-width 2 :color ,monokaix-background)))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-highlight-line
                                        :weight bold
                                        :slant normal
                                        :inverse-video nil
                                        :height ,monokaix-height-plus-1
                                        :underline nil
                                        :box (:line-width 2 :color ,monokaix-256-background)))))

   `(org-agenda-calendar-event
     ((,monokaix-class (:foreground ,monokaix-emphasis))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis))))

   `(org-agenda-calendar-sexp
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :slant italic))))

   `(org-agenda-date
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video nil
                                   :overline nil
                                   :slant normal
                                   :height 1.0
                                   :box (:line-width 2 :color ,monokaix-background)))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video nil
                                        :overline nil
                                        :slant normal
                                        :height 1.0
                                        :box (:line-width 2 :color ,monokaix-256-background)))) t)

   `(org-agenda-date-weekend
     ((,monokaix-class (:inherit org-agenda-date
                                :inverse-video nil
                                :background unspecified
                                :foreground ,monokaix-comments
                                :weight unspecified
                                :underline t
                                :overline nil
                                :box unspecified))
      (,monokaix-256-class (:inherit org-agenda-date
                                     :inverse-video nil
                                     :background unspecified
                                     :foreground ,monokaix-256-comments
                                     :weight unspecified
                                     :underline t
                                     :overline nil
                                     :box unspecified))) t)

   `(org-agenda-date-today
     ((,monokaix-class (:inherit org-agenda-date
                                :inverse-video t
                                :weight bold
                                :underline unspecified
                                :overline nil
                                :box unspecified
                                :foreground ,monokaix-blue
                                :background ,monokaix-background))
      (,monokaix-256-class (:inherit org-agenda-date
                                     :inverse-video t
                                     :weight bold
                                     :underline unspecified
                                     :overline nil
                                     :box unspecified
                                     :foreground ,monokaix-256-blue
                                     :background ,monokaix-256-background))) t)

   `(org-agenda-done
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :slant italic))) t)

   `(org-archived
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :weight normal))))

   `(org-block
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-highlight-alt))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-highlight-alt))))

   `(org-block-background
     ((,monokaix-class (:background ,monokaix-highlight-alt))
      (,monokaix-256-class (:background ,monokaix-256-highlight-alt))))

   `(org-block-begin-line
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-gray-d
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-gray-d
                                        :slant italic))))

   `(org-block-end-line
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-gray-d
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-gray-d
                                        :slant italic))))

   `(org-checkbox
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground
                                   :box (:line-width 1 :style released-button)))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground
                                        :box (:line-width 1 :style released-button)))))

   `(org-code
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(org-date
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline t))))

   `(org-done
     ((,monokaix-class (:weight bold
                               :foreground ,monokaix-green))
      (,monokaix-256-class (:weight bold
                                    :foreground ,monokaix-256-green))))

   `(org-ellipsis
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(org-formula
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(org-headline-done
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(org-hide
     ((,monokaix-class (:foreground ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-background))))

   `(org-level-1
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :height ,monokaix-height-plus-4
                                :foreground ,monokaix-orange))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :height ,monokaix-height-plus-4
                                     :foreground ,monokaix-256-orange))))

   `(org-level-2
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :height ,monokaix-height-plus-3
                                :foreground ,monokaix-green))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :height ,monokaix-height-plus-3
                                     :foreground ,monokaix-256-green))))

   `(org-level-3
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :height ,monokaix-height-plus-2
                                :foreground ,monokaix-blue))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :height ,monokaix-height-plus-2
                                     :foreground ,monokaix-256-blue))))

   `(org-level-4
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :height ,monokaix-height-plus-1
                                :foreground ,monokaix-yellow))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :height ,monokaix-height-plus-1
                                     :foreground ,monokaix-256-yellow))))

   `(org-level-5
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-cyan))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-cyan))))

   `(org-level-6
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-green))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-green))))

   `(org-level-7
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-red))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-red))))

   `(org-level-8
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-blue))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-blue))))

   `(org-link
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :underline t))))

   `(org-sexp-date
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(org-scheduled
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(org-scheduled-previously
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(org-scheduled-today
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :weight normal))))

   `(org-special-keyword
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :weight bold))))

   `(org-table
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(org-tag
     ((,monokaix-class (:weight bold))
      (,monokaix-256-class (:weight bold))))

   `(org-time-grid
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(org-todo
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold)))
     ((,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold))))

   `(org-upcoming-deadline
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal
                                        :underline nil))))

   `(org-warning
     ((,monokaix-class (:foreground ,monokaix-orange
                                   :weight normal
                                   :underline nil))
      (,monokaix-256-class (:foreground ,monokaix-256-orange
                                        :weight normal
                                        :underline nil))))

   ;; org-habit (clear=blue, ready=green, alert=yellow, overdue=red. future=lower contrast)
   `(org-habit-clear-face
     ((,monokaix-class (:background ,monokaix-blue-lc
                                   :foreground ,monokaix-blue-hc))
      (,monokaix-256-class (:background ,monokaix-256-blue-lc
                                        :foreground ,monokaix-256-blue-hc))))

   `(org-habit-clear-future-face
     ((,monokaix-class (:background ,monokaix-blue-lc))
      (,monokaix-256-class (:background ,monokaix-256-blue-lc))))

   `(org-habit-ready-face
     ((,monokaix-class (:background ,monokaix-green-lc
                                   :foreground ,monokaix-green))
      (,monokaix-256-class (:background ,monokaix-256-green-lc
                                        :foreground ,monokaix-256-green))))

   `(org-habit-ready-future-face
     ((,monokaix-class (:background ,monokaix-green-lc))
      (,monokaix-256-class (:background ,monokaix-256-green-lc))))

   `(org-habit-alert-face
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-yellow-lc))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-yellow-lc))))

   `(org-habit-alert-future-face
     ((,monokaix-class (:background ,monokaix-yellow-lc))
      (,monokaix-256-class (:background ,monokaix-256-yellow-lc))))

   `(org-habit-overdue-face
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-red-lc))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-red-lc))))

   `(org-habit-overdue-future-face
     ((,monokaix-class (:background ,monokaix-red-lc))
      (,monokaix-256-class (:background ,monokaix-256-red-lc))))

   ;; latest additions
   `(org-agenda-dimmed-todo-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(org-agenda-restriction-lock
     ((,monokaix-class (:background ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-yellow))))

   `(org-clock-overlay
     ((,monokaix-class (:background ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-yellow))))

   `(org-column
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :strike-through nil
                                   :underline nil
                                   :slant normal
                                   :weight normal
                                   :inherit default))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :strike-through nil
                                        :underline nil
                                        :slant normal
                                        :weight normal
                                        :inherit default))))

   `(org-column-title
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :underline t
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :underline t
                                        :weight bold))))

   `(org-date-selected
     ((,monokaix-class (:foreground ,monokaix-red
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :inverse-video t))))

   `(org-document-info
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(org-document-title
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :weight bold
                                   :height ,monokaix-height-plus-4))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :weight bold
                                        :height ,monokaix-height-plus-4))))

   `(org-drawer
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(org-footnote
     ((,monokaix-class (:foreground ,monokaix-magenta
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :underline t))))

   `(org-latex-and-export-specials
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(org-mode-line-clock-overrun
     ((,monokaix-class (:inherit mode-line))
      (,monokaix-256-class (:inherit mode-line))))

   ;; outline
   `(outline-1
     ((,monokaix-class (:inherit org-level-1))
      (,monokaix-256-class (:inherit org-level-1))))

   `(outline-2
     ((,monokaix-class (:inherit org-level-2))
      (,monokaix-256-class (:inherit org-level-2))))

   `(outline-3
     ((,monokaix-class (:inherit org-level-3))
      (,monokaix-256-class (:inherit org-level-3))))

   `(outline-4
     ((,monokaix-class (:inherit org-level-4))
      (,monokaix-256-class (:inherit org-level-4))))

   `(outline-5
     ((,monokaix-class (:inherit org-level-5))
      (,monokaix-256-class (:inherit org-level-5))))

   `(outline-6
     ((,monokaix-class (:inherit org-level-6))
      (,monokaix-256-class (:inherit org-level-6))))

   `(outline-7
     ((,monokaix-class (:inherit org-level-7))
      (,monokaix-256-class (:inherit org-level-7))))

   `(outline-8
     ((,monokaix-class (:inherit org-level-8))
      (,monokaix-256-class (:inherit org-level-8))))

   ;; parenface
   `(paren-face
     ((,monokaix-256-class (:foreground ,monokaix-comments))))

   ;; perspective
   `(persp-selected-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :weight bold))))

   ;; pretty-mode
   `(pretty-mode-symbol-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight normal))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight normal))))

   ;; popup
   `(popup-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   `(popup-isearch-match
     ((,monokaix-class (:background ,monokaix-green))
      (,monokaix-256-class (:background ,monokaix-256-green))))

   `(popup-menu-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   `(popup-menu-mouse-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-foreground))))

   `(popup-menu-selection-face
     ((,monokaix-class (:background ,monokaix-magenta
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-magenta
                                        :foreground ,monokaix-256-background))))

   `(popup-scroll-bar-background-face
     ((,monokaix-class (:background ,monokaix-comments))
      (,monokaix-256-class (:background ,monokaix-256-comments))))

   `(popup-scroll-bar-foreground-face
     ((,monokaix-class (:background ,monokaix-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-emphasis))))

   `(popup-tip-face
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   ;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(rainbow-delimiters-depth-2-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(rainbow-delimiters-depth-3-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(rainbow-delimiters-depth-4-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(rainbow-delimiters-depth-5-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(rainbow-delimiters-depth-6-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(rainbow-delimiters-depth-7-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(rainbow-delimiters-depth-8-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(rainbow-delimiters-depth-9-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(rainbow-delimiters-depth-10-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(rainbow-delimiters-depth-11-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(rainbow-delimiters-depth-12-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(rainbow-delimiters-unmatched-face
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :background ,monokaix-background
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :background ,monokaix-256-background
                                        :inverse-video t))))

   ;; realgud
   `(realgud-overlay-arrow1
     ((,monokaix-class (:foreground ,monokaix-green-d))
      (,monokaix-256-class (:foreground ,monokaix-256-green-d))))

   `(realgud-overlay-arrow2
     ((,monokaix-class (:foreground ,monokaix-yellow-d))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow-d))))

   `(realgud-overlay-arrow3
     ((,monokaix-class (:foreground ,monokaix-orange-d))
      (,monokaix-256-class (:foreground ,monokaix-256-orange-d))))

   `(realgud-bp-enabled-face
     ((,monokaix-class (:inherit error)))
     ((,monokaix-256-class (:inherit error))))

   `(realgud-bp-disabled-face
     ((,monokaix-class (:inherit secondary-selection)))
     ((,monokaix-256-class (:inherit secondary-selection))))

   `(realgud-bp-line-enabled-face
     ((,monokaix-class (:foreground ,monokaix-red-d)))
     ((,monokaix-256-class (:foreground ,monokaix-256-red-d))))

   `(realgud-bp-line-disabled-face
     ((,monokaix-class (:inherit secondary-selection)))
     ((,monokaix-256-class (:inherit secondary-selection))))

   `(realgud-line-number
     ((,monokaix-class (:inerhit monokaix-line-number)))
     ((,monokaix-256-class (:inerhit monokaix-line-number))))

   `(realgud-backtrace-number
     ((,monokaix-class (:foreground ,monokaix-yellow-d
                                   :weight bold)))
     ((,monokaix-256-class (:foreground ,monokaix-256-yellow
                                       :weight bold))))

   ;; rhtm-mode
   `(erb-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background))))

   `(erb-delim-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :background ,monokaix-256-background))))

   `(erb-exec-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background))))

   `(erb-exec-delim-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :background ,monokaix-256-background))))

   `(erb-out-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background))))

   `(erb-out-delim-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :background ,monokaix-256-background))))

   `(erb-comment-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background))))

   `(erb-comment-delim-face
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :background ,monokaix-256-background))))

   ;; rst-mode
   `(rst-level-1-face
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-background))))

   `(rst-level-2-face
     ((,monokaix-class (:background ,monokaix-cyan
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-cyan
                                        :foreground ,monokaix-256-background))))

   `(rst-level-3-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background))))

   `(rst-level-4-face
     ((,monokaix-class (:background ,monokaix-violet
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-violet
                                        :foreground ,monokaix-256-background))))

   `(rst-level-5-face
     ((,monokaix-class (:background ,monokaix-magenta
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-magenta
                                        :foreground ,monokaix-256-background))))

   `(rst-level-6-face
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-background))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-background))))

   ;; rpm-mode
   `(rpm-spec-dir-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(rpm-spec-doc-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(rpm-spec-ghost-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(rpm-spec-macro-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(rpm-spec-obsolete-tag-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(rpm-spec-package-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(rpm-spec-section-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(rpm-spec-tag-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(rpm-spec-var-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; sh-mode
   `(sh-quoted-exec
     ((,monokaix-class (:foreground ,monokaix-violet
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-violet
                                        :weight bold))))

   `(sh-escaped-newline
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   `(sh-heredoc
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :weight bold))))

   ;; smartparens
   `(sp-pair-overlay-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(sp-wrap-overlay-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(sp-wrap-tag-overlay-face
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(sp-show-pair-enclosing
     ((,monokaix-class (:inherit highlight))
      (,monokaix-256-class (:inherit highlight))))

   `(sp-show-pair-match-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   `(sp-show-pair-mismatch-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   ;; show-paren
   `(show-paren-match
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   `(show-paren-mismatch
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   ;; mic-paren
   `(paren-face-match
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   `(paren-face-mismatch
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   `(paren-face-no-match
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-background
                                   :weight normal
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-background
                                        :weight normal
                                        :inverse-video t))))

   ;; SLIME
   `(slime-repl-inputed-output-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; smerge
   `(smerge-base
      ((,monokaix-class (:background ,monokaix-diff-blue-base))
        (,monokaix-256-class (:background ,monokaix-256-diff-blue-base))))
   `(smerge-upper
      ((,monokaix-class (:background ,monokaix-diff-red-base))
        (,monokaix-256-class (:background ,monokaix-256-diff-red-base))))
   `(smerge-lower
      ((,monokaix-class (:background ,monokaix-diff-green-base))
        (,monokaix-256-class (:background ,monokaix-256-diff-green-base))))
   ;; WARNING: defining this face will overwrite the next two when displaying a
   ;; smerge diff in a file.
   ;; `(smerge-refined-changed
   ;;    ((,monokaix-class (:background ,monokaix-diff-blue-emphasis))
   ;;      (,monokaix-256-class (:background ,monokaix-256-diff-blue-emphasis))))
   `(smerge-refined-added
      ((,monokaix-class (:background ,monokaix-diff-green-emphasis))
        (,monokaix-256-class (:background ,monokaix-256-diff-green-emphasis))))
   `(smerge-refined-removed
      ((,monokaix-class (:background ,monokaix-diff-red-emphasis))
        (,monokaix-256-class (:background ,monokaix-256-diff-red-emphasis))))

   ;; speedbar
   `(speedbar-button-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-comments))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-comments))))

   `(speedbar-directory-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-blue))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-blue))))

   `(speedbar-file-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-foreground))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-foreground))))

   `(speedbar-highlight-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :background ,monokaix-highlight-line))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :background ,monokaix-256-highlight-line))))

   `(speedbar-selected-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-yellow
                                :underline t))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-yellow
                                     :underline t))))

   `(speedbar-separator-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :background ,monokaix-blue
                                :foreground ,monokaix-background
                                :overline ,monokaix-cyan-lc))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :background ,monokaix-256-blue
                                     :foreground ,monokaix-256-background
                                     :overline ,monokaix-256-cyan-lc))))

   `(speedbar-tag-face
     ((,monokaix-class (:inherit ,monokaix-pitch
                                :foreground ,monokaix-green))
      (,monokaix-256-class (:inherit ,monokaix-pitch
                                     :foreground ,monokaix-256-green))))

   ;; sunrise commander headings
   `(sr-active-path-face
     ((,monokaix-class (:background ,monokaix-blue
                                   :foreground ,monokaix-background
                                   :height ,monokaix-height-plus-1
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-blue
                                        :foreground ,monokaix-256-background
                                        :height ,monokaix-height-plus-1
                                        :weight bold))))

   `(sr-editing-path-face
     ((,monokaix-class (:background ,monokaix-yellow
                                   :foreground ,monokaix-background
                                   :weight bold
                                   :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:background ,monokaix-256-yellow
                                        :foreground ,monokaix-256-background
                                        :weight bold
                                        :height ,monokaix-height-plus-1))))

   `(sr-highlight-path-face
     ((,monokaix-class (:background ,monokaix-green
                                   :foreground ,monokaix-background
                                   :weight bold
                                   :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:background ,monokaix-256-green
                                        :foreground ,monokaix-256-background
                                        :weight bold
                                        :height ,monokaix-height-plus-1))))

   `(sr-passive-path-face
     ((,monokaix-class (:background ,monokaix-comments
                                   :foreground ,monokaix-background
                                   :weight bold
                                   :height ,monokaix-height-plus-1))
      (,monokaix-256-class (:background ,monokaix-256-comments
                                        :foreground ,monokaix-256-background
                                        :weight bold
                                        :height ,monokaix-height-plus-1))))

   ;; sunrise commander marked
   `(sr-marked-dir-face
     ((,monokaix-class (:inherit dimonokaix-red-marked))
      (,monokaix-256-class (:inherit dimonokaix-red-marked))))

   `(sr-marked-file-face
     ((,monokaix-class (:inherit dimonokaix-red-marked))
      (,monokaix-256-class (:inherit dimonokaix-red-marked))))

   `(sr-alt-marked-dir-face
     ((,monokaix-class (:background ,monokaix-magenta
                                   :foreground ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-magenta
                                        :foreground ,monokaix-256-background
                                        :weight bold))))

   `(sr-alt-marked-file-face
     ((,monokaix-class (:background ,monokaix-magenta
                                   :foreground ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-magenta
                                        :foreground ,monokaix-256-background
                                        :weight bold))))

   ;; sunrise commander fstat
   `(sr-directory-face
     ((,monokaix-class (:inherit dimonokaix-red-directory
                                :weight normal))
      (,monokaix-256-class (:inherit dimonokaix-red-directory
                                     :weight normal))))

   `(sr-symlink-directory-face
     ((,monokaix-class (:inherit dimonokaix-red-directory
                                :slant italic
                                :weight normal))
      (,monokaix-256-class (:inherit dimonokaix-red-directory
                                     :slant italic
                                     :weight normal))))

   `(sr-symlink-face
     ((,monokaix-class (:inherit dimonokaix-red-symlink
                                :slant italic
                                :weight normal))
      (,monokaix-256-class (:inherit dimonokaix-red-symlink
                                     :slant italic
                                     :weight normal))))

   `(sr-broken-link-face
     ((,monokaix-class (:inherit dimonokaix-red-warning
                                :slant italic
                                :weight normal))
      (,monokaix-256-class (:inherit dimonokaix-red-warning
                                     :slant italic
                                     :weight normal))))

   ;; sunrise commander file types
   `(sr-compressed-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(sr-encrypted-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(sr-log-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(sr-packaged-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(sr-html-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(sr-xml-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   ;; sunrise commander misc
   `(sr-clex-hotchar-face
     ((,monokaix-class (:background ,monokaix-red
                                   :foreground ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:background ,monokaix-256-red
                                        :foreground ,monokaix-256-background
                                        :weight bold))))

   ;; syslog-mode
   `(syslog-ip-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-yellow))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-yellow))))

   `(syslog-hour-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-green))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-green))))

   `(syslog-error-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-red
                                        :weight bold))))

   `(syslog-warn-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-orange
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-orange
                                        :weight bold))))

   `(syslog-info-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-blue
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-blue
                                        :weight bold))))

   `(syslog-debug-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-cyan
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-cyan
                                        :weight bold))))

   `(syslog-su-face
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-magenta))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-magenta))))

   ;; table
   `(table-cell
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :background ,monokaix-highlight-line))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :background ,monokaix-256-highlight-line))))

   ;; term
   `(term-color-black
     ((,monokaix-class (:foreground ,monokaix-background
                                   :background ,monokaix-highlight-line))
      (,monokaix-256-class (:foreground ,monokaix-256-background
                                        :background ,monokaix-256-highlight-line))))

   `(term-color-red
     ((,monokaix-class (:foreground ,monokaix-red
                                   :background ,monokaix-red-d))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :background ,monokaix-256-red-d))))

   `(term-color-green
     ((,monokaix-class (:foreground ,monokaix-green
                                   :background ,monokaix-green-d))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :background ,monokaix-256-green-d))))

   `(term-color-yellow
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background ,monokaix-yellow-d))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background ,monokaix-256-yellow-d))))

   `(term-color-blue
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-blue-d))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-blue-d))))

   `(term-color-magenta
     ((,monokaix-class (:foreground ,monokaix-magenta
                                   :background ,monokaix-magenta-d))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :background ,monokaix-256-magenta-d))))

   `(term-color-cyan
     ((,monokaix-class (:foreground ,monokaix-cyan
                                   :background ,monokaix-cyan-d))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan
                                        :background ,monokaix-256-cyan-d))))

   `(term-color-white
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-foreground))))

   `(term-default-fg-color
     ((,monokaix-class (:inherit term-color-white))
      (,monokaix-256-class (:inherit term-color-white))))

   `(term-default-bg-color
     ((,monokaix-class (:inherit term-color-black))
      (,monokaix-256-class (:inherit term-color-black))))

   ;; tooltip. (NOTE: This setting has no effect on the os widgets for me
   ;; zencoding uses this)
   `(tooltip
     ((,monokaix-class (:background ,monokaix-yellow-hc
                                   :foreground ,monokaix-background
                                   :inherit ,monokaix-pitch))))

   ;; treemacs
   `(treemacs-directory-face
      ((,monokaix-class (:foreground ,monokaix-violet
                         :background ,monokaix-background
                         :weight bold))
        (,monokaix-256-class (:foreground ,monokaix-256-violet
                              :background ,monokaix-256-background
                              :weight bold))))

   `(treemacs-header-face
      ((,monokaix-class (:foreground ,monokaix-yellow
                         :background ,monokaix-background
                         :underline t
                         :weight bold))
        (,monokaix-256-class (:foreground ,monokaix-256-yellow
                              :background ,monokaix-256-background
                              :underline t
                              :weight bold))))

   `(treemacs-git-modified-face
      ((,monokaix-class (:foreground ,monokaix-green
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-green
                              :background ,monokaix-256-background))))

   `(treemacs-git-renamed-face
      ((,monokaix-class (:foreground ,monokaix-red
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-red
                              :background ,monokaix-256-background))))

   `(treemacs-git-ignored-face
      ((,monokaix-class (:foreground ,monokaix-gray-l
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-gray-l
                              :background ,monokaix-256-background))))

   `(treemacs-git-untracked-face
      ((,monokaix-class (:foreground ,monokaix-red
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-red
                              :background ,monokaix-256-background))))

   `(treemacs-git-added-face
      ((,monokaix-class (:foreground ,monokaix-green
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-green
                              :background ,monokaix-256-background))))

   `(treemacs-git-conflict-face
      ((,monokaix-class (:foreground ,monokaix-orange
                         :background ,monokaix-background))
        (,monokaix-256-class (:foreground ,monokaix-256-orange
                              :background ,monokaix-256-background))))

   ;; tuareg
   `(tuareg-font-lock-governing-face
     ((,monokaix-class (:foreground ,monokaix-magenta
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta
                                        :weight bold))))

   `(tuareg-font-lock-multistage-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :background ,monokaix-highlight-line
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :background ,monokaix-256-highlight-line
                                        :weight bold))))

   `(tuareg-font-lock-operator-face
     ((,monokaix-class (:foreground ,monokaix-emphasis))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis))))

   `(tuareg-font-lock-error-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :background ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :background ,monokaix-256-red
                                        :weight bold))))

   `(tuareg-font-lock-interactive-output-face
     ((,monokaix-class (:foreground ,monokaix-cyan))
      (,monokaix-256-class (:foreground ,monokaix-256-cyan))))

   `(tuareg-font-lock-interactive-error-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   ;; undo-tree
   `(undo-tree-visualizer-default-face
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-background))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-background))))

   `(undo-tree-visualizer-unmodified-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(undo-tree-visualizer-current-face
     ((,monokaix-class (:foreground ,monokaix-blue
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-blue
                                        :inverse-video t))))

   `(undo-tree-visualizer-active-branch-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :background ,monokaix-background
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :background ,monokaix-256-background
                                        :weight bold))))

   `(undo-tree-visualizer-register-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   ;; volatile highlights
   `(vhl/default-face
      ((,monokaix-class (:background ,monokaix-highlight-alt))
        (,monokaix-256-class (:background ,monokaix-256-highlight-alt))))

   ;; w3m
   `(w3m-anchor
     ((,monokaix-class (:inherit link))
      (,monokaix-256-class (:inherit link))))

   `(w3m-arrived-anchor
     ((,monokaix-class (:inherit link-visited))
      (,monokaix-256-class (:inherit link-visited))))

   `(w3m-form
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground))))

   `(w3m-header-line-location-title
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-yellow))))

   `(w3m-header-line-location-content

     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   `(w3m-bold
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :weight bold))))

   `(w3m-image-anchor
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-cyan
                                   :inherit link))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-cyan
                                        :inherit link))))

   `(w3m-image
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-cyan))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-cyan))))

   `(w3m-lnum-minibuffer-prompt
     ((,monokaix-class (:foreground ,monokaix-emphasis))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis))))

   `(w3m-lnum-match
     ((,monokaix-class (:background ,monokaix-highlight-line))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line))))

   `(w3m-lnum
     ((,monokaix-class (:underline nil
                                  :bold nil
                                  :foreground ,monokaix-red))
      (,monokaix-256-class (:underline nil
                                       :bold nil
                                       :foreground ,monokaix-256-red))))

   `(w3m-session-select
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(w3m-session-selected
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :bold t
                                   :underline t))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :bold t
                                        :underline t))))

   `(w3m-tab-background
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground))))

   `(w3m-tab-selected-background
     ((,monokaix-class (:background ,monokaix-background
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-background
                                        :foreground ,monokaix-256-foreground))))

   `(w3m-tab-mouse
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-yellow))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-yellow))))

   `(w3m-tab-selected
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-emphasis
                                   :bold t))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-emphasis
                                        :bold t))))

   `(w3m-tab-unselected
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-foreground))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-foreground))))

   `(w3m-tab-selected-retrieving
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-red))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-red))))

   `(w3m-tab-unselected-retrieving
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-orange))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-orange))))

   `(w3m-tab-unselected-unseen
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :foreground ,monokaix-violet))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :foreground ,monokaix-256-violet))))

   ;; web-mode
   `(web-mode-builtin-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(web-mode-comment-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(web-mode-constant-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(web-mode-current-element-highlight-face
     ((,monokaix-class (:underline unspecified
                                  :weight unspecified
                                  :background ,monokaix-highlight-line))
      (,monokaix-256-class (:underline unspecified
                                       :weight unspecified
                                       :background ,monokaix-256-highlight-line))))

   `(web-mode-doctype-face
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :slant italic
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :slant italic
                                        :weight bold))))

   `(web-mode-folded-face
     ((,monokaix-class (:underline t))
      (,monokaix-256-class (:underline t))))

   `(web-mode-function-name-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(web-mode-html-attr-name-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(web-mode-html-attr-custom-face
     ((,monokaix-class (:inherit web-mode-html-attr-name-face))
      (,monokaix-256-class (:inherit web-mode-html-attr-name-face))))

   `(web-mode-html-attr-engine-face
     ((,monokaix-class (:inherit web-mode-block-delimiter-face))
      (,monokaix-256-class (:inherit web-mode-block-delimiter-face))))

   `(web-mode-html-attr-equal-face
     ((,monokaix-class (:inherit web-mode-html-attr-name-face))
      (,monokaix-256-class (:inherit web-mode-html-attr-name-face))))

   `(web-mode-html-attr-value-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(web-mode-html-tag-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(web-mode-keyword-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(web-mode-preprocessor-face
     ((,monokaix-class (:foreground ,monokaix-yellow
                                   :slant normal
                                   :weight unspecified))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow
                                        :slant normal
                                        :weight unspecified))))

   `(web-mode-string-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(web-mode-type-face
     ((,monokaix-class (:inherit font-lock-type-face))
      (,monokaix-256-class (:inherit font-lock-type-face))))

   `(web-mode-variable-name-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(web-mode-warning-face
     ((,monokaix-class (:inherit font-lock-warning-face))
      (,monokaix-256-class (:inherit font-lock-warning-face))))

   `(web-mode-block-face
     ((,monokaix-class (:background unspecified))
      (,monokaix-256-class (:background unspecified))))

   `(web-mode-block-delimiter-face
     ((,monokaix-class (:inherit font-lock-preprocessor-face))
      (,monokaix-256-class (:inherit font-lock-preprocessor-face))))

   `(web-mode-block-comment-face
     ((,monokaix-class (:inherit web-mode-comment-face))
      (,monokaix-256-class (:inherit web-mode-comment-face))))

   `(web-mode-block-control-face
     ((,monokaix-class (:inherit font-lock-preprocessor-face))
      (,monokaix-256-class (:inherit font-lock-preprocessor-face))))

   `(web-mode-block-string-face
     ((,monokaix-class (:inherit web-mode-string-face))
      (,monokaix-256-class (:inherit web-mode-string-face))))

   `(web-mode-comment-keyword-face
     ((,monokaix-class (:box 1 :weight bold))
      (,monokaix-256-class (:box 1 :weight bold))))

   `(web-mode-css-at-rule-face
     ((,monokaix-class (:inherit font-lock-constant-face))
      (,monokaix-256-class (:inherit font-lock-constant-face))))

   `(web-mode-css-pseudo-class-face
     ((,monokaix-class (:inherit font-lock-builtin-face))
      (,monokaix-256-class (:inherit font-lock-builtin-face))))

   `(web-mode-css-color-face
     ((,monokaix-class (:inherit font-lock-builtin-face))
      (,monokaix-256-class (:inherit font-lock-builtin-face))))

   `(web-mode-css-filter-face
     ((,monokaix-class (:inherit font-lock-function-name-face))
      (,monokaix-256-class (:inherit font-lock-function-name-face))))

   `(web-mode-css-function-face
     ((,monokaix-class (:inherit font-lock-builtin-face))
      (,monokaix-256-class (:inherit font-lock-builtin-face))))

   `(web-mode-css-function-call-face
     ((,monokaix-class (:inherit font-lock-function-name-face))
      (,monokaix-256-class (:inherit font-lock-function-name-face))))

   `(web-mode-css-priority-face
     ((,monokaix-class (:inherit font-lock-builtin-face))
      (,monokaix-256-class (:inherit font-lock-builtin-face))))

   `(web-mode-css-property-name-face
     ((,monokaix-class (:inherit font-lock-variable-name-face))
      (,monokaix-256-class (:inherit font-lock-variable-name-face))))

   `(web-mode-css-selector-face
     ((,monokaix-class (:inherit font-lock-keyword-face))
      (,monokaix-256-class (:inherit font-lock-keyword-face))))

   `(web-mode-css-string-face
     ((,monokaix-class (:inherit web-mode-string-face))
      (,monokaix-256-class (:inherit web-mode-string-face))))

   `(web-mode-javascript-string-face
     ((,monokaix-class (:inherit web-mode-string-face))
      (,monokaix-256-class (:inherit web-mode-string-face))))

   `(web-mode-json-comment-face
     ((,monokaix-class (:inherit web-mode-comment-face))
      (,monokaix-256-class (:inherit web-mode-comment-face))))

   `(web-mode-json-context-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(web-mode-json-key-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(web-mode-json-string-face
     ((,monokaix-class (:inherit web-mode-string-face))
      (,monokaix-256-class (:inherit web-mode-string-face))))

   `(web-mode-param-name-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(web-mode-part-comment-face
     ((,monokaix-class (:inherit web-mode-comment-face))
      (,monokaix-256-class (:inherit web-mode-comment-face))))

   `(web-mode-part-face
     ((,monokaix-class (:inherit web-mode-block-face))
      (,monokaix-256-class (:inherit web-mode-block-face))))

   `(web-mode-part-string-face
     ((,monokaix-class (:inherit web-mode-string-face))
      (,monokaix-256-class (:inherit web-mode-string-face))))

   `(web-mode-symbol-face
     ((,monokaix-class (:foreground ,monokaix-violet))
      (,monokaix-256-class (:foreground ,monokaix-256-violet))))

   `(web-mode-whitespace-face
     ((,monokaix-class (:background ,monokaix-red))
      (,monokaix-256-class (:background ,monokaix-256-red))))

   ;; whitespace-mode
   `(whitespace-space
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-comments
                                   :inverse-video unspecified
                                   :slant italic))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-comments
                                        :inverse-video unspecified
                                        :slant italic))))

   `(whitespace-hspace
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-emphasis
                                   :inverse-video unspecified))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-emphasis
                                        :inverse-video unspecified))))

   `(whitespace-tab
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-red
                                   :inverse-video unspecified
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-red
                                        :inverse-video unspecified
                                        :weight bold))))

   `(whitespace-newline
     ((,monokaix-class(:background unspecified
                                  :foreground ,monokaix-comments
                                  :inverse-video unspecified))
      (,monokaix-256-class (:background unspecified
                                       :foreground ,monokaix-256-comments
                                       :inverse-video unspecified))))

   `(whitespace-trailing
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-orange-lc
                                   :inverse-video t))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-orange-lc
                                        :inverse-video t))))

   `(whitespace-line
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-magenta
                                   :inverse-video unspecified))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-magenta
                                        :inverse-video unspecified))))

   `(whitespace-space-before-tab
     ((,monokaix-class (:background ,monokaix-red-lc
                                   :foreground unspecified
                                   :inverse-video unspecified))
      (,monokaix-256-class (:background ,monokaix-256-red-lc
                                        :foreground unspecified
                                        :inverse-video unspecified))))

   `(whitespace-indentation
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-yellow
                                   :inverse-video unspecified
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-yellow
                                        :inverse-video unspecified
                                        :weight bold))))

   `(whitespace-empty
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-red-lc
                                   :inverse-video t))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-red-lc
                                        :inverse-video t))))

   `(whitespace-space-after-tab
     ((,monokaix-class (:background unspecified
                                   :foreground ,monokaix-orange
                                   :inverse-video t
                                   :weight bold))
      (,monokaix-256-class (:background unspecified
                                        :foreground ,monokaix-256-orange
                                        :inverse-video t
                                        :weight bold))))

   ;; wanderlust
   `(wl-highlight-folder-few-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(wl-highlight-folder-many-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(wl-highlight-folder-path-face
     ((,monokaix-class (:foreground ,monokaix-orange))
      (,monokaix-256-class (:foreground ,monokaix-256-orange))))

   `(wl-highlight-folder-unread-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-folder-zero-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(wl-highlight-folder-unknown-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-message-citation-header
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(wl-highlight-message-cited-text-1
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(wl-highlight-message-cited-text-2
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-cited-text-3
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-message-cited-text-4
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-message-header-contents-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-headers-face
     ((,monokaix-class (:foreground ,monokaix-red))
      (,monokaix-256-class (:foreground ,monokaix-256-red))))

   `(wl-highlight-message-important-header-contents
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-header-contents
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-important-header-contents2
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-signature
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   `(wl-highlight-message-unimportant-header-contents
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(wl-highlight-summary-answemonokaix-red-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-summary-disposed-face
     ((,monokaix-class (:foreground ,monokaix-foreground
                                   :slant italic))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground
                                        :slant italic))))

   `(wl-highlight-summary-new-face
     ((,monokaix-class (:foreground ,monokaix-blue))
      (,monokaix-256-class (:foreground ,monokaix-256-blue))))

   `(wl-highlight-summary-normal-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(wl-highlight-summary-thread-top-face
     ((,monokaix-class (:foreground ,monokaix-yellow))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow))))

   `(wl-highlight-thread-indent-face
     ((,monokaix-class (:foreground ,monokaix-magenta))
      (,monokaix-256-class (:foreground ,monokaix-256-magenta))))

   `(wl-highlight-summary-refiled-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(wl-highlight-summary-displaying-face
     ((,monokaix-class (:underline t
                                  :weight bold))
      (,monokaix-256-class (:underline t
                                       :weight bold))))

   ;; weechat
   `(weechat-error-face
     ((,monokaix-class (:inherit error))
      (,monokaix-256-class (:inherit error))))

   `(weechat-highlight-face
     ((,monokaix-class (:foreground ,monokaix-emphasis
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-emphasis
                                        :weight bold))))

   `(weechat-nick-self-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight unspecified
                                   :inverse-video t))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight unspecified
                                        :inverse-video t))))

   `(weechat-prompt-face
     ((,monokaix-class (:inherit minibuffer-prompt))
      (,monokaix-256-class (:inherit minibuffer-prompt))))

   `(weechat-time-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   ;; which-func-mode
   `(which-func
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   ;; which-key
   `(which-key-key-face
     ((,monokaix-class (:foreground ,monokaix-green
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-green
                                        :weight bold))))

   `(which-key-separator-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(which-key-note-face
     ((,monokaix-class (:foreground ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments))))

   `(which-key-command-description-face
     ((,monokaix-class (:foreground ,monokaix-foreground))
      (,monokaix-256-class (:foreground ,monokaix-256-foreground))))

   `(which-key-local-map-description-face
     ((,monokaix-class (:foreground ,monokaix-yellow-hc))
      (,monokaix-256-class (:foreground ,monokaix-256-yellow-hc))))

   `(which-key-group-description-face
     ((,monokaix-class (:foreground ,monokaix-red
                                   :weight bold))
      (,monokaix-256-class (:foreground ,monokaix-256-red
                                        :weight bold))))
   ;; window-number-mode
   `(window-number-face
     ((,monokaix-class (:foreground ,monokaix-green))
      (,monokaix-256-class (:foreground ,monokaix-256-green))))

   ;; yascroll
   `(yascroll:thumb-text-area
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-comments))))

   `(yascroll:thumb-fringe
     ((,monokaix-class (:foreground ,monokaix-comments
                                   :background ,monokaix-comments))
      (,monokaix-256-class (:foreground ,monokaix-256-comments
                                        :background ,monokaix-256-comments))))

   ;; zencoding
   `(zencoding-preview-input
     ((,monokaix-class (:background ,monokaix-highlight-line
                                   :box ,monokaix-emphasis))
      (,monokaix-256-class (:background ,monokaix-256-highlight-line
                                        :box ,monokaix-256-emphasis)))))

  (custom-theme-set-variables
   'monokaix
   `(ansi-color-names-vector [,monokaix-background ,monokaix-red ,monokaix-green ,monokaix-yellow
                                                  ,monokaix-blue ,monokaix-magenta ,monokaix-cyan ,monokaix-foreground])

   ;; compilation
   `(compilation-message-face 'default)

   ;; fill-column-indicator
   `(fci-rule-color ,monokaix-highlight-line)

   ;; magit
   `(magit-diff-use-overlays nil)

   ;; highlight-changes
   `(highlight-changes-colors '(,monokaix-magenta ,monokaix-violet))

   ;; highlight-tail
   `(highlight-tail-colors
     '((,monokaix-highlight-line . 0)
       (,monokaix-green-lc . 20)
       (,monokaix-cyan-lc . 30)
       (,monokaix-blue-lc . 50)
       (,monokaix-yellow-lc . 60)
       (,monokaix-orange-lc . 70)
       (,monokaix-magenta-lc . 85)
       (,monokaix-highlight-line . 100)))

   ;; pos-tip
   `(pos-tip-foreground-color ,monokaix-background)
   `(pos-tip-background-color ,monokaix-yellow-hc)

   ;; vc
   `(vc-annotate-color-map
     '((20 . ,monokaix-red)
       (40 . "#CF4F1F")
       (60 . "#C26C0F")
       (80 . ,monokaix-yellow)
       (100 . "#AB8C00")
       (120 . "#A18F00")
       (140 . "#989200")
       (160 . "#8E9500")
       (180 . ,monokaix-green)
       (200 . "#729A1E")
       (220 . "#609C3C")
       (240 . "#4E9D5B")
       (260 . "#3C9F79")
       (280 . ,monokaix-cyan)
       (300 . "#299BA6")
       (320 . "#2896B5")
       (340 . "#2790C3")
       (360 . ,monokaix-blue)))
   `(vc-annotate-very-old-color nil)
   `(vc-annotate-background nil)

   ;; weechat
   `(weechat-color-list
     '(unspecified ,monokaix-background ,monokaix-highlight-line
                  ,monokaix-red-d ,monokaix-red
                  ,monokaix-green-d ,monokaix-green
                  ,monokaix-yellow-d ,monokaix-yellow
                  ,monokaix-blue-d ,monokaix-blue
                  ,monokaix-magenta-d ,monokaix-magenta
                  ,monokaix-cyan-d ,monokaix-cyan
                  ,monokaix-foreground ,monokaix-emphasis))))

;;;###autoload
(when (and (boundp 'custom-theme-load-path) load-file-name)
  (add-to-list 'custom-theme-load-path
               (file-name-as-directory (file-name-directory load-file-name))))

(provide-theme 'monokaix)

;; Local Variables:
;; no-byte-compile: t
;; fill-column: 95
;; End:

;;; monokaix-theme.el ends here
