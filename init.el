;; Set the config directory
(add-to-list 'load-path (concat (getenv "HOME") "/.config/emacs/assets/site-lisp/"))

(defvar +/config/reldir-config "config/"
  "Configuration subdirectory within the user's emacs directory.")

(defvar +/config/dir-config
  (expand-file-name "config/" user-emacs-directory) "Configuration directory.")

(defvar +/config/dir-assets (expand-file-name "assets/" user-emacs-directory) "Assets directory.")

(add-to-list 'load-path +/config/dir-config)
(load (expand-file-name "lib/utils.el"  +/config/dir-config))
(load (expand-file-name "bootstrap.el" +/config/dir-config))

(+/config/load-relative-directory "config/core" )
(+/config/load-relative-directory "config/core/keybindings" )
(+/config/load-relative-directory "config/modules" )
(+/config/load-relative-directory "config/modules" )
(+/config/load-relative-directory "config/modules/dev" )
(+/config/load-relative-directory "config/modules/filemanager" )
(+/config/load-relative-directory "config/modules/org" )

(+/config/load-relative-directory "config/custom" )


;; (+/UIX/base-look)
;; (server-mode)

(if (daemonp)
    (add-hook 'after-make-frame-functions
	            (lambda (frame)
		         ;;   (+/UIX/term-keys)
		            (select-frame frame)
		            (if(display-graphic-p)
		                (+/UIX/base-look)(+/UIX/base-look-term)
		                )
		            )
	            )
  (add-hook 'window-setup-hook (lambda()
                                (if(display-graphic-p)
                                     (+/UIX/base-look)(+/UIX/base-look-term)
                                     )))
  )

