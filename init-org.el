(setq use-package-always-defer t )

(setq gc-cons-threshold (* 128 1024 1024))
(setq gc-cons-percentage 0.7)
(add-to-list 'load-path (concat (getenv "HOME") "/.config/emacs/config/site-lisp/"))

(defvar +/config/reldir-config "config/"
  "Configuration subdirectory within the user's emacs directory.")
(defvar +/config/dir-config
  (expand-file-name "config/" user-emacs-directory)
  "Configuration directory.")
;; Set up the utilities
(load (expand-file-name "lib/utils.el"  +/config/dir-config))
;; straight bootstrapping
(load (expand-file-name "lib/bootstrap-straight.el" +/config/dir-config))
;; Set up base libraries
(load (expand-file-name "lib/base.el" +/config/dir-config))
;; config: settings and user-settings directory and files

(setq
 +/info/name  "Sander Boer"
 +/info/email "sander@starling.studio")

(setq
 ;; Folder for non-MELPA packages (relative to X/C/)
 +/packages/reldir-non-melpa "src/non-melpa-packages/"

 ;; Ligature support (ligature.el)
 ;; This should be emacs proper in the future.
 +/packages/file-ligature "ligature")

(setq
 ;; Font
 +/fonts/font "JuliaMono-14:regular"

 ;; Modeleader key
 +/UIX/modeleader "SPC"

 ;; Split width threshold that works for 1920x1080 screen
 +/UIX/vertical-split-threshold 125)

(setq
 ;; org
 +/org/dir-org "~/.org/"

 ;; org-journal
 +/org/dir-journal "~/.org/journal/"

 ;; org-roam
 +/org/dir-roam "~/.org/roam/"

 ;; org-roam: Loop and notes directories (relative to the roam directory)
 +/org/reldir-roam-loops-open   "loops/open/"
 +/org/reldir-roam-loops-cold   "loops/cold/"
 +/org/reldir-roam-loops-closed "loops/closed/"
 +/org/reldir-roam-loops-binned "loops/binned/"
 +/org/reldir-roam-notes        "notes/"

 ;; org-roam: Template directory (relative to X/C/)
 +/org/reldir-roam-templates "templates/roam/"

 ;; org-capture (relative to the roam directory)
 +/org/reldir-capture +/org/reldir-roam-loops-open
 +/org/file-capture "_-captures.org"

 ;; org-capture: Templates
 +/org/config-capture-templates
 '(;; Quick tasks

   ("P" . ("process-soon" "Tasks"
  "* TODO %:fromname: %a %?\nDEADLINE: %(org-insert-time-stamp (org-read-date nil t \"+2d\"))"))
   
   ("t" . ("quick task" "Tasks"
           "* TODO [#%^{Priority|A|B|C}] %^{Task}\nSCHEDULED: %^t\n%?"))
   ;; email follow-ups
   ("e" . ("email follow-up" "email follow-ups"
           "* TODO [#%^{Priority|A|B|C}] :email: %^{Recipients}\nSCHEDULED: %^t\n%?"))
   ;; Notes
   ("n" . ("quick note" "Notes" "* %^{title} :triage:\n%?")))

 ;; org-agenda: Directories for generating agenda
 +/org/dirs-agenda (list
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-notes)
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-loops-cold)
                    (+/FS/create-path +/org/dir-roam
                                      +/org/reldir-roam-loops-open)))

(setq
 ;; yasnippets (relative to X/C/)
 +/yas/reldir-snippets "snippets/")

(setq
 ;; Set the locale
 +/lang/LANG "nl_NL"

 ;; Paths for the dictionaries
 +/lang/DICPATH "/usr/share/hunspell/:/home/sander/.config/emacs/config/dic"

 ;; Configure multi-language dictionary
 +/lang/MULTIDIC "en_GB,en_US")

(setq
 ;; Directories
 +/FS/bookmarks/dirs
 `(("root"      . ("/" "/"))
   ("home"      . ("~" "~/"))
   ("loops"     . ("l" ,(+/FS/create-path +/org/dir-roam
                                          +/org/reldir-roam-loops-open)))
   ("notes"     . ("n" ,(+/FS/create-path +/org/dir-roam
                                          +/org/reldir-roam-notes)))
   ("config"    . ("c" ,+/config/dir-config))
   )

 ;; Files
 +/FS/bookmarks/files
 `(;; captures (SPC #)
   ("captures"         . ("#" ,(+/FS/create-path +/org/dir-roam
                                                 +/org/reldir-capture
                                                 +/org/file-capture)))))

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)
(setq-default indent-tabs-mode nil)  ;; SPACES over TABS !
(setq pixel-scroll-mode 1)
(require 'uniquify)
(setq uniquify-buffer-name-style 'forward)

;;(setq linum-format "%4d \u2502 " )
(setq linum-format "%4d ")
(set-face-attribute 'show-paren-mismatch nil :background "red")
;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)
 ;; Performance tweaks, see
  ;; https://github.com/emacs-lsp/lsp-mode#performance
(setq read-process-output-max (* 1024 1024)) ;; 1mb
(setq lsp-idle-delay 0.500)

(fset 'yes-or-no-p 'y-or-n-p)

;; Speed up startup
;; Adapted from https://github.com/seagle0128/.config/emacs/.
(defvar default-file-name-handler-alist file-name-handler-alist)
(setq file-name-handler-alist nil)
(unless debug-on-error
  (setq inhibit-redisplay t))
(add-hook 'emacs-startup-hook
          (lambda ()
            "Restore defalut values after init."
            (setq file-name-handler-alist default-file-name-handler-alist)
            (setq inhibit-redisplay nil)
            (redisplay t)
            (if (boundp 'after-focus-change-function)
                (add-function :after after-focus-change-function
                              (lambda ()
                                (unless (frame-focus-state)
                                  (garbage-collect))))
              (add-hook 'focus-out-hook 'garbage-collect))))
(unless debug-on-error
  (setq inhibit-redisplay t))

(use-package esup
  :straight t
  )

(setq user-full-name    +/info/name
      user-mail-address +/info/email)

;; UTF-8 please and thank you
(set-language-environment "English")    ; Set up multilingual environment
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top
(define-coding-system-alias 'UTF-8 'utf-8)
(define-coding-system-alias 'utf8 'utf-8)


;; Character encodings default to utf-8.
(set-language-environment "UTF-8")

(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)

;; MS Windows clipboard is UTF-16LE
(if (eq system-type 'gnu/linux)
    (set-clipboard-coding-system 'utf-8)
  (set-clipboard-coding-system 'utf-16le-dos)
  )

(setq auto-save-list-file-prefix ; Prefix for generating auto-save-list-file-name
      (expand-file-name ".auto-save-list/.saves-" user-emacs-directory)
      auto-save-default t        ; Auto-save every buffer that visits a file
      auto-save-timeout 20       ; Number of seconds between auto-save
      auto-save-interval 200)    ; Number of keystrokes between auto-saves


;; Use no-littering to automatically set common paths to the new user-emacs-directory


(setq user-emacs-directory (expand-file-name "~/.cache/emacs"))
(use-package no-littering
  :straight t)
(setq
 auto-save-file-name-transforms `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
 custom-file (expand-file-name "custom.el" user-emacs-directory)
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

(setq create-lockfiles nil)

;; Keep customization settings in a temporary file (thanks Ambrevar!)
;; (setq custom-file
;;       (if (boundp 'server-socket-dir)
;;           (expand-file-name "custom.el" server-socket-dir)
;;         (expand-file-name (format "emacs-custom-%s.el" (user-uid)) temporary-file-directory)))


(setq custom-file (+/FS/create-path user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

(setq backup-directory-alist       ; File name patterns and backup directory names.
      `(("." . ,(expand-file-name "backups" user-emacs-directory)))
      make-backup-files t          ; Backup of a file the first time it is saved.
      vc-make-backup-files t       ; No backup of files under version contr
      backup-by-copying t          ; Don't clobber symlinks
      version-control t            ; Version numbers for backup files
      delete-old-versions t        ; Delete excess backup files silently
      kept-old-versions 6          ; Number of old versions to keep
      kept-new-versions 9          ; Number of new versions to keep
      delete-by-moving-to-trash t) ; Delete files to trash

;; Back
(use-package vc-backup
  :straight t)

(require 'savehist)

(setq kill-ring-max 50
      history-length 50)

(setq savehist-additional-variables
      '(kill-ring
        command-history
        set-variable-value-history
        custom-variable-history   
        query-replace-history     
        read-expression-history   
        minibuffer-history        
        read-char-history         
        face-name-history         
        bookmark-history
        file-name-history))

(put 'minibuffer-history         'history-length 50)
(put 'file-name-history          'history-length 50)
(put 'set-variable-value-history 'history-length 25)
(put 'custom-variable-history    'history-length 25)
(put 'query-replace-history      'history-length 25)
(put 'read-expression-history    'history-length 25)
(put 'read-char-history          'history-length 25)
(put 'face-name-history          'history-length 25)
(put 'bookmark-history           'history-length 25)

(setq history-delete-duplicates t)
;;(let (message-log-max)
;;   (savehist-mode))
(setq save-place-file (expand-file-name "saveplace" user-emacs-directory)
      save-place-forget-unreadable-files t)

(save-place-mode 1)

(when (daemonp)
  (message "Starting emacs daemon..."))

(use-package exec-path-from-shell
  :straight t
  :config
  (when (daemonp)
    (exec-path-from-shell-initialize)))

(use-package asdf
  :straight (:type git :host github :repo "tabfugnic/asdf.el")
  :config
  (require 'asdf))

(defun +/fonts/setup-JuliaMono ()
  "Set up JuliaMono font. Tested on Debian 11."
  (interactive)

  (copy-directory
   (+/FS/create-path +/config/dir-config "fonts/JuliaMono/")
   "~/.local/share/fonts/JuliaMono/"
   nil t t)

  (shell-command-to-string "fc-cache -f -v"))

;; Install JuliaMono if set in user-settings
(if (and (string-match-p "JuliaMono" +/fonts/font)
         (not (find-font (font-spec :name "JuliaMono")))
         (not (daemonp)))
    (+/fonts/setup-JuliaMono))

;; Set the default frame font
;; (add-to-list 'default-frame-alist `(font . ,+/fonts/font))

(global-prettify-symbols-mode nil)
(setq prettify-symbols-unprettify-at-point t)

(defun +/fonts/prettify-symbols-push-to-alist (symbol-list)
  "Set symbols for prettification. Used in conjuction with modal hooks."

  (mapc (lambda (pair) (push pair prettify-symbols-alist))
        symbol-list)
  (prettify-symbols-mode 1))

(use-package all-the-icons
  :straight t
  :if (display-graphic-p)
  :config
  (unless (find-font (font-spec :name "all-the-icons"))
    (all-the-icons-install-fonts t)))

(use-package emojify
  :straight t
  :init (setq emojify-download-emojis-p t)
  :hook (after-init . global-emojify-mode))

(defun +/fonts/setup-unicode-fonts ()
  "Set up unicode fonts AFTER installation."
  (interactive)

  (when (require 'unicode-fonts nil 'noerror)
    (unicode-fonts-setup)))

;; (use-package unicode-fonts
;;   :straight t
;;   :config
;;   (when (eq unicode-fonts-setup-done nil)
;;     (+/fonts/setup-unicode-fonts)))

(defun +/fonts/text-scale-reset ()
  (interactive)
  (text-scale-set 0))

(setq visible-bell 1)
(setq inhibit-startup-message t
      initial-scratch-message nil)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(set-fringe-mode 10)
(setq scroll-conservatively 101)
(menu-bar-mode -1)
(setq ediff-split-window-function #'split-window-horizontally)
(global-visual-line-mode t)
(set-mouse-color "white")

(defun +/UIX/disable-visual-line-wrapping-hook ()
  "Disable visual line wrapping in the current buffer."

  (visual-line-mode nil)
  (setq truncate-lines 1))

(use-package imenu-list
  :after org
  :straight t
  :config
  (setq
   org-imenu-depth 5
   imenu-max-item-length 30
   imenu-list-auto-resize t
   imenu-list-mode-line-format nil
   imenu-list-position 'left)

  ;; Hook
  (add-hook 'imenu-list-major-mode-hook
            #'+/UIX/disable-visual-line-wrapping-hook))

(setq calendar-week-start-day 1)

(setq calendar-intermonth-text
      '(propertize
        (format "%2d"
                (car
                 (calendar-iso-from-absolute
                  (calendar-absolute-from-gregorian (list month day year)))))
        'font-lock-face 'font-lock-warning-face))

(setq calendar-intermonth-header
      (propertize "Wk"                  ; or e.g. "KW" in Germany
                  'font-lock-face 'font-lock-keyword-face))

(use-package dashboard
  :straight t
  :demand t
  :after projectile
  :config
  (setq
   ;; Sections
   dashboard-items '((projects . 7)
                     (recents  . 7))
   ;; Items/Widgets
   dashboard-item-names '(("Projects:"     . "[p] Recent projects:")
                          ("Recent Files:" . "[r] Recent files:"))
   ;; Banner
   dashboard-startup-banner (+/FS/create-path
                             +/config/dir-config
                             "assets/starling-logo.svg")
   dashboard-image-banner-max-width 120
   dashboard-banner-logo-title "Welcome to Emacs, groundup!"
   ;; Hide/Show shortcut key
   dashboard-show-shortcuts nil
   ;; Centre content
   dashboard-center-content t
   ;; Icons
   dashboard-set-heading-icons t
   dashboard-set-file-icons t
   ;; No footer
   dashboard-set-footer nil
   ;; Project switching with projectile/persp
   dashboard-projects-switch-function 'projectile-persp-switch-project
   ;; Shorten path
   dashboard-path-style 'truncate-middle
   dashboard-path-max-length 60
   ;; dashboard-shorten-by-window-width t
   ;; Load dashboard for each new frame
   ;; initial-buffer-choice (lambda () (get-buffer "*dashboard*"))

   )
  ;; No visual wrapping
  (add-hook 'dashboard-mode-hook
            #'+/UIX/disable-visual-line-wrapping-hook)
  ;; Set everything up
  (dashboard-setup-startup-hook))

(require 'color)
(defun hexcolour-luminance (color)
  "Calculate the luminance of a color string (e.g. \"#ffaa00\", \"blue\").
  This is 0.3 red + 0.59 green + 0.11 blue and always between 0 and 255."
  (let* ((values (x-color-values color)) (r (car values)) (g (cadr values))
	 (b (caddr values))) (floor (+ (* .3 r) (* .59 g) (* .11 b)) 256)))

(defun set-colors-based-on-theme ()
  (interactive)
  "Sets the hl-line face to have no foregorund and a background
        that is 10% darker than the default face's background."
  (setq bg
        (if (or (eq (face-background 'default) nil)
		            (eq (face-background 'default) "unspecified-bg"))
            (color-darken-name "#000000" 0 )
          (face-background 'default )
          ))
  (if (< (hexcolour-luminance bg ) 20)
      (setq sgn -1) (setq sgn 1) )
  (set-face-attribute 'hl-line nil
                      :foreground nil
                      :background (color-desaturate-name
			                             (color-lighten-name bg (* sgn 120))
			                             30))
(set-face-background 'show-paren-match-expression
		                   (color-desaturate-name
			                  (color-lighten-name bg (* sgn 130))
			100))
  (set-face-background 'show-paren-match
		                   (color-desaturate-name
			                  (color-lighten-name bg (* sgn 130))
			100))
  (set-face-background 'region
		                   (color-desaturate-name
			                  (color-lighten-name bg (* sgn 80))
			                   50))
  )

(defun sndr-term-keys()
  (interactive)
  (progn
    (define-key input-decode-map "\e[1;2D" [S-left])
    (define-key input-decode-map "\e[1;2C" [S-right])
    (define-key input-decode-map "\e[1;2B" [S-down])
    (define-key input-decode-map "\e[1;2A" [S-up])
    (define-key input-decode-map "\e[1;2F" [S-end])
    (define-key input-decode-map "\e[1;2H" [S-home])
    (define-key input-decode-map "\e[1;5D" [C-left])
    (define-key input-decode-map "\e[1;5C" [C-right])
    (define-key input-decode-map "\e[1;5B" [C-down])
    (define-key input-decode-map "\e[1;5A" [C-up])
    (define-key input-decode-map "\e[1;3A" [M-up])
    (define-key input-decode-map "\e[1;3B" [M-down])
    (define-key input-decode-map "\e[1;3C" [M-right])
    (define-key input-decode-map "\e[1;3D" [M-left])
    )
  )

(defun base-look-term()
  "How a term should look."
  (interactive)
  (global-hl-line-mode t)
  (load-theme theme-default t)
  (set-colors-based-on-theme)
  ;; (sndr-set-nano-theme) 
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  (set-face-background 'default "unspecified-bg" (selected-frame))
  (sndr-term-keys)
  ;;(set-colors-based-on-theme )
  )


(defun base-look()
  "How a window should look."
  (interactive)
  (progn
    (if (string= (system-name) "sun-ra")
	      (progn
	        ;; (set-face-attribute 'default nil :height 120)
	        )
      (progn
	      ;; (set-face-attribute 'default nil :height 120)
	      )
      )
    (menu-bar-mode -1)
    (tool-bar-mode -1)
    (scroll-bar-mode -1)
    (tooltip-mode -1)
    (global-hl-line-mode t)
    (load-theme theme-default t)
    ;; (sndr-set-nano-theme) 
    (set-colors-based-on-theme)
    (set-frame-parameter (selected-frame) 'internal-border-width 30)
    ;; (set-frame-parameter (selected-frame) 'alpha '(95 90))
    ;; (add-to-list 'default-frame-alist '(alpha 95 90))
    (setq header-line-format " ")
    (set-frame-parameter (selected-frame) 'bottom-divider-width 1)
    (dolist
        (buf (list " *Minibuf-0*" " *Minibuf-1*" " *Echo Area 0*" " *Echo Area 1*" "*Quail Completions*"))
      (when (get-buffer buf)
        (with-current-buffer buf
          (setq-local face-remapping-alist '((default (:height 1.0)))))))
    )
  )

(defun +/UIX/buffer-to-clipboard ()
    "Copy entire buffer to clipboard"
    (interactive)
    (clipboard-kill-ring-save (point-min) (point-max)))

;; (if (daemonp)
;;     (add-hook 'after-make-frame-functions
;; 	            (lambda (frame)
;; 		            (sndr-term-keys)
;; 		            (select-frame frame)
;; 		            (if(display-graphic-p)
;; 		                (base-look)(base-look-term)
;; 		                )
;; 		            )
;; 	            )
;;   (add-hook 'window-setup-hook (lambda()
;;                                  (if(display-graphic-p)
;;                                      (base-look)(base-look-term)
;;                                      ))))

;; SVG tags, progress bars & icons
(use-package svg-lib
  :straight( :type git :host github :repo "rougier/svg-lib")
  )

;; Replace keywords with SVG tags
(use-package svg-tag-mode
  :straight 
  (:type git :host github :repo "rougier/svg-tag-mode")
  )

(defface sndr-org-todo
  '((t :foreground "black"
       :background "#cd927b"
       :font-weight 500
       ))
  "Face for function parameters."
  :group 'sndr-faces )

(defface sndr-org-next
  '((t :foreground "black"
       :background "#bb9b6c"
       :font-weight 500
       ))
  "Face for global variables."
  :group 'sndr-faces )
(defface sndr-org-done
  '((t :foreground "black"
       :background "#83ab79"
       :font-weight 500
       ))
  "Face for global variables."
  :group 'sndr-faces )
(defface sndr-org-fade
  '((t :foreground "black"
       :background "#444"
       :font-weight 500
       ))
  "Face for global variables."
  :group 'sndr-faces )

(setq svg-tag-tags
      '(
        ("DOEN" . ((lambda (tag) (svg-tag-make "DOEN"
                                                 :face 'sndr-org-todo
                                                 :radius 3
                                                 :margin 0
                                                 ))))
        ("VOLGENDE" . ((lambda (tag) (svg-tag-make "VOLGENDE"
                                                     :face 'sndr-org-next
                                                     :radius 3
                                                     :margin 0
                                                     ))))
        ("VOLTOOID" . ((lambda (tag) (svg-tag-make "VOLTOOID"
                                                     :face 'sndr-org-done
                                                     :radius 3
                                                     :margin 0 
                                                     ))))
        ("OOIT" . ((lambda (tag) (svg-tag-make "OOIT"
                                                 :face 'sndr-org-fade
                                                 :radius 3
                                                 :margin 0 
                                                 ))))
        ("WACHTEN" . ((lambda (tag) (svg-tag-make "WACHTEN"
                                                    :face 'sndr-org-fade
                                                    :radius 3
                                                    :margin 0 
                                                    ))))
        ("#\\+[bB][eE][gG][iI][nN]_[sS][rR][cC]\\( [a-zA-Z\-]+\\)" .  ((lambda (tag)
                                                  (svg-tag-make (upcase tag)
                                                                :face 'org-meta-line
                                                                :inverse t
                                                                :crop-left t))))
        ("#\\+[eE][nN][dD]_[sS][rR][cC]" .    ((lambda (tag) (svg-tag-make "END"
                                                          :face 'org-meta-line))))
        ("\\(#\\+[bB][eE][gG][iI][nN]_[sS][rR][cC]\\)" .    ((lambda (tag) (svg-tag-make "BEGIN"
                                                            :face 'org-meta-line
                                                            :crop-right t
                                                            ))))
        )
      )


(add-hook 'org-mode-hook (lambda () 
                           (plist-put svg-lib-style-default :font-family "RobotoMono Nerd Font")
                           (plist-put svg-lib-style-default :font-size 12)
                           (svg-tag-mode t)
                           ))

(defun sndr-set-nano-theme()
  (interactive)
  (require 'nano-theme)
  (require 'nano-theme-light)
  (require 'nano-theme-dark)
  (nano-theme-set-dark)
  (call-interactively 'nano-refresh-theme)
  (set-colors-based-on-theme )
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; My Custom Modeline ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; from:
;; https://emacs-fu.blogspot.com/2011/08/customizing-mode-line.html

(defun mode-line-fill (face reserve)
  "Return empty space using FACE and leaving RESERVE space on the right."
  (when
      (and window-system (eq 'right (get-scroll-bar-mode)))
    (setq reserve (- reserve 3)))
  (propertize " "
              'display
              `((space :align-to (- (+ right right-fringe right-margin) ,reserve)))
              'face face
              )
  )

(use-package nyan-mode
  :straight t
  :config(progn
           (setq nyan-bar-length 15))
  )
(require 'nyan-mode)

        ;;;(let (sndr-font-base (cdr (assoc "zenbu-salmon" zenbu-colors-alist)) )
;; ■◧  ▥

(defun get-evil-state-icons()
  (interactive)
  (cond
   ((memq evil-state '(emacs))
    (propertize "⭘ ⭘" 'face '((:foreground "orange" :weight bold )))
    )

   ((memq evil-state '(hybrid insert))
    (propertize " " 'face '((:foreground "green" :weight bold )))
    )

   ((memq evil-state '(visual))
    (propertize "⭘ ⭘" 'face '((:foreground "red" :weight bold )))
    )

   (t
    (propertize "⭘ ⭘" 'face '((:weight ultra-light )))
    )
   )
  )

(setq-default mode-line-format
              (list
               " "
               '(:eval (get-evil-state-icons) );; end evil-state
               " "
               '(:eval (when buffer-read-only
                         (propertize " " 'help-echo "Buffer is read-only")))
               '(:eval
                 (if (buffer-modified-p)
                     (propertize " %b " 'face '((:weight bold )) 'help-echo (buffer-file-name) )
                   (propertize "%b " 'help-echo (buffer-file-name))
                   ))

               (propertize " · " 'face 'font-lock-type-face)

               ;; '%02' to set to 2 chars at least; prevents flickering
               (propertize "%02l, %02c" 'face 'font-lock-type-face)

               ;; the current major mode for the buffer.
               (propertize " · %m ·" 'face 'font-lock-type-face)

               mode-line-misc-info

               (propertize " · " 'face 'font-lock-type-face)

               (mode-line-fill 'mode-line 30)
               (propertize " · " 'face 'font-lock-type-face)

               '(:eval (list (nyan-create)))
               (propertize " · " 'face 'font-lock-type-face)

               '(:eval (if-let (vc vc-mode)
                           (list "  " (substring vc 5))
                         (list "        " )
                         ))

               ))

(use-package hide-mode-line
  :straight t)

(add-to-list 'custom-theme-load-path "~/.config/emacs/config/themes/")
(setq theme-default 'zenbu)

;; (use-package golden-ratio
;;   :straight t
;;   :hook (after-init . golden-ratio)
;;   :custom
;;   (golden-ratio-mode 1)
;;   (setq golden-ratio-exclude-modes '("dired-mode"
;;                                    "ediff-mode"
;;                                    "eshell-mode"
;;                                    "neotree"
;;                                    "neotree-mode"
;;                                    "sr-speedbar-mode"))
;;   )

  ;; (eval-after-load "neotree"
  ;;   '(add-to-list 'window-size-change-functions
  ;;                 (lambda (frame)
  ;;                   (let ((neo-window (neo-global--get-window)))
  ;;                     (unless (null neo-window)
  ;;                       (setq neo-window-width (window-width neo-window)))))))

;; (defun neotree-resize-window (&rest _args)
;;     "Resize neotree window.
;; https://github.com/jaypei/emacs-neotree/pull/110"
;;     (interactive)
;;     (neo-buffer--with-resizable-window
;;      (let ((fit-window-to-buffer-horizontally t))
;;        (fit-window-to-buffer))))

;;   (add-hook 'neo-change-root-hook #'neotree-resize-window)
;;   (add-hook 'neo-enter-hook #'neotree-resize-window)
;;   (add-hook 'neo-after-create-hook 'neotree-resize-window)

;; (setq golden-ratio-exclude-modes '("dired-mode"
;;                                    "ediff-mode"
;;                                    "eshell-mode"
;;                                    "neotree"
;;                                    "neotree-mode"
;;                                    "sr-speedbar-mode"))

(defmacro save-column (&rest body)
  `(let ((column (current-column)))
     (unwind-protect
         (progn ,@body)
       (move-to-column column))))
(put 'save-column 'lisp-indent-function 0)

(defun move-line-up ()
  (interactive)
  (save-column
    (transpose-lines 1)
    (forward-line -2)))

(defun move-line-down ()
  (interactive)
  (save-column
    (forward-line 1)
    (transpose-lines 1)
    (forward-line -1)))

;; Backup directory (create if it doesn't exist)
(defvar +/UIX/dir-backup (+/FS/create-path user-emacs-directory "backups/"))
(unless (file-exists-p +/UIX/dir-backup)
  (make-directory +/UIX/dir-backup t))

;; Parameters
(setq make-backup-files t
      backup-by-copying t
      vc-make-backup-files t
      backup-directory-alist `(("." . ,+/UIX/dir-backup))
      version-control t
      kept-new-versions 3
      kept-old-versions 3
      delete-old-versions t)

(setq auto-save-default t
      auto-save-timeout 20
      auto-save-interval 150)

(global-auto-revert-mode 1)

;; for non-file buffers like dired
(setq global-auto-revert-non-file-buffers t)

(setq delete-by-moving-to-trash t)

(use-package undo-fu
  :straight t)

(show-paren-mode t)
(setq show-paren-style 'expression)
(electric-pair-mode t)

;; (recentf-mode 1)

(use-package rainbow-mode
  :straight t
  :config
  (rainbow-mode +1))

(defadvice text-scale-increase (around all-buffers (arg) activate)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      ad-do-it)))

(use-package tramp
  :straight t
  :disabled t
  ;; :straight (:build t :pre-build (("make" "autoloads")))
  :config
  (setq tramp-default-method "ssh"))

(use-package sudo-edit
  :straight t)

(use-package direnv
  :straight t
  :config
  (direnv-mode))

(use-package vterm
  :straight t
  :after evil-collection
  :init (setq vterm-always-compile-module t))

(use-package which-key
  :straight t
  :init (which-key-mode)
  :config
  (setq
   which-key-idle-delay 0.3
   ;; which-key-sort-order 'which-key-key-order))
   ;; which-key-sort-order 'which-key-prefix-then-key-order))
   ;; which-key-sort-order 'which-key-description-order))
   which-key-sort-order 'which-key-key-order-alpha))

(use-package helpful
  :straight t)

(use-package key-chord
  :straight t
  :after evil
  :config
  (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "jj" 'evil-normal-state)
  (key-chord-define evil-insert-state-map "kk" 'evil-normal-state)

  (key-chord-mode 1))

(use-package general
  :straight t
  :after (evil pdf-tools)
  :config
  (general-evil-setup t)

  ;; Risky but we'll take it...
  (general-auto-unbind-keys nil))

(general-create-definer +/UIX/map-to-leader
  :keymaps 'override
  :prefix "SPC"
  "" '(:ignore t :wk "leader-key"))

(general-create-definer +/UIX/map-to-modeleader
  :keymaps 'override
  :prefix (concat "SPC " +/UIX/modeleader)
  "" '(:ignore t :wk "modebindings"))

(with-eval-after-load 'evil
  (general-add-hook 'after-init-hook
                    (lambda (&rest _)
                      (when-let ((messages-buffer (get-buffer "*Messages*")))
                        (with-current-buffer messages-buffer
                          (evil-normalize-keymaps))))
                    nil nil t))

(use-package hydra
  :straight t)

(use-package yasnippet
  :straight t
  :config
  (setq yas-snippet-dirs `(,(+/FS/create-path +/config/dir-config
                                              +/yas/reldir-snippets)))
  (yas-global-mode 1))

(use-package dired-x
  :straight nil
  :hook
  ((dired-mode . (lambda () (interactive)
                   (all-the-icons-dired-mode)
                   (dired-omit-mode))))
  :config
  (setq dired-dwim-target t
        find-file-visit-truename nil
        dired-omit-files "^\\.?#\\|^\\.$\\|^\\.\\.$\\|^\\..*$")

  ;; macos hack
  (when (string= system-type "darwin")
    (setq dired-use-ls-dired t
          insert-directory-program "/opt/homebrew/bin/gls"
          dired-listing-switches "-aBhl --group-directories-first"))

  (add-hook 'dired-mode-hook #'+/UIX/disable-visual-line-wrapping-hook))

(use-package all-the-icons-dired
  :straight t
  :config
  (setq all-the-icons-dired-monochrome nil))

(use-package dired-du
  :straight t
  :after dired
  :config
  (setq dired-du-size-format t))

(use-package dired-quick-sort
  :straight t
  :after dired
  :init
  (add-hook 'dired-mode-hook 'dired-quick-sort))

(use-package ace-window
  :straight t
  :custom
  (aw-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  :config
  (setq aw-scope 'frame)
  (custom-set-faces
   '(aw-leading-char-face
     ((t (:foreground "red" :background nil :weight bold :height 3.0 :box nil))))))

(require 'ace-window)
(defun +/FS/visit-file-from-dired-using-ace-window ()
  "Use ace window to select a window for opening a file from dired."
  (interactive)

  (let ((file (dired-get-file-for-visit)))
    (if (> (length (aw-window-list)) 1)
        (aw-select "" (lambda (window)
                        (aw-switch-to-window window)
                        (find-file file)))
      (find-file-other-window file))))

(defun +/FS/prompt-and-kill-current-dired-buffer ()
  "Prompt and kill dired buffer."
  (interactive)

  (if (y-or-n-p "kill this dired buffer?")
      (kill-current-buffer)))

(use-package neotree
     :straight t
     :config
     (setq neo-theme (if (display-graphic-p) 'nerd 'ascii))
     (setq neo-window-width 35)
     (setq neo-window-fixed-size nil)
     ;; (setq neo-autorefresh 1)
     (setq neo-autorefresh nil)
     (setq neo-auto-indent-point 1)

     )
   ;; Disable line-numbers minor mode for neotree
(add-hook 'neo-after-create-hook (lambda (&optional dummy)
                                   (display-line-numbers-mode -1)
                                   (visual-line-mode nil)
                                   (setq truncate-lines 1)
                                   ))

(use-package treemacs
  :straight t
  :custom
  (treemacs-is-never-other-window t)
  :hook
  (treemacs-mode . treemacs-project-follow-mode)
  (treemacs-mode . +/UIX/disable-visual-line-wrapping-hook) 
  )

(use-package nerd-icons
  :straight (nerd-icons
             :type git
             :host github
             :repo "rainstormstudio/nerd-icons.el"
             :files (:defaults "data"))
  :custom
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want
  (nerd-icons-font-family "Symbols Nerd Font Mono")
  )
  (require 'treemacs-nerd-icons)
  (treemacs-load-theme "nerd-icons")

(use-package dirvish
  :straight t
  :disabled t
  :config(
          ;; Don't worry, Dirvish is still performant even if you enable all these attributes
          (setq dirvish-attributes
                '(vc-state subtree-state all-the-icons collapse git-msg file-time file-size))
          ;; Placement
          ;; (setq dirvish-use-header-line nil)     ; hide header line (show the classic dired header)
          ;; (setq dirvish-use-mode-line nil)       ; hide mode line
          (setq dirvish-use-header-line 'global)    ; make header line span all panes
          
          ;; Height
          ;; '(25 . 35) means
          ;;   - height in single window sessions is 25
          ;;   - height in full-frame sessions is 35
          (setq dirvish-header-line-height '(25 . 35))
          (setq dirvish-mode-line-height 25) ; shorthand for '(25 . 25)

          ;; Segments
          ;; 1. the order of segments *matters* here
          ;; 2. it's ok to place raw string inside
          (setq dirvish-header-line-format
                '(:left (path) :right (free-space))
                dirvish-mode-line-format
                '(:left (sort file-time " " file-size symlink) :right (omit yank index)))
          )
  )

(use-package ranger
   :straight t)

;; (use-package tex
;;   :straight auctex
;;   :after org
;;   :config
;;   (setq org-format-latex-options
;;    (plist-put org-format-latex-options :scale 2.0)))

;; (use-package cdlatex
;;   :straight t
;;   :config
;;   (add-hook 'LaTeX-mode-hook 'turn-on-cdlatex))

(use-package evil
  :straight t
  :demand t
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-undo-system 'undo-fu
        evil-respect-visual-line-mode t)

  :config
  (setq
   evil-auto-indent t
   evil-want-fine-undo t
   ;; evil-default-cursor t
   evil-want-minibuffer t
   evil-overriding-maps t)

  (evil-normalize-keymaps)
  (evil-mode 1))

(use-package evil-collection
  :straight t
  :after evil
  :custom
  (evil-collection-setup-minibuffer t)
  :init
  (evil-collection-init))

(use-package evil-surround
  :straight t
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-org
  :straight t
  :after (org evil)
  :hook (org-mode . (lambda () evil-org-mode))
  :config
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package evil-snipe
  :straight t
  :demand t
  :after evil
  :hook (magit-mode . turn-off-evil-snipe-override-mode)
  :custom
  (evil-snipe-scope 'whole-visible)
  (evil-snipe-repeat-scope 'whole-buffer)
  :config
  (evil-snipe-mode t)
  (evil-snipe-override-mode t))

(use-package flyspell
  :straight nil
  :after ispell

  :init
  ;; Set the locale for the dictionary
  (setenv "LANG" +/lang/LANG)

  ;; Dictionary path
  (setenv "DICPATH" +/lang/DICPATH)

  :config
  ;; Use hunspell
  (setq ispell-program-name "hunspell")

  ;; ispell-set-spellchecker-params has to be called before ispell-hunspell-add-multi-dic will work
  (ispell-set-spellchecker-params)

  ;; Declare a multi-language dictionary and switch to it
  (ispell-hunspell-add-multi-dic +/lang/MULTIDIC)
  (ispell-change-dictionary +/lang/MULTIDIC t))

(use-package flyspell-correct
  :straight t
  :after flyspell)

(use-package powerthesaurus
  :straight t)

(defun +/writing/spellcheck-and-correct-buffer ()
  "Spellcheck and then correct the entire buffer."
  (interactive)

  (flyspell-buffer)
  (flyspell-correct-wrapper))

(defun +/writing/spellcheck-region (beg end)
  "Spellcheck and correct the active region."
  (interactive "r")

  (let ((saved-point (point)))
    (when (use-region-p)
      (flyspell-region beg end)
      (goto-char saved-point))))

(defun +/writing/spellcheck-and-correct-region (beg end)
  "Spellcheck and correct the active region."
  (interactive "r")

  (when (use-region-p)
    (flyspell-region beg end)
    (flyspell-correct-wrapper)))

(use-package pass
  :straight t)

(use-package pdf-tools
  :straight (:type git :host github :repo "vedang/pdf-tools")
  :mode ("\\.[pP][dD][fF]\\'" . pdf-view-mode)
  :magic ("%PDF" . pdf-view-mode)
  :demand t
  :init
  ;; Stop cursor blinking hack
  (add-hook 'pdf-view-mode-hook (lambda () (blink-cursor-mode -1)))
  ;; Remove modeline from the outline-buffer
  (add-hook 'pdf-outline-buffer-mode-hook #'hide-mode-line-mode)

  :config
  (setq pdf-view-use-scaling t
        ;; pdf-outline-display-labels t
        pdf-annot-activate-created-annotations t
        pdf-annot-list-format '((page . 3)
                                (type . 10)
                                (date . 24)))

  ;; outline buffer appearance (SPC / m)
  ;; FIXME: How to do something similar for annots buffer?
  (customize-set-variable
   'display-buffer-alist
   '(("^\\*outline"
      display-buffer-in-side-window
      (side . left)
      (window-width . 0.35)
      (inhibit-switch-frame . t))))

   (pdf-loader-install))

(use-package org-pdftools
  :after org
  :straight t
  :after (org pdf-tools)
  :hook (org-mode . org-pdftools-setup-link))

(use-package org-noter
  :after org
  :straight t
  :after (org pdf-tools)
  :config
  (setq org-noter-doc-split-fraction '(0.6 . 0.5)))

(use-package saveplace-pdf-view
  :straight t
  :after pdf-view)

;; 125 works for 1920x1080 screens
(setq split-width-threshold +/UIX/vertical-split-threshold)

(use-package transpose-frame
  :demand t
  :straight t)

(use-package buffer-move
  :demand t
  :straight t)

(defun +/UIX/kill-this-buffer ()
  "Kill the current buffer."
  (interactive)

  (kill-buffer (current-buffer)))

(use-package projectile
  :demand t
  :straight t
  :after (dired magit)
  :init
  (projectile-mode +1)
  :config
  (setq projectile-require-project-root t
        ;; projectile-switch-project-action #'projectile-dired
        projectile-switch-project-action #'projectile-find-file
        projectile-sort-order 'recentf))

(defun +/projectile/run-vterm-other-window ()
  "Open vterm in other window."
  (interactive)

  (save-window-excursion (projectile-run-vterm))
  (switch-to-buffer-other-window (concat "*vterm "
                                         (projectile-project-name)
                                         "*")))

(defun +/projectile/vc-here ()
  "Open magit in this window."
  (interactive)

  (save-window-excursion (projectile-vc))
  (switch-to-buffer (concat "magit: " (projectile-project-name))))

(use-package perspective
  :demand t
  :straight t
  :after (consult projectile)
  :config
  (setq persp-modestring-short t
        persp-sort 'access
        persp-suppress-no-prefix-key-warning t
        persp-state-default-file (+/FS/create-path user-emacs-directory
                                                   "persp-state"))

  (persp-mode))

(use-package persp-projectile
  :demand t
  :straight t
  :after (perspective projectile))

(use-package consult-projectile
  :demand t
  :straight t
  :after (consult projectile))

(defun +/consult-projectile/enhanced-find-file ()
  "Show a list of recently visited files in a project using `consult'."
  (interactive)

  (let ((consult-projectile-sources
         '(consult-projectile--source-projectile-recentf
           consult-projectile--source-projectile-file)))
    (call-interactively #'consult-projectile)))

(defun +/UIX/kill-current-persp-if-in-project ()
  "If inside a project, kill perspective and close project."
  (interactive)

  (let* ((project-name (projectile-project-name)))
    (if project-name
        (progn (projectile-kill-buffers)
               (persp-kill project-name))
      (message "Not in a project. You need to kill the persp using \"SPC TAB d\"."))))

(use-package vertico
  :demand t
  :straight t
  :custom
  (vertico-cycle t)
  :init
  (vertico-mode))

(use-package vertico-posframe
  :straight t
  :demand t
  :after
  (vertico-mode)
  :init
  (vertico-posframe-mode 1))

(use-package orderless
  :straight t
  :demand t
  :custom
  (completion-styles '(orderless)))

(use-package marginalia
  :straight t
  :demand t
  :after vertico
  :init (marginalia-mode 1))

(use-package consult
  :straight t
  :demand t
  :config
  (setq completion-in-region-function
        (lambda (&rest args)
          (apply (if vertico-mode
                     #'consult-completion-in-region
                   #'completion--in-region)
                 args))))

(use-package company
  :straight t
  ;; :disabled nil
  :demand t
  :config
  (setq company-minimum-prefix-length 1
        company-idle-delay (lambda () (if (company-in-string-or-comment) nil 0.1))
        company-selection-wrap-around t
        company-require-match nil
        company-tooltip-align-annotations t
        company-tooltip-limit 5
        company-icon-size '(auto-scale . 20)
        company-icon-margin 2
        company-tooltip-minimum 10
        company-search-regexp-function 'company-search-words-in-any-order-regexp)

  (global-company-mode))

(use-package corfu
  ;; Optional customizations
  :straight t
  :disabled t
  :demand nil
  :custom
  (corfu-cycle t)                 ; Allows cycling through candidates
  (corfu-auto nil)                  ; Enable auto completion
  (corfu-auto-prefix 2)
  (corfu-auto-delay 0.0)
  (corfu-popupinfo-delay '(0.5 . 0.2))
  (corfu-preview-current 'insert) ; Do not preview current candidate
  (corfu-preselect 'prompt)
  (corfu-on-exact-match nil)      ; Don't auto expand tempel snippets

  ;; Optionally use TAB for cycling, default is `corfu-complete'.
  :bind (:map corfu-map
              ("M-SPC"      . corfu-insert-separator)
              ("TAB"        . corfu-complete)
              ([tab]        . corfu-next)
              ("S-TAB"      . corfu-previous)
              ([backtab]    . corfu-previous)
              ("S-<return>" . corfu-insert)
              ("RET"        . nil))

  :init
  (global-corfu-mode)
  ;; (corfu-history-mode)
  ;; (corfu-popupinfo-mode) ; Popup completion info
  :config
  (add-hook 'eshell-mode-hook
            (lambda () (setq-local corfu-quit-at-boundary t
                              corfu-quit-no-match t
                              corfu-auto nil)
              (corfu-mode))))

(use-package cape
  :straight t
  :disabled t
  :demand t
  :bind ("C-c f" . cape-file)
  :init
  ;; Add `completion-at-point-functions', used by `completion-at-point'.
  (defalias 'dabbrev-after-2 (cape-capf-prefix-length #'cape-dabbrev 2))
  (add-to-list 'completion-at-point-functions 'dabbrev-after-2 t)
  (cl-pushnew #'cape-file completion-at-point-functions)
  :config
  ;; Silence then pcomplete capf, no errors or messages!
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-silent)

  ;; Ensure that pcomplete does not write to the buffer
  ;; and behaves as a pure `completion-at-point-function'.
  (advice-add 'pcomplete-completions-at-point :around #'cape-wrap-purify))

(use-package all-the-icons-completion
  :demand t
  :straight t
  :after (all-the-icons marginalia)
  :config
  (add-hook 'marginalia-mode-hook
            #'all-the-icons-completion-marginalia-setup)
  (all-the-icons-completion-mode))

(defun +/org/toggle-markup (&optional force-show force-hide)
  "Toggle emphasis markers in org.

The optional arguments, evaluated left to right, force showing and hiding respectively."
  (interactive)

  (when force-show
    (setq org-hide-emphasis-markers nil)
    (when org-link-descriptive
      (org-toggle-link-display)))

  (when force-hide
    (setq org-hide-emphasis-markers t)
    (unless org-link-descriptive
      (org-toggle-link-display)))

  (when
      (not (or force-show force-hide))
    (setq org-hide-emphasis-markers
          (not org-hide-emphasis-markers))
    (org-toggle-link-display)))

(use-package org
  :straight t
  :demand t
  :init (setq org-directory +/org/dir-org)
  :config

  ;; Create org-directory if it does not exist
  (make-directory +/org/dir-org t)

  ;; Config
  (setq

   org-archive-location (concat org-directory "/misc/archive.org::")
   org-default-notes-file (concat org-directory "/verzameling.org")


   ;; UI/UX
   ;; org-startup-folded 'content
   org-startup-folded t
   org-startup-indented t
   org-startup-with-inline-images t
   org-hide-leading-stars t
   ;; org-blank-before-new-entry t ;; messes up M-RER
   org-blank-before-new-entry '((heading . nil) (plain-list-item . nil))
   org-M-RET-may-split-line '((item . nil))
   org-cycle-separator-lines 1
   org-edit-src-content-indentation 0
   org-cycle-separator-lines 2
   org-image-actual-width 200
   org-ellipsis "  " ;; folding symbol 
   org-outline-path-complete-in-steps nil
   org-pretty-entities t
   org-hide-emphasis-markers t ;; show actually italicized text instead of /italicized text/
   org-fontify-done-headline t
   org-fontify-whole-heading-line t

   org-refile-targets (quote ((nil :maxlevel . 1)
                              (org-agenda-files :maxlevel . 1)))
   org-refile-use-outline-path nil
   org-refile-allow-creating-parent-nodes (quote confirm)
   org-tag-alist '(
                   ("aquisitie" . ?a)
                   ("project" . ?p)
                   ("home" . ?h)
                   )

   ;; Agenda
   org-agenda-block-separator ""

   ;; TODOs
   org-todo-keywords '((sequence "DOEN" "VOLGENDE" "|" "VOLTOOID" "OOIT" "WACHTEN" ))
   ;; org-todo-keywords '((sequence "TODO(t)" "WAITING(w!/!)" "|" "CANCELLED(c!/!)" "DONE(d!/!)"))
   org-todo-keyword-faces
   '(("DOEN" . "#cd927b")
     ("VOLGENDE" . "#bb9b6c")
     ("OPVOLGEN" . "#75a6d4")
     ("AFSPRAAK" . "#75a6d4")
     ("DOORGESTUURD" . "#75a6d4")
     ("WACHTEN" . "#53afad")
     ("VOLTOOID" . "#83ab79")
     ("OOIT" . "#53afad")
     ("OPGEHEVEN" . "grey25")
     )


   ;; Priorities
   org-priority-highest '?A
   org-priority-lowest  '?C
   org-priority-default '?B

   org-priority-faces '((?A :foreground "magenta2"
                            :background "white smoke"
                            :box (:line-width 1))
                        (?B :foreground "darkorange1"
                            :background "white smoke"
                            :box (:line-width 1))
                        (?C :foreground "turquoise3"
                            :background "white smoke"
                            :box (:line-width 1)))
   org-priority-start-cycle-with-default t

   ;; quotes and verses
   org-fontify-quote-and-verse-blocks t

   ;; src-blocks
   org-src-tab-acts-natively t
   org-src-fontify-natively t
   org-edit-src-content-indentation 0

   ;; latex previews
   org-preview-latex-image-directory (+/FS/create-path
                                      user-emacs-directory
                                      "ltximg/")

   ;; logging
   org-log-into-drawer t)

  ;; hide markup
  (+/org/toggle-markup nil t)

  )

(setq
 org-use-sub-superscripts "{}"
 )

(defun +/org/evil-insert-state-in-edit-buffer (fun &rest args)
  "Bind `evil-default-state' to `insert' before calling FUN with ARGS."

  (let ((evil-default-state 'insert)
        ;; Force insert state
        evil-emacs-state-modes
        evil-normal-state-modes
        evil-motion-state-modes
        evil-visual-state-modes
        evil-operator-state-modes
        evil-replace-state-modes)
    (apply fun args)
    (evil-refresh-cursor)))

(advice-add 'org-babel-do-key-sequence-in-edit-buffer
            :around #'+/org/evil-insert-state-in-edit-buffer)

(defun +/org/org-sensible-m-ret ()
  "Sensible M-ret."
  (interactive)
  (org-insert-heading-after-current)
  (evil-append 1))

(defun +/org/org-sensible-c-ret ()
  "Sensible C-ret."
  (interactive)
  (org-insert-heading-respect-content)
  (evil-append 1))

(defun +/org/indent-org-src-block ()
  "Indent org source block."
  (interactive)

  (when (org-in-src-block-p)
    (org-edit-special)
    (indent-region (point-min) (point-max))
    (org-edit-src-exit)))

(defun +/org/org-sensible-m-j ()
  "Move items and headings down."
  (interactive)
  (cond ((org-at-heading-p) (org-move-subtree-down))
        ((org-at-item-p) (org-move-item-down))))

(defun +/org/org-sensible-m-k ()
  "Move items and headings up."
  (interactive)
  (cond ((org-at-heading-p) (org-move-subtree-up))
        ((org-at-item-p) (org-move-item-up))))

(setq inhibit-compacting-font-caches t)
(use-package org-superstar
  :after org
  :straight t
  :config
  (org-superstar-configure-like-org-bullets)
  (setq org-superstar-headline-bullets-list
        ;; '(128312 	128313) ;; orange and blue diamonds
        '(8858 8226 8728 8278 8759)
 )
  :hook (org-mode . org-superstar-mode))

(use-package org-fancy-priorities
  :after org
  :straight t
  :after org
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  ;; (setq org-fancy-priorities-list '("🅐" "🅑" "🅒")))
  (setq org-fancy-priorities-list '("[A]" "[B]" "[C]")))

;; This is needed as of Org 9.2
(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src sh"))
(add-to-list 'org-structure-template-alist '("pu" . "src plantuml :file images/xxx.png"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("ts" . "src typescript"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("go" . "src go"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))

(require 'ox-latex)
(setq-default
 org-latex-logfiles-extensions (quote ("lof" "lot" "tex" "tex~" "aux" "idx" "log" "out" "toc" "nav" "snm" "vrb" "dvi" "fdb_latexmk" "blg" "brf" "fls" "entoc" "ps" "spl" "bbl")) ;; these extensions get deleted on export.
 org-latex-compiler "xelatex"
 org-export-in-background 'nil) ;; TODO: Seems buggy when set to t
(setq-default org-latex-listings 'minted)  ;; Use python minted to fontify
(setq org-latex-minted-options '(("bgcolor" "gray!5") ))
(setq org-export-publishing-directory "/tmp/org-export/")
;; PDF output process with comments
;; 1. `--shell-escape' required for minted. See `org-latex-listings'
;; 2. "bibtex %b" ensures bibliography (of org-ref) is processed during pdf generation
;; 3. Remove output logs, out, auto at the end of generation
(setq org-latex-pdf-process
      '("%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "bibtex %b"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "%latex -shell-escape -interaction nonstopmode -output-directory %o %f"
        ))

(setq org-latex-default-packages-alist
      (quote (("AUTO" "inputenc" t)
              ("" "fixltx2e" nil)
              ("" "graphicx" t)
              ("" "longtable" nil)
              ("" "float" nil)
              ("" "wrapfig" nil)
              ("" "soul" t)
              ("" "svg" t)
              ("" "marvosym" t)
              ("" "wasysym" t)
              ("" "latexsym" t)
              ("" "amssymb" t)
              ("" "hyperref" nil)
              "\\tolerance=1000")))

(setq org-latex-title-command "\\maketitle\\cleardoublepage")

(defun org-latex-format-toc-default (depth)
  (when depth
    (format "\\setcounter{tocdepth}{%s}\n\\tableofcontents\n\\vspace*{1cm}\n\\cleardoublepage"
            depth)))
(add-to-list 'org-latex-classes
             '("basic"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-basic.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc.tex}
\\input{/home/sander/.config/emacs/config/templates/tex/niji.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("starling"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-starling.tex}
\\input{/home/sander/.config/emacs/config/templates/tex/niji.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("starling-landscape"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-starling-landscape.tex}
  \\input{/home/sander/.config/emacs/config/templates/tex/niji.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc-landscape"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-landscape.tex}
                                    [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-landscape-3cols"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-landscape-3cols.tex}
                                    [NO-DEFAULT-PACKAGES]
                 \\begin{multicols}{3}
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("mauc-notes"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-notes.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))


(add-to-list 'org-latex-classes
             '("vabi"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-vabi.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-article"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-article.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("mauc-scape"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-scape.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(require 'ox-koma-letter)
(add-to-list 'org-latex-classes
             '("mauc-letter"
               "\\input{/home/sander/.config/emacs/config/templates/tex/class-mauc-letter.tex}
                                   [NO-DEFAULT-PACKAGES]
                                   "
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")))

(add-to-list 'org-latex-classes
             '("my-letter"
               "\\documentclass\{scrlttr2\}
                                    \\usepackage[english]{babel}
                                    \\setkomavar{frombank}{(1234)\\,567\\,890}
                                    \[DEFAULT-PACKAGES]
                                    \[PACKAGES]
                                    \[EXTRA]"))

(setq org-koma-letter-default-class "mauc-letter")

(add-hook 'org-agenda-mode-hook
          (lambda ()
            (define-key org-agenda-mode-map " " 'org-agenda-cycle-show)))

(add-hook 'org-agenda-mode-hook '(lambda () (hl-line-mode 1)))  ;; Always hilight the current agenda line
(setq org-agenda-todo-ignore-deadlines nil)
(setq org-agenda-date-weekend t)  ;; weekends in a different colour
(setq org-agenda-todo-ignore-with-date t)
(setq org-agenda-todo-list-sublevels nil)
(setq org-agenda-skip-deadline-if-done t)
(setq org-agenda-skip-scheduled-if-done t)
(setq org-agenda-skip-scheduled-if-deadline-is-shown nil)
(setq org-agenda-skip-additional-timestamps-same-entry nil)
(setq org-agenda-start-on-weekday 1)

(setq org-agenda-custom-commands
      '(("a" "Primary agenda view"
         ((agenda "day" ((org-agenda-ndays 1) (org-agenda-overriding-header "Vandaag")))

          (todo ":VOLGENDE:"
                ((org-agenda-todo-ignore-scheduled nil)
                 (org-agenda-todo-ignore-deadlines nil)
                 (org-agenda-todo-ignore-with-date nil)
                 (org-agenda-overriding-header "Unstuck project tasks")))
          (todo ":WACHTEN:" ((org-agenda-todo-ignore-scheduled nil)
                           (org-agenda-todo-ignore-deadlines nil)
                           (org-agenda-todo-ignore-with-date nil)
                           (org-agenda-overriding-header "Things waiting on the perennially disorganised masses")))
          (todo "DOEN"
                ((org-agenda-todo-ignore-scheduled nil)
                 (org-agenda-todo-ignore-deadlines nil)
                 (org-agenda-todo-ignore-with-date nil)
                 (org-agenda-overriding-header "Ontheemde taken")))
          )
         ((org-habit-show-habits t)
          ;;(org-agenda-log-mode-items '(closed clock state))
          (org-agenda-start-with-log-mode t)
          ;;(org-agenda-start-with-log-mode '(4))
          (org-agenda-start-with-follow-mode nil)
          (org-agenda-dim-blocked-tasks 'invisible))
         ("~/.org/agenda.html" )
         )

        ("w" "Weekly agenda"
         ((agenda "week" ((org-agenda-ndays 7)))))


        ("fm" "Other" tags-todo "-ToRead-ToWrite-ToCheckOut-ProjectIdea-TechFix/OOIT"
         ((org-use-tag-inheritance nil)
          (org-agenda-todo-ignore-scheduled nil)
          (org-agenda-todo-ignore-deadlines nil)
          (org-agenda-todo-ignore-with-date nil)
          (org-agenda-overriding-header "Miscellaneous SOMEDAY")))
        ))

;; (use-package org-roam
;;   :defer t
;;   :after org
;;  :straight (:host github :repo "org-roam/org-roam"
;;             :files (:defaults "extensions/*"))
;;  :custom
;;  (org-roam-directory "~/.org/roam")
;;  (org-roam-capture-templates
;;   '(("d" "default" plain
;;      "%?"
;;      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
;;      :unnarrowed t)
;;     ("p" "project" plain (file "~/.org/roam/templates/project.org")
;;      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
;;      :unnarrowed t)

;;     ("b" "book notes" plain (file "~/.org/roam/templates/book.org")
;;      :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
;;      :unnarrowed t)
;;     ))
;;  :config (org-roam-db-autosync-enable)
;;          (require 'org-roam-protocol)
;;   )

(use-package org-capture
  :straight nil
  :after org
  :config

(setq org-capture-templates
 '(
   ("t" "Taak" entry (file+headline org-default-notes-file "Taken")
    "* DOEN %?\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n%i\n\n")

   ("i" "Idee" entry (file+headline org-default-notes-file "Taken")
    "* :OOIT: %?\n:PROPERTIES:\n:Context: %a\nCaptured: %U\n:END:\n%i\n\n")

   ("d" "Dagboek" entry
    (file+olp+datetree "misc/dagboek.org")
    "* %?\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n %i\n\n")

   ("s" "Screenshot" entry
    (file+olp+datetree "misc/screenshots.org")
    "* %T\n:PROPERTIES:\n:Context: %a\n:Captured: %U\n:END:\n%i\n\n")

   ("l" "Link" entry (file+headline org-default-notes-file "Weblinks")
    "* %a\n %?\n %i \n")
   )
 ))

;; (add-hook
;;   'org-mode-hook
;;   (lambda () (+/fonts/prettify-symbols-push-to-alist
;;         '(("#+BEGIN_SRC"   . ?»)
;;           ("#+begin_src"   . ?»)
;;           ("#+END_SRC"     . ?«)
;;           ("#+end_src"     . ?«)
;;           ("#+BEGIN_QUOTE" . ?❝)
;;           ("#+begin_quote" . ?❝)
;;           ("#+END_QUOTE"   . ?❞)
;;           ("#+end_quote"   . ?❞)))))

;; Make windmove work in Org mode:
(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

(use-package org-auto-tangle
  :after org
  :demand t
  :straight t
  ;; :load-path "site-lisp/org-auto-tangle/"    ;; this line is necessary only if you cloned the repo in your site-lisp directory 
  :hook (org-mode . org-auto-tangle-mode))

(use-package magit
  :demand t
  :straight t)

(use-package git-modes
  :straight t)

(add-hook 'prog-mode-hook (lambda ()
                            (display-line-numbers-mode t)
                            ;; (apheleia-mode +1)
                            ))

(defun sndr-typescript-mode-hook()
     (interactive)
     (lsp-deferred)
  )

  (add-hook 'typescript-ts-mode-hook 'sndr-typescript-mode-hook)
  (add-hook 'typescript-mode- 'sndr-typescript-mode-hook)

;; (use-package typescript-mode
;;   :straight t
;;   :mode "\\.ts\\'"
;;   ;; :hook (typescript-mode .(lambda()
;;   ;;                           (lsp-deferred)
;;   ;;                           (prettier-js-mode t)
;;   ;;                           ))
;; :hook
;;   (typescript-mode . lsp-deferred)
;;   (typescript-mode . prettier-js-mode)
;;   :after tree-sitter
;;   :config
;;   (setq typescript-indent-level 2)
;;   )

(use-package lsp-pyright
  :straight t
  :after lsp
  ;; :hook
  ;; (python-mode . (lambda () (require 'lsp-pyright)
  ;;                  (lsp-deferred)
  ;;                  ))
  :config
  (setq lsp-pyright-auto-import-completions t
        lsp-pyright-use-library-code-for-types t
        lsp-pyright-auto-search-paths t
        lsp-pyright-typechecking-mode "basic" ;; {"off","basic","strict"}
	      ;; lsp-pyright-venv-path "~/.local/share/virtualenvs/"
        )
  )

(add-hook 'python-mode-hook (lambda () (message "Python mode hook activated")))
(add-hook 'python-ts-mode-hook (lambda () (message "Python treesitter mode hook activated")))

;; (use-package python-black
;;   :straight t
;;   :demand t
;;   :after python)

(use-package py-isort
  :straight t
  :after python)

;; (add-hook
;;  'python-mode-hook
;;  (lambda () (+/fonts/prettify-symbols-push-to-alist
;;              '(("def"    . ?𝒇) ;; ʩ ƒ ⨍ 𝒇
;;                ("class"  . ?𝓢)
;;                ("for"    . ?∀)
;;                ("in"     . ?∈)
;;                ("return" . ?⟼)
;;                ("True"   . ?𝕋)
;;                ("False"  . ?𝔽)
;;                ("int"    . ?ℤ)
;;                ("float"  . ?ℝ)
;;                ("bool"   . ?𝔹)
;;                ("str"    . ?𝕊)
;;                ("None"   . ?∅)))))

(use-package poetry
 :straight t)

(use-package markdown-mode
  :straight t
  :config
  (setq markdown-display-inline-images t
        markdown-enable-math t
        markdown-max-image-size '(500 . 500)))

;; (use-package web-mode
;;       :straight t
;;       :commands (web-mode)
;;       :mode (("\\.htm[l]?" . web-mode)
;;              ("\\.css"     . web-mode)
;;              ("\\.js[x]?"  . web-mode)
;;              ;; ("\\.ts[x]?"  . web-mode)
;;              ("\\.astro"   . web-mode))
;;       :config
;;       (setq web-mode-enable-auto-pairing t)

;;       (add-hook 'web-mode-hook (
;;                                 lambda () (setq
;;                                            tab-width 2
;;                                            indent-tabs-mode nil
;;                                            ))));;;; web-mode : Support various web files

(use-package web-mode
  :straight t
  :mode (("\\.htm[l]?" . web-mode)
         ("\\.css"     . web-mode)
         ;; ("\\.njk"  . web-mode)
         ;; ("\\.nunjucks"  . web-mode)
         ("\\.js[x]?"  . web-mode)
         ;; ("\\.ts[x]?"  . web-mode)
         ("\\.astro"   . web-mode)
         )
  :hook (web-mode . (lambda () (when (match-buffer-extension "ts" "js" "vue")
                                 (setq-local lsp-auto-format t))))
  (web-mode . lsp-deferred)
  :custom
  (web-mode-script-padding 0) ; For vue.js SFC : no initial padding in the script section
  (web-mode-markup-indent-offset 2)) ; For html : use an indent of size 2 (default is 4)


(use-package jinja2-mode
  :straight t
  :mode(
          ("\\.njk"  .      jinja2-mode)
          ("\\.nunjucks"  . jinja2-mode)
          ("\\.jinja"  .    jinja2-mode)
         )
  :config
  (setq jinja2-basic-offset 2)
  )

;; (add-hook
;;  'web-mode-hook
;;  (lambda () (+/fonts/prettify-symbols-push-to-alist
;;              '(("function" . ?𝒇) ;; ʩ ƒ ⨍ 𝒇
;;                ("true"     . ?𝕋)
;;                ("false"    . ?𝔽)))))

(use-package prettier-js :straight t :demand t)
;;;; prettier-js : Formatting on save, used by my-ts-mode for .js and .ts files
;; (use-package prettier-js
;;   :custom
;;   (prettier-js-show-errors nil)
;;   (prettier-js-args '("--semi" "false"
;;                       "--single-quote" "false"
;;                       "--tab-width" "4"
;;                       "--trailing-comma" "all"
;;                       "--print-width" "150")))

(use-package vue-mode
  :straight t
  :after tree-sitter
  :mode "\\.vue\\'"
  :hook
  (vue-mode . lsp-deferred)
  (vue-mode . web-mode)
  (vue-mode . prettier-js-mode)
  :config
  (setq mmm-submode-decoration-level 0)
  (setq-default vue-html-extra-indent 0)
  (setq-default vue-indent-level 0)
  (setq css-indent-offset 2)
  (setq prettier-js-args '("--parser vue")
        )

  (defconst vue--front-tag-lang-regex
    (concat "<%s"                               ; The tag name
            "\\(?:"                             ; Zero of more of...
            "\\(?:\\s-+\\w+=[\"'].*?[\"']\\)"   ; Any optional key-value pairs like type="foo/bar"
            "\\|\\(?:\\s-+scoped\\)"            ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"            ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"             ; The optional "setup" attribute
            "\\)*"
            "\\(?:\\s-+lang=[\"']%s[\"']\\)"    ; The language specifier (required)
            "\\(?:"                             ; Zero of more of...
            "\\(?:\\s-+\\w+=[\"'].*?[\"']\\)"   ; Any optional key-value pairs like type="foo/bar"
            "\\|\\(?:\\s-+scoped\\)"            ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"            ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"             ; The optional "setup" attribute
            "\\)*"
            " *>\n")                            ; The end of the tag
    "A regular expression for the starting tags of template areas with languages.
To be formatted with the tag name, and the language.")

  (defconst vue--front-tag-regex
    (concat "<%s"                        ; The tag name
            "\\(?:"                      ; Zero of more of...
            "\\(?:\\s-+" vue--not-lang-key "[\"'][^\"']*?[\"']\\)" ; Any optional key-value pairs like type="foo/bar".
            ;; ^ Disallow "lang" in k/v pairs to avoid matching regions with non-default languages
            "\\|\\(?:\\s-+scoped\\)"      ; The optional "scoped" attribute
            "\\|\\(?:\\s-+module\\)"      ; The optional "module" attribute
            "\\|\\(?:\\s-+setup\\)"      ; The optional "module" attribute
            "\\)*"
            "\\s-*>\n")                     ; The end of the tag
    "A regular expression for the starting tags of template areas.
To be formatted with the tag name.")

  )

(use-package vue-html-mode :straight t)

;; Hook for inline display of images
  (add-hook 'org-babel-after-execute-hook
            'org-display-inline-images 'append)

  ;; Editing src-blocks (evil-keys don't work otherwise)
  ;; org-src minor mode buffer
  (add-hook 'org-src-mode-hook 'evil-normalize-keymaps)

  ;; Don't ask before evaluating each block
  (setq org-confirm-babel-evaluate nil)

  ;; Support the following languages
  (with-eval-after-load 'org
    (org-babel-do-load-languages
     'org-babel-load-languages
     '((emacs-lisp . t)
       (python     . t)
       (plantuml . t)
))
  )

;; Enable plantuml-mode for PlantUML files
(use-package plantuml-mode
  :straight t
  :config
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
    ;; Sample jar configuration
    (setq plantuml-jar-path "~/.local/mybin/plantuml.jar")
    (setq org-plantuml-jar-path plantuml-jar-path)
    (setq plantuml-default-exec-mode 'jar)
    ;; Sample executable configuration
    ;; (setq plantuml-executable-path "/path/to/your/copy/of/plantuml.bin")
    ;; (setq plantuml-default-exec-mode 'executable)
    (add-to-list
  'org-src-lang-modes '("plantuml" . plantuml))
  )

(use-package rustic
  :disabled t
  :straight t
  :after lsp ;; test
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status)
              ("C-c C-c e" . lsp-rust-analyzer-expand-macro)
              ("C-c C-c d" . dap-hydra)
              ("C-c C-c h" . lsp-ui-doc-glance))
  :config
  ;; uncomment for less flashiness
  ;; (setq lsp-eldoc-hook nil)
  ;; (setq lsp-enable-symbol-highlighting nil)
  ;; (setq lsp-signature-auto-activate nil)

  ;; comment to disable rustfmt on save
  (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook)
  )

(defun rk/rustic-mode-hook ()
  ;; so that run C-c C-c C-r works without having to confirm, but don't try to
  ;; save rust buffers that are not file visiting. Once
  ;; https://github.com/brotzeit/rustic/issues/253 has been resolved this should
  ;; no longer be necessary.
  (when buffer-file-name
    (setq-local buffer-save-without-query t))
  (add-hook 'before-save-hook 'lsp-format-buffer nil t))

;; (add-to-list 'treesit-language-source-alist
;;              '(typst "https://github.com/uben0/tree-sitter-typst"))
;; (treesit-install-language-grammar 'typst)

;; (use-package typst-ts-mode
;;   :straight (:type git :host sourcehut :repo "meow_king/typst-ts-mode" :files (:defaults "*.el"))
;;   ;; (optional) checking typst grammar version needs it
;;   ;; (typst-ts-mode-grammar-location (expand-file-name "tree-sitter/libtree-sitter-typst.so" user-emacs-directory))
;;   )

(use-package flycheck
  :straight t
  :init
  (add-hook 'after-init-hook #'global-flycheck-mode)
  (global-flycheck-mode))

(use-package consult-flycheck
  :straight t)

(use-package lsp-mode
  :straight t
  :hook
  (
   ;; (lsp-mode    . lsp-enable-which-key-integration) ;; redundant ?
   (python-ts-mode . lsp-deferred)
   ;; (python-mode . lsp-deferred)
   (typescript-mode-hook . lsp-deferred)
   (typescript-ts-mode-hook . lsp-deferred)
   ;; (vue-mode-hook . lsp)
   ;; (vue-mode-hook . lsp-vue-mmm-enable)
   ;; (web-mode    . lsp-deferred)
   )

  :config
  (setq
   ;; UI
   lsp-ui-doc-show-with-mouse nil
   lsp-ui-sideline-show-diagnostics nil
   lsp-headerline-breadcrumb-enable nil
   lsp-modeline-code-actions-enable nil

   ;; Enforce python3
   ;; Ref: https://github.com/emacs-lsp/lsp-pyright/issues/42#issuecomment-812910334
   lsp-pyright-python-executable-cmd "python3")

  :commands
  (lsp lsp-deferred))

(use-package lsp-ui
  :straight t
  )

(use-package lsp-treemacs
  :straight t
  :after lsp
)

(use-package consult-lsp
  :straight t
  :after (consult lsp-mode))

(setq treesit-language-source-alist
   '((bash "https://github.com/tree-sitter/tree-sitter-bash")
     (cmake "https://github.com/uyha/tree-sitter-cmake")
     (css "https://github.com/tree-sitter/tree-sitter-css")
     (elisp "https://github.com/Wilfred/tree-sitter-elisp")
     (go "https://github.com/tree-sitter/tree-sitter-go")
     (html "https://github.com/tree-sitter/tree-sitter-html")
     (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
     (json "https://github.com/tree-sitter/tree-sitter-json")
     (make "https://github.com/alemuller/tree-sitter-make")
     (markdown "https://github.com/ikatyang/tree-sitter-markdown")
     (python "https://github.com/tree-sitter/tree-sitter-python")
     (toml "https://github.com/tree-sitter/tree-sitter-toml")
     (tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
     (typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
     (yaml "https://github.com/ikatyang/tree-sitter-yaml")))

;; (mapc #'treesit-install-language-grammar (mapcar #'car treesit-language-source-alist))

(use-package treesit-auto
  :straight t
  :demand t
  :custom
  (treesit-auto-install 'prompt)
  :config
  (treesit-auto-add-to-auto-mode-alist 'all)
  (global-treesit-auto-mode))

(use-package apheleia
  :straight (apheleia :host github :repo "radian-software/apheleia")
  :config
  (apheleia-global-mode t)
  )

(use-package evil-nerd-commenter
  :straight t
  :after evil
  :bind ("M-/" . evilnc-comment-or-uncomment-lines)
  )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Full width comment box                                                 ;;
;; from http://irreal.org/blog/?p=374                                     ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun bjm-comment-box (b e)
"Draw a box comment around the region but arrange for the region to extend to at least the fill column. Place the point after the comment box."

(interactive "r")

(let ((e (copy-marker e t)))
  (goto-char b)
  (end-of-line)
  (insert-char ?  (- fill-column (current-column)))
  (comment-box b e 1)
  (goto-char e)
  (set-marker e nil)))

(add-hook 'prog-mode-hook #'hs-minor-mode)

(defun sndr-editorconfig-reformat()
  "reformats buffer based on editorconfig"
  (interactive)
  ;; (setq lsp-enable-indentation nil)
  ;; (editorconfig-apply)
  ;; (editorconfig-format-buffer)
  ;; (setq lsp-enable-indentation t)
  )

(use-package editorconfig
  :straight t
  :demand t
  :config
  (editorconfig-mode 1)
  ;; (define-key global-map (kbd "<M-s M-f>") #'sndr-editorconfig-reformat)
  )

(use-package copilot
  :straight (
     :host github
     :repo "zerolfx/copilot.el"
     :files ("dist" "*.el"))
  :config (setq copilot-max-char 200000)
  (define-key copilot-completion-map (kbd "C-<tab>") 'copilot-accept-completion)
  :ensure t)

(add-hook 'prog-mode-hook 'copilot-mode)


;; (with-eval-after-load 'company
;;   ;; disable inline previews
;;   (delq 'company-preview-if-just-one-frontend company-frontends))

;; we recommend using use-package to organize your init.el
(use-package codeium
    :straight '(:type git :host github :repo "Exafunction/codeium.el")
    :disabled t
    :demand t
    :init
    ;; use globally
    (add-to-list 'completion-at-point-functions #'codeium-completion-at-point)
    ;; or on a hook
    ;; (add-hook 'python-mode-hook
    ;;     (lambda ()
    ;;         (setq-local completion-at-point-functions '(codeium-completion-at-point))))

    ;; if you want multiple completion backends, use cape (https://github.com/minad/cape):
    ;; (add-hook 'python-mode-hook
    ;;     (lambda ()
    ;;         (setq-local completion-at-point-functions
    ;;             (list (cape-super-capf #'codeium-completion-at-point #'lsp-completion-at-point)))))
    ;; an async company-backend is coming soon!

    ;; codeium-completion-at-point is autoloaded, but you can
    ;; optionally set a timer, which might speed up things as the
    ;; codeium local language server takes ~0.2s to start up
    ;; (add-hook 'emacs-startup-hook
    ;;  (lambda () (run-with-timer 0.1 nil #'codeium-init)))

    ;; :defer t ;; lazy loading, if you want
    :config
    ;; (setq use-dialog-box nil) ;; do not use popup boxes

    ;; if you don't want to use customize to save the api-key
    ;; (setq codeium/metadata/api_key "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx")

    ;; get codeium status in the modeline
    (setq codeium-mode-line-enable
        (lambda (api) (not (memq api '(CancelRequest Heartbeat AcceptCompletion)))))
    (add-to-list 'mode-line-format '(:eval (car-safe codeium-mode-line)) t)
    ;; alternatively for a more extensive mode-line
    ;; (add-to-list 'mode-line-format '(-50 "" codeium-mode-line) t)

    ;; use M-x codeium-diagnose to see apis/fields that would be sent to the local language server
    (setq codeium-api-enabled
        (lambda (api)
            (memq api '(GetCompletions Heartbeat CancelRequest GetAuthToken RegisterUser auth-redirect AcceptCompletion))))
    ;; you can also set a config for a single buffer like this:
    ;; (add-hook 'python-mode-hook
    ;;     (lambda ()
    ;;         (setq-local codeium/editor_options/tab_size 4)))

    ;; You can overwrite all the codeium configs!
    ;; for example, we recommend limiting the string sent to codeium for better performance
    (defun my-codeium/document/text ()
        (buffer-substring-no-properties (max (- (point) 3000) (point-min)) (min (+ (point) 1000) (point-max))))
    ;; if you change the text, you should also change the cursor_offset
    ;; warning: this is measured by UTF-8 encoded bytes
    (defun my-codeium/document/cursor_offset ()
        (codeium-utf8-byte-length
            (buffer-substring-no-properties (max (- (point) 3000) (point-min)) (point))))
    (setq codeium/document/text 'my-codeium/document/text)
    (setq codeium/document/cursor_offset 'my-codeium/document/cursor_offset)

)

(use-package gptel
  :straight t
  :config
  (setq gptel-api-key
        (string-trim (shell-command-to-string "pass openai/apikey")))
  )

(use-package openai
  :straight (openai :type git :host github :repo "emacs-openai/openai")
  :config
  (setq openai-key
      (string-trim (shell-command-to-string "pass openai/apikey")))
  )


(use-package dall-e
  :straight (dall-e :type git :host github :repo "emacs-openai/dall-e")
  :config
  (setq

   dall-e-n 3 ;; - The number of images to generate. Must be between 1 and 10. (Default: 5)
   dall-e-size "512x512" ;;- The size of the generated images. (Default: "256x256")
   dall-e-spinner-type 'moon;; - The type of the spinner. (Default: 'moon)
   dall-e-cache-dir '(expand-file-name "~/.org/org-ai-images") ;; - Absolute path to download image files.
   dall-e-display-width  200
   )
 )

;; (use-package chatgpt
;;   :straight (chatgpt :type git :host github :repo "emacs-openai/chatgpt")
;;  :config
;;  )

(use-package org-ai
  :after org
  :straight (org-ai :type git :host github :repo "rksm/org-ai"
                    :local-repo "org-ai"
                    :files ("*.el" "README.md" "snippets"))
  :config
  (setq org-ai-openai-api-token
      (string-trim (shell-command-to-string "pass openai/apikey")))
 )

;; (use-package realgud
;;   :straight t)

(use-package go
  :straight t
  :config
  (require 'go)
  )

; (add-hook 'org-metaup-hook 'move-line-up)
;; (add-hook 'org-metadown-hook 'move-line-down)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "C-M-3") 'split-window-horizontally) ; was digit-argument
(global-set-key (kbd "C-M-2") 'split-window-vertically) ; was digit-argument
(global-set-key (kbd "C-M-1") 'delete-other-windows) ; was digit-argument
(global-set-key (kbd "C-M-0") 'delete-window) ; was digit-argument
(global-set-key (kbd "M-o") 'other-window) ; was facemenu-keymap
(global-set-key (kbd "M-b") 'ido-switch-buffer) ; was facemenu-keymap
(global-set-key (kbd "M-t") 'treemacs)
(global-set-key (kbd "M-n") 'neotree-toggle)
(global-set-key (kbd "C-s") 'save-buffer)

(global-set-key (kbd "C-S") 'isearch-forward-symbol-at-point)
(global-set-key [f5] 'revert-buffer)
(global-set-key "\C-h\C-e" 'hippie-expand)
(global-set-key (kbd "M-f") 'find-file)
(global-set-key (kbd "M-q") 'delete-frame)
(global-set-key (kbd "M-p") 'fill-paragraph)
(global-set-key (kbd "M-z") 'keyboard-quit)

(global-unset-key (kbd "C-x 3")) ; was split-window-horizontally
(global-unset-key (kbd "C-x 2")) ; was split-window-vertically
(global-unset-key (kbd "C-x 1")) ; was delete-other-windows
(windmove-default-keybindings)
;; (windmove-default-keybindings 'meta)

(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)

(global-set-key (kbd "M-C-/") 'bjm-comment-box)
(global-set-key (kbd "C-S-<prior>") 'text-scale-increase)
(global-set-key (kbd "C-S-<next>") 'text-scale-decrease)

(defun +/FS/bookmarks/keybind-dirs (binding-prefix bookmarks)
  "Keybind directory bookmarks.

First argument is the binding prefix, e.g., 'SPC o d', and the second argument is a list of directory bookmarks where each entry is an alist. The format of this alist is ('description' . ('x' '/path/')), where 'x' is the character bound to the directory at '/path/', and 'description' is a short key which describes the directory."

  (let ((bm))
    (dolist (bm bookmarks)
      (general-define-key
       :states '(normal visual motion)
       :keymaps 'override
       (concat binding-prefix " " (nth 0 (cdr bm)))
       `((lambda () (interactive) (dired ,(nth 1 (cdr bm)))) :wk ,(car bm))))))

(defun +/FS/bookmarks/keybind-files (binding-prefix bookmarks)
  "Keybind file bookmarks.

First argument is the binding prefix, e.g., 'SPC o f', and the second argument is a list of file bookmarks where each entry is an alist. The format of this alist is ('description' . ('x' '/path-to-file')), where 'x' is the character bound to the file at '/path-to-file', and 'description' is a short key which describes the file."

  (let ((bm))
    (dolist (bm bookmarks)
      (general-define-key
       :states '(normal visual motion)
       :keymaps 'override
       (concat binding-prefix " " (nth 0 (cdr bm)))
       `((lambda () (interactive) (find-file ,(nth 1 (cdr bm)))) :wk ,(car bm))))))

;; Scroll non-focussed window
(general-define-key
 :keymaps 'override
 "C-M-k" 'scroll-other-window
 "C-M-j" 'scroll-other-window-down)

(general-define-key
 :states '(visual normal motion)
 :keymaps 'dired-mode-map
 "SPC" nil ;; Makes modeleader bindings possible in dired
 "TAB" #'+/FS/visit-file-from-dired-using-ace-window
 "s"   #'hydra-dired-quick-sort/body
 "q"   #'+/FS/prompt-and-kill-current-dired-buffer)

;; Sensible M/C-rets
(general-define-key
 :states '(normal visual motion insert)
 :keymaps 'org-mode-map
 "<M-return>" #'+/org/org-sensible-m-ret
 "<C-return>" #'+/org/org-sensible-c-ret
 )


(general-define-key
 :states '(normal visual motion)
 :keymaps 'org-mode-map
 ;; orgshifts
 "<" 'org-shiftleft
 ">" 'org-shiftright
 ;; Sensible M-j/k
 "M-j" #'+/org/org-sensible-m-j
 "M-k" #'+/org/org-sensible-m-k)

(+/UIX/map-to-modeleader
  :states '(normal visual)
  :keymaps 'org-src-mode-map
  "s" '(:ignore t :wk "src-block")
  "s q" '(org-edit-src-exit  :wk "quit/finish edit")
  "s k" '(org-edit-src-abort :wk "kill/abort edit"))

(general-define-key
 :states '(visual normal motion)
 :keymaps 'org-agenda-mode-map
 ">" '(org-agenda-later :wk "next")
 "<" '(org-agenda-earlier :wk "prev"))

(general-define-key
 :keymaps 'org-super-agenda-header-map
 "<tab>" '(origami-toggle-node :wk "visit")
 "r" '(org-agenda-redo :wk "refresh")
 "q" '(org-agenda-quit :wk "quit"))

;; Unset unhelpful bindings
(general-define-key
 :mode evil-insert-state-minor-mode
 :states 'insert
 "C-k" nil)

;; Bindings
(general-define-key
 :states '(insert motion)
 :keymaps 'vertico-map
 "C-j" 'vertico-next
 "C-k" 'vertico-previous)

(general-define-key
 :keymaps 'isearch-mode-map
 "C-j" 'isearch-ring-advance
 "C-k" 'isearch-ring-retreat)

(general-define-key
 :keymaps 'minibuffer-local-isearch-map
 "C-j" 'next-history-element
 "C-k" 'previous-history-element)

(general-define-key
 :keymaps 'pdf-outline-buffer-mode-map
 :states 'normal
  "<return>" '(pdf-outline-follow-link :wk "follow link"))

(general-define-key
 :keymaps 'imenu-list-major-mode-map
 :states 'normal
  "M-<return>" '(imenu-list-display-entry :wk "show")
  "C-S-i" '(sndr-editorconfig-reformat :wk "format buffer")
  )

(general-define-key
 :keymaps 'prog-mode-map
 :states '(normal visual motion insert)
  "C-<f12>" '(xref-go-back :wk "jump back definition")
  "<f12>" '(lsp-find-definition :wk "find definition")
  "C-S-i" '(sndr-editorconfig-reformat :wk "format buffer")
  "C-<return>" '(hs-toggle-hiding :wk "toggle folding")
  "M-<return>" '(hs-hide-all :wk "fold all")
  "M-C-<return>" '(hs-show-all :wk "show all")
  "M-<up>" '(move-line-up :wk "move line up")
  "M-<down>" '(move-line-down :wk "move line down")
  "C-/" '(evilnc-comment-or-uncomment-lines  :wk "toggle comment")
  )

(general-define-key
 :keymaps 'mu4e-headers-mode-map
 :states '(normal visual motion insert)
 "C-c c" '(mu4e-org-store-and-capture :wk "org store and capture")
 "C-c c" '(mu4e-org-store-and-capture :wk "org store and capture")
 )

;; hydra: org priority actions
(defhydra +/UIX/hydra-org-priority ()
  "Set priority: "
  ("a" (lambda () (interactive) (org-priority ?a)) "A" :color teal)
  ("b" (lambda () (interactive) (org-priority ?b)) "B" :color teal)
  ("c" (lambda () (interactive) (org-priority ?c)) "C" :color teal)
  ("j" org-priority-down "-" :color amaranth)
  ("k" org-priority-up   "+" :color amaranth)
  ("x" (lambda () (interactive) (org-priority 'remove)) "Remove" :color teal)
  ("SPC" nil "quit"))

;; hydra: org todo actions
(defhydra +/UIX/hydra-org-todo ()
  "Set TODO state: "
  ("t" (lambda () (interactive) (org-todo 1)) "todo" :color teal)
  ("w" (lambda () (interactive) (org-todo 2)) "waiting" :color teal)
  ("c" (lambda () (interactive) (org-todo 3)) "cancelled" :color teal)
  ("d" (lambda () (interactive) (org-todo 4)) "done" :color teal)
  ("j" (org-call-with-arg 'org-todo 'left) "previous" :color amaranth)
  ("k" (org-call-with-arg 'org-todo 'right) "next" :color amaranth)
  ("x" (lambda () (interactive) (org-todo 5)) "remove" :color teal)
  ("SPC" nil "quit"))

;; Keybindings
(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'org-mode-map

  ;; Priorities
  "p" '(+/UIX/hydra-org-priority/body :wk "priority")

  ;; TODOs
  "t" '(+/UIX/hydra-org-todo/body :wk "todo")

  ;; Checklist toggling
  "x" '(org-toggle-checkbox :wk "toggle checkbox")

  ;; Cookie update
  "k" '(:wk "update cookie(s)")
  "k k" '(org-update-statistics-cookies :wk "update at point")
  "k K" '((lambda () (interactive) (org-update-statistics-cookies t))
          :wk "update buffer")

  ;; Tags
  "T" '(org-set-tags-command :wk "choose tags")

  ;; Dates/Schedules/Deadlines
  "d" '(:wk "date")
  "d s" '(org-schedule :wk "schedule")
  "d d" '(org-deadline :wk "deadline")
  "d t" '((lambda () (interactive) (org-schedule nil "")) :wk "today")
  "d T" '((lambda () (interactive) (org-deadline nil "")) :wk "deadline today")

  ;; Links
  "l" '(:wk "link")
  "l o" '(org-open-at-point :wk "open")
  "l l" '(org-insert-link :wk "insert")
  "l y" '(org-store-link :wk "copy")
  "l t" '(org-toggle-link-display :wk "toggle")

  ;; Source blocks
  "s" '(:wk "src-block")
  "s s" '(org-babel-execute-src-block :wk "execute")
  "s e" '(org-edit-src-code :wk "edit")
  "s i" '(+/org/indent-org-src-block :wk "indent")

  ;; Toggle markup
  "M" '(+/org/toggle-markup :wk "toggle markup")
  "I" '(org-toggle-inline-images :wk "toggle images")

  ;; Toggle latex previews
  "L" '(org-latex-preview :wk "toggle LaTex")

  ;; Narrow/Widen subtree
  "n" '(org-toggle-narrow-to-subtree :wk "narrow/widen")

  ;; Add to agenda
  "A" '(org-agenda-file-to-front :wk "add to agenda")

  ;; Archiving logs
  "Z" '(+/org/org-archive-logbook :wk "archive logbook"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map
  "f" '(:wk "fold")
  "f f" '(hs-toggle-hiding :wk "toggle")
  "f x" '(hs-hide-block :wk "fold block")
  "f X" '(hs-hide-all :wk "fold all")
  "f o" '(hs-show-block :wk "show block")
  "f O" '(hs-show-all :wk "show all")
  "f R" '(hs-hide-level :wk "fold sub-blocks"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map
  "d" '(:wk "debug")
  "d d" '(consult-flycheck        :wk "buffer diagnostic")
  "d D" '(consult-lsp-diagnostics :wk "project diagnostic"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'prog-mode-map

  ;; Code-related operation
  "c" '(:wk "code")
  "c /" '(consult-lsp-file-symbols :wk "consult local symbols")
  "c ?" '(consult-lsp-symbols :wk "consult project symbols")
  "c r" '(lsp-rename :wk "rename symbol")
  "c i" '(lsp-organize-imports :wk "organise imports")

  ;; Jump to/from code
  "." '(:wk "jump to")
  ". d"  '(lsp-find-definition :wk "definition")
  ". r"  '(lsp-find-references :wk "reference")
  ". j"  '(evil-jump-backward :wk "backward")
  ". k"  '(evil-jump-forward :wk "forward again"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'web-mode-map
  "f t" '(web-mode-fold-or-unfold :wk "toggle-tag"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'prog-mode
  :keymaps 'python-ts-mode-map

  "c f" '(:wk "format")
  "c f I" '(py-isort-buffer :wk "isort buffer")
  "c f i" '(py-isort-region :wk "isort region")
  "c f F" '(python-black-buffer :wk "black buffer")
  "c f f" '(python-black-partial-dwim :wk "black region")

  "R" '(run-python :wk "REPL")

  "r" '(:wk "eval in REPL")
  "r r" '(python-shell-send-region :wk "region")
  "r b" '(python-shell-send-buffer :wk "buffer"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'dired-mode
  :keymaps 'dired-mode-map
  "d" '(dired-du-mode :wk "dired-du")
  "o" '(dired-omit-mode :wk "dired-omit")
  "h" '(dired-hide-details-mode :wk "hide details"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  :keymaps 'markdown-mode-map

  ;; Links
  "l" '(:wk "link")
  "l o" '(markdown-follow-link-at-point :wk "open")
  "l l" '(markdown-insert-link :wk "insert")
  "l t" '(markdown-toggle-url-hiding :wk "toggle")

  ;; Show/Hide markup
  "M" '(markdown-toggle-markup-hiding :wk "toggle markup"))

(+/UIX/map-to-modeleader
  :states '(normal visual motion)
  ;; :mode 'pdf-view-mode
  :keymaps 'pdf-view-mode-map

  ;; Activate noter
  "N" '(org-noter :wk "org-noter"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "TAB" '(:wk "perspective")

  ;; Move b/w persps
  "TAB TAB" '(persp-switch :wk "switch/new")
  "TAB SPC" '(persp-switch-last :wk "toggle")
  "TAB j" '(persp-prev :wk "previous")
  "TAB k" '(persp-next :wk "next")

  ;; Operations on persps
  "TAB r" '(persp-rename :wk "rename")
  "TAB d" '(persp-kill :wk "delete")
  "TAB s" '(persp-state-save :wk "save state")
  "TAB l" '(persp-state-load :wk "load state")

  ;; Buffers in a persp
  "TAB b"  '(:wk "buffer")
  "TAB b a" '(persp-add-buffer :wk "add buffer")
  "TAB b A" '(persp-set-buffer :wk "add buffer exclusively")
  "TAB b d" '(persp-remove-buffer :wk "delete buffer")
  "TAB b B" '(persp-ibuffer :wk "list persp buffers"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "#" '(org-capture :wk "capture"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "." '(:wk "registers")
  ". a" '(point-to-register :wk "add")
  ". ." '(register-to-point :wk "goto")
  ". /" '(consult-register :wk "consult registers"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/" '(:wk "utils")

  ;; Outline
  "/ /" '(consult-outline :wk "outline")

  ;; insert emoji
  "/ e" '(emojify-insert-emoji :wk "insert emoji")

  ;; imenu sidebar toggle
  "/ m" '(imenu-list-smart-toggle :wk "imenu sidebar")

  ;; Exchange point and mark
  "/ ." '(exchange-point-and-mark :wk "exchange point & mark")

  ;; Pass (may be best to remove keybinding to pass)
  "/ ?"  '(pass :wk "pass")

  ;; Diff
  "/ D" '(:wk "ediff")
  "/ D f"'(ediff :wk "files")
  "/ D b"'(ediff-buffers :wk "buffers")

  ;; Save unsaved buffers
  "/ S" '(save-some-buffers :wk "save unsaved")

  ;; Remove whitespace
  "/ W" '(whitespace-cleanup :wk "remove whitespace"))

;; Define a hydra menu
(defhydra +/UIX/hydra-font-scaling ()
  "font scaling"
  ("=" text-scale-increase "bigger")
  ("-" text-scale-decrease "smaller")
  ("r" +/fonts/text-scale-reset "reset")
  ("SPC" nil "quit"))

;; Map the hydra
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ F" '(+/UIX/hydra-font-scaling/body :wk "font scaling"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :mode 'override
  "/ c"    '(:wk "comment")

  "/ c c"  '(evilnc-comment-or-uncomment-lines :wk "comment/uncomment")
  "/ c p"  '(evilnc-copy-and-comment-lines :wk "comment and copy")
  "/ c y"  '(evilnc-comment-and-kill-ring-save :wk "comment and yank"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ d" '(:wk "thesaurus/dictionary")

  "/ d a" '(powerthesaurus-lookup-antonyms-dwim :wk "antonyms")
  "/ d s" '(powerthesaurus-lookup-synonyms-dwim :wk "synonyms")
  "/ d r" '(powerthesaurus-lookup-related-dwim :wk "related")
  "/ d d" '(powerthesaurus-lookup-definitions-dwim :wk "show definitions")
  "/ d e" '(powerthesaurus-lookup-sentences-dwim :wk "show examples")
  "/ d M" '(powerthesaurus-lookup-dwim :wk "menu") )

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override

  "/ n" '(:wk "narrow/widen")

  "/ n f" '(narrow-to-defun :wk "function")
  "/ n r" '(narrow-to-region :wk "region")
  "/ n p" '(narrow-to-page :wk "page")

  "/ n SPC" '(widen :wk "widen"))

;; hydra: Resize olivetti region
(defhydra +/UIX/hydra-resize-olivetti ()
  "Resize olivetti region size."
  ("-" olivetti-shrink "shrink")
  ("=" olivetti-expand "expand")
  ("R" olivetti-set-width "set width")
  ("SPC" nil "quit"))

;; Mapping
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ o" '(:wk "olivetti-mode")

  ;; Toggle olivetti mode
  "/ o o" '(olivetti-mode :wk "toggle")
  ;; Resize
  "/ o R" '(+/UIX/hydra-resize-olivetti/body :wk "resize"))

;; (+/UIX/map-to-leader
;;   :states '(normal visual motion)
;;   :keymaps 'override
;;   "/ p" '(:wk "packages")

;;   "/ p i" '(package-install :wk "install")
;;   "/ p I" '(package-reinstall :wk "re-install")
;;   "/ p d" '(package-delete :wk "delete")

;;   "/ p r" '(package-refresh-contents :wk "refresh")
;;   "/ p l" '(package-list-packages :wk "list")
;;   "/ p D" '(package-autoremove :wk "autoremove"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "/ s" '(:wk "spelling")

  ;; Spelling corrections (no spell-checking)
  "/ s" '(:wk "spelling")
  "/ s s" '(flyspell-correct-at-point :wk "correct at point")

  ;; Buffer-wide operations
  "/ s b" '(flyspell-buffer :wk "spellcheck buffer")
  "/ s B" '(+/writing/spellcheck-and-correct-buffer :wk "check & correct buffer")

  ;; Region/Selection restricted operations
  ;; "/ e r" '(+/writing/spellcheck-region :wk "spellcheck region")
  ;; "/ e r" '(+/writing/spellcheck-and-correct-region :wk "check & correct region")

  ;; Continuous checking: Only works if the buffer has already been spellchecked
  "/ s j" '(flyspell-correct-next :wk "correct next")
  "/ s k" '(flyspell-correct-previous :wk "correct previous"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "b" '(:wk "buffer")

  "b SPC" '(evil-switch-to-windows-last-buffer :wk "switch")

  "b b" '(switch-to-buffer :wk "switch")
  "b B" '(ibuffer-other-window :wk "list")
  "b c" '(+/UIX/buffer-to-clipboard :wk "clipboard")
  "b s" '(save-buffer :wk "save")
  "b k" '(+/UIX/kill-this-buffer :wk "kill")
  "b e" '(eval-buffer :wk "evaluate")
  "b U" '(sudo-edit :wk "sudo visited"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "d" '(:wk "dired")

  "d d" '(dired-jump :wk "pwd")
  "d o" '(dired-jump-other-window :wk "pwd in other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "e" '(:ignore t :wk "evaluate")

  ;; M-x
  "e SPC" '(execute-extended-command :wk "M-x")

  ;; Evaluate sexp/region/buffer
  "e e" '(eval-last-sexp :wk "sexp")
  "e r" '(eval-region :wk "region")
  "e b" '(eval-buffer :wk "buffer")

  ;; Compose and evaluate expression
  "e E" '(eval-expression :wk "expression"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "f" '(:ignore t :wk "file")

  "f f" '(find-file :wk "find/visit")
  "f t" '(+/FS/touch-file-recursive :wk "touch")
  "f r" '(consult-recent-file :wk "recently visited")
  "f u" '(sudo-edit-find-file :wk "sudo find/visit")

  "f s" '(save-buffer :wk "save")
  "f c" '(write-file :wk "copy & visit")
  "f C" '(+/FS/copy-buffer-file-silent :wk "copy silently")
  "f R" '(+/FS/rename-buffer-file :wk "rename")
  "f !" '(recover-this-file :wk "recover")

  "f d" '(dired :wk "find directory"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "h" '(:wk "help")

  ;; Colours and faces help
  "h C" '(list-colors-display :wk "colours")
  "h F" '(describe-face    :wk "face at point")

  "h c" '(helpful-command  :wk "commands")
  "h f" '(helpful-callable :wk "callables") ;; Callables include functions.
  "h k" '(helpful-key      :wk "keys")
  "h m" '(describe-mode    :wk "local modes")
  "h p" '(describe-package :wk "package")
  "h v" '(helpful-variable :wk "variables")

  "h x" '(helpful-at-point :wk "at point"))

(+/UIX/map-to-leader
    :states '(normal visual motion)
    :keymaps 'override
    "k" '( :wk "kill")
    "k b" '(kill-buffer :wk "kill buffer")
    "k k" '(kill-this-buffer :wk "kill this buffer")
)

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "m" '(:wk "bookmarks")
  "m a" '(bookmark-set :wk "new")
  "m m" '(consult-bookmark :wk "consult bookmarks")
  "m d" '(bookmark-delete :wk "delete"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "n" '(treemacs :wk "treemacs")
  ;; "n" '(neotree-toggle :wk "neotree")
  ;; M-x
  ;; "n SPC" '(execute-extended-command :wk "M-x")
)

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o" '(:wk "open"))

(+/UIX/map-to-leader
 :states '(normal visual motion)
 :keymaps 'override
 "o a" '(:wk "agenda")
 "o a a" '((lambda () (interactive) (org-agenda "a" "A")) :wk "daily")
 "o a p" '((lambda () (interactive) (org-agenda "a" "P")) :wk "prune & reassess")
 "o a r" '((lambda () (interactive) (org-agenda "a" "R")) :wk "re-plan")
 "o a t" '((lambda () (interactive) (org-agenda "a" "T")) :wk "triage"))

;; Header
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o d" '(:wk "directory"))

;; User-configured keybindings
(+/FS/bookmarks/keybind-dirs "SPC o d" +/FS/bookmarks/dirs)

;; Header
(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o f" '(:wk "file"))

;; User-configured keybindings
(+/FS/bookmarks/keybind-files "SPC o f" +/FS/bookmarks/files)

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "o j" '(org-journal-new-entry :wk "journal"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "o l" '(view-echo-area-messages :wk "*Messages*"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "o m" '(magit :wk "magit"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override

  "o t" '("vterm")
  "o t t" '(vterm :wk "this window")
  "o t o" '(vterm-other-window :wk "other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "p" '(:wk "projects")
  "p a" '(projectile-add-known-project :wk "add new")
  "p c" '(projectile-compile-project :wk "compile project; run make.")
  ;; "p p" '(consult-projectile-switch-project :wk "open/switch")
  "p p" '(projectile-persp-switch-project :wk "open/switch")
  ;; "p p" '(projectile-switch-project :wk "open/switch")
  "p D" '(projectile-remove-known-project :wk "delete")

  ;; "p f" '(projectile-find-file :wk "find in project")
  "p b" '(consult-projectile-switch-to-buffer :wk "switch buffers")
  "p f" '(+/consult-projectile/enhanced-find-file :wk "find in project")
  "p r" '(consult-projectile-recentf :wk "recent in project")
  "p B" '(projectile-ibuffer :wk "ibuffer project")

  "p d" '(:wk "project root")
  "p d d" '(projectile-dired :wk "this window")
  "p d o" '(projectile-dired-other-window :wk "other window")

  ;; "p b" '(projectile-switch-to-buffer :wk "switch buffer")
  "p s" '(projectile-save-project-buffers :wk "save project buffers")
  "p k" '(projectile-kill-buffers :wk "kill project buffers")

  "p m" '(:wk "git")
  "p m m" '(+/projectile/vc-here :wk "this window")
  "p m o" '(projectile-vc :wk "other window")

  "p t" '(:wk "terminal")
  "p t t" '(projectile-run-vterm :wk "this window")
  "p t o" '(+/projectile/run-vterm-other-window :wk "other window"))

(+/UIX/map-to-leader
  :states '(normal visual motion)
  :keymaps 'override
  "q" '(:ignore t :wk "quit")
  "q q" '(delete-frame :wk "quit frame")
  ;; "q q" '(+/UIX/quit-frame :wk "quit frame")
  "q r" '(restart-emacs :wk "restart")
  "q Q" '(kill-emacs :wk "kill server"))

;; Remap for standalone mode
(unless (daemonp)
  (+/UIX/map-to-leader
    :states '(normal visual motion)
    :keymaps 'override
    "q q" '(kill-emacs :wk "quit frame")))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "r" '(:ignore t :wk "roam")
  "r r" '(org-roam-node-find :wk "find node")
  ;; "r c" '(+/org/roam-freeze-loop :wk "cold-store loop")
  ;; "r x" '(+/org/roam-close-loop :wk "close loop")
  ;; "r d" '(+/org/roam-bin-loop :wk "bin loop")
  "r f" '(org-roam-node-find :wk "find node")
  "r i" '(org-roam-node-insert :wk "insert reference")
  "r l" '(org-roam-buffer-toggle :wk "toggle backlinks buffer")
  "r L" '(org-roam-buffer-display-dedicated :wk "backlinks buffer")

  "r s" '(org-roam-db-sync :wk "sync database")
  "r g" '(org-roam-graph :wk "graph"))

(+/UIX/map-to-leader
  :states '(visual normal motion)
  :keymaps 'override
  "s" '(:wk "search")

  "s s" '(consult-line    :wk "buffer")
  "s p" '(consult-ripgrep :wk "project/directory"))

;; hydra: Switch focus
(defhydra +/UIX/hydra-window-switch-focus ()
  "switch focus"
  ("l" windmove-right "right" :color amaranth)
  ("h" windmove-left "left" :color amaranth)
  ("j" windmove-down "down" :color amaranth)
  ("k" windmove-up "up" :color amaranth)
  ("SPC" nil "quit"))

;; hydra: Move windows
(defhydra +/UIX/hydra-window-move-window ()
  "move window"
  ("l" buf-move-right "move right" :color teal)
  ("h" buf-move-left "move left" :color teal)
  ("j" buf-move-down "move down" :color teal)
  ("k" buf-move-up "move up" :color teal)
  ("a" rotate-frame-anticlockwise "rotate +90" :color amaranth)
  ("c" rotate-frame-clockwise "rotate -90" :color amaranth)
  ("SPC" nil "quit"))

;; hydra: Resize window
(defhydra +/UIX/hydra-resize-window ()
  "resize window"
  ("=" balance-windows "balance windows" :color teal)
  ("l" enlarge-window-horizontally "+ width" :color amaranth)
  ("h" shrink-window-horizontally "- width" :color amaranth)
  ("j" shrink-window "- height" :color amaranth)
  ("k" enlarge-window "+ height" :color amaranth)
  ("SPC" nil "quit"))

;; Mapping
(+/UIX/map-to-leader
  :states '(normal visual motion emacs)
  :keymaps 'override

  "w" '(:ignore t :wk "window")

  "w w" '(ace-window :wk "other window")
  "w d" '(delete-window :wk "delete")
  "w f" '(other-frame :wk "other frame")
  "w m" '(delete-other-windows :wk "maximise")
  "w s" '(split-window-below :wk "horizontal split")
  "w v" '(split-window-right :wk "vertical split")

  ;; Move focus
  "w F" '(+/UIX/hydra-window-switch-focus/body :wk ":focus")

  ;; Move windows
  "w M" '(+/UIX/hydra-window-move-window/body :wk ":move")

  ;; Resize windows
  "w R" '(+/UIX/hydra-resize-window/body :wk ":resize"))
